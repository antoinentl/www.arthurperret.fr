# Makefile for www.arthurperret.fr
# Based on Pandoc-SSP https://github.com/infologie/pandoc-ssp/
# by Arthur Perret

.PHONY: html tidy clean

# Global rules

default: html

clean:
	rm -rf output/*

# Variables

PAGES := $(sort $(shell find text -maxdepth 1 -type f -iname '*.md' -not -name 'index.md' -not -name 'cv.md'))
PAGES_OUT := $(patsubst %.md, output/%.html, $(notdir $(PAGES)))
ARTICLES := $(sort $(shell find text/articles -type f -iname '*.md'))
ARTICLES_OUT := $(patsubst %.md, output/articles/%.html, $(notdir $(ARTICLES)))
BLOG := $(sort $(shell find text/blog -type f -iname '*.md'))
BLOG_OUT := $(patsubst %.md, output/blog/%.html, $(notdir $(BLOG)))
COURS := $(sort $(shell find text/cours -type f -iname '*.md'))
COURS_OUT := $(patsubst %.md, output/cours/%.html, $(notdir $(COURS)))
VEILLE := $(sort $(shell find text/veille -type f -iname '*.md'))
VEILLE_OUT := $(patsubst %.md, output/veille/%.html, $(notdir $(VEILLE)))
ALL := $(PAGES_OUT) $(ARTICLES_OUT) $(BLOG_OUT) $(COURS_OUT) $(VEILLE_OUT)

# Copy static files recursively :
# (Adapted from https://stackoverflow.com/questions/41993726/)

STATIC := $(shell find static -type f -not -name '.DS_Store')
STATIC_OUT := $(patsubst static/%, output/%, $(STATIC))
$(foreach s,$(STATIC),$(foreach t,$(filter %$(notdir $s),$(STATIC_OUT)),$(eval $t: $s)))
$(STATIC_OUT):; $(if $(wildcard $(@D)),,mkdir -p $(@D) &&) cp $^ $@

# Pandoc conversion

html: $(STATIC_OUT) output/index.html output/cv.html $(ARTICLES_OUT) $(BLOG_OUT) $(COURS_OUT) $(VEILLE_OUT) $(PAGES_OUT)

output/index.html: index.md
	pandoc $< \
	--variable root="true" \
	--data-dir static \
	--standalone \
	--from=markdown+bracketed_spans+ascii_identifiers \
	--to=html5 \
	--template=template.html \
	--section-divs \
	--wrap=none \
	--css=css/styles.css \
	--css=css/styles-index.css \
	--output=$@
	sed -i '' 's/www\.arthurperret\.fr\/output\//www\.arthurperret\.fr\//' $@

output/cv.html: text/cv.md
	pandoc $< \
	--variable root="true" \
	--data-dir static \
	--standalone \
	--from=markdown+bracketed_spans+ascii_identifiers \
	--to=html5 \
	--template=template.html \
	--section-divs \
	--wrap=none \
	--citeproc \
	--filter=/Users/aperret/.local/bin/pandoc-sidenote \
	--bibliography=references.json \
	--csl=thesis-fr.csl \
	--css=css/styles.css \
	--css=css/styles-cv.css \
	--output=$@
	sed -i '' 's/www\.arthurperret\.fr\/output\//www\.arthurperret\.fr\//' $@

output/articles/%.html: text/articles/%.md
	mkdir -p $(@D)
	pandoc $< \
	--variable articles="true" \
	--data-dir static \
	--standalone \
	--from=markdown+bracketed_spans+ascii_identifiers \
	--to=html5 \
	--template=template.html \
	--section-divs \
	--wrap=none \
	--citeproc \
	--filter=/Users/aperret/.local/bin/pandoc-sidenote \
	--bibliography=references.json \
	--csl=thesis-fr.csl \
	--css=css/styles.css \
	--css=css/styles-page.css \
	--toc \
	--toc-depth=2 \
	--number-sections \
	--output=$@
	sed -i '' 's/www\.arthurperret\.fr\/output\//www\.arthurperret\.fr\//' $@

output/blog/%.html: text/blog/%.md
	mkdir -p $(@D)
	pandoc $< \
	--variable blog="true" \
	--data-dir static \
	--standalone \
	--from=markdown+bracketed_spans+ascii_identifiers \
	--to=html5 \
	--template=template.html \
	--section-divs \
	--wrap=none \
	--citeproc \
	--filter=/Users/aperret/.local/bin/pandoc-sidenote \
	--bibliography=references.json \
	--csl=thesis-fr.csl \
	--css=css/styles.css \
	--css=css/styles-page.css \
	--output=$@
	sed -i '' 's/www\.arthurperret\.fr\/output\//www\.arthurperret\.fr\//' $@

output/veille/%.html: text/veille/%.md
	mkdir -p $(@D)
	pandoc $< \
	--variable veille="true" \
	--data-dir static \
	--standalone \
	--from=markdown+bracketed_spans+ascii_identifiers \
	--to=html5 \
	--template=template.html \
	--section-divs \
	--wrap=none \
	--citeproc \
	--filter=/Users/aperret/.local/bin/pandoc-sidenote \
	--bibliography=references.json \
	--csl=thesis-fr.csl \
	--css=css/styles.css \
	--css=css/styles-page.css \
	--css=css/styles-veille.css \
	--output=$@
	sed -i '' 's/www\.arthurperret\.fr\/output\//www\.arthurperret\.fr\//' $@

output/cours/%.html: text/cours/%.md
	mkdir -p $(@D)
	pandoc $< \
	--variable cours="true" \
	--data-dir static \
	--standalone \
	--from=markdown+bracketed_spans+ascii_identifiers \
	--to=html5 \
	--template=template.html \
	--section-divs \
	--wrap=none \
	--citeproc \
	--filter=/Users/aperret/.local/bin/pandoc-sidenote \
	--bibliography=references.json \
	--csl=iso690-author-date-fr-no-abstract.csl \
	--metadata link-citations=true \
	--css=css/styles-cours.css \
	--output=$@
	sed -i '' 's/www\.arthurperret\.fr\/output\//www\.arthurperret\.fr\//' $@

$(PAGES_OUT): $(PAGES)
	mkdir -p $(@D)
	pandoc $< \
	--variable root="true" \
	--data-dir static \
	--standalone \
	--from=markdown+bracketed_spans+ascii_identifiers \
	--to=html5 \
	--template=template.html \
	--section-divs \
	--wrap=none \
	--css=css/styles.css \
	--output=$@
	sed -i '' 's/www\.arthurperret\.fr\/output\//www\.arthurperret\.fr\//' $@
