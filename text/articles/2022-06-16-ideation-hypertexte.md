---
title: Idéation et hypertexte
date: 2022-06-16
abstract: "**Ce texte n’a pas encore été évalué.** Il accompagne ma communication au colloque <a href=https://questyourdigital.com/colloque/>Méthodes créatives de recherche</a> (Bordeaux, 16-17 juin 2022). Je discute ici une méthode pour élaborer des idées dans le cadre d’un travail de recherche. Cette méthode est ancrée dans la tradition de la fiche mais réactualisée par certaines capacités nouvelles que nous donne l’informatique (hypertexte, visualisation). À partir de cela, mon objectif est de définir une approche créative de l’organisation des connaissances, peu discutée dans la littérature de ce champ, et pensée notamment pour les jeunes chercheurs."
type: Article de colloque
article: true
custom-css: 2022-06-16-ideation-hypertexte.css
hypothesis: true
sommaire: true
---

# Une approche de l’idéation

## Un modèle réticulaire de l’idéation

Toute activité de recherche est travaillée par des questions d’information et de communication. Le problème général que j’aborde ici est celui de la gestion de l’information scientifique dans le cadre d’un travail de recherche. Je m’intéresse plus particulièrement à la documentation personnelle de recherche, qui comprend tout ce qui se situe entre les flux d’information entrants et sortants, et qui nous aide à gérer notre activité de lecture et d’écriture. Et en réponse à la thématique du colloque Méthodes créatives de recherche, je me focalise ici sur la dimension créative de ce sujet, en partant de la notion d’idéation :

> « faculté de former des idées^[<https://cnrtl.fr/definition/idéation>] » ;
>
> « processus cognitif de production d’une idée […] formation et enchaînement des idées^[<https://fr.wiktionary.org/wiki/idéation>] ».

L’idéation représente une part importante du travail de recherche. Toutefois, elle suscite beaucoup de questions, notamment chez les étudiants (du premier cycle jusqu’au doctorat) : comment créer des idées lorsqu’il ne s’agit pas simplement d’un processus vécu spontanément mais recherché volontairement dans le cadre d’un travail ?

Cette question pourrait être traitée de différentes manières, notamment en raison de sa nature transdiciplinaire. Ici, je vais la traiter suivant une perspective info-communicationnelle, en examinant comment concrétiser la notion d’idéation à l’échelle individuelle dans l’outillage du travail de recherche.

En premier lieu, il est utile de décomposer le fonctionnement de l’idéation. La seconde définition que j’ai citée évoque l’« enchaînement des idées ». En 1945, dans un essai d’anticipation devenu célèbre, l’ingénieur américain [Vannevar Bush](https://fr.wikipedia.org/wiki/Vannevar_Bush) (1890-1974) aborde justement cette question, à propos de l’outillage du travail intellectuel :

> [*“The human mind […] operates by association. With one item in its grasp, it snaps instantly to the next that is suggested by the association of thoughts, in accordance with some intricate web of trails carried by the cells of the brain.”*]{.marginnote}« L’esprit humain fonctionne par association. Il prend un élément et passe instantanément au suivant, par association de pensées, conformément à un réseau complexe de pistes portées par les cellules du cerveau[@bush1945, 106] ».

Selon Bush, il est impossible de dupliquer artificiellement le fonctionnement de notre esprit ; en revanche, il est possible de s’en inspirer pour améliorer nos processus de traitement automatique de l’information. Pour cela, on peut passer par un modèle, c’est-à-dire un instrument permettant de mieux connaître quelque chose à travers la réduction et l’interprétation [@sinaceur2006, 757-759]. En simplifiant donc, le fonctionnement de notre esprit peut être modélisé suivant une logique réticulaire, c’est-à-dire faite d’unités (les idées) et de liens (l’association d’idées). Dit autrement, la structure abstraite du graphe – on préfèrera cette notion précise à la figure plus métaphorique du réseau – permet de concevoir des objets sémantiques et des systèmes favorables à l’association d’idées. Il en va ainsi des graphes de connaissances, qui sont similaires aux représentations réticulaires internes à notre esprit – théorisées en sciences cognitives sous l’expression « modèles mentaux » [@mayr2022, 1-2].

On retrouve une logique réticulaire dans l’hypertexte, dont le développement a été influencé par les propos de Bush. On en trouve aussi des exemples antérieurs : Christian Vandendorpe identifie rétrospectivement une logique réticulaire dans le système de renvois de *L’Encyclopédie* dirigée par Diderot et D’Alembert [@vandendorpe1999, 117]. Il y a également une proximité entre le fonctionnement associatif suggéré par Bush et les processus intellectuels classiques d’analyse et de synthèse :

> « Analyse : décomposition d’une chose en ses éléments, d’un tout en ses parties^[<https://cnrtl.fr/definition/analyse>] ».
>
> « Synthèse : par opposition à analyse, opération, méthode par laquelle on procède du simple au complexe, des éléments au tout, de la cause aux effets^[<https://cnrtl.fr/definition/synthese>] ».

Dans son *Traité de documentation*, le bibliographe belge [Paul Otlet](https://fr.wikipedia.org/wiki/Paul_Otlet) (1868-1944) met ces deux processus à l’honneur et ajoute une remarque importante : ils servent aussi bien à organiser des connaissances qu’à en créer. En effet, l’analyse produit des unités que l’on peut recomposer par synthèse pour faire émerger des ensembles inédits :

> « Par une analyse bien conduite, les choses se décomposent en éléments derniers ; par une synthèse complète, les éléments divisés peuvent se regrouper dans leur unité première **ou selon d’autres types d’unité**. Analyse et synthèse sont des méthodes fondamentales, qu’il s’agisse de conceptions ou de choses réelles. Appliquées intégralement et systématiquement, elles facilitent l’**organisation** [@otlet1934, 384, l’emphase est mienne] ».

Il y a donc là un mouvement de décomposition puis de recomposition qui constitue potentiellement un exemple de processus d’idéation basé sur une logique réticulaire.

## La prise de notes sur fiches interreliées

Comment matérialiser cette logique réticulaire à l’échelle individuelle dans l’outillage de notre travail de recherche ?

Il existe une longue histoire de la prise de notes comme gestion de l’information chez les travailleurs de la connaissance [@blair2020, 89-157]. La fiche émerge en tant que support pour cette activité à partir du XVII^e^ siècle [@waquet2015, 74-89; @bert2017, 30-41]. Avec son format compact et mobile, la fiche peut servir de base à un système de prise de notes flexible, compatible avec une logique réticulaire, mais qui nécessite une grande organisation. Cette pratique est donc progressivement affinée à travers plusieurs principes méthodiques.

C’est d’abord le fait de consacrer une fiche à une seule idée, évoquée indépendamment par plusieurs auteurs [@bert2017, 59, 64]. Paul Otlet en fera son « principe monographique » ou « principe de la monographie » :

> « Principe de la monographie : chaque élément intellectuel d’un livre est (après avoir été sectionné de l’ensemble du texte) incorporé en un élément matériel correspondant […] En combinant le système des fiches et feuilles avec le principe monographique, on obtient une coïncidence parfaite dans le document entre l’unité intellectuelle et l’unité physique du support écrit [@otlet1934, 385-386] ».

C’est ensuite le fait de catégoriser les fiches suivant leur place dans l’organisation des connaissances, en fonction du travail à accomplir. Umberto Eco évoque ce principe dans *Comment écrire sa thèse* :

> « Fichier bibliographique […] fichier de lecture de livres ou d’articles, fichier thématique, fichier par auteurs, fichier de citations, fichier de travail […] Le nombre et la nature des fichiers dépend du travail de thèse » [@eco2018, 196-197].

C’est enfin le fait de relier les fiches entre elles. Parmi les fiches listées ci-dessus, Umberto Eco insiste sur les fiches de travail, qui constituent le tissu connectif du fichier de thèse :

> « Fiches de travail de différentes sortes : fiche de raccord entre idées et sections du plan, fiches de problèmes (comment traiter telle ou telle question ?), fiches de suggestions (recueillant des idées que d’autres vous ont données, des développements possibles), etc. [@eco2018, 196] ».

Le sociologue allemand [Niklas Luhmann](https://fr.wikipedia.org/wiki/Niklas_Luhmann) (1927-1998) est connu notamment pour sa technique de documentation personnelle, un fichier (en allemand *Zettelkasten*) qu’il qualifie de véritable partenaire de communication et d’écriture [@luhmann1992]. Les fiches y sont numérotées de manière séquentielle, avec des embranchements qui créent progressivement une arborescence, complétée par des liens transverses qui la transforment en réseau.

Le fichier de Luhmann marque une étape importante dans la formalisation d’une logique réticulaire appliquée aux fiches. C’est l’aboutissement d’environ 500 ans de tradition érudite, et le point de départ d’une transposition au numérique. À partir de cet exemple historique, Markus Krajewski résume l’importance épistémologique des liens entre fiches dans une démarche d’organisation des connaissances et d’idéation :

> [*“What is important is not the individual entry, but rather the arrangement of a multitude of notes according to a refined set of rules that combines and interconnects the modular textual building blocks in keeping with a nuanced scheme of classification […] One may use it as a computer (Rechenmaschine), in the strict etymological sense of rechnen as ‘to organize’, ‘to guide’ and ‘to prepare’, as with potential lines of reasoning through links and cross-references which the box offers up to the user browsing through it.”*]{.marginnote}« L’important n’est pas la note en elle-même mais bien l’agencement d’une multitude de notes selon un ensemble de règles affinées avec le temps, qui combinent et interconnectent les blocs textuels de façon modulaire selon un schéma de classification nuancé […] On peut l’utiliser comme un ordinateur (*Rechenmaschine*), au sens étymologique strict de *rechnen* qui signifie “organiser”, “guider” et “préparer”, les liens et les références croisées formant des lignes de raisonnement potentielles que le fichier offre à l’utilisateur qui le parcourt[@krajewski2016, 319] ».
>
> [*“A simple but consistent cross-reference produces fertile excesses, in that the re-combinatorial power of connections enhances the utility of excerpts through interwoven chains of reference.”*]{.marginnote}« Des références croisées simples mais cohérentes produisent un surplus fertile, au sens où le pouvoir recombinatoire des connexions renforce l’utilité des extraits textuels par le biais de chaînes de référence entrelacées[@krajewski2016, 320] ».
>
> [*“What must be added to the individual notes are connections, as it is only with these cross-references between the individual entries consciously entered by the user or unconsciously provided by the apparatus that the disparate material takes on a structure with evident relations, and which becomes at the same time something distinctly different.”*]{.marginnote}« Ce qu’il faut absolument ajouter aux notes individuelles, ce sont des connexions, car ce n’est qu’avec ces références croisées […] que le matériau disparate prend une structure avec des relations évidentes, et qui devient en même temps quelque chose de singulier[@krajewski2016, 327] ».

En résumé, il existe une tradition érudite qui consiste à prendre des notes sur des fiches catégorisées et interreliées, en consacrant une fiche à une idée, dans le but non seulement de gérer l’information mais de créer des idées nouvelles.

## Progrès récents des systèmes hypertextuels

Comment mettre en œuvre la prise de notes sur fiches interreliées avec l’informatique ?

La littérature scientifique sur cette question est très réduite. Le travail le plus complet et le plus récent sur cette question est celui de Markus Krajewski, détaillé dans l’article de 2016 cité précédemment. Cet article s’appuie notamment sur le logiciel Synapsen, développé par Krajewski depuis 1996. Les années 1990 correspondent à un « âge d’or » de la recherche sur le concept d’hypertexte, avec de nombreuses expérimentations en matière d’outillage. Depuis la fin des années 1990 en revanche, l’hypertexte a été globalement supplanté par le Web en tant qu’objet d’étude et peu de personnes travaillent désormais sur la conception de dispositifs hypertextuels inédits dans le monde académique [@ridi2018, 410].

Pourtant, des évolutions notables se sont produites depuis. Si le consensus académique autour de la notion d’hypertexte n’a pas sensiblement évolué, des progrès importants ont en revanche été accomplis dans le domaine des logiciels basés sur cette notion. On peut souligner deux éléments en particulier.

Premièrement, de nouveaux langages ont été inventés, dits « langages de balisage léger » (*lightweight markup languages*), c’est-à-dire ayant une empreinte syntaxique plus faible que les langages de balisage existant. C’est le cas de Markdown, équivalent réduit et simplifié de HTML, inventé par John Gruber avec la collaboration d’Aaron Swartz en 2004.

Deuxièmement, les efforts d’innovation consacrés à l’hypertexte dans le monde académique durant les années 1990 ont été poursuivis par l’industrie du développement logiciel durant les deux décennies suivantes. Ceci a produit une nouvelle génération d’outils pour penser – ou bien, pour reprendre les mots du philosophe François Dagognet, d’outils de la réflexion [@dagognet1999]. Ceci inclut différentes catégories de logiciels : idéateurs (*outliners*), cartographie heuristique (*mind mapping*), éditeurs de texte, etc. Ce champ n’a pas encore trouvé de résonance dans le monde académique, notamment parce qu’il n’est pas organisé formellement. Il est cependant caractérisé dans le monde anglophone par l’expression *tools for thought*, portée par des auteurs comme Andy Matuschak [@matuschak2019].

Ces deux évolutions se combinent. En effet, les nouveaux outils de la réflexion s’appuient souvent sur les langages de balisage léger, au premier rang desquels Markdown. Ce dernier, initialement pensé pour le Web, préfigure ainsi une nouvelle norme d’écriture hypertextuelle [@fauchie2018a], plus accessible que la précédente et pensée pour d’autres dispositifs que la publication web – par exemple pour l’idéation.

Ces progrès doivent donc nous inciter à contribuer au travail entamé par Markus Krajewski mais en posant à nouveaux frais la question de la mise en œuvre technique.

# De la fiche au graphe

## La fiche numérique : format texte et hypertexte

La fiche est un type de support d’écriture qui possède deux caractéristiques principales. La première, ce sont ses dimensions réduites et normées, qui contraignent à la concision en même temps qu’elles favorisent l’accumulation. La seconde est sa robustesse : le papier est généralement plus épais que celui d'une feuille, car la fiche doit résister à de nombreuses manipulations.

Le passage de la fiche papier à la fiche numérique peut se faire par simple analogie : tout format de fichier qui répond aux mêmes caractéristiques que la fiche papier peut être utilisé comme fiche numérique.

Je propose de considérer que le format texte répond idéalement à cette définition. Un fichier au format texte est un fichier dont le code binaire se traduit par des caractères textuels uniquement ; ces caractères peuvent être utilisés pour exprimer tout type d’information (texte, données tabulaires, images, etc.). Le format texte est réputé économe en espace de stockage. Par ailleurs, les données au format texte peuvent être considérées comme plus robustes que la moyenne. En effet, ce format fait l’objet de normes internationales, mises en œuvre par des codages rétrocompatibles et interopérables de l’ASCII (1963) à l’UTF-8 (1993). Un fichier au format texte possède donc, toutes choses égales par ailleurs, une compacité et une robustesse analogues à celles d’une fiche classique.

La mise en relation de ces fiches, quant à elle, peut se faire via l’hypertexte [@ridi2018]. Ce terme renvoie à une technique informatique inventée au XX^e^ siècle, avec les travaux visionnaires de Ted Nelson (qui invente le terme dans les années 1960) ou encore de Douglas Engelbart. Aujourd’hui, l’hypertexte est généralement associé au Web, qui est une application de cette technique à une échelle sociale inédite, simplifiant au passage la vision des pionniers afin de trancher certaines difficultés de mise en œuvre. Mais l’hypertexte est aussi une notion théorique travaillée par les chercheurs en SHS ; par exemple, Gérard Genette le définit comme un type de relation transtextuelle [@genette1982, 13]. Une définition synthétique de l’hypertexte a été proposée par Riccardo Ridi : l’hypertexte est une modalité d’organisation des connaissances non-linéaire, granulaire et extensible [@ridi2018, 394].

Une telle définition s’applique trait pour trait aux fiches interreliées. Je considère donc que l’hypertextualité est constitutive de cette pratique, indépendamment de la modalité d’inscription (dans le monde analogique) ou de stockage des données (dans le monde numérique). À titre d’exemple, Markus Krajewski qualifie son logiciel Synapsen de système hypertextuel, bien qu’il repose sur une base de données relationnelle et non sur le format texte.

## Analyse et synthèse sur fiches numériques

La fiche peut donc être transposée au numérique par le format texte, et la mise en relation des fiches par l’hypertexte. Ceci nous fournit une base pour transposer également au numérique les méthodes d’analyse et de synthèse évoquées précédemment.

L’analyse consiste à décomposer le phénomène étudié en éléments plus petits. En suivant le principe monographique d’Otlet, nous devons faire coïncider unité de sens (idée) et unité matérielle (document). Chaque élément du phénomène correspondra donc à une fiche, c’est-à-dire à un fichier texte, dont le titre décrit l’élément en question. En suivant les conseils d’Umberto Eco, nous devons également catégoriser ces fiches de manière à refléter l’organisation des connaissances décomposées à partir du phénomène. Enfin, pour permettre l’interrelation, il est nécessaire d’associer à chaque fiche un identifiant unique. Titre, catégorie de fiche et identifiant unique peuvent être déclarés sous forme de métadonnées explicites et structurées. Sur le modèle du Web et des URL, on préfèrera attribuer à chaque fiche un identifiant pérenne indépendant du titre, qui est lui susceptible d’être modifié.

Différents langages permettent de mettre tout ceci en œuvre. Les plus couramment utilisés dans les nouveaux outils de la réflexion sont Markdown pour le texte et YAML pour les métadonnées. Tous deux sont conçus pour faciliter l’écriture par les humains, à travers des règles d’écriture qui allègent la syntaxe (en contrepartie, le travail d’interprétation que doivent fournir les machines est plus complexe). Certaines variantes de Markdown, notamment celle du convertisseur de documents Pandoc, permettent également une intégration étroite des données bibliographiques au texte en Markdown, les références étant elles aussi identifiées de manière unique. Ceci s’inspire du format de données bibliographiques BibTeX et de son utilisation dans les documents rédigés en LaTeX, ce qui démontre une certaine continuité des pratiques liées au format texte académique.

La synthèse, quant à elle, consiste à composer de nouvelles idées. C’est ici qu’interviennent l’idéation et l’hypertexte. Concrètement, une nouvelle idée fera l’objet d’une nouvelle fiche. On pourra la formuler par des phrases dont certaines constituent des renvois vers d’autres fiches via un lien hypertexte. Ainsi, la mise en relation d’idées contenues dans d’autres fiches permet de décrire une nouvelle idée.

Différentes syntaxes permettent de faire cela. Elles imitent généralement le principe des URL sur le Web, c’est-à-dire que le lien se fait sur la base de l’identifiant du document cible ; l’écriture en revanche est simplifiée. Une forme répandue consiste à écrire l’identifiant du document cible entre doubles crochets. Présente notamment dans les logiciels de création d’encyclopédies en ligne (wikis), elle a par la suite été adoptée par les logiciels associés à la méthode Zettelkasten puis par d’autres familles d’outils.

L’intégration des liens dans le texte appelle une remarque. Le lien hypertexte est simplement un mécanisme interprété par une interface logicielle qui permet de passer d’un document à un autre en cliquant. Dans le contexte d’une documentation personnelle de recherche, il est important d’attacher une signification aux liens. La manière la plus simple de le faire passe par le contexte d’énonciation : c’est la phrase autour du lien qui en explique le sens. Ce contexte d’énonciation peut être mis en évidence par l’interface logicielle, il en sera question dans la section suivante. De manière plus formelle, on peut également ajouter au lien une métadonnée interprétable par la machine ; la logique est la même que pour la catégorisation des fiches.

## Rôle des interfaces et offre logicielle actuelle

L’ensemble des fiches interreliées répond à la définition d’un graphe documentaire [@arribe2014]. Un graphe est une structure abstraite définie comme un ensemble de sommets reliés par des arêtes ; par suite, un graphe documentaire est la structure formée par des documents interreliés.

L’écriture scientifique produit couramment ce type de structure, notamment si on considère la citation comme un lien. De façon plus générale, les travaux de Gérard Genette sur la transtextualité ont permis de faire progresser l’idée que l’écriture est souvent réticulaire. Toutefois, elle est peu identifiée comme telle. Or nous savons à la suite de Jack Goody que l’écriture a des implications notables sur les processus cognitifs [@goody1979]. L’écriture est réflexive, c’est-à-dire qu’on peut l’utiliser en tant que technique pour la décrire elle-même, et pour réfléchir aux effets qu’elle a sur notre pensée. Ceci s’applique à l’écriture réticulaire. Par conséquent, la visualisation de la structure qu’une telle écriture produit est une étape cruciale dans la prise de conscience de ses spécificités. La visualisation du graphe documentaire constitué par les fiches interreliées participe ainsi au développement des processus cognitifs liés à l’idéation.

De manière générale, les logiciels ont un rôle important à jouer dans la mise en œuvre de la méthode décrite plus haut. À partir de nos processus d’organisation (catégorisation, lien), ils peuvent proposer des fonctionnalités qui ont une utilité dans le processus d’idéation. Afin de compléter la démarche déjà esquissée avec le format texte et l’hypertexte, il faut donc nous intéresser à l’offre logicielle qui s’appuie sur ces techniques.

Les nouveaux outils de la réflexion marquent une évolution intéressante : ils utilisent des technologies inventées pour le Web (HTML, CSS, JavaScript, Markdown) mais sont fonctionnellement plus riches que le Web. Par exemple, les liens bi-directionnels, imaginés par les pionniers de l’hypertexte et absents du Web, font partie des fonctionnalités courantes de ces logiciels. Parmi les raisons qui permettent d’expliquer ce progrès, il faut noter que les technologies associées au Web ont abaissé le coût d’entrée à la fois côté conception et côté utilisation. Ce n’est pas seulement qu’elles sont plus simples que les bases de données relationnelles et les langages de programmation antérieurs : le Web a aussi engendré une offre pédagogique à la mesure des besoins de son économie florissante. Tout ceci a contribué à faire progresser les savoir-faire dans un public plus large, conduisant à un regain d’innovation.

Je travaille sur ces questions avec un petit groupe informel de chercheurs de l’université Bordeaux Montaigne, appelé GraphLab. Dans le cadre de nos échanges, nous avons passé en revue une quarantaine de systèmes hypertextuels (voir [Annexe](#annexe)). Il s’agit d’un échantillon basé sur la correspondance entre la proposition de valeur de chaque logiciel et une série de mots-clés, dont l’expression *tool for thought*.

Sur la base d’une distinction entre trois modes d’interactivité (saisie, rendu, saisie + rendu), nous avons établi quatre grandes familles de fonctionnalité :

- édition (saisie de texte)
- structuration (liste, tableau, arborescence, réseau)
- rendu (ex : tâche, calendrier, liste, visualisation de données…)
- navigation (menus, boutons, recherche…)

À partir de là, nous avons identifié quelques catégories d’outils dans notre échantillon :

- éditeur (édition)
- outliner (édition + structuration en liste)
- carte heuristique (édition + structuration en diagramme)
- explorateur (rendu + navigation)
- organiseur (édition + structuration + rendu + navigation)

Les organiseurs sont des systèmes particulièrement complets. Ils accordent souvent une place importante à la gestion, via des modèles de rendus appropriés (tâches, calendrier, rappels, etc.) et sont volontiers orientés vers le travail collaboratif. Les organiseurs adoptent généralement le modèle du document (organisation linéaire sur une page) ou du canevas (disposition arbitraire sur un plan).

Ces quelques repères ayant été établis, nous avons procédé à un tri en évaluant positivement les logiciels répondant à trois critères : premièrement, le choix de modèles de développement, de commerce et de données en adéquation avec le long cycle de vie de la documentation personnelle (nous avons accordé une prime aux logiciels libres, ouverts et qui reposent sur des formats eux-mêmes ouverts) ; deuxièmement, la présence de fonctionnalités avancées liées aux liens, en particulier la représentation graphique sous forme de réseau et la mise en évidence contextualisée des rétroliens ; et troisièmement, la prise en compte des besoins spécifiques à l’écriture académique, en particulier le traitement automatique des données bibliographiques.

Trois logiciels populaires concentrent actuellement l’intérêt du public auquel nous appartenons, les travailleurs de la connaissance : Roam (un outliner), Notion (un organiseur) et Obsidian (un éditeur). Malheureusement, aucun d’eux ne satisfait tous nos critères. Le logiciel le plus proche de nos besoins s’est avéré Zettlr : c’est un éditeur de texte Markdown académique inspiré par la méthode Zettelkasten, qui permet de créer des fiches avec des métadonnées et de les relier entre elles par une syntaxe de type wiki. Il ne manque à Zettlr que certaines fonctionnalités de rendu et de navigation, c’est-à-dire correspondant à notre deuxième critère. Pour les besoins de ma recherche doctorale, j’ai alors suggéré que nous concevions notre propre outil.

## Cosma

Ma thèse a été financée initialement par le programme ANR HyperOtlet. Au sein de ce programme, mon directeur de thèse Olivier Le Deuff a coordonné la conception et le développement d’un outil pour représenter le réseau social de Paul Otlet sous la forme d’un graphe interactif, l’Otletosphère. Guillaume Brioudes, qui a développé la seconde version de l’Otletosphère, a créé à ma demande un prototype de visualisation similaire à partir de fiches rédigées avec Zettlr. C’est ainsi que nous nous sommes engagés dans la conception et le développement de Cosma [@cosma].

Cosma est un logiciel de visualisation de graphe documentaire. Il propose des fonctionnalités liées aux rétroliens et facilite le partage des données. Il n’est pas lui-même un logiciel de prise de notes mais est pensé pour fonctionner en complémentarité avec ces logiciels. C’est un logiciel de recherche expérimental, gratuit et dont le code est publié sous licence libre.

L’interface de Cosma se divise en trois zones. À gauche, un panneau contient des fonctionnalités de navigation (recherche, index, filtres d’affichage). À droite, un panneau affiche la fiche sélectionnée, avec une bibliographie générée automatiquement à partir des sources citées dans le texte de la fiche, et une liste des liens et rétroliens avec leur contexte d’énonciation affichable au survol. Ceci permet de savoir où la fiche a été mentionnée, et de quelle manière. Au centre se trouve une représentation visuelle interactive du graphe formé par les fiches interreliées. Cosma reconnaît la catégorisation des fiches ; il lui associe des codes graphiques (couleurs, tracés) et des interactions (filtrage des éléments affichés). Cette visualisation apporte plusieurs avantages : elle permet d’appréhender globalement une structure complexe ; elle constitue un mode de navigation alternatif à la liste et au moteur de recherche ; surtout, c’est un instrument de réflexivité sur le processus d’écriture.

La plupart des outils de visualisation concentrent leurs fonctionnalités dans une application à interface graphique, à partir de laquelle il est possible d’exporter des données structurées ou des images statiques. Cosma inverse cette logique : la partie application, surnommée cosmographe, est un simple formulaire de création, et c’est l’export ainsi créé, un fichier HTML surnommé cosmoscope, qui constitue la véritable interface de visualisation. Ce fichier est affiché dans la fenêtre principale de Cosma. Il peut également être exporté et utilisé de manière autonome ; par exemple, il possible de le transmettre par mail ou encore de le mettre en ligne. Partager un cosmoscope permet ainsi de transmettre simultanément des données et des capacités heuristiques. Ceci en fait un support de discussion dans le cadre d’un travail de recherche mais aussi d’enseignement.

Cosma complète ainsi le panorama proposé dans cette communication : le format texte comme transposition de la fiche au numérique ; l’hypertexte comme modalité à la fois théorique et pratique de mise en relation des fiches ; le logiciel comme interface complémentaire indispensable au processus d’écriture. Pris ensemble, ces éléments constituent une forme hypertextuelle de documentation personnelle. Celle-ci illustre une manière de concrétiser la notion d’idéation à l’échelle individuelle dans l’outillage du travail de recherche.

# Organisation des connaissances créative

Pour finir, ce travail me conduit à discuter la place des méthodes créatives en organisation des connaissances (OC), dont Paul Otlet est un lointain pionnier. L’OC est une discipline voisine des SIC, encore peu théorisée, définie essentiellement par les pratiques [@hjorland2008]. Elle étudie des processus et des systèmes qui sous-tendent toutes les pratiques de recherche d’information. Les processus d’OC sont des activités fondamentales : classement, indexation, catalogage, etc. Les systèmes d’OC sont des objets sémantiques produits par ces activités : classifications, thésaurus, ontologies, etc.

En recherche, l’OC est fréquemment mobilisée dans une logique extractive. J’entends par là toutes les pratiques qui consistent à récupérer des connaissances existantes, souvent en moissonnant automatiquement des sources en ligne via des protocoles standardisés, et à les analyser en s’appuyant sur des systèmes d’OC bien établis. C’est ainsi que se présente généralement l’analyse de corpus, de la textométrie à la bibliométrie en passant par l’analyse visuelle de réseaux. Cette logique extractive suppose l’existence de connaissances déjà inscrites, repose sur des systèmes d’OC existants, requiert des compétences spécialisées, et s’inscrit généralement dans une démarche expérimentale au périmètre bien défini.

On peut également mobiliser l’OC dans une logique créative, qui s’avère fort différente. La forme de documentation personnelle que j’ai décrite l’illustre bien, à travers deux aspects qui concernent les deux objets de l’OC (les processus et les systèmes).

Premièrement, je propose de considérer le fait de relier comme un processus d’OC, au même titre que le fait d’indexer, classer et cataloguer. Ceci prolonge et précise l’affirmation de Riccardo Ridi suivant laquelle l’hypertextualité est une modalité d’organisation des connaissances, tout en ramenant la notion à une échelle plus maîtrisable que dans sa théorie du « document/univers hypertextuel » [@ridi2018, 417]. Relier est un processus doublement créatif puisqu’il permet à la fois de composer de nouvelles idées et de produire un graphe, c’est-à-dire qu’il y a simultanément création de connaissances et de leur organisation.

Deuxièmement, une documentation personnelle peut être associée à des systèmes d’OC personnels, c’est-à-dire qui n’adhèrent pas strictement à un schéma pré-établi. Comme l’affirme Umberto Eco, classer ses fiches en différentes catégories dépend nécessairement du sujet d’étude ; j’ajouterai que cela dépend également de l’individu. Il en va de même pour le fait de classer les liens, pour lesquels il peut être pertinent de définir un ensemble inédit de catégories adaptées à un sujet précis et suivant une perspective personnelle. La même approche peut être appliquée aux mots-clés. De manière générale, on rejoint ici une idée associée au concept de folksonomie – taxonomie sociale, élaborée de manière libre et collaborative –, mais ramenée à une échelle individuelle : l’idée que les systèmes d’OC puissent être conçus localement, empiriquement, plutôt que par une autorité située en amont. Comme pour le fait de relier, tout ceci correspond également à une pratique d’OC qu’on peut qualifier de créative.

La créativité intervient dans de multiples thématiques abordées par la littérature en OC. Elle s’observe dans l’élaboration des métadonnées, qui n’est pas toujours le fait de professionnels, et qui tend alors à être idiosyncratique [@mayernik2020, 706]. Les travaux sur les folksonomies décrivent la tension qui existe entre créativité individuelle et systèmes d’OC [@rafferty2018, 507, 510]. Et dans la pyramide conceptuelle données-information-connaissance-sagesse, cette dernière est caractérisée comme résultant d’un travail créatif d’interconnexion entre connaissance et expérience [@fricke2019, 39].

Toutefois, l’approche créative de l’OC n’est jamais discutée en tant que telle. Elle a pourtant une singularité, et elle présente surtout deux différentes intéressantes avec l’approche extractive. D’abord, elle est plus accessible techniquement. Tout savoir-faire nécessite un apprentissage mais les nouveaux outils de la réflexion sont considérablement plus simples à prendre en main que les systèmes d’information experts qui reposent aussi sur l’hypertextualité, comme par exemple ceux associés au Web sémantique. Et ensuite, l’utilité principale de l’approche créative, qui est la réflexivité, a une portée large. Contrairement à l’approche extractive, généralement située dans le périmètre précis d’une expérimentation, l’OC créative a pour objet la documentation personnelle, qui touche à l’ensemble du travail de recherche.

En raison de cette accessibilité technique et de cette portée réflexive, je pense qu’une approche créative de l’OC présente un intérêt notable pour la recherche doctorale, de manière transdisciplinaire. Le doctorat est une période appropriée pour commencer à construire une documentation personnelle : pour la majorité des étudiants, l’ampleur du travail de recherche que représente la thèse est inédite ; c’est un moment où la question de l’idéation se pose plus urgemment qu’auparavant ; c’est aussi un temps de formation et d’acquisition de savoir-faire, durant lequel on peut se permettre d’investir du temps pour en gagner par la suite. Or le bénéfice de la documentation personnelle augmente précisément avec le temps. Les exemples de Niklas Luhmann et Umberto Eco, entre autres, illustrent bien l’effet positif que peut avoir l’accumulation des connaissances au fil du temps, sous réserve qu’elle ne soit ni subie ni désorganisée. La documentation personnelle est donc à penser comme une forme d’idiotexte, cette mémoire prothétique théorisée par Bernard Stiegler et qui requiert le développement d’une véritable culture instrumentale [@stiegler2010].

Concluons donc en citant une dernière fois Markus Krajewski :

> [*“One would be well-advised to mind the recommendations of the scholars, and set about the construction of such an apparatus as soon as possible.”*]{.marginnote}« On serait bien avisé de tenir compte des recommandations de nos prédécesseurs, et s’atteler à la construction d’un tel dispositif dès que possible[@krajewski2016, 322] ».

# Références {-}

::: {#refs}
:::

# Annexe {-}

[Liste d’outils permettant de gérer des connaissances interreliées](https://docs.google.com/spreadsheets/d/1md1hy8n_17hfki6k-GHvbBlqZXZyNfZwafOxOitFUnw/) (Google Sheets, lecture seule)