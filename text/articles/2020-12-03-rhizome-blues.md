---
title: "Rhizome Blues: Introducing Document Teratology"
date: 2020-12-03
description: "The aim of this paper is to defend a richer theoretical understanding of what we call monsters, and to argue for the development of document teratology, which we see as an important scientific issue for documentology. We start from the premise that the default state of communication is incommunication, and that documentation, developed to counter this, seems to have become overwhelmed from the inside by its own problematic development. We then discuss the opportunity of a document teratology, based on nuanced description of what the word monster means. We describe two strong imperatives, monstration and categorisation, and the tension between them. We show that monsters are a product of modernity, and not pure aberrations. We suggest some metaphorical monsters from literary or scientific works to be used as tools of theoretical and practical work in the context of documentology. Finally, we reassess the rhizome as a model and suggest replacing with the stolon, more appropriate in some cases."
type: Article de colloque
article: true
---

::: abstract
**Abstract:** [^publication] The aim of this paper is to defend a richer theoretical understanding of what we call monsters, and to argue for the development of document teratology, which we see as an important scientific issue for documentology. We start from the premise that the default state of communication is incommunication, and that documentation, developed to counter this, seems to have become overwhelmed from the inside by its own problematic development. We then discuss the opportunity of a document teratology, based on nuanced description of what the word monster means. We describe two strong imperatives, monstration and categorisation, and the tension between them. We show that monsters are a product of modernity, and not pure aberrations. We suggest some metaphorical monsters from literary or scientific works to be used as tools of theoretical and practical work in the context of documentology. Finally, we reassess the rhizome as a model and suggest replacing with the stolon, more appropriate in some cases.
:::

# Introduction 

The aim of this paper is to defend a richer theoretical understanding of what we call monsters, and to argue for the development of document teratology, which we see as an important scientific issue for documentology.

We start from the premise that the default state of communication can be seen as something monstrous: we are referring to incommunication—excessive, asynchronous, uncertain [@robert2010, 378]. Our “information society” could then be characterised as a society that is aware of incommunication and strives to respond to it through science and technology. This has not always been the case. In classical antiquity, orality prevailed over writing: in the hierarchy of practices for dealing with excess information, arts of memory (mnemonics) prevailed over what we might today call memory technology or mnemotechnologies [@robert2010, 377]. As Umberto Eco points out, these arts of memory were later criticised by some as “monstrous, overloading the mind, making it obtuse and leading it into madness” [@eco2010, 100]. With the invention of the codex, and then of printing, society moved from *ars memoriae* to *ars excerpendi*, mitigating the “terror of excess” through reduction and conservation [@eco2010, 108]. This reduction, or principle of rarefaction as Michel Foucault calls it [@foucault1971], is characteristic of documentation, which could be defined as a vast strategy aimed at these intrinsic problems of information-communication.

Nowadays, documentation seems to have become overwhelmed from the inside. Its development is subject to many ills: infobesity, fluctuating content, crypto-documentation, processing errors, greedy and negligent accumulation, misrepresentation and misevaluation, etc. Artificial intelligence, or, to provide a more accurate definition, iterative algorithmic indexing corrected by man, tends to replace proper training in documentary practices; this is accompanied by a shift from indexing knowledge to indexing existences [@day2014; @ledeuff2015a]. Faced with this ‘neo-documentation’, our fear of excess has returned; it seems that the solution is now part of the problem. This situation has been criticised as a “documentary teratogenesis” [@ledeuff2007], an analogy we explore more systematically here.

# The double meaning of ‘monster’: monstrosity/monstration

In its most common sense, the word ‘monster’ refers to that which is monstrous, abnormal, on the fringe, outside of established categories. Monstrosity defines a fascinating and terrifying anomaly, although what is monstrous may one day become normal, following a shift in the mainstream.

Consequently, the documentary monster refers to the normative aspects of documentation that have gone wrong. Infobesity, and nowadays the data deluge, is a fundamental dysfunction of documentation as a strategy of reduction, of rarefaction. But other normative aspects can be disrupted as well. While documentation aims to bring stability, accessibility and accuracy to data, network models favour content fluctuation, concealment and erroneous processing. Exchange protocols, for example in the case of data harvesting, are designed for harmonious sharing, but in practice they proceed by blind accumulation and lead overall to a decrease of documentarity.

By erring so, the dysfunctional normative aspects of documentation seem to represent incommunication re-emerging back from communication. When analyzing the nature of the monster, Michel Foucault highlighted something which applies here: the absence of discernible law and structure [@foucault1966, 168-170].

To this well-known meaning of ‘monster’, another one must be added. Etymologically, ‘monster’ means something extraordinary, which must be shown and seen. We use the term ‘monstration’ to refer to an imperative of designation, the injunction to show the anomaly, to demonstrate it if possible. The word ‘monster’ shares this meaning with the word ‘legend’, which comes from the Latin *legenda*: that which must be read [@ledeuff2007, 2].

In this second sense, documentary monstration (and not monstrosity) is in direct competition with scholarly knowledge, the laborious work of synthesis, the vast oceans of grey information, qualitative but often perceived as dull. Wisdom can be too wise for its own good, and legends may prevail over wisdom, because information is of a processual and emergent nature [@buckland1991; @frohmann2004, 138]. The superiority of legends is built on the *agora* of communication. In a digital paradigm which includes social networks, this phenomenon is becoming more and more pronounced.

# Monsters are a product of modernity

So there is more complexity to the monster than a mere scarecrow: it is defined by both its abnormality and an imperative of designation, which we call monstration. But more importantly, it is crucial to understand that monsters are not *aberrations*.

Indeed, the need to think/classify [@perec2015] requires that we ascribe to an *operational* logic, which etymologically consists of opening and cutting in order to understand and analyse the world. Operation, like anatomy, enables us to consider taking a fresh look at the world, and redefine it. “To classify is the highest operation of the mind”, writes Otlet [-@otlet1934, 379], following in the footsteps of previous classifiers and ordinators, as was the mathematician, philosopher but also librarian and politician Leibniz. This ordering makes it possible to separate the different members in order to envisage new thematic and organisational configurations. This can lead to strange experiments, as in Mary Shelley’s *Frankenstein*.

This means that the concept of monster is only a product of modernity, of the spirit of exercising one’s understanding. The monster was simply not visible as such before. Order is what creates monsters, by setting aside the non-standard and the unclassifiable in the name of an imperative of categorisation: to remove what does not fit into the constructed norm.

This imperative is a by-product of the critique of ancient constructions, and specifically the progressive apparition of new disciplinary fields, ones that proceed by evaluating and excluding what is deemed unacceptable. Foucault, in the preface to *The Order of Things*, mentions the naturalist Georges Louis Leclerc de Buffon (1708-1788), who was shocked by the works of previous centuries because they made a weak (and sometimes non-existent) distinction between what was real and what was fictional. Buffon refers in particular to Ulisse Aldrovandi’s (1522-1605) work, but one can also think of the important work of Conrad Gesner (1516-1565), bibliographer and botanist, who proceeded by compilation rather than exclusion. Gesner’s aim was to prevent loss: he sought to preserve the different types of knowledge he had been able to accumulate. His other idea was that each and every document could potentially interest some reader, a position he defends in his *Biblioteca universalis*: he explains that he did not seek to verify the debatable or even heretical nature of the publications he listed, and defers this task to his reader—ironically, Gesner’s work was eventually diverted by the catholic Church to expand the *Index librorum prohibitorum*. Gesner did not dismiss creatures outside the norm, perceiving them as monsters in the sense that they deserved to be shown. Unicorns and sea monsters appear in his natural history, as if to remind us that they exist because they were invented, to use Boris Vian’s expression[^1].

The ordering of discourse (as defended by Buffon and others) thwarted Gesner’s way of seeing the world. What remains unclear is whether this was part of a revolutionary spirit, weaponizing reason in the form of Ockham’s razor in order to avoid the proliferation of superfluous categories and concepts, or, on the contrary, whether it should be seen as a spirit of counter-revolution (Toulmin), aiming to restrict the expansion of possibilities and to categorise all forms, whether natural or cultural. In any case, it made the monster into an element that had to be fought and designated as abnormal.

Despite this, the monster retained a powerful mix of attractiveness and repulsiveness that would make the success of the Gothic and Romantic novels of the 19th century, and also explain the popularity of cabinets of curiosities. Moreover, the scientific construction of knowledge simply could not get rid of it, as if it were a “monster in the wardrobe” just waiting to pop out again. One is reminded of the mummies that haunt the *Extraordinary Adventures of Adèle Blanc-Sec* and which sometimes appear more rational than the mad scientists Adèle is faced with. Mostly, we think of a humanity whose monstrosity would eventually express itself through devastating global conflicts.

On this matter, Bruno Latour offers some useful thoughts. He shows that divisions are often effective when it comes to trying to understand the world: the principle is to reduce it into observable and comprehensible forms, through what Latour calls centres of computation (*centres de calcul*). These are the environments where knowledge is legitimised and science practised—laboratory or library. However, this exercise of reason does not totally exclude a form of magical discourse:

> “If we call ‘magic’ that body of practices which gives to the one who possesses a hundred words the power to extract all the others and to act on things with these words, then we must call magic the world of logic, deduction and theory, but it is our magic [@latour1984, 292]”.

It is not possible to fully study the world by constantly seeking to reduce it to what appears rational only. Latour defends the need for a more modest viewpoint than the great division between nature and culture; a reintegration, a form of disaffection with the acceptance of an “irreduction.”

This choice appears all the more essential right now, because marginalisation and exclusion does not allow us to grasp well the issues between science and pseudo-science, conspiracy theories, futility and other signs of the restlessness of the mind. The attempts of Otlet’s friend and collaborator Charles Richet to integrate the paranormal into scientific fields of study famously failed, in particular because he was not able to conceive the deception in the case of the Villa Carmen [@lemalefan2002]. This may be a clue as to why Otlet was reluctant to integrate the paranormal into the Universal Decimal Classification (UDC). However, we should not be too quick to dismiss Otlet as a simple essentialist: he was more interested in flow than essence. His call for a “hyper-documentation” expressed a desire to extend documentary territories to all known physical senses as well as the sixth sense, with the over-arching hope of countering the hyperseparatism of both nation states and scientific disciplines.

It seems to us, therefore, that monsters should no longer be rejected, but treated as legitimate documentary issues. For this, we need to name our approach, and suggest methods of analysis.

# Document teratology

The word teratology, which we have chosen to introduce here in the context of documentology, has two meanings. The first concerns the monster as a legend, etymologically what must be shown, as we discussed earlier. The second meaning concerns a branch of biology.

> “Teratology: a discourse or narrative concerning prodigies; a marvelous tale, or collection of such tales. Teratology (biology): the study of monstrosities or abnormal formations in animals or plants[^2]”.

In the second sense, an even more precise definition can be given:

> “Teratology: science of monsters, which deals more particularly with the most aberrant congenital or hereditary anomalies, classifies them according to their anatomical aspect (*morphological teratology*), studies the development of the malformed embryo (*pathogenic teratology*) and tries to detect the causes of these malformations (*etiological teratology* or *teratogenesis*)[^3]”.

On this basis, we can imagine a framework for what would be called documentary teratology, based on the existing branch of biology: 1) defining anomalies 2) establishing criteria for their classification 3) describing their development 4) searching for causes.

Such an approach is very normative; therefore, it can proceed from within documentation itself, with some benefits. However, it also has the disadvantage of replicating the existing documentary schemes. From our understanding of monsters (monstrosity and monstration), this cannot suffice. It is necessary to go beyond the imperative of categorisation, in particular because the problem has been reversed: the abnormal, the monstrous has become the norm. Documentology now requires new forms of expeditions into information spaces, whose mechanical arrangements escape us and whose connections cannot be grasped so easily. So if teratology must become an essential branch of documentology, it is because the scientific study of current documents cannot be part of a classical approach, but must, on the contrary, resemble a sort of dynamic genetics, which cannot be satisfied with traditional tree structures and classifications.

A first teratological attempt was made at the beginning of the 20th century by Charles Fort, the paranormal documentalist, who devoted his life to compiling everything that seemed out of the ordinary in the press and in the documents he could access through libraries. A prolific note-taker, Fort inspired Lovecraft’s stories by making not divisions but improbable connections, for example between animals, where the zoologist would have chosen to operate by separation. Fort’s logic also inspired Pauwells and Bergier’s book, *Le matin des magiciens*, in which they develop fantastic realism, taking over from the *merveilleux-scientifique* genre a few decades earlier. In many ways, Fort’s stories have also become sources for conspiracy thinking. Should we, however, judge them too quickly? Would it not be fair to study all their facets?

If hyperdocumentation includes the rational and the irrational, it is undoubtedly time to start looking for new associations, to allow the mind to come up with new syntheses. This is what Paul Otlet wanted, and which he expressed in *Monde, essai d’universalisme* in 1935. Author A. E. Van Vogt, part of whose work was inspired by the non-Aristotelian forms developed in Korzybsky’s general semantics, had also grasped this idea. He was of the opinion that, in a way, the map never completely matches the territory. In *The Voyage of the Space Beagle*, he suggests another approach, a new scientific discipline stemming from this spirit of synthesis rather than from the logic of division and sub-disciplines: nexialism. In 1978, at the first French congress of information and communication sciences (SIC), Robert Escarpit suggested that the character of the nexialist in Van Vogt’s novel, who in his view resembled the profession of documentalist, could be an example for the new discipline to follow [@escarpit1978].

# A gallery of monsters

How do we proceed? We would like to suggest a first possibility, which consists in undertaking a teratological and documentological re-reading of certain literary or scientific works, known for their evocative power, in search of the monsters that populate them. Indeed, monsters can be extremely useful metaphorical tools for theoretical thinking, but also for pedagogical work and outreach. To clarify, ‘monster’ is taken here in all the complexity of its double meaning: it represents the singularity, the new form; one that arouses curiosity or causes fright; one that, when found in writing, is the object of both striking description and thoughtful reflection.

```{=openxml}
<w:p>
  <w:r>
    <w:br w:type="page"/>
  </w:r>
</w:p>
```

Monster         | Reference | Concept
----------------|-----------|------------------------
Antelope, gorilla | Suzanne Briet, Robert Pagès | Self-documentation, self-(de)monstration.
Unicorn | Conrad Gesner | Documentation of a ‘fact’ despite the lack of any evidence.
Melanicus | Charles Fort | Mixture of fact and fiction, written record paradoxically introduced as unthinkable and unspeakable. The rhetoric of the unthinkable and the unspeakable is regularly used by H. P. Lovecraft in his works.
Giant | Phlegon of Tralles | Artefact by reconstitution, extrapolation from a trace. This reconstruction attempt from the time of Roman emperor Tiberius prefigures the much later work of the anatomist Cuvier. Phlegon relates the episode in a compilation entitled *On Marvels* (*Peri thaumasion*).
Hologram of the prima donna (from *The Carpathian Castle*) | Jules Verne | Artifact by artifice, absence made present again, and reproducible.
Flying Spaghetti Monster | Bobby Henderson | Political, satirical monster, the absurd monstration of the invisible.

: Analysis of a few “monsters” from the perspective of documentary teratology

# From rhizome to stolon

The second possibility we wish to discuss concerns models of knowledge organisation, which are metaphorical tools as well.

There are many metaphors to describe the organisation of knowledge: trees, forests and labyrinths, rivers, oceans and islands, unexplored territories, canvases and much more [@borel2014, 74-81]. Trees and networks can be considered the most popular. This is no coincidence: in his *General Theory of Schematisation*, Robert Estivals has shown that human cognition is based on two consecutive movements: arborescent reduction and reticular organisation [@estivals2002, 35]. Trees and networks form the basis of many types of diagrams, from fixed representations to “knowledge generators” [@drucker2014, 95-115], and they were particularly important for early theories of documentation [@vanacker2011, 62]. These metaphors are still relevant today, especially in the context of computer networks. The *Document Object Model*, a central concept of markup and serialisation formats (HTML and XML), represents a tree. The *World Wide Web* takes its name from the web woven by the links between hypertext and namespaces, which create complex networks of data and metadata.

The initially predominant tree metaphor has been the subject of significant criticism, which has gradually shifted the focus to network structures and has led to the emergence of new metaphors. The most emblematic is that of the rhizome [@eco2010]. The work of Gilles Deleuze and Félix Guattari [-@deleuze1980] on this concept is essential. Their starting point is a reassessment of the hierarchical tree as a model, including the idea of deep structure it implies (by analogy with the tree’s roots). They propose an alternative, the rhizome, which they characterise by its capacity to establish connections along branches based on non-significant forks; this produces heterogeneity and multiplicity, and offers a cartographic potential more in line with the major issues of our time.

As a botanical analogy, Deleuze and Guattari’s rhizome has a few weaknesses. Contrary to what they claim [@deleuze1980, 13-16], bulbs and tubers are not rhizomes. The fact that a rhizome has roots somewhat weakens their criticism of deep structure. Notwithstanding these reservations, the impact of this new metaphor on the scientific literature has been immense.

One reflection that has not been raised so far in the scientific literature is the link between the rhizomatic model and the problematic development of documentation we mentioned earlier. With its anarchic development, the rhizome offers a striking illustration of a disrupted documentation. Because it exists underground, it confronts us with the difficulty of representing what is buried and the reflex of repression that often follows. Lastly, for Deleuze and Guattari “the rhizome is an anti-genealogy, a short memory, or an anti-memory [@deleuze1980, 32]”; it seems therefore an unreliable metaphor when attempting an analysis of documentary teratogenesis. Within our gallery of documentological monsters, the rhizomatic model would have to be placed alongside the Flying Spaghetti Monster: it illustrates both insignificant monstration and almost impossible demonstration.

Perhaps we should then turn to hybrid models. Samuel Szoniecky defends this approach in his works, suggesting we adopt a tree-rhizome hybrid model [@szoniecky2012]. But we could also look for different models entirely, such as the stolon. Firstly, because the runner allows us to bring the reticular logic to the surface, rather than to fall back on a buried rhizome:

> “Stolon (botanical): secondary stem of certain plants, which runs on the surface of the ground and takes root from place to place to produce new plants by natural layering[^6]”.

And secondly, because it provides us with novel opportunities for analysis on the communicational and organisational aspects of documentation. This is because ‘stolon’ has another meaning, whose metaphorical potential concerns questions of centrality and sustainability that the rhizome can hardly address :

> “Stolon (zoology): budding organ of certain lower marine animals; in particular, a thin cord, a bud generator, which connects each individual in a colony to the mother organism and makes them communicate with each other. Many species of sessile invertebrates (sponges and ascidians in particular) have forms of resistance (stolons, dormant buds)[^6]”.

Beyond metaphorical monsters and new models of monstration, the idea of document teratology may open many paths for exploration and discovery.

# Conclusion

The current information crisis and its excesses—overinformation, infopollution and disinformation—force us to move beyond the classical position according to which information alone shapes the mind in a logic of documentary transmission. We can no more separate information from disinformation than we can separate formation from deformation, or the provision of proof from the fabrication of forgeries. And we cannot reduce our study of world to a scientific approach, which would turn documents into mere methods and subjects of demonstration. This is the challenge of irreduction in the context of documentation: to understand that documentology is now just as much a teratology. It must study and recount both demonstration and monstration, as if in a house of mirrors, and we must come to terms with the fact that the documentologist is as much Jekyll as Hyde.

# References {-}

[^1]: From the foreword to his book *L’Écume des jours*: “This is a completely true story, since I imagined it from start to finish.” (“*L’histoire est entièrement vraie, puisque je l’ai imaginée d’un bout à l’autre.*”)

[^2]: “Teratology, n.”. *Oxford English Dictionary Online*. <https://oed.com/view/Entry/199333>

[^3]: “Tératologie, subst. masc.”. *Trésor de la Langue Française Informatisé*. <https://cnrtl.fr/definition/tératologie>

<!-- [^4]: The rhetoric of the unthinkable and the unspeakable is regularly used by H. P. Lovecraft in his works. -->

<!-- [^5]: This reconstruction attempt from the time of Roman emperor Tiberius prefigures the much later work of the anatomist Cuvier. Phlegon relates the episode in a compilation entitled *On Marvels* (*Peri thaumasion*). -->

[^6]: « Stolon, subst. masc. ». *Trésor de la Langue Française Informatisé*. <https://cnrtl.fr/definition/stolon>

[^publication]: {-} Post-print. Perret, Arthur, Le Deuff, Olivier & Borel, Clément. “Rhizome Blues: Introducing Document Teratology”. *Proceedings from the Document Academy*. 2020, Vol. 7, n° 1, [online]. <https://doi.org/10.35492/docam/7/1/13>.

