---
title: Sémiotique des fluides
date: 2021-03-07
description: "Ce texte propose d'examiner la fluidité introduite par l'informatisation de la typographie et qui apparaît dans le livre numérique. La métaphore de la fluidité est posée comme complémentaire de celles de la plasticité et de la liquidité, car elle rend compte du mouvement et pas seulement d'un état. Nous recontextualisons d'abord le lien entre typographie et fluidité en rappelant l'impossibilité de la classification typographique objective. Nous analysons ensuite la fluidification du sémiotique, c’est-à-dire la plasticité accrue de notre système de signes. Ceci nous permet de contribuer à une description de l'expérience de lecture en termes de négociation entre les intentions des concepteurs et les usages des lecteurs. Nous examinons enfin la sémiotisation de la fluidité, c’est-à-dire sa mise en œuvre par un nouveau jeu d’écriture. Ceci nous permet de mettre en évidence le caractère dispositif de l'écriture et de considérer la notion d'architexte sous l'angle de la scripturation (ou invention typographique). Ces éléments nourrissent une réflexion sur les nouvelles pratiques d’écriture, d’édition et de lecture à l'ère numérique."
type: Article de colloque
nocite: |
  @bernardot2018a,@berthier2006,@blanchard1982,@buckland1997,@caws1981,@christin1995,@collomb2017,@collomb2020,@drucker2011a,@duplan2007,@eisenstein1979,@eisenstein2002,@escarpit1976,@feenberg2004,@goody1979,@goody2012,@goyet2017,@herrenschmidt2007,@jeanneret1999,@jeanneret2005a,@keith2016,@latour2007,@laufer1980,@laufer1986,@lemon2013,@perret2018b,@rigaud2017,@robert2010,@robert2012,@souchier2006,@tangaro2017,@truong2019,@vanleeuwen2006,@vox1954
article: true
---

::: abstract
**Rétractation** (20/05/2021) – J'ai proposé ce texte en 2018 à la suite du colloque Écridil, pour la publication des actes. Le texte a été évalué en 2019, sa publication a été recommandée sous réserve de modifications. En mars 2021, j'ai pré-publié la version corrigée dans l'attente de la publication définitive. J'ai été informé ce jour qu'une évaluation datant de 2019 et qui ne m'avait pas été transmise recommande le rejet de ce texte sur la base d'arguments que je dois prendre en compte. Je ne pré-publie ni ne publie de textes rejetés, je retire donc ma pré-publication.
:::

# Liste de lecture

