---
title: Les tableaux au format texte
date: 2021-01-07
veille: true
---

<https://plaintextproject.online/articles/2020/11/12/tables.html>

Des modèles et des générateurs pour éviter de s'arracher les cheveux avec les tableaux au format texte. À combiner avec de bons styles à l'export si on convertit souvent des textes contenant ce type de contenu. 

