---
title: Pandoc et les citations au format docx
date: 2022-04-07
veille: true
---

<https://pandoc.org/releases.html#pandoc-2.18-2022-04-22>

Pandoc v2.18 introduit un progrès majeur pour les gens qui souhaitent faire des conversions aller-retour docx et Markdown, typiquement dans le cas d'une navette entre auteur et éditeur. Auparavant, les citations constituaient le point faible de ce *workflow*. Je me suis arraché les cheveux plusieurs fois sur cette question. Une nouvelle extension au module docx de Pandoc change la donne : le convertisseur reconnaît désormais les citations embarquées dans un docx par Zotero, Mendeley et Endnote (trois logiciels bibliographiques parmi les plus utilisés) et peut les convertir dans son propre format.

> “If you add the `citations` extension to `docx` (`-f docx+citations`), pandoc can now extract embedded Zotero, Mendeley, and EndNote citations and treat them as native pandoc citations (so that you can use `--citeproc` with them).”

