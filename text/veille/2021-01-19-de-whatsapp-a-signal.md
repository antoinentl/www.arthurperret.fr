---
title: De WhatsApp à Signal
date: 2021-01-19
veille: true
---

<https://biblionumericus.fr/2021/01/13/whatsapp-signal-et-les-rabats-joie-de-la-protection-de-la-vie-privee/>

De l'inertie des migrations sociales numériques, entre injonctions, résistance et frictions. Court billet d'humeur que les *early adopters* de Signal pourront savourer.

