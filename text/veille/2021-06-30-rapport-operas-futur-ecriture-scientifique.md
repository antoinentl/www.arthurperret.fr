---
title: Rapport OPERAS sur le futur de l'écriture scientifique
date: 2021-06-30
veille: true
---

<https://zenodo.org/record/4922512>

OPERAS est une infrastructure européenne pour le développement de la communication scientifique ouverte en sciences humaines et sociales (description copiée directement de leur [blog Hypothèses](https://operas.hypotheses.org)). Le volet publications (OPERAS-P) vient de livrer une série de rapports suite à un long travail d'enquête. J'attire évidemment votre attention sur le rapport D6.5, qui concerne l'écriture académique (et pas seulement parce que j'ai fait partie des enquêtés !) mais il faut parcourir le reste aussi, en commençant par [la synthèse générale](https://zenodo.org/record/5017705). C'est dans cette littérature blanche que se dessine, sinon le futur de la communication scientifique, au moins le futur des services pour la recherche en SHS à l'échelle européenne.
