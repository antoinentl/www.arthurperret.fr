---
title: Tout sur l’email
date: 2021-05-12
veille: true
---

<https://explained-from-first-principles.com/email/>

Tout ce que vous avez toujours voulu savoir sur l'email sans jamais oser le demander. Un travail sidérant – imprimé, l'article fait 141 pages –, dont seuls les spécialistes pourraient juger l'exactitude mais qui semble déjà susciter un certain enthousiasme. C'est la deuxième technique décortiquée par l'auteur après [Internet](https://explained-from-first-principles.com/internet/) et on a hâte de connaître la troisième, même s'il faudra probablement attendre à nouveau plusieurs mois.

Via [Just Use Email](https://www.justuseemail.com/email-explained-from-first-principles/), un site lui-même lancé très récemment et qui tient un discours plutôt radical sur les outils de communication, un peu dans la veine de certains sites dédiés au format texte que je surveille ([Use plaintext Email](https://useplaintext.email) par exemple).
