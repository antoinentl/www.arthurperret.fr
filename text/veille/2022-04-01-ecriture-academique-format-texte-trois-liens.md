---
title: "Écriture académique au format texte : trois liens"
date: 2022-04-01
veille: true
---

<https://plaintextproject.online/articles/2022/03/29/links.html>

Scott Nesbit du *Plain Text Project* partage quatre liens, dont trois me semblent très intéressants, à propos de l'écriture académique au [format texte](../cours/format-texte.html) :

<https://economicsfromthetopdown.com/2020/12/10/why-and-how-i-write-scientific-documents-in-plain-text/>

Accessible et complet, ce texte est une très bonne introduction à l'enjeu du format texte pour l'écriture académique. La promesse est claire : créer des documents complexes via une syntaxe simple, en se débarrassant des défauts des traitements de texte.

<https://jaantollander.com/post/scientific-writing-with-markdown/>

Beaucoup plus technique (écrit par un informaticien diplômé en maths), très fouillé, ce deuxième texte est une bonne ressource également. Mais l'auteur recommande VS Code et Atom sans mentionner d'alternatives. Or ce sont plus des éditeurs de code que de texte, dans leur design en tout cas ; ils sont empreint d'une culture du développement web qui fait sans doute partie du bagage de l'auteur mais pas de tous les chercheurs. Personnellement, je recommande plus volontiers des éditeurs de texte généralistes, ou bien spécialisés en Markdown.

<https://scientificallysound.org/2021/03/09/markdown-for-science-and-academia-part-2/>

Enfin, le troisième lien mène à une introduction très didactique, doublement recommandable : d'abord parce que l'auteur base son texte sur Linux ; et ensuite parce qu'il illustre son propos par un geste tout simple mais transversal et cher à mon cœur, la prise de notes pour faire des fiches de lecture.


