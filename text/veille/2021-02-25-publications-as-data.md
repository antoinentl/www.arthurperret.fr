---
title: Publications as data?
date: 2021-02-25
veille: true
---

<https://twitter.com/arthurperret/status/1364947919275778061>

[Restitution des travaux menés dans le cadre d'OPERAS-P](https://operas.hypotheses.org/4512). J'ai twitté mes réflexions à chaud sur le workshop portant sur les publications en tant que données en SHS – sujet sur lequel j'ai une réflexion en cours, voir [mon abstract pour DH2020](https://arthurperret.fr/all-papers-are-data-papers.html).

