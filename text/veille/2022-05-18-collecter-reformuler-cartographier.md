---
title: Collecter, reformuler, cartographier
date: 2022-05-18
veille: true
---

<https://www.baldurbjarnason.com/2022/the-different-kinds-of-notes/>

Baldur Bjarnason est en train de rejoindre Gordon Brander dans le cercle des auteurs anglophones dont j’attends impatiemment chaque billet de blog. Ici, il propose carrément une théorie de la prise de notes, une sorte de méthodologie très générale résumée en trois points : collecter, reformuler, cartographier. Un matériau riche, que je me suis empressé de… collecter, reformuler et cartographier.

