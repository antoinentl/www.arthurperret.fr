---
title: Déplacement des graphes
date: 2021-04-14
veille: true
---

<https://www.infotoday.com/it/apr21/Davis--Knowledge-Graphs-The-Next-Revolution-in-Scientific-Publishing.shtml>

Le graphe de connaissance est un mode d'organisation des connaissances dans lequel on relie des descriptions de connaissances. Dans le champ de la représentation des connaissances, les graphes servent à représenter le langage. Le langage n'a pas une structure de graphe : le graphe modélise le langage. D'où un sens radicalement différent de celui de graphe documentaire, dans lequel ce sont directement des documents (ou fragments de documents) qui sont interreliés.

Cette distinction commence à bouger. Dans cet article, l'auteur présente d'abord le concept de *knowledge graph* en des termes qui semblent coller à l'usage dominant en *knowledge organization*. Mais il l'applique ensuite à la cartographie relationnelle entre chercheurs et entre publications. Le graphe n'est alors plus une couche de données venant se superposer aux données existantes, c'est une représentation des relations entre ces données existantes. C'est un glissement vers le graphe documentaire.

Via [Angèle Stalder](https://twitter.com/staldera/status/1381247979651223552).
