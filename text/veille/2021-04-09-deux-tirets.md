---
title: Deux tirets
date: 2021-04-09
veille: true
---

<https://blog.djmnet.org/2019/08/02/why-do-long-options-start-with/>

Un petit morceau d'histoire des relations entre informatique et caractères typographiques. Si quelqu'un écrit l'histoire entière un jour, je serai le premier à acheter son bouquin.

Via [Hacker News](https://news.ycombinator.com/item?id=26744519).