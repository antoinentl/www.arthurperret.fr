---
title: Un manuel de visualisation de données
date: 2021-05-26
veille: true
---

<https://clauswilke.com/dataviz/>

Ce site donne accès au manuscrit auteur d'un manuel édité par O'Reilly. Ça me semble être un bon portail pour les personnes souhaitant acquérir des connaissances formalisées en visualisation.

Et surtout, le livre inclut une [bibliographie annotée](https://clauswilke.com/dataviz/bibliography.html) ! Si vous n'avez pas encore cliqué sur l'entrée de veille précédente, allez lire [la synthèse du blog Zotero francophone](https://zotero.hypotheses.org/3556) et [mon billet](https://www.arthurperret.fr/bibliographie-annotee.html) sur le sujet.