---
title: Le vrai paradoxe de LaTeX
date: 2022-02-11
veille: true
---

<https://etnadji.fr/pagxoj/6775eb99-da9c-43ac-a571-5b1569f04646.html>

Il y a deux ans, j'ai écrit [un billet](../blog/2020-05-22-ecrire-et-editer.html) qui s'appuyait notamment sur un texte de Daniel Allington à propos de LaTeX, son intérêt comme outil de composition et ses faiblesses comme outil d’écriture. Depuis, le texte a circulé : Antoine Fauchié en a parlé [sur son site](https://www.quaternum.net/2022/01/05/latex-un-outil-de-composition-pas-un-outil-decriture/) et aujourd'hui Étienne Nadji ajoute une perspective qui m’intéresse. Je le cite :

> « LATEX est pour moi un outil d’écriture ; la fracture n’est pas entre l’écriture / la composition mais entre le métier d’auteur et celui d’éditeur (“métier”, “auteur” et “éditeur” au singulier, pour simplifier…) ».

Étienne fait la distinction entre écrire et éditer (ou composer) sur la base des métiers, des pratiques plutôt que des fonctionnalités de l’outil. Ainsi, LaTeX étant plutôt utilisé par les auteurs, il appartiendrait de fait à la catégorie des outils d’écriture.

Je trouve que c’est plutôt sensé, et du coup cela m'a fait réfléchir à ce qui est peut-être le vrai paradoxe de LaTeX. Donald Knuth a conçu TeX pour permettre la composition du texte, pas pour faciliter son écriture. Leslie Lamport a ajouté à TeX un ensemble de macros destinées à rendre la prise en main plus facile mais le résultat, LaTeX, n’efface pas du tout la rugosité de l'outil d'origine : certes, on passe moins de temps à écrire du code mais on reste empêtré dans un balisage lourd, au vocabulaire étrange. LaTeX ne transforme pas magiquement TeX en un outil conçu pour écrire. Or il est bel et bien utilisé pour écrire, d'où le paradoxe et une vraie tension à l'usage.

Pour esquiver cette tension, on peut passer par un format intermédiaire comme le [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), qui permet de générer du PDF via LaTeX sans écrire de LaTeX ou presque. Mais personnellement, je suis un peu fatigué de LaTeX. J'attends patiemment le jour où je pourrai tout faire en CSS, notamment grâce au travail de la fondation [Coko](https://coko.foundation) sur [Paged.js](https://www.pagedjs.org), outil de pagination 100% web, récemment ajouté à Pandoc pour les exports PDF.
