---
title: Récit d'une migration de Jekyll à Pandoc
date: 2021-01-07
veille: true
---

<https://plaintextproject.online/articles/2020/11/05/migration.html>

Passer d'un générateur clé en main à un générateur sur mesure, ou comment réapprendre tout ce qui fait un site : page d'index, éléments récurrents dans les articles, flux RSS… Un vécu similaire à [la migration de mon site](https://www.arthurperret.fr/dr-pandoc-and-mr-make.html) (qui était heureusement moins volumineux). 

