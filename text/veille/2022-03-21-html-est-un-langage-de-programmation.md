---
title: HTML est un langage de programmation
date: 2022-03-21
veille: true
---

<https://briefs.video/videos/is-html-a-programming-language/>

Une vidéo hilarante sur HTML, qui m’a fait penser aux collages des [Monty Python](https://fr.wikipedia.org/wiki/Monty_Python), à [« history of the entire world, i guess » de Bill Wurtz](https://www.youtube.com/watch?v=xuCn8ux2gbs), aux jeux [Amanita Design](https://www.amanita-design.net)… J'étais obligé de la partager, puisque j'ai écrit dans mon entrée de veille précédente que HTML n'est pas un langage de programmation. Pour ma défense, c'est un argument que j'utilise pour détendre des étudiants à qui le mot « programmation » fait peur ; il faudra que je trouve une autre tournure de phrase.

Via [Antoine Fauchié](https://www.quaternum.net/2022/03/15/html-est-un-langage-de-programmation/).