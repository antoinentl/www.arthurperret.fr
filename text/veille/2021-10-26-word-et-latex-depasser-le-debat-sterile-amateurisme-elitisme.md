---
title: "Word et LaTeX : dépasser le débat stérile entre amateurisme et élitisme"
date: 2021-10-26
veille: true
---

<https://etnadji.fr/blog/latex-amateurisme.html>

Étienne Nadji réagit avec une exaspération compréhensible à une énième dispute entre partisans de Word et LaTeX. Il écrit notamment ceci :

> « Si vous tenez tant que ça à ce que des auteurs emploient les outils qui vous plaisent, penchez-vous un peu sur leur condition, leurs besoins, accompagnez-les, payez leur une formation, dégagez-leur du temps pour apprendre ce qui ne relève pas strictement de leur métier. »

Cette phrase résume bien la position que j'ai progressivement adoptée et que je défends maintenant. L'usage du traitement de texte ne va pas s'évaporer sous le coup de sarcasmes bien placés ; l'élitisme anti-Word est contre-productif. Surtout, les solutions alternatives reposant sur le format texte ont besoin d'infrastructures et d'une offre de formation qui sont encore balbutiantes. Malheureusement, on n'arrivera pas à élever le niveau du débat avant d'avoir avancé sur ces points.

