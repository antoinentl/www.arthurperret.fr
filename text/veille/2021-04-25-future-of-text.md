---
title: Future of Text
date: 2021-04-25
veille: true
---

<https://futuretextpublishing.com/future-of-text-2020-download/>

Frode Hegland est un chercheur et entrepreneur dans le domaine du texte informatisé. C'est lui qui est derrière l'un des meilleurs utilitaires pour la recherche d'information sur macOS : [Liquid](https://www.augmentedtext.info/liquid). Il pilote également le développement de plusieurs logiciels d'écriture et de lecture aux fonctionnalités insolites.

Hegland a dirigé un ouvrage collectif paru fin 2020, *The Future of Text*. C'est une anthologie de très courts essais (de 1 à 4 pages) sur l'avenir de la textualité, une sorte de grande foire à l'anticipation avec des projections utopiques et dystopiques et de la rétro-prospective. On y trouve de tout et surtout du beau monde : je suis directement allé lire les contributions d'Andy Matuschak, Elaine Treharne, Johanna Drucker, Keith Houston, Matthew Kirschenbaum, Ted Nelson, Vint Cerf.

Le livre est disponible gratuitement aux formats PDF et EPUB. Voici un petit échantillon extrait de la contribution de Keith Houston, qui illustre assez bien le ton du livre :

> « Finally, and as absurd as it sounds, is the prospect of emoji censorship. From 2016 to 2019, for example, Samsung devices did not display the Latin cross (✝️) or the star and crescent (☪️). These omissions had mundane technical explanations, but it is not difficult to imagine more sinister motives for suppressing such culturally significant symbols. In fact, one need not look far to find a genuinely troubling case. Starting in 2017, Apple modified its iOS software at China’s behest so that devices sold on mainland China would not display the Taiwanese flag emoji. At the time of writing, as protests against Chinese rule rock Hong Kong, ‘🇹🇼’ has disappeared from onscreen keyboards there, too. In this there are echoes of Amazon’s surreptitious deletion of George Orwell’s 1984 from some users’ Kindles because of a copyright dispute. A missing emoji might seem like small fry by comparison, but it is every bit as Orwellian. »