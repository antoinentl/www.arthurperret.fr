---
title: Digital scholarship workflows
date: 2021-03-30
veille: true
---

<https://tom.medak.click/en/workflows/>

Une ressource exceptionnelle. Je découvre enfin une solution pour concilier Pandoc Markdown et collaboration éditoriale via Word/LibreOffice. Lecture chaudement conseillée.

Via [Plain Text Project](https://plaintextproject.online/articles/2021/03/30/links.html).