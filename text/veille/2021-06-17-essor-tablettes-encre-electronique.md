---
title: L’essor des tablettes à encre électronique
date: 2021-06-17
veille: true
---

<https://www.hanselman.com/blog/the-quiet-rise-of-e-ink-tablets-and-infinite-paper-note-takers-remarkable-2-vs-onyx-boox-note-air>

On connaît généralement l'[encre électronique](https://fr.wikipedia.org/wiki/Papier_électronique) à travers les liseuses. Depuis quelques années, des fabricants sont parvenus à proposer des tablettes permettant non seulement de lire mais aussi d'écrire, avec une expérience proche de celle du papier. Des produits de niche, parfaitement adaptés à certains usages bien spécifiques (annoter des PDF, faire des diagrammes), encore pas mal freinés selon moi par leurs infrastructures de synchronisation. Ce billet fait un comparatif entre les gammes reMarkable et Onyx Boox, les meilleures propositions actuelles.

