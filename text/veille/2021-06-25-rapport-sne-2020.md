---
title: Rapport du SNE 2020-2021
date: 2021-06-25
veille: true
---

<https://www.sne.fr/publications-du-sne/ledition-en-perspective-2020-2021/>

Le rapport d'activité annuel du Syndicat national de l'édition vient de paraître. C'est l'heure de mettre à jour les supports de cours en édition… Les points saillants mis en avant sont la relative résilience du marché et la disparité des segments éditoriaux face à la pandémie en 2020. Une année qui risque d'être difficile à analyser.

