---
title: Chérie, j'ai rétréci le Web
date: 2021-04-18
veille: true
---

<https://neustadt.fr/essays/the-small-web/>

Je découvre cet essai un an après sa parution. Il est question du « *small Web* », ce que d'autres appellent le web indépendant (web indé, *indie Web*) : plus petit, moins visible, plus artisanal, moins léché, plus étonnant. L'effet « JT de 13h avec Jean-Pierre Pernaut » n'est pas loin. Mais je dois avouer qu'en lisant le passage où l'auteur explique qu'il écrit son flux RSS à la main, je me suis senti profondément légitimé dans ma propre démarche.

Via « [Les passages secrets du web](https://serveur410.com/les-passages-secrets-du-web/) » [dans la veille d'Antoine](https://www.quaternum.net/2020/11/16/les-dessous-du-web/).

