---
title: Bibliographies réutilisables
date: 2021-01-04
veille: true
---

<https://zotero.hypotheses.org/3503>

Du fichier de données joint en annexe jusqu'à l'intégration d'une biblio interactive sur une page web, beaucoup de solutions. Le blog Zotero francophone livre encore une fois une ressource épatante.

