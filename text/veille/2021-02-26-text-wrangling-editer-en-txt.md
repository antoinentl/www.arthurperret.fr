---
title: Text wrangling, éditer en txt
date: 2021-02-26
veille: true
---

<https://storyneedle.com/the-lazy-persons-guide-to-text-wrangling/>

Une liste très complète des opérations d'édition au format texte, et des fonctionnalités des éditeurs de texte qui sont les plus utiles pour les réaliser.

Via [Plain Text Project](https://plaintextproject.online/articles/2021/02/25/links.html).

