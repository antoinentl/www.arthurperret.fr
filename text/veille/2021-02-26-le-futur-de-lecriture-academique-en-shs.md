---
title: Le futur de l'écriture académique en SHS
date: 2021-02-26
veille: true
---

<https://twitter.com/arthurperret/status/1365226371560144898>

Suite de la [restitution des travaux menés dans le cadre d'OPERAS-P](https://operas.hypotheses.org/4512). J'ai twitté quelques réflexions à chaud sur ce workshop, celui que j'attendais avec le plus d'impatience.

