---
title: Pourquoi fabriquer son propre générateur de site statique ?
date: 2020-12-03
veille: true
---

<https://erikwinter.nl/articles/2020/why-i-built-my-own-shitty-static-site-generator/>

Pour éviter dispersion, dépendances et contraintes génériques. Super billet réflexif et critique qui mentionne Hugo et AsciiDoc.

Via [Etienne Nadji](https://etnadji.fr/pagxoj/12de72e4-597a-4cc2-b9ba-ff5350501f94.html) et [Hacker News](https://news.ycombinator.com/item?id=25227181).