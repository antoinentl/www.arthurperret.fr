---
title: Markdown et vous
date: 2022-01-05
veille: true
---

<https://infolit.be/md/>

[Bernard Pochet](https://infolit.be/wordpress/a-propos/lauteur) publie un cours en ligne sur l'écriture académique avec Markdown et Pandoc, c'est-à-dire sur l'usage du format texte pour rédiger des contenus scientifiques et éventuellement les publier dans plusieurs formats à partir d'une seule source. C'est l'une des rares ressources en français de ce type. Le cours s'adresse plutôt aux personnes qui débutent, ainsi qu'à celles qui les accompagnent.

À noter, l'introduction du cours est résolument politique puisqu'elle mentionne la science ouverte, le logiciel libre et l'écologie numérique. Cette façon d'entrer dans le sujet me plaît beaucoup.
