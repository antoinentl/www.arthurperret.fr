---
title: "Tree-query : prémices d’une évolution ?"
date: 2021-07-24
veille: true
---

<https://github.com/CrazyPython/tree-query>

Tree-query est un moteur de recherche pour fichiers texte, inspiré par le fonctionnement de Roam Research (fichiers Markdown interreliés, hiérarchie, requêtes).

Un utilisateur de Roam, Tom Bielecki, livre [une réaction](https://twitter.com/tombielecki/status/1418695151334039553) qui mérite d'être signalée. Selon lui, Tree-query illustre le fait que les nouveaux « outils pour penser » (*tools for thought*) comme Roam et Obsidian sont en train d'inspirer petit à petit la création d'utilitaires tiers qui reproduisent leurs idées sous forme de logiciels libres et modulaires. C'est très positif : j'y vois la possibilité de nous émanciper de ces jardins fermés façon « votre deuxième cerveau clé en main ».

Robert Haisfield, co-créateur de Tree-query, ajoute [une réflexion](https://twitter.com/RobertHaisfield/status/1418699468002660353) tout aussi importante. Il mentionne le fait qu'Obsidian, concurrent de Roam, doit sa popularité à un riche écosystème d'extensions, et suggère que ces extensions pourraient très bien devenir des utilitaires autonomes. Cela requiert toutefois une phase intermédiaire. En effet, si Obsidian possède autant d'extensions intéressantes, c'est qu'il met à disposition des développeurs une véritable mine d'or : des caches de données indexées à partir des contenus qui sont créés dans le logiciel. Les contributeurs n'ont plus qu'à moissonner ces index pour inventer toutes sortes de fonctionnalités. Il me semble donc que le prochain moment crucial dans ce champ des *tools for thought* pourrait bien être l'apparition d'outils libres et ouverts capables de générer ce genre d'index. Ce serait en tout cas un moyen de résorber la difficulté à innover que rencontre ce champ, difficulté bien établie par Andy Matuschak comme je l'avais signalé [précédemment ici-même](https://www.arthurperret.fr/veille/difficile-progression-tools-for-thought.html).

