---
title: Ordinateurs et créativité
date: 2021-07-04
veille: true
---

<https://www.mollymielke.com/cc>

Je partage cet article pour son design éditorial autant que pour son contenu textuel. C'est l'accumulation de petits détails qui m'a fait chavirer : le lien vers une version paginée ; le code couleur pour les titres de section, mais seulement pour les plus importantes ; l'alternance de couleur de fond, mais uniquement pour souligner le passage à ces mêmes sections ; les liens qui savent rester discrets jusqu'à ce qu'on les survole ; les petites notes et figures de marge ; l'encart avec des onglets ; les petites touches magazine, comme les extraits mis en exergue – j'arrête là, vous avez saisi l'idée.

L'auteur réfléchit aux principes d'un design d'interaction humain-machine plus profitable aux professionnels de la création (comprendre : artistes et designers). Elle fait trois préconisations : des formats standards, la possibilité de personnaliser profondément les outils et la mise en œuvre de logiques issues de la programmation (classe/instance, boucles logiques, etc.). Quand on est plongé tous les jours dans le format texte comme moi, on ne sera pas vraiment étonné de la voir aboutir à ces trois éléments. Mais cela correspond à la réalité des outils de design : Figma, qui est mentionné dans l'article, ne fait que gratter la surface de ce qui est possible. Dommage que l'auteur ne commente pas les logiques industrielles en jeu : de l'extérieur, j'ai l'impression qu'Adobe verrouille pas mal les choses et j'aurais aimé lire une analyse mieux informée de la situation sur ce point.
