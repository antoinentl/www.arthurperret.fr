---
title: Gemini et la typographie
date: 2021-02-10
veille: true
---

<https://etnadji.fr/pagxoj/f299e660-de5f-48db-97af-0702eb8349b7.html>

Le paradoxe de Gemini (alternative à HTTP) est de confier les clés de la bagnole au lecteur après avoir soigneusement démonté le moteur. Ce n'est plus du design par soustraction mais par le vide… Raboter à ce point les possibilités de mettre en forme le texte dans un système presque entièrement textuel est un choix étrange, défendu comme un « enrichissement typographique » par rapport à un format précédent (Gopher). Les choix de conception détaillés dans [la spécification de Gemini](https://gemini.circumlunar.space/docs/specification.gmi) semblent influencés par les interfaces familières aux développeurs (ex : choix de la ligne comme unité) et montrent un rapport singulier au texte, pas seulement à la typographie.

Je reste sceptique face à cette initiative, qui me fait penser (à tort ou à raison) à un projet développé par des informaticiens pour des informaticiens. Vision étroite des désirs d'écriture, de l'expérience de lecture, et hostilité inquiétante à la culture typographique.

