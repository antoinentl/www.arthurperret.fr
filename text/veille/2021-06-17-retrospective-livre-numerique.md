---
title: Rétrospective sur le livre numérique
date: 2021-06-17
veille: true
---

<https://actualitte.com/dossier/189/le-livre-numerique-fete-ses-50-ans-un-anniversaire-tout-en-histoire>

Actualitté publie un dossier sur le livre numérique par Marie Lebert. Le terme de livre numérique est assez fuyant ; presque chaque colloque sur le sujet commence ou finit par une tentative avortée de le définir (voire le redéfinir). Le dossier restitue bien certaines raisons de cette difficulté, notamment parce que le point de départ choisi (le PDF) n'appartient pas du tout à un passé révolu, ou parce que les usages résistent au cadrage a priori.

