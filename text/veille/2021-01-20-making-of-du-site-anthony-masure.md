---
title: Making-of du site d'Anthony Masure
date: 2021-01-20
veille: true
---

<https://www.anthonymasure.com/blog/2021-01-12-website-redesign>

C'est quoi votre genre littéraire préféré ? Moi, c'est les coulisses de fabrication de sites personnels académiques. J'en ai une pleine collection Zotero, que je vais m'empresser d'augmenter avec le billet que voici.

