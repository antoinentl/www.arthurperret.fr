---
title: What's in a name?
date: 2021-03-11
veille: true
---

<http://danah.org/name.html>

Se réapproprier la valeur du nom, une métadonnée parmi d'autres, en utilisant la typographie. Un joli exemple de réflexivité.

Via [Julien Taquet](https://twitter.com/John_Tax/).

