---
title: Les perspectives vertigineuses de l’open access
date: 2021-12-05
veille: true
---

<http://musingsaboutlibrarianship.blogspot.com/2021/11/some-more-reflections-for-oa-week-2021.html>

Aaron Tay évoque quelques perspectives possibles du mouvement pour l'accès libre aux publications académiques. Il constate que les outils et services marquants de ces dernières années (parfois même de ces derniers mois) mettent tous en lumière l'utilité d'une ouverture et d'une centralisation croissante du texte intégral ainsi que des métadonnées des publications. L'exemple éphémère du *Microsoft Academic Graph* montre en particulier que la fouille de données automatisée à grande échelle est capable de nous rapprocher des ambitions autrefois inatteignables de Paul Otlet, à savoir un Répertoire bibliographique universel (RBU) exhaustif et adossé à un vocabulaire contrôlé lui-même universel.

