---
title: "Prise de notes : pré-requis et solutions"
date: 2021-06-17
veille: true
---

<http://blog.dornea.nu/2021/06/13/note-taking-in-2021/>

Dans l'océan des billets de blog consacrés à la prise de notes, celui-ci a retenu mon attention pour deux aspects. D'abord, il reflète la diversité des solutions pour capturer des idées, aussi bien sur le papier que via une interface numérique ; je ne crois pas à l'outil unique qui fait tout, donc cette représentation est la bienvenue. Et ensuite, l'auteur donne la liste des pré-requis pour qu'une solution lui convienne personnellement ; un bon rappel de l'importance de formaliser ses besoins en un cahier des charges.

