---
title: The Meme Hustler
date: 2021-03-23
veille: true
---

<https://thebaffler.com/salvos/the-meme-hustler>

Un portrait de Tim O'Reilly par Evgeny Morozov tissé avec un essai magistral sur l'histoire des expressions “open source”, “Web 2.0”, “état-plateforme”, etc. Une heure de lecture qui laisse intellectuellement bouleversé (en bien).

Via [Hubert Guillaud](https://www.internetactu.net/2021/02/17/regouverner-1-2-la-nouvelle-ere-des-licences-libres/).