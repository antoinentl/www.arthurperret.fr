---
title: Le libre, une idée obsolète ?
date: 2021-03-25
veille: true
---

<https://r0ml.medium.com/free-software-an-idea-whose-time-has-passed-6570c1d8218a>

Plutôt sceptique vis-à-vis de cette rhétorique, fragilisée par le recours au sophisme et la tentation de la propagande (voir les réactions sur la page et [sur Hacker News](https://news.ycombinator.com/item?id=26573425)). Je reste marqué par [la lecture récente de Morozov](https://thebaffler.com/salvos/the-meme-hustler).

Via [Alexis Kauffmann](https://twitter.com/framaka/status/1375078648911396868).