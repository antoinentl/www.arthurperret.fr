---
title: Débugue tes humanités
date: 2022-01-14
veille: true
---

<https://debugue.ecrituresnumeriques.ca>

La [Chaire de recherche du Canada sur les écritures numériques](http://ecrituresnumeriques.ca) propose un cours éponyme. C'est en ligne, gratuit et ouvert à tous. En deux fois dix séances d'une heure et demie, le cours constitue un tour d'horizon des outils numériques qui servent aux activités d'écriture et d'édition, avec une emphase sur le logiciel libre et le format texte. Les leçons partent du tout début (qu'est-ce que l'informatique ?) pour arriver aux usages les plus avancés dans différents domaines (rédaction, gestion, automatisation, publication). Un très gros travail didactique, partagé librement (en licence CC BY-SA). Chapeau !