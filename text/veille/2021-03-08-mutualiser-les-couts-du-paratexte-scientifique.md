---
title: Mutualiser les coûts du paratexte scientifique
date: 2021-03-08
veille: true
---

<https://scholarlykitchen.sspnet.org/2021/03/08/can-we-re-engineer-scholarly-journal-publishing-an-interview-with-richard-wynne-rescognito/>

Utiliser des registres (ex : ORCID) pour faciliter l'ingénierie de la reconnaissance et diminuer ainsi le coût de production du paratexte (métadonnées + pagination), principale valeur ajoutée de l'édition scientifique.

Toutefois si on a vu passer [les chiffres de RELX](https://twitter.com/bedform/status/1368201881777041412) (société mère d'Elsevier) parus récemment (bénéfice 1,9 milliards £, marge opérationnelle 29%), on a un peu de mal à accepter que le problème soit posé entièrement en termes de coût pour l'éditeur.

