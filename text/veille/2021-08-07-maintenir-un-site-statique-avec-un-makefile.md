---
title: Maintenir un site statique avec un Makefile
date: 2021-08-07
veille: true
---

<https://si3t.ch/log/2021-01-27.html>

Un retour d'expérience sur l'utilisation de Make pour créer un site web, rédigé en français, ce qui est suffisamment rare pour mériter un signalement. Make est extrêmement intéressant de par sa simplicité et son interopérabilité. C'est une bonne alternative aux générateurs de site statique (comme Jekyll, Hugo et Eleventy) lorsqu'on a des besoins particuliers pour lesquels il est plus efficace d'assembler quelques outils *ad hoc* via un script.

Via [Antoine Fauchié](https://www.quaternum.net/2021/08/06/fabriques-de-publication-hugo/).

