---
title: PanWriter
date: 2020-12-17
veille: true
---

<https://panwriter.com>

Un éditeur Markdown gratuit, open source et multiplateforme basé sur Pandoc, CSS et Paged.js. Super interface d'écriture (minimaliste, défilement synchronisé) et de conversion (glisser-déposer, presse-papiers, raccourcis…). Bluffant ! Gros coup de cœur pour ce logiciel. L'interface rappelle iA Writer. Pouvoir créer du PDF via LaTeX de façon classique mais aussi via CSS avec PagedJS plaira à beaucoup d'experts de la publication. Ça peut aussi satisfaire ceux qui souhaitent une prévisualisation avec pagination pendant l'écriture.

Comme de nombreux éditeurs de texte, PanWriter fonctionne à merveille avec une installation Zotero + [Better BibTeX](https://retorque.re/zotero-better-bibtex/citing/cayw/) + [Zotpick](https://github.com/davepwsmith/zotpick-applescript) pour faciliter l'insertion des références bibliographiques dans un fichier Markdown.


