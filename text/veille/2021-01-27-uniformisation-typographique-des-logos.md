---
title: Uniformisation typographique des logos
date: 2021-01-27
veille: true
---

<https://velvetshark.com/articles/why-do-brands-change-their-logos-and-look-like-everyone-else>

« Pourquoi tant de marques changent leur logo pour ressembler à toutes les autres ? » Haro sur la *sans-serif*-isation ! Analyse mordante et détaillée d'une tendance marketing typographique à la noix (certains logos comme celui d'Yves Saint-Laurent étaient très beaux). 

