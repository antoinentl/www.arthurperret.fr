---
title: Sans contexte, un rétrolien ne sert à rien
date: 2022-05-08
veille: true
---

<https://zettelkasten.de/posts/backlinks-are-bad-links/>

Si vous êtes sur le Web, vous savez ce qu’est un lien. Vous savez peut-être aussi que sur le Web, ces liens sont à sens unique mais que dans les premières visions de l’hypertexte, des gens comme Ted Nelson avaient imaginé des liens à double sens, avec lien et rétrolien (*backlink*). Vous avez sans doute déjà vu des rétroliens sur le Web, par exemple un *pingback* dans les commentaires d’un blog (« Ce billet a été mentionné à telle URL »), mais ils sont rares et ne se différencient pas des liens normaux en matière de fonctionnalités.

Dans « Backlinking Is Not Very Useful – Often Even Harmful », Sascha Fast critique les rétroliens simples, dénués de contexte : ils ne font que créer du bruit, ce qui les rend potentiellement contre-productifs dans une démarche de gestion des connaissances. Il leur préfère des rétroliens contextualisés, c’est-à-dire présentés avec leur contexte d’énonciation (*link context*), généralement une phrase ou un paragraphe.

C’est évidemment une idée que je partage, et qui est centrale dans Cosma. Attention toutefois à ne pas aller trop vite en besogne et jeter le bébé (rétrolien) avec l’eau du bain (absence de contexte de lien). Le permalien du billet n’est pas génial de ce point de vue.

Comme souvent sur Zettelkasten.de, la discussion sous le billet est tout aussi intéressante. [Un commentateur](https://forum.zettelkasten.de/discussion/comment/8894/#Comment_8894) fait notamment le rapprochement entre la contextualisation des liens en général et des techniques plus formelles de qualification sémantique, en prenant comme exemple les citations dans la syntaxe de Pandoc.

Via [Baldur Bjarnason](https://www.baldurbjarnason.com/2022/why-people-use-markdown/).

