---
title: Sources pour une veille sur la culture numérique
date: 2021-11-17
veille: true
---

<https://louisderrac.com/2021/11/15/sources-pour-une-veille-cultures-numeriques-version-2021/>

Voici une liste de sources sur la culture numérique, classée et présentée de façon pertinente, puisqu'on retrouve à la fois le type de source et la fréquence de publication. Il ne manque qu'un fichier OPML pour ceux qui voudraient importer tout ça en bloc. Tout ça mériterait un petit billet sur la sitographie annotée au format texte… Après la thèse !

Via [Karl Pineau](https://twitter.com/KarlPineau/status/1460921585376935936).