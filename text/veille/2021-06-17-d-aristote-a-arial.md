---
title: "D'Aristote à Arial : introduction à la classification typographique"
date: 2021-06-17
veille: true
---

<https://ilovetypography.com/2021/05/31/talking-about-fonts/>

Après l'[acte de décès de la Vox-ATypI](https://www.arthurperret.fr/veille/au-revoir-vox-atypi.html), John Boardley fait le point sur la classification des polices de caractères : d'où ça vient, à quoi ça sert, pourquoi c'est critiqué, quels sont les enjeux d'une nouvelle classification et comment procéder. Il y a des critiques encore plus radicales qui ne sont pas mentionnées, comme [celle d'Émilie Rigaud](https://aisforfonts.com/the-absurdity-of-typographic-taxon). Mais je trouve que ce billet est une belle introduction au problème.

