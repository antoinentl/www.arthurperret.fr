---
title: Défendre XML
date: 2021-04-16
veille: true
---

<https://blog.frankel.ch/defense-xml/>

Un article sur XML qui sort du lot, renvoyant dos-à-dos XML d'un côté et JSON et YAML de l'autre. Une analyse sans concession des défauts des trois langages, et une critique de la logique de *hype* – mélange d'excitation collective et d'éléments de langage – chez les développeurs.
