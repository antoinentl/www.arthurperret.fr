---
title: To markdown or not to markdown
date: 2022-05-08
veille: true
---

<https://www.baldurbjarnason.com/2022/why-people-use-markdown/>

Baldur Bjarnason, en plein travail de conception sur sur son projet Colophon Cards, partage quelques réflexions sur les interfaces d’écriture. Il s’interroge sur le choix du mode d’édition : texte enrichi avec interface WYSIWYG ou format texte avec balisage léger ?

Deux choses attirent mon attention. D’abord, dans l’expression que j’ai reprise en titre de cette note de veille, il y a ce « markdown » sans majuscule, utilisé comme verbe (*to markdown*). J’avais repéré et commenté cet usage [dans un billet fin 2020](https://www.arthurperret.fr/blog/2020-10-25-histoire-typographique-legerete.html#markdown-markdown). Et ensuite, il y a cette phrase lancée par Bjarnason à propos des retours émis par son *focus group* d’experts :

> « *If expert users aren’t using markdown for markdown’s sake, what are the chances that a non-expert markdown user is motivated to use it out of genuine interest in a markup format?* »

Ces deux choses traduisent une même idée : le déplacement de la focale depuis Markdown (une technique particulière) vers le format texte (une méta-technique).

Via [José Afonso Furtado](https://mobile.twitter.com/jafurtado/status/1522969991947063296).