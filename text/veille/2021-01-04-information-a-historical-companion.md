---
title: "Information: A Historical Companion"
date: 2021-01-04
veille: true
---

<https://press.princeton.edu/books/hardcover/9780691179544/information>

Une dizaine de chapitres et une centaine d'entrées de glossaire pour replacer la notion d'information dans le temps long. À feuilleter côte-à-côte avec l'Encyclopedia of Library and Information Science.


