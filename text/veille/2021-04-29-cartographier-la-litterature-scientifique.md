---
title: Cartographier la littérature scientifique
date: 2021-04-29
veille: true
---

<https://musingsaboutlibrarianship.blogspot.com/p/list-of-innovative-literature-mapping.html>

Un comparatif des outils permettant de cartographier la littérature scientifique : Connected Papers, Litmaps, Scite, Inciteful… La liste ne va faire que s'allonger, car le duopole des bases de données bibliographiques Web of Science et Scopus touche à sa fin. On peut également lire un [billet du même auteur sur Medium](https://aarontay.medium.com/3-new-tools-to-try-for-literature-mapping-connected-papers-inciteful-and-litmaps-a399f27622a) sur les outils les plus récents et les plus prometteurs dans cette liste.

