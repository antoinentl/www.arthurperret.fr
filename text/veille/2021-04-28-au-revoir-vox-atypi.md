---
title: Au revoir Vox-ATypI
date: 2021-04-28
veille: true
---

<https://www.atypi.org/atypi-de-adopts-vox-atypi-typeface-classification>

L'Association Typographique Internationale (ATypI) a décidé d'entériner le caractère obsolète de la classification inventée par Maximilien Vox dans les années 1950. L'argument est présenté en termes d'inclusivité (la Vox-ATypI est trop centrée sur les graphies européennes) mais les connaisseurs savent que ce n'est pas la seule limite de ce système. Mon texte « [Sémiotique des fluides](https://www.arthurperret.fr/semiotique-des-fluides.html) » consacre quelques paragraphes à la question ; je mentionne quelques classifications alternatives en renvoyant aux écrits de personnes plus expertes que moi (comme [Émilie Rigaud](https://aisforfonts.com/the-absurdity-of-typographic-taxon)).

