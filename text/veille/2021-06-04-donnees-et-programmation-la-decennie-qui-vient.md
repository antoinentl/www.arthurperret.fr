---
title: "Données et programmation : la décennie qui vient"
date: 2021-06-04
veille: true
---

<http://benschmidt.org/post/2020-01-15/2020-01-15-webgpu/>

Ben Schmidt prédit que R et Python vont refluer au profit de JavaScript, notamment parce que le paradigme du *notebook* a modifié nos attentes en matière d'interactivité dans les phases exploratoires du travail sur les données. C'est long, dense et technique, mais si vous vous intéressez à la façon dont les langages de programmation se succèdent les uns aux autres, et à l'expansion du domaine du Web, c'est une lecture recommandée.

Via [Mathieu Jacomy](https://twitter.com/jacomyma).