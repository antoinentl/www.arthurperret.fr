---
title: "Le non-livre, ou l'indifférence vis-à-vis de la chose mal faite"
date: 2021-08-28
veille: true
---

<https://album50.hypotheses.org/4625>

Cécile Boulaire, spécialiste de la littérature jeunesse, livre dans ce billet le portrait d'un non-livre, ou livre non édité. De la couverture aux plus petits détails typographiques, c'est une critique décapante de pratiques qu'on peine à qualifier d'éditoriales tant on est éloigné de ce « goût de la chose bien faite » [dont on parlait récemment](https://www.arthurperret.fr/veille/le-gout-de-la-chose-bien-faite.html).

