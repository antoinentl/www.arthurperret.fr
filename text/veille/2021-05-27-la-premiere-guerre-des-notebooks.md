---
title: La première guerre des notebooks
date: 2021-05-27
veille: true
---

<https://yihui.org/en/2018/09/notebook-war/>

Ce récit d'une controverse à propos des *notebooks* Jupyter et R Markdown se révèle doublement instructif, puisqu'il contient aussi une réflexion très utile sur les différences entre deux cultures techniques : l'analyse de données (R) et l'ingénierie logicielle (Python).

Via Sébastien Rey-Coyrehourcq dans [le pad de l'Atelier HN Lab](https://demo.hedgedoc.org/s/kTsJrdJLj). Ce pad est une mine d'or et va probablement devenir au moins un billet de blog, qui sera illico signalé ici.