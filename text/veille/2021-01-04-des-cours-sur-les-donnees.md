---
title: Des cours sur les données
date: 2021-01-04
veille: true
---

<http://lespetitescases.net/des-supports-pour-former-a-la-question-de-la-donnee>

Encore une ressource géniale de début d'année : Gautier Poupeau ouvre (CC-BY) tous ses supports de cours, fruits de six ans de travail de formation. Merci à lui !



