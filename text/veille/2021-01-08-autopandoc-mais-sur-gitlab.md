---
title: Autopandoc, mais sur GitLab
date: 2021-01-08
veille: true
---

<https://forgemia.inra.fr/dimitri.szabo/autopandoc-gitlab>

Mon [autopandoc](https://github.com/infologie/autopandoc) montre comment utiliser les services de GitHub pour déclencher automatiquement des conversions avec Pandoc sur les fichiers du dépôt. Dimitri Szabo de l'INRA en a fait une version GitLab.

