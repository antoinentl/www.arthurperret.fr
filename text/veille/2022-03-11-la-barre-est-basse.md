---
title: La barre est basse
date: 2022-03-11
veille: true
---

<https://susam.net/maze/wall/comfort-of-bloated-web.html>

Susam Pal a un site sur lequel certaines interactions sont tellement rapides que les internautes refusent de croire qu'elles fonctionnent vraiment. Dans les commentaires, on lit que la question est sûrement un peu plus complexe, peut-être que la rapidité n'est pas seule en cause mais qu'un manque de retour (*feedback*) trouble aussi les visiteurs du site. Le débat est très intéressant. Quoiqu'il en soit, Susam Pal aborde dans son texte un principe de design qui m'a sidéré, et qui consiste à ralentir volontairement un logiciel pour coller aux attentes d'utilisateurs habitués à des performances médiocres. Elle en tire cette réflexion désabusée sur l'état du génie logiciel en 2022 :

> ^[{-} *“We are trained into accepting crappy software, and the most common examples are, of course, bloated websites.”*]« On nous conditionne à accepter des logiciels médiocres, et l'exemple le plus courant de cela, c'est la tendance aux sites web boursouflés. »

Pour en savoir plus, je vous recommande un article de magazine au titre racoleur mais au contenu sérieux, [“The UX Secret That Will Ruin Apps For You”](https://www.fastcompany.com/3061519/the-ux-secret-that-will-ruin-apps-for-you), car basé sur un article scientifique que certains iront peut-être lire directement :
[“The Labor Illusion: How Operational Transparency Increases Perceived Value”](https://www.hbs.edu/faculty/Pages/item.aspx?num=40158) 

