---
title: Écriture exécutable au HN Lab
date: 2021-06-27
veille: true
---

<https://hnlab.huma-num.fr/blog/2021/05/27/rencontres-hn-2021-la-session-hnlab/>

Le HN Lab, nouvelle équipe de recherche et développement au sein d'Huma-Num, alimente son tout nouveau blog avec des ressources liées à l'atelier « Écriture exécutable » qui s'est tenu en mai dernier. Le billet contient désormais les présentations des invités, ainsi qu'un bloc-notes collaboratif débordant de liens utiles.

La discussion fut particulièrement riche. J'en suis ressorti avec un intérêt renouvelé pour la notion de milieu (merci à Nicolas Sauret), ainsi qu'avec l'envie de réfléchir à une traduction française du concept anglo-saxon de *notebook*, ce qui a débouché sur mon billet [« Du notebook au bloc-code »](https://www.arthurperret.fr/du-notebook-au-bloc-code.html).

