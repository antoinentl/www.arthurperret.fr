---
title: Un moteur de découverte de flux RSS
date: 2021-10-25
veille: true
---

<https://github.com/quakkels/rssdiscoveryengine>

Dans [« *RSS is Wonderful* »](https://quakkels.com/posts/rss-is-wonderful/), Brandon Quakkelaar défend magnifiquement les flux RSS/Atom pour le partage d'idées, le suivi de l'actualité et la découverte de nouvelles sources d'information. Il nous offre un outil dont le concept est très simple, encore fallait-il y penser et le réaliser : un *crawler*… mais de flux ! C'est le *RSS Discovery Engine* : entrez l'URL d'un site ; s'il a un flux, alors le moteur récupère les liens inclus dans le flux ; pour chaque lien, le moteur examine le site et vérifie l'existence d'un flux ; s'il en trouve un, il répète l'opération, et ainsi de suite. Le résultat est merveilleux.

Ces derniers mois, j'ai lu plusieurs articles qui évoquent le Web des années 1990, parlant avec nostalgie de diversité, créativité, sérendipité. Évidemment, ces choses n'ont pas disparu, mais le Web des plateformes a quand même asséché une grande partie du cyberespace. Parfois, les auteurs proposent des formes d'exploration « à l'ancienne » pour retrouver cette vitalité des premières années. Je n'ai jamais vraiment réussi à accrocher à ces propositions, peut-être parce que je n'ai pas connu l'époque originelle. Le *RSS Discovery Engine* est le premier outil que je découvre qui pourrait jouer ce rôle et qui me parle vraiment, parce qu'il repose sur une technologie que j'utilise tous les jours.

Via [Étienne Nadji](https://etnadji.fr/pagxoj/ad91c835-6e8c-4ffd-8a8b-bb369abc1484.html).

