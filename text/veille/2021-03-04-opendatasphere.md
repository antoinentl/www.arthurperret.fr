---
title: OpenDataSphère
date: 2021-03-04
veille: true
---

<https://lpmind2021.medium.com/opendatasphèreatasph%C3%A8re-un-projet-collaboratif-autour-de-lopen-data-francophone-2359c5c445be>

Très fier des étudiants de la LP MIND 2020-2021 (IUT Bordeaux Montaigne) qui ont su proposer une vraie réappropriation de l'[Otletosphère](https://hyperotlet.huma-num.fr/otletosphere/) sur la thématique de l'open data : l'[OpenDataSphère](https://hyperotlet.huma-num.fr/opendatasphere/).

