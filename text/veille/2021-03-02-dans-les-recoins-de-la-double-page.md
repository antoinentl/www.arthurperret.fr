---
title: Dans les recoins de la double page
date: 2021-03-02
veille: true
---

<https://polylogue.org/apres-la-page-la-double-page/>

C'est brillant… J'ai l'impression d'assister pour le web à ce qui s'est passé entre 1970 et 1990 pour l'imprimé avec TeX puis LaTeX. Nicolas s'inscrit dans l'héritage mallarméen du *Coup de dés*, qui avait inspiré Anne-Marie Christin dans son élaboration du concept de « pensée de l'écran ».