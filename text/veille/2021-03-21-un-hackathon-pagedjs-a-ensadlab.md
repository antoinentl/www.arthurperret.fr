---
title: Un hackathon Paged.js à EnsadLab
date: 2021-03-21
veille: true
---

<https://www.pagedjs.org/posts/2021-03-hackathon/>

Compte-rendu très clair et richement illustré. Le script de Louis Eveillard me fait penser à [PanWriter](https://panwriter.com) : bientôt d'autres éditeurs équipés de Paged.js ?

Via [Julie Blanc](https://twitter.com/julieblancfr/status/1374789669670559746).