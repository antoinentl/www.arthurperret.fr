---
title: Temporalité et texte brut
date: 2021-01-07
veille: true
---

<https://plaintextproject.online/articles/2021/01/05/schedule.html>

J'ai lu ce billet du Plain Text Project qui examine le remplacement du calendrier par des fichiers texte. Incompréhensible : on perd alertes, vue globale, synchronisation, facilité de manipulation et de navigation… Selon moi, le texte brut est nul pour planifier, en revanche il est excellent pour archiver. Exemple : le journal de bord, façon `log.txt`. Pensez non pas *to-do list* mais *done list*, pour reprendre [les mots de Jeff Huang](https://jeffhuang.com/productivity_text_file/).

