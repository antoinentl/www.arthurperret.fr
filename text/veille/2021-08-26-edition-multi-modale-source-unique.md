---
title: Édition multi-modale à partir d’une source unique
date: 2021-08-26
veille: true
---

<https://coko.foundation/single-source-publishing/>

Adam Hyde expose sa vision du *single-source publishing* en six parties : problème, solution, critique de l'automatisation, critique de la logique systémique, importance des processus et gestion de la simultanéité. Le tout s'appuie sur l'expérience accumulée au sein de sa fondation Coko. Une lecture indispensable pour qui s'intéresse au sujet.

