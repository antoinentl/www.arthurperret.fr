---
title: "Enfants du numérique, perdus en bureautique"
date: 2021-09-24
veille: true
---

<https://www.theverge.com/22684730/students-file-folder-directory-structure-education-gen-z>

On sait depuis un moment que les « natifs numériques » ne sont pas à l'aise avec tous les aspects de l'informatique, notamment parce que numérique ≠ informatique. Récemment, des enseignants réalisent que les apprenants ne connaissent pas le concept d'une structure hiérarchique de dossiers dans un ordinateur. Dans cet article, un passage m'a particulièrement intéressé :

> « More broadly, directory structure connotes physical placement — the idea that a file stored on a computer is located somewhere on that computer, in a specific and discrete location. That’s a concept that’s always felt obvious to Garland but seems completely alien to her students. “I tend to think an item lives in a particular folder. It lives in one place, and I have to go to that folder to find it,” Garland says. “They see it like one bucket, and everything’s in the bucket.” »

Je ne sais pas si c'est intentionnel, mais l'expression *everything bucket* existe réellement : elle désigne un système dans lequel on enregistre et on organise toutes les données au même endroit. Le concept a été simultanément inventé et critiqué par Alex Payne dans [« *The Case Against Everything Buckets* »](https://web.archive.org/web/20101025204731/http://al3x.net/2009/01/31/against-everything-buckets.html) (2009) ; il a été défendu de manière notable par David Karger dans [« *Why All Your Data Should Live in One Application* »](https://web.archive.org/web/20101023040611/http://groups.csail.mit.edu/haystack/blog/2010/10/20/why-all-your-data-should-live-in-one-application) (2010). Il y a donc de l'organisation des connaissances dans ce problème pédagogique, ainsi que de l'architecture de l'information.

