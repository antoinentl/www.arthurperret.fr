---
title: Zotero passe un cap
date: 2021-03-04
veille: true
---

<https://zotero.org/support/pdf_reader_preview>

Nouvelle beta Zotero avec lecteur PDF et refonte de l'annotation. Extraire des citations via le surlignement, une excellente fonctionnalité. Zotero est en train de franchir un cap.

