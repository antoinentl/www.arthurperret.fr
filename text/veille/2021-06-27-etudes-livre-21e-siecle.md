---
title: Études du livre au XXIe siècle
date: 2021-06-27
veille: true
---

<https://projets.ex-situ.info/etudesdulivre21/bilan/>

Voici le bilan d'un colloque sur les études du livre au XXIe siècle auquel j'ai participé en tant qu'observateur curieux. Le format, assez original, ne m'a convaincu qu'en partie mais surtout pour des raisons logistiques ; le bilan, lui, est extrêmement enthousiasmant. C'est un véritable programme de recherche.

