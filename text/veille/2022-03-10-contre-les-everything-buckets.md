---
title: Contre les everything buckets
date: 2022-03-10
veille: true
---

<https://www.al3x.net/blog/2009/01/31/the-case-against-everything-buckets>

Un *everything bucket* est un dispositif utilisé pour enregistrer et organiser n’importe quel type de données. L'idée est de mettre tout au même endroit, puis de tirer parti de certaines fonctionnalités comme les moteurs de recherche pour retrouver les choses.

Le concept a été proposé initialement par Alex Payne dans [“The Case Against Everything Buckets”](https://web.archive.org/web/20101025204731/http://al3x.net/2009/01/31/against-everything-buckets.html). Comme le titre l'indique, le nom péjoratif (« seau à tout mettre ») avait une visée critique. Le chercheur David Karger l'avait défendu dans [“Why All Your Data Should Live in One Application”](https://web.archive.org/web/20101023040611/http://groups.csail.mit.edu/haystack/blog/2010/10/20/why-all-your-data-should-live-in-one-application).

Ici, on retrouve l'orientation critique du début, une bonne dizaine d'années après les premières discussions. Ce qui est intéressant, c'est l'alternative discutée : la [philosophie d'Unix](https://fr.wikipedia.org/wiki/Philosophie_d'Unix), à savoir qu'un logiciel devrait faire une seule chose mais bien, et que l'*output* de l'un soit l'*input* de l'autre.

Via [Derek Sivers](https://sive.rs/plaintext#comment-4).