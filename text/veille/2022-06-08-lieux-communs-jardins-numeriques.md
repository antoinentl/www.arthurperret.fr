---
title: Des lieux communs aux jardins numériques
date: 2022-06-08
veille: true
---

<https://boffosocko.com/2021/07/03/differentiating-online-variations-of-the-commonplace-book-digital-gardens-wikis-zettlekasten-waste-books-florilegia-and-second-brains/>

Jardins numériques, wikis, Zettelkasten… Selon Chris Aldrich, toutes ces nouvelles formes de prise de notes s’inscrivent dans une histoire longue, qui remonte au moins aux livres de lieux communs – ce que confirme la littérature [@waquet2015; @bert2017; @blair2020]. Le problème des nouveaux outils, et j’y faisais allusion presque avec les mêmes mots [dans un billet récent](https://www.arthurperret.fr/blog/2022-05-20-et-toi-qu-est-ce-que-tu-fiches.html), c’est d’ignorer royalement cette histoire longue (Aldrich marque également un point pour sa juste critique de Vannevar Bush à ce propos) :

> « ^[{-} *Many of these products are selling themselves based on ideas or philosophies which sound and even feel solid, but they’re completely ignoring their predecessors to the tune of feeling like they’re trying to reinvent the wheel. As a result, some of the pitches for these products sound like they’re selling snake oil rather than tried and true methods that go back over 2,000 years of intellectual history […] Even Vannevar Bush’s dream of the Memex as stated in his article As We May Think (The Atlantic, 1945), which many of these note taking applications might point to as an inspiration, ignores this same tradition and background.*] Beaucoup de ces produits se vendent sur la base d'idées ou de philosophies qui semblent solides, mais ils ignorent complètement leurs prédécesseurs, au point de donner l'impression d'essayer de réinventer la roue. En conséquence, les discours qui promeuvent ces produits donnent l'impression de vendre de la poudre de perlimpinpin plutôt que des méthodes éprouvées qui remontent à plus de 2 000 ans d'histoire intellectuelle […] Même Vannevar Bush, avec sa vision du Memex, telle qu'énoncée dans son article As We May Think (The Atlantic, 1945), dont beaucoup de ces applications de prise de notes s'inspirent, ignore cette même tradition et ce même contexte. »

# Références