---
title: La difficile progression des “tools for thought”
date: 2021-06-09
veille: true
---

<https://www.patreon.com/posts/47976114>

L'appellation *tools for thought* recouvre un sous-domaine des outils de gestion de connaissance, souvent personnelle (*personal knowledge management*, PKM). Pensez [Roam Research](https://roamresearch.com), [Notion](https://www.notion.so), [Obsidian](https://obsidian.md)… Dans « Ratcheting progress in tools for thought », [Andy Matuschak](https://andymatuschak.org) regrette que le domaine progresse de manière encore trop éparpillée, notamment parce qu'il manque une conversation globale (ce qu'il appelle une « scène ») pour discuter les observations généralisables livrées par les projets – observations déjà rares à cause du manque de terrains concrets pour susciter ces observations dans les projets universitaires, et du manque de volonté du côté des entreprises, qui y voient une menace envers leur modèle économique.

Cet article était initialement réservé aux contributeurs du Patreon de Matuschak. Chacun doit subvenir à ses besoins comme il le peut, et le financement participatif permet à Matuschak de travailler comme chercheur indépendant quand l'université est hors d'atteinte[^1]. Mais si cela implique la réclusion des écrits derrière un *paywall* à barrière mobile, alors on a le pire des deux mondes : la précarité des chercheurs et le verrouillage de la connaissance. Le problème ici n'est pas seulement la jeunesse d'un domaine composé de figures influentes mais isolées les unes des autres ; c'est aussi la situation catastrophique de l'emploi académique qui me paraît être en cause.

[^1]: Matuschak [a écrit sur sa situation](https://andymatuschak.org/2020/) et motive son rejet de l'université par un commentaire sur la discipline de l'interaction humain-machine. Je ne connais pas sa position sur les sciences de l'information, [l'organisation des connaissances](https://www.isko.org/cyclo/knowledge_organization) et le partenariat recherche-ingénierie à l'œuvre dans les humanités numériques, un écosystème pourtant parfait pour le développement des *tools for thought*.
