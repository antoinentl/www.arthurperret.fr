---
title: Élaborer un plan au format texte
date: 2021-01-07
veille: true
---

<https://plaintextproject.online/articles/2020/10/20/outline.html>

Je le fais tout le temps, pour la thèse, les articles, les billets, les cours… C'est le pouvoir combiné de la liste et d'un bon éditeur de texte pour rendre l'opération simple et agile.

