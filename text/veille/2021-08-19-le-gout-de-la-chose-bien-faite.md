---
title: Le goût de la chose bien faite
date: 2021-08-19
veille: true
---

<https://insecable.hypotheses.org/13>

*Insécable* est le blog Hypothèses tout neuf de Céline Barthonnat, qui publie un premier billet consacré à son travail d'éditrice. Je suis sensible à la dimension artisanale de la fonction éditoriale, donc naturellement j'ai beaucoup aimé le passage sur le « goût de la chose bien faite ». Le reste du billet est tout aussi intéressant. C'est précis tout en restant concis, et il y a des jeux de mots dans les intertitres, chez moi ça signifie abonnement immédiat.

Via [Étienne Nadji](https://etnadji.fr/pagxoj/911a5bea-49dc-4b72-93dc-18af20097ed4.html).

