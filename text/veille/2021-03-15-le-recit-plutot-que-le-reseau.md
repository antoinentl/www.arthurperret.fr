---
title: Le récit plutôt que le réseau
date: 2021-03-15
veille: true
---

<http://www.marcjahjah.net/5455-arts-litteraires-et-donnees-re-presentons-autrement-les-tensions-et-nos-ordinaires-numeriques>

Cartographier autrement la polyphonie et les différents médias impliqués dans les controverses (entre autres). Lecture utile pour considérer autrement un sujet (le réseau) sur lequel je bosse tous les jours.

