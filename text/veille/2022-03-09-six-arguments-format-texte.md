---
title: Six arguments en faveur du format texte
date: 2022-03-09
veille: true
---

<https://sive.rs/plaintext>

Puisqu'[il faut faire l'effort de vendre le format texte](2022-02-09-de-l-importance-de-vendre-le-format-texte.html), voici une liste d'arguments en sa faveur : portable, non-commercial, hors-ligne, sans dépendances logicielles, facile à convertir, versatile. On peut trouver d'autres articles de ce genre, qui avancent d'autres arguments mais j'ai apprécié celui-ci. Au passage, j'ai découvert un auteur dont l'approche stoïciste m'intéresse : Derek Sivers.

Via [Thierry Stoehr](https://twitter.com/ThierryStoehr/status/1501513637302837256).
