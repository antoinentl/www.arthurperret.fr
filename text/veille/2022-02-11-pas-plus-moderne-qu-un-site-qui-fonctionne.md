---
title: Il n’y a pas plus moderne qu’un site qui fonctionne
date: 2022-02-11
veille: true
---

<https://marienfressinaud.fr/pour-batir-un-morceau-de-web.html>

Après le format texte [hier](2022-02-09-de-l-importance-de-vendre-le-format-texte.html), voici un autre billet qui met les mots exacts sur une approche qui me tient à cœur. Cette fois, il s’agit des technologies du Web, avec d’abord et avant tout HTML, loué ici pour sa simplicité. Le billet m’a donné le sourire, parce que c’est exactement comme ça que j’introduis mon cours d’écriture et publication web en BUT Métiers du livre : on écrit « Coucou ! » dans un fichier texte, et hop, on a une page web.

Et ensuite ? Eh bien, HTML est un simple codage, c’est-à-dire un ensemble de conventions arbitraires pour représenter de l’information d’une certaine manière. Ce n’est pas un langage de programmation : il n’est pas nécessaire de savoir calculer, appliquer des règles de logique ou travailler avec des niveaux d’abstraction élevés pour créer sa première page web. Il suffit de savoir que `p` désigne un paragraphe, `a` un lien, `em` une emphase, etc. Donc avec une bonne page de référence (comme [celle du Mozilla Developer Network](https://developer.mozilla.org/fr/docs/Web/HTML/Reference)) et un cours en ligne éprouvé (comme [celui d’OpenClassrooms](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3), anciennement le Site du Zéro), on peut avoir un site fonctionnel en une poignée d’heures, avec peu d’effort et pas mal de fun.

Mais ce n’est pas tout. Dans le billet d’hier sur le format texte, Boris Marinov critiquait les technologies « avancées », adjectif qui se traduit en fait par un coût et des inconvénients bien supérieurs à ceux des technologies « simples », celles qu’on ne prend pas le temps de vendre parce qu’elles ne coûtent rien. On retrouve une idée un peu similaire dans le billet de Marien Fressinaud :

> « Oubliez [le terme “moderne”], il est généralement utilisé par des personnes qui aiment se compliquer la vie avec beaucoup trop d’outils. Il n’y a pas plus moderne qu’un site qui fonctionne ».

Je partage assez cette idée. Et je pense que « fonctionner » peut être pris dans une acception assez large. [Le site du Low-tech Magazine](https://solar.lowtechmagazine.com), par exemple, ne fonctionne pas quand l’énergie accumulée par ses panneaux solaires est épuisée. Mais quand il est en ligne, il fonctionne mieux que la plupart des magazines web, grâce à une conception technique radicalement minimaliste. Ça, c'est moderne !

Via [Étienne Nadji](https://etnadji.fr/pagxoj/057b34b5-2d74-4040-8a48-c722af798ded.html).