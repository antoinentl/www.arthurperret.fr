---
title: Rédaction durable avec Pandoc et Markdown
date: 2020-11-06
veille: true
---

<https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown>

Un tutoriel classique de 2014 sur la syntaxe Markdown et le convertisseur Pandoc, légèrement révisé pour 2020, dont j'ai évalué la traduction. À lire en testant l'éditeur en ligne [Stylo](https://stylo.huma-num.fr), désormais sur Huma-Num, qui implémente les outils dont il est question ici.

