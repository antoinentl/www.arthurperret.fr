---
title: Des espaces d'échange pour les tools for thought
date: 2021-06-30
veille: true
---

<https://blog.fission.codes/tools-for-thought-interchange-part-1/>

<https://blog.fission.codes/tools-for-thought-interchange-part-2/>

Une rencontre a eu lieu récemment entre quelques acteurs impliqués dans le développement de *tools for thought*, ces logiciels qui ciblent les travailleurs de la connaissance. Un premier pas pour pallier à [la difficulté de faire progresser rapidement ce champ](https://www.arthurperret.fr/veille/difficile-progression-tools-for-thought.html), avec [un dépôt Git](https://github.com/TFTInterchange/ToolsForThoughtInterchange/) et [une gestion événementielle collaborative](https://lu.ma/toolsforthought).

À surveiller également, la « non-conférence » [Augment Minds](https://opencollective.com/psionica/events/augment-minds-7d13842a), qui poursuit sensiblement le même objectif. Andy Matuschak, qui déplorait la fragmentation des projets dans ce champ, doit être rassuré.

