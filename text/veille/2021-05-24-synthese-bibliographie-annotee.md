---
title: Une synthèse sur la bibliographie annotée
date: 2021-05-24
veille: true
---

<https://zotero.hypotheses.org/3556>

Le blog Zotero francophone a encore frappé. Merci à eux pour le signalement de [mon billet sur le sujet](https://www.arthurperret.fr/bibliographie-annotee.html). Si vous utilisez Zotero, cette synthèse contient à peu près tout ce qu'il vous faut pour produire des bibliographies annotées.