---
title: Telescope
date: 2021-01-04
veille: true
---

<https://github.com/Greaby/telescope/>

Telescope est un logiciel de visualisation qui permet de créer des graphes interactifs représentant des ressources reliées par des mots-clés. La mécanique est similaire à ce qu’on trouve dans un [Obsidian](https://obsidian.md), par exemple : pour chaque ressource, on crée un fichier Markdown, et on lui ajoute différents mots-clés ; Telescope génère ensuite un graphe qui représente à la fois les ressources et les mots-clés, ce qui crée une cartographie des ressources en fonction de l'indexation.

Telescope fait évidemment penser à [Cosma](https://cosma.graphlab.fr). La logique interne est différente, puisque Cosma dans ce sont des liens hypertextuels directs entre fiches qui composent le graphe. Mais le rendu final est très proche : un fichier HTML statique avec le texte des fiches, un graphe et au moins un outil de navigation (dans Telescope, le moteur de recherche). Les deux logiciels partagent ce souci de publier la documentation, pas seulement l'accumuler dans un coin, et de tirer parti du format HTML pour cette publication.

Via [Korben](https://korben.info/telescope-graph.html), relayé par [Daniel Bourrion](https://twitter.com/dbourrion/status/1478298952713785344) et repéré par Olivier Le Deuff.