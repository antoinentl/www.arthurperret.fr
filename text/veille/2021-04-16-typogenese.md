---
title: Typogénèse
date: 2021-04-16
veille: true
---

<https://www.itsnicethat.com/articles/character-type-newsserif-graphic-design-110221>

À quel point faut-il que les différentes polices d'une même famille se ressemblent ? Un designer explique son processus de création et critique la logique génétique qui prévaut dans l'industrie du design de caractères. Des propos qui rejoignent les critiques des classification typographique que j'avais relevées pour mon article « [Sémiotique des fluides](https://www.arthurperret.fr/semiotique-des-fluides.html) ».

Source : *I Love Typography*'s « The Month in Type » ([avril 2021](https://news.ilovetypography.com/archive/April-16-2021-Month-in-Type.html)).