---
title: "De l’importance de “vendre” le format texte"
date: 2022-02-09
veille: true
---

<https://boris-marinov.github.io/text/>

Je lis beaucoup de choses consacrées au format texte mais ce billet a retenu mon attention dès les premières lignes en soulevant un point extrêmement intéressant, que j'ai voulu partager ici en le traduisant rapidement :

> ^[{-} *“It is a well-known engineering principle, that you should always use the weakest technology capable of solving your problem - the weakest technology is likely the cheapest, easiest to maintain, extend or replace and there are no sane arguments for using anything else.  
The main problem with this principle is marketing - few people would sell you a 10\$ product that can solve your problem for ever, when they can sell you a 1000\$ product, with 10\$ per month maintenance cost, that will become obsolete after 10 years. If you listen to the “experts” you would likely end up not with the simplest, but with the most advanced technology.  
And with software the situation is particularly bad, because the simplest technologies often cost zero, and so they have zero marketing budget. And since nobody would be benefiting from convincing you to use something that does not cost anything, nobody is actively selling those.  
The problem of text is one of those problems where the simplest of all solutions works great - plain text files do the job. I’ve yet to see a use-case where considering any other technology is worth it. […] Anything you write and that you want to last should be put on plain text files.”*]« Il y a un principe d'ingénierie bien connu qui dit qu'il faut toujours choisir la technologie la plus simple pour résoudre un problème – c'est probablement la moins chère et la plus facile à entretenir, à étendre ou à remplacer, et donc il n'y a pas vraiment d'argument sensé qui justifierait d'en utiliser une plus complexe.
>
> Le problème avec ce principe, c’est le marketing – peu de gens essaieront de vous vendre un produit à 10\$ capable de résoudre votre problème pour toujours, s'ils peuvent à la place vous vendre à la place un produit à 1000\$, avec un coût de maintenance de 10\$ par mois, qui deviendra obsolète au bout de 10 ans. C'est ainsi qu'en suivant les “experts”, vous risquez de vous retrouver non pas avec la technologie la plus simple, mais avec la plus “avancée”.
>
> Le cas des logiciels est particulièrement criant : les technologies logicielles les plus simples ne coûtent souvent rien, et donc il n'y a aucun budget marketing derrière. Comme il n'y a rien à gagner à vous convaincre d'utiliser quelque chose qui ne coûte rien, personne ne fait l'effort de vendre la chose en question. […]
>
> En informatique, créer du texte fait partie de ces choses pour lesquelles la solution la plus simple fonctionne très bien – en l'occurrence, de simples fichiers texte font généralement l'affaire. Je n'ai encore jamais vu de cas d'utilisation où une autre solution vaille plus la peine. […] Tout ce que vous écrivez et que vous voulez voir perdurer devrait être stocké dans de simples fichiers au format texte. »

Voilà pourquoi je parle autant du format texte sur mon site et dans mon travail en général : sa notoriété est inverse à son utilité, et notamment à son potentiel éducatif et émancipateur. D'où l'importance de le “vendre”, car peu de gens prendront le temps de le faire.

Via [Hacker News](https://news.ycombinator.com/item?id=30241565).