---
title: Colophon Cards
date: 2022-05-08
veille: true
---

<https://www.colophon.cards>

Baldur Bjarnason travaille sur un projet d’application de prise de notes, Colophon Cards, et partage son processus de conception via un site dédié. Je vais suivre attentivement ce développement, car j’y retrouve des éléments sur lesquels je travaille à travers Cosma notamment : la documentation personnelle, la fiche, les liens.

