---
title: Le Web est-il devenu trop compliqué ?
date: 2021-01-07
veille: true
---

<https://framablog.org/2020/12/30/le-web-est-il-devenu-trop-complique/>

Usage stratégique néfaste de la complexité (réduire la concurrence, surveiller), limites de la simplification et de la disruption (dégradation de l'expérience, inégale adéquation aux besoins). Un billet qui fait réfléchir.

