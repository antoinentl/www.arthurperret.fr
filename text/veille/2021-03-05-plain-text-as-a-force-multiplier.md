---
title: Plain Text as a Force Multiplier
date: 2021-03-05
veille: true
---

<https://plaintextproject.online/articles/2021/03/04/force.html>

Un emprunt au vocabulaire militaire pour vanter la capacité du format texte à décupler la productivité. En fait c'est surtout en le combinant à d'autres technologies (souvent une bonne interface graphique) qu'il révèle son efficacité.

