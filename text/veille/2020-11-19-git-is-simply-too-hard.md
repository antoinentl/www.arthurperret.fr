---
title: Git is simply too hard
date: 2020-11-19
veille: true
---

<https://changelog.com/posts/git-is-simply-too-hard>

J'ai un profil différent de l'auteur (qui bosse chez GitHub) mais cet extrait résonne avec mon expérience : je suis un adepte du terminal mais j'ai récemment basculé sur l'interface graphique pour utiliser git et c'est un soulagement.

