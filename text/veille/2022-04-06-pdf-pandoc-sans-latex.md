---
title: "Des PDF via Pandoc avec d’autres moteurs que LaTeX"
date: 2022-04-06
veille: true
---

<https://plaintextproject.online/articles/2022/04/06/pdf.html>

Créer un PDF via LaTeX par l'intermédiaire de Pandoc, c'est possible. Le faire sans même écrire de LaTeX, c'est possible aussi (j'en ai parlé à la fin d'[un billet publié fin 2020](2020-10-19-enseignement-automatisation-pandoc)). Mais je découvre avec ce lien que Pandoc est aussi capable d'utiliser d'autres moteurs que LaTeX, beaucoup plus légers, qui suffisent pour de nombreux cas. Cette possibilité intéressera notamment les personnes initiées à HTML et CSS, puisque ce sont les technologies impliquées. Une information à rapprocher de l'annonce récente du support de Paged.js dans Pandoc (depuis la version [2.17.1](https://pandoc.org/releases.html#pandoc-2.17.1-2022-01-30)).

