---
title: "Les données ont-elles évincé ou éclipsé les documents ?"
date: 2021-11-30
veille: true
---

<https://www.marieannechabin.fr/2021/11/les-donnees-ont-elles-evince-ou-eclipse-les-documents-1-3/>

Marie-Anne Chabin vient de publier trois billets sur la relation entre données et documents aujourd'hui. Il s'agit de notes de recherche, partagées avant de prendre une forme plus structurée. Elle explore la question de manière systématique, depuis les définitions jusqu'aux pratiques professionnelles. Dans le dernier billet, deux éléments ont retenu mon attention : la fin annoncée de la gestion électronique de documents (GED) et l'idée d'un grand colloque interdisciplinaire sur la donnée.