---
title: "Treeverse : le fil, l'arbre et le réseau"
date: 2021-07-23
veille: true
---

<https://treeverse.app>

Treeverse est une extension pour navigateur internet (disponible sur Firefox et Chrome) qui permet de visualiser des discussions Twitter sous forme d'arbre interactif. Ou comment passer du fil (*thread*) au réseau…

