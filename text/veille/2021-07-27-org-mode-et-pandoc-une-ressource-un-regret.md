---
title: "Org-mode et Pandoc : une ressource et un regret"
date: 2021-07-27
veille: true
---

<https://awarm.space/fast/0000-org-and-pandoc-static-site>

Voici un retour d'expérience sur la création d'un générateur de site statique basé sur Org-mode et Pandoc. L'article est bien écrit et bien documenté mais ce n'est pas pour ça que je le signale. Ce qui m'a donné envie de l'inclure dans ma veille, c'est sa conclusion :

> « What's next? Well actually writing for one. I've got the infrastructure in place and now it's time to start using it! »

Le fait que la question de l'écriture arrive en dernier m'a fait rire. Jamais je n'aurais écrit sur la conception d'un outil de publication sans parler de mes besoins d'écriture et de mon processus de travail. Je ne veux pas faire de procès d'intention à l'auteur : il a peut-être fait consciemment le choix de s'adresser à une certaine communauté, et donc de ne pas expliciter tous les facteurs à prendre en compte. Le problème, c'est que la vaste majorité des ressources de ce genre font pareil : le référentiel, souvent celui des développeurs, est trop implicite, et on peut malheureusement perdre beaucoup de temps à tenter de s'y conformer sans le savoir, pour découvrir bien plus tard qu'il faut tout repenser du début.

Évidemment, j'appartiens à un public très restreint et peu représenté, donc je dois de toute façon faire des recherches assez précises pour trouver ce dont j'ai besoin. Mais je regrette tout de même qu'il y ait si peu de ressources qui parlent d'ingénierie éditoriale sans situer clairement la perspective dans laquelle elles sont rédigées.

