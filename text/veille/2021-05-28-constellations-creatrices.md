---
title: Constellations créatrices
date: 2021-05-28
veille: true
---

<https://lesjaseuses.hypotheses.org/3017>

Un compte-rendu du séminaire des Jaseuses dont la partie [« Les outils »](https://lesjaseuses.hypotheses.org/3017#les-outils) décrit des questionnements qui sont au cœur de ma recherche doctorale : le rayonnement de l'objet de recherche, le traçage des liens, la formation de constellations, le [blues du rhizome](https://www.arthurperret.fr/rhizome-blues.html), les lignées théoriques…

Via [Aurore Turbiau](https://twitter.com/AuroreTurbiau/status/1398237339206443008).