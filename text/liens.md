---
title: Liens
---

::: liens

Une liste de sites auxquels je suis abonné via flux RSS ou Atom et qui alimentent ma veille. Dernière mise à jour le 07 avril 2022.

[Télécharger la liste au format OPML](liens.opml){download="arthurperret-liens.opml"}

- Appleton, Maggie — <https://maggieappleton.com/>
- Bawden, David — <https://theoccasionalinformationist.com/>
- Blanc, Julie — <https://julie-blanc.fr/>
- Bortzmeyer, Stéphane — <http:///>
- Brander, Gordon (Subconscious) — <https://subconscious.substack.com/>
- Brassard, Louis-O — <https://journal.loupbrun.ca/>
- Chabin, Marie-Anne — <https://www.marieannechabin.fr/>
- Delhaye, Marlène — <https://marlenescorner.net/>
- Deschamps, Christophe (Outils froids) — <https://www.outilsfroids.net/>
- DLIS — <https://dlis.hypotheses.org/>
- Durier, Manon (Ecritac) — <https://ecritac.hypotheses.org/>
- EchosDoc — <http://www.echosdoc.net/>
- Etter, Kaspar (Explained from First Principles) — <https://explained-from-first-principles.com/>
- Fauchié, Antoine — <https://www.quaternum.net/>
- Huma-Num Lab — <https://hnlab.huma-num.fr/blog/>
- Hyde, Adam — <https://www.adamhyde.net/>
- iA (Information Architects) — <https://ia.net/>
- Infodoc microveille — <https://microblogging.infodocs.eu/>
- Jacomy, Mathieu — <https://reticular.hypotheses.org/>
- Jahjah, Marc — <https://marcjahjah.net/>
- La boîte à outils — <https://boiteaoutils.info/>
- LaLIST — <https://lalist.inist.fr/>
- Le Deuff, Olivier — <http://www.guidedesegares.info/>
- Lee, Linus (TheSephist) — <https://thesephist.com/>
- MacWright, Tom — <https://macwright.com/>
- Masure, Anthony — <https://www.anthonymasure.com/blog>
- Masutti, Christophe — <https://golb.statium.link/>
- Nadji, Étienne — <https://etnadji.fr/>
- Picard, Yves (#cours_édition) — <http://yvpicshare.farmserv.org/>
- Piotrowski, Michael — <https://nlphist.hypotheses.org/>
- Plain Text Project — <https://plaintextproject.online/>
- Poupeau, Gautier (LesPetitesCases) — <http://www.lespetitescases.net/>
- Rendle, Robin — <http://robinrendle.com/feed.xml>
- SFSIC — <https://www.sfsic.org/>
- Sound of Science — <https://www.soundofscience.fr/>
- Stoehr, Thierry — <http://formats-ouverts.org/>
- Taffin, Nicolas — <https://polylogue.org/>
- Tay, Aaron — <https://follow.it/musings-about-librarianship-aaron-tay>
- Vitali-Rosati, Marcello — <http://blog.sens-public.org/marcellovitalirosati>
- ZefSIC — <https://retrodev.net/zefsic/>
- Zettlr — <https://www.zettlr.com/blog>
- Zotero (blog francophone) — <https://zotero.hypotheses.org/>
:::