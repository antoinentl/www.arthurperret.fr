---
title: Arthur Perret - CV
---

# Curriculum vitæ

Je suis doctorant en sciences de l’information et de la communication à l'[Université Bordeaux Montaigne](https://www.u-bordeaux-montaigne.fr/fr/index.html). J'enseigne à l’[IUT Bordeaux Montaigne](https://www.iut.u-bordeaux-montaigne.fr/) comme ATER et je fais partie de l’équipe E3D du laboratoire [MICA](https://mica.u-bordeaux-montaigne.fr/) (Médiations, Informations, Communication, Arts). Je travaille sur les données, documents et dispositifs impliqués dans les processus d'écriture, de documentation et d’édition. Ma thèse porte sur l’héritage épistémologique de Paul Otlet ; elle est co-dirigée par [Olivier Le Deuff](http://www.guidedesegares.info/a-propos/) et [Bertrand Müller](https://cnrs.academia.edu/BertrandMüller), et a été financée initialement dans le cadre du programme ANR [HyperOtlet](https://hyperotlet.hypotheses.org/). Je suis également co-organisateur des journées d'étude [Reticulum](http://reticulum.info) en SIC et design, ainsi que des rencontres [Inachevé d'imprimer](https://inacheve-dimprimer.net) sur la publication numérique. Depuis 2021, je conçois le logiciel [Cosma](https://cosma.graphlab.fr) avec [Guillaume Brioudes](https://myllaume.fr) et l'équipe du [GraphLab](https://www.graphlab.fr).

::: columns

# Informations & contact

Arthur Perret - Doctorant en sciences de l'information et de la communication, ATER à l'IUT Bordeaux Montaigne  
HAL : [arthur-perret](https://cv.archives-ouvertes.fr/arthur-perret)  
ORCID : [0000-0002-4130-6669](https://orcid.org/0000-0002-4130-6669)  
Twitter : [\@arthurperret](https://twitter.com/arthurperret)  
Site web : <https://www.arthurperret.fr/>

# Formation

## Depuis 2017

**Université Bordeaux Montaigne** Doctorat en sciences de l'information et de la communication.  
Thèse (en cours) : *De l’héritage épistémologique de Paul Otlet à une théorie réticulaire de l’organisation des connaissances* (dir. Olivier Le Deuff & Bertrand Müller). Mots-clés : documentation, organisation des connaissances, données, métadonnées, information scientifique, visualisation, écriture, édition, publication, épistémologie.

## 2016-2017

**Enssib** Master 2 publication numérique.  
Mémoire : *Écrire la carte. Les apports intellectuels de la cartographie numérique* (dir. Éric Guichard). <https://www.enssib.fr/bibliotheque-numerique/documents/67716-ecrire-la-carte-les-apports-intellectuels-de-la-cartographie-numerique.pdf>

## 2014-2016

**Enssib** Master information scientifique et technique.  
Mémoire : *Écrire l'image. Approche pragmatique et conceptuelle de la photographie numérique* (dir. Pascal Robert). <https://www.enssib.fr/bibliotheque-numerique/documents/67249-ecrire-l-image-approche-pragmatique-et-conceptuelle-de-la-photographie-numerique.pdf>

# Emploi

## Depuis 2020

**IUT Bordeaux Montaigne** ATER, 71^e^ section, département Information-communication (filières Information numérique dans les organisations, Métiers du livre et du patrimoine).

## 2017-2020

**IUT Bordeaux Montaigne** Contrat doctoral dans le cadre du programme ANR HyperOtlet.

# Publications

### Articles de revue évalués par les pairs

Harmand, Florian & Perret, Arthur (2022). « Former à la notion de réseau par la conception et l’interprétation : l’atelier Reticulum ». *Humanités numériques*, nᵒ 5. DOI : [10.4000/revuehn.2930](https://doi.org/10.4000/revuehn.2930).

Le Deuff, Olivier & Perret, Arthur (2019). « Hyperdocumentation: origin and evolution of a concept ». *Journal of documentation* 75(6). DOI : [10.1108/JD-03-2019-0053](https://doi.org/10.1108/JD-03-2019-0053). [https://arthurperret.fr/hyperdocumentation.html](hyperdocumentation.html). Outstanding Paper, [Emerald Literati Awards 2020](https://www.emeraldgrouppublishing.com/journal/jd/literati-awards/journal-documentation-literati-award-winners-2020).

### Articles de congrès évalués par les pairs

Perret, Arthur (2021). « Fonction documentaire de preuve et données numériques ». *Humains et données : création, médiation, décision, narration*. Louvain-la-neuve : De Boeck. pp. 25‑36. ISBN 978-2-8073-3263-8. [https://www.arthurperret.fr/articles/2020-09-09-fonction-documentaire-preuve-donnees-numeriques.html](https://www.arthurperret.fr/articles/2020-09-09-fonction-documentaire-preuve-donnees-numeriques.html)

Perret, Arthur, Le Deuff, Olivier & Borel, Clément (2020). « Rhizome Blues: Introducing Document Teratology ». *Proceedings from the Document Academy*, 7(1). DOI : [10.35492/docam/7/1/13](https://doi.org/10.35492/docam/7/1/13). [https://www.arthurperret.fr/rhizome-blues.html](rhizome-blues.html)

Le Deuff, Olivier & Perret, Arthur (2019). « Paul Otlet and the ultimate prospect of documentation ». *Proceedings from the Document Academy*, 6(2). DOI : [10.35492/docam/6/1/9](https://doi.org/10.35492/docam/6/1/9). [https://arthurperret.fr/paul-otlet-and-the-ultimate-prospect-of-documentation.html](paul-otlet-and-the-ultimate-prospect-of-documentation.html)

Perret, Arthur (2019). « Writing documentarity ». *Proceedings from the Document Academy*, 6(2). DOI : [10.35492/docam/6/1/10](https://doi.org/10.35492/docam/6/1/10). [https://arthurperret.fr/writing-documentarity.html](writing-documentarity.html)

Perret, Arthur (2019). « La logique hyperdocumentaire dans l'organisation de l'information et sa représentation ». *Colloque Jeunes Chercheurs PRAXILING 2019 : Représentations et transmission des connaissances à la lumière de l'innovation numérique*. <https://hal.archives-ouvertes.fr/hal-02358067>. [https://arthurperret.fr/logique-hyperdocumentaire-structuration-visualisation.html](logique-hyperdocumentaire-structuration-visualisation.html)

Le Deuff, Olivier, David, Jean, Perret, Arthur, Borel, Clément (2019). « Surfer dans l’Otletosphère. Des outils pour visualiser et interroger le réseau de Paul Otlet ». *H2PTM'19 : De l'hypertexte aux humanités numériques*. Londres : ISTE éditions. <https://archivesic.ccsd.cnrs.fr/sic_02480515>.

Le Deuff, Olivier, David, Jean, Perret, Arthur, Borel, Clément (2019). « Paul Otlet sur le Web. Une étude cartographique ». *H2PTM'19 : De l'hypertexte aux humanités numériques*. Londres : ISTE éditions. <https://archivesic.ccsd.cnrs.fr/sic_02480493v1>.

Perret, Arthur & Le Deuff, Olivier (2019). « Documentarité et données, instrumentation d'un concept ». *12ème Colloque international d’ISKO-France : Données et mégadonnées ouvertes en SHS : de nouveaux enjeux pour l’état et l’organisation des connaissances ?*. <https://hal.archives-ouvertes.fr/hal-02307039>. [https://arthurperret.fr/documentarite-et-donnees.html](documentarite-et-donnees.html)

Perret, Arthur (2018). « The Architext of Biblion: Digital Echoes of Paul Otlet ». *Proceedings from the Document Academy*, 5(2). DOI : [10.35492/docam/5/2/6](https://doi.org/10.35492/docam/5/2/6). [https://arthurperret.fr/the-architext-of-biblion.html](the-architext-of-biblion.html)

Perret, Arthur (2018). « Matière à pensées. Outils d’édition et médiation de la créativité ». *Actes du XXIe Congrès de la SFSIC*, 3, 190‑201. <https://hal.archives-ouvertes.fr/hal-02358098>. [https://arthurperret.fr/matiere-a-pensees.html](matiere-a-pensees.html)

### Autres travaux évalués par les pairs

Perret, Arthur (2019) « Pour un réinvestissement critique du concept de dispositif ». *Études digitales* 6. Recension de : Larroche, V. (2018). *Le dispositif : un concept pour les sciences de l'information et de la communication*. [https://arthurperret.fr/pour-un-reinvestissement-critique-du-concept-de-dispositif.html](reinvestissement-critique-concept-dispositif.html)

Perret, Arthur (2018). « Technologies intellectuelles ». *Abécédaire des mondes lettrés*. (dir. C. Jacob). <http://abecedaire.enssib.fr/t/technologies-intellectuelles/notices/138>. [https://arthurperret.fr/technologies-intellectuelles.html](technologies-intellectuelles.html).

### Logiciels

Perret, Arthur, Brioudes, Guillaume, Borel, Clément et Le Deuff, Olivier. Cosma. Zenodo. 2020. <http://cosma.graphlab.fr/>. DOI : [10.5281/zenodo.5920615](https://doi.org/10.5281/zenodo.5920615)

Brioudes, Guillaume, Perret, Arthur et Le Deuff, Olivier. Otletosphère (Version v2.0.0). Zenodo. 2020. <http://hyperotlet.huma-num.fr/otletosphere/>. DOI : [10.5281/zenodo.3981189](https://doi.org/10.5281/zenodo.3981189)

# Communications

## 2022

Chagué, Alix (modération), Perret, Arthur, Pouyllau, Stéphane, Porte, Guillaume, Idmhand, Fatiha & Suchecka, Karolina. Table ronde : enjeux et débats autour de l’organisation du travail dans les projets en humanités numériques. [Colloque DH Nord 2022](https://dhnord2022.sciencesconf.org) (en ligne). 21 juin.

Perret, Arthur. Idéation et hypertexte : organisation des connaissances créative avec Cosma. [Colloque Méthodes créatives de recherche](https://questyourdigital.com/colloque/). 16 juin.

## 2021

Perret, Arthur. Documenter la documentation, de la fiche au graphe. [Assemblée générale ADBS Auvergne-Rhône-Alpes](https://www.adbs.fr/agenda/assemblee-regionale-de-289970) (en ligne). 29 novembre.

Perret, Arthur. HyperOtlet et les graphes. [Journée d’étude HyperOtlet Enssib](https://www.enssib.fr/JE-HyperOtlet-documenter-la-documentation) (en ligne). 29 octobre.

Perret, Arthur. L’écriture académique au format texte. [Formation Urfist de Bordeaux](https://www.arthurperret.fr/cours/2021-09-21-ecriture-academique-format-texte.html) (en ligne). 21 septembre.

Harmand, Florian & Perret, Arthur. Introduction et conclusion de la [journée d’étude Reticulum 3 Faire savoir et pouvoir faire.](http://reticulum.info/3/) (en ligne). 8 février.

## 2020

Perret, Arthur. Fonction documentaire de preuve et données numériques. [7e conférence Document numérique et société](https://docsoc2020.sciencesconf.org). 28 septembre.

Perret, Arthur, Le Deuff, Olivier & Borel, Clément. Rhizome Blues: Introducing Document Teratology. [DOCAM 2020](http://documentacademy.org/?2020) (en ligne). 6 août.

Perret, Arthur & Le Deuff, Olivier. All papers are data papers: from open principles to digital methods. [DH2020](https://dh2020.hcommons.org) (en ligne). 22 juillet. <https://dh2020.adho.org/wp-content/uploads/2020/07/582_Allpapersaredatapapersfromopenprinciplestodigitalmethods.html>

Harmand, Florian & Perret, Arthur. Enchevêtrement de la documentation et du design : éléments interdisciplinaires pour les humanités numériques. [Humanistica 2020](http://www.humanisti.ca/colloque2020/) (en ligne). <https://hal.archives-ouvertes.fr/hal-02876089>

Perret, Arthur (2020). Quelques liens entre documentation, sémiotique et architecture de l'information autour de l'organisation du travail intellectuel. [Reticulum 2](http://reticulum.info/2/) (Pessac). 20 février.

## 2019

Perret, Arthur (2019). La logique hyperdocumentaire dans l'organisation de l'information et sa représentation. [CJC Praxiling 2019](https://cjc19-praxiling.sciencesconf.org/) (Montpellier). 8 novembre.

Le Deuff, Olivier, David, Jean, Perret, Arthur, Borel, Clément (2019). Paul Otlet sur le Web : une étude cartographique. [H2PTM'19](http://h2ptm.univ-paris8.fr/) (Montbéliard). 17 octobre.

Le Deuff, Olivier, David, Jean, Perret, Arthur, Borel, Clément (2019). Surfer dans l’Otletosphère : des outils pour visualiser et interroger le réseau de Paul Otlet. [H2PTM'19](http://h2ptm.univ-paris8.fr/) (Montbéliard). 16 octobre.

Le Deuff, Olivier & Perret, Arthur (2019). Documentarité et données, instrumentation d'un concept. [Colloque ISKO France 2019](http://www.isko-france.asso.fr/colloque2019/) (Montpellier). 9-11 octobre.

Perret, Arthur (2019). Raison cartographique et territoire intellectuel. [École d’été de cartographie et visualisation 2019](http://barthes.enssib.fr/ECV-2019/) (Villeurbanne). 2 juillet.

Perret, Arthur (2019). Écrire, documenter et publier : nouvelles techniques pour l'édition scientifique. [Atelier Internet Lyonnais](http://barthes.enssib.fr/atelier/) (Villeurbanne). 17 juin.

Perret, Arthur (2019). Writing documentarity. [DOCAM 2019](http://documentacademy.org/?2019) (Toulon). 13 juin.

Le Deuff, Olivier & Perret, Arthur (2019). Paul Otlet and Hyperdocumentation: the Ultimate Prospect of Documentation. [DOCAM 2019](http://documentacademy.org/?2019) (Toulon). 13 juin.

David, Jean, Sergent, Henri, Le Deuff, Olivier, Perret, Arthur, Müller, Bertrand & Borel, Clément (2019). The role of Paul Otlet in Information sciences (poster). [DOCAM 2019](http://documentacademy.org/?2019) (Toulon). 12-14 juin.

Perret, Arthur (2019). Documentation et numérique. [Rencontres doctorales Le numérique pour quelle humanité ?](https://www.iea-nantes.fr/fr/actualites/rencontres-doctorales-le-numerique-pour-quelle-humanite_851) (Nantes). 5 juin.

Müller, Bertrand, Le Deuff, Olivier & Perret, Arthur (2019). HyperOtlet. Documenter la documentation. [Les entrepreneurs du savoir](http://intd.cnam.fr/les-entrepreneurs-du-savoir-de-paul-otlet-aux-nouvelles-utopies-numeriques--1070559.kjsp) (Paris). 21 mai.

Perret, Arthur (2019). Regard dispositif sur l'écriture. [Reticulum #1](http://reticulum.info) (Pessac). 4 mars.

Perret, Arthur (2019). Du document à l'hyperdocument. [Masterclass DNHD #1](https://mica.u-bordeaux-montaigne.fr/events/masterclass-dnhd-1-hyperedition-documentarite-technologies-intellectives/) (Pessac). 22 février.

## 2018

Perret, Arthur (2018). The Architext of Biblion: Digital Echoes of Paul Otlet. [DOCAM 2018](http://documentacademy.org/?2018) (Turin). 4 octobre.

Perret, Arthur (2018). Sémiotique des fluides : quelques remarques sur l’énonciation typographique. [66èmes Rencontres internationales de Lure](http://delure.org/les-rencontres/rencontres-precedentes/rencontres-2018) (Lurs). 24 août.

Perret, Arthur (2018). L’Otléidoscope : heuristique et herméneutique du Traité de documentation de Paul Otlet. [École d’été de cartographie et visualisation 2018](http://barthes.enssib.fr/ECV-2018/) (Villeurbanne). 9 juillet.

Perret, Arthur (2018). Matière à pensées : outils d’édition et médiation de la créativité.. XXIe Congrès de la SFSIC (Saint-Denis). 14 juin. <https://hal.archives-ouvertes.fr/hal-02358098>

Perret, Arthur (2018). Sémiotique des fluides : la typographie du livre numérique, entre fluidité et frictions. [Colloque ÉCRIDIL](http://ecridil.ex-situ.info/programme) (Montréal). 1er mai.

# Organisation de manifestations scientifiques

## 2021

Reticulum 3. *Faire savoir et pouvoir faire*. Rencontres interdisciplinaires entre SIC et design. Avec Florian Harmand (MICA, UBM). 8 février 2021. <http://reticulum.info/3/>

## 2020

Reticulum 2. *Architecture(s) de l'information*. Rencontres interdisciplinaires entre SIC et design. Avec Florian Harmand (MICA, UBM). 20 février 2020. <http://reticulum.info/2/>

## 2019

Reticulum 1. *Opérabilité du concept de dispositif*. Rencontres interdisciplinaires entre SIC et design. Avec Florian Harmand (MICA, UBM). 4 mars 2019. <http://reticulum.info/1/>

# Enseignement

**Université Bordeaux Montaigne**

## 2020-2021

- **DUT** (Information numérique dans les organisations) : Bases informatiques (24h) ; Contextes professionnels spécifiques (24h) ; Culture générale et humanités (12h) ; Approfondissement en SHS (24h) ; Ouverture interculturelle (24h) ; Expression écrite, mémoire (12h) ; Veille (8h) ; Suivi de projet tuteuré (6h) et de stage (15h)
- **Licence professionnelle** (Médiation de l'information numérique et des données) : De l'archivage au partage des données (18h) ; Métadonnées (12h)
- **Master** (Document numérique et humanités digitales) : Corpus, analyse de contenu, éditorialité (13h)

## 2019-2020

- **DUT Infonum** : XML (12h) ; EPUB (12h) ; Techniques d'écriture et d'édition (12h) ; Mémoire professionnel (6h).
- **Licence professionnelle MIND** : Métadonnées et web de données (12h).
- **Master DNHD** : Édition numérique (10h).

## 2018-2019

- **DUT Infonum** : XML (10h) ; EPUB (12h) ; Culture numérique (12h).
- **Licence professionnelle MIND** : Métadonnées et web de données (12h).
- **Master DNHD** : Édition numérique (20h).

## 2017-2018

- **DUT Infonum** : XML (10h) ; EPUB (12h) ; Culture numérique (12h).

# Outils utilisés et enseignés

- Gestion de projet : Trello, Kanboard
- Veille : RSS, Twitter, Sympa
- Recherche documentaire : SUDOC, Worldcat, Google Scholar, BASE, Isidore
- Gestion bibliographique : Zotero
- Édition de texte : expressions régulières
- Balisage, sérialisation : Markdown, HTML, XML, JSON, YAML, CSV
- Mise en forme : CSS, CSL, LaTeX, LibreOffice
- Publication : générateurs de sites statiques, EPUB
- Conversion : Pandoc
- Gestion des versions : Git, GitHub, GitLab
- Bibliométrie : Publish or Perish
- Visualisation : Iramuteq, RAWGraphs, Gephi
- Programmation interactive : Jupyter, Observable

:::