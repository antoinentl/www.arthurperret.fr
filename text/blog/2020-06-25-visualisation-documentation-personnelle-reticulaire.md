---
title: Visualisation d’une documentation personnelle réticulaire
date: 2020-06-25
abstract: "Aménager un lieu de savoir à soi, cultiver son rhizome, nourrir l'écriture par la fiche et le réseau."
image: 2020-06-25-visualisation-documentation-personnelle-reticulaire-3.jpg
---

La méthode Zettelkasten conçue par Niklas Luhmann [@luhmann1992] s'inscrit dans un ensemble de techniques qu'on pourrait regrouper sous l'appellation de documentation personnelle réticulaire. Il s'agit d'une pratique de la fiche érudite [@bert2017] adossée à un système de liens internes et d'indexation par mots-clés. *Zettelkasten* signifie à peu près « boîte à fiches » en allemand, et désigne dans son acception numérique une collection de fichiers interreliés suivant cette méthode particulière. On peut faire un parallèle pertinent avec le modèle encyclopédique et hypertextuel. Pour en savoir plus, lisez l'excellent article très illustré de David B. Clear sur le sujet [dans *The Writing Cooperative*](https://writingcooperative.com/zettelkasten-how-one-german-scholar-was-so-freakishly-productive-997e4e0ca125).

![Le principe du Zettelkasten. CC BY-NC-SA David B. Clear.](https://www.arthurperret.fr/img/2020-06-25-visualisation-documentation-personnelle-reticulaire-1.png)

Il s'agit d'une technique particulièrement intéressante pour l'écriture scientifique dès qu'on brasse beaucoup de matériaux textuels. Fiches de lecture, fiches conceptuelles, fragments d'idées, brouillons, publications terminées… Si on centralise tous ces types de documents et qu'on commence à les relier, alors se recoupent plusieurs logiques documentaires qui viennent nourrir le travail intellectuel : lexique, glossaire, encyclopédie – une véritable hyperdocumentation [@ledeuff2019].

![Une fiche de mon Zettelkasten, dont la configuration reflète mes besoins : métadonnées structurées, citations.](https://www.arthurperret.fr/img/2020-06-25-visualisation-documentation-personnelle-reticulaire-2.jpg)

Au fur et à mesure que cette base de connaissances s'accroît, le bénéfice pour la recherche d'information et la réflexion augmente aussi, à deux conditions. La première, c'est la rigueur : seules des métadonnées structurées et une curation exigeante des liens et mots-clés permettront une exploitation vraiment libre et agile de ces savoirs interreliés, sauf à accepter de s'enfermer dans un outil unique qui prétend tout gérer de manière semi-automatique, au prix d'un endettement technique certain[^1].

La deuxième condition, c'est la visualisation. Une bonne fonctionnalité de recherche et d'affichage des mots-clés constitue un premier pas. Mais elle ne donnera pas une vision d'ensemble et restera de toute façon cantonnée à la liste.

Alexandre Chabot-Leclerc a développé un outil appelé [zkviz](https://github.com/Zettelkasten-Method/zkviz) qui permet de représenter un Zettelkasten sous forme de graphe, via la bibliothèque de visualisation Plotly. Voici une démonstration faite sur mes fichiers au 25 juin 2020 :

![Zkviz fournit un indicateur classique de l'analyse de réseaux : la centralité. Ceci permet d'identifer la note contenant mon plan de thèse (en jaune, au milieu) et quelques notes constituant des concepts essentiels dans ma réflexion (en bleu plus clair).](https://www.arthurperret.fr/img/2020-06-25-visualisation-documentation-personnelle-reticulaire-3.jpg)

Chaque point correspond à une note et chaque flèche à la somme des liens entre deux notes. Environ 40% des notes sont reliées à au moins une autre note. Le reste est réparti automatiquement par Plotly et forme une sorte de « ceinture d'astéroïdes » très dense à proximité du centre.

L'utilité de cette image ne réside pas dans ce qu'elle suggère des connexions entre les savoirs représentés (qui sont éphémères), mais dans sa valeur réflexive. En me donnant une vision d'ensemble, elle m'aide à passer d'une représentation mentale limitée (liens note à note) à une conscience plus globale de la structure documentaire que je suis en train de produire. Par exemple, je constate qu'il existe déjà des notes qui servent de passerelle entre différents champs de savoirs, et cela m'aide à voir l'aménagement qu'il faudrait faire (ouvrir à certains endroits, fermer à d'autres). C'est un outil de travail.

En tant que représentation graphique d'un réseau, cette image ne bousculera probablement pas les théoriciens et praticiens du domaine. Il faudrait pour cela inclure une représentation des mots-clés, ce qui passerait par des stratégies visuelles innovantes. [Il y a des pistes dans le somptueux ouvrage de @aittouati2019; il faut aussi examiner le « millefeuille » proposé récemment par @grandjean2020] Mais l'essentiel est déjà là : c'est un outil qui stimule l'écriture, qui vient renforcer immédiatement le processus d'élaboration des savoirs. Ce qui me suffit amplement, car mon travail consiste avant tout à lire et écrire.

# Références

[^1]: Ils semblent se multiplier dernièrement : Roam, Obsidian… Je préfère recommander un logiciel qui promeut des pratiques standardisées et interopérables, comme [Zettlr](https://zettlr.com/).