---
title: L’âge de raison (technologique)
date: 2020-04-08
abstract: "Faire le tri dans les outils quand la fin de thèse approche : un schéma et un bilan."
---

Je suis en thèse depuis deux ans et demi ; durant cette période, j'ai appris à utiliser de façon plus ou moins approfondie beaucoup de nouveaux outils. À l'approche de la rédaction, qui interdit de se disperser dans l'expérimentation, j'ai dû faire le tri. J'en ai tout de même tiré une réflexion, influencé comme toujours par ceux dont les recherches portent sur la technique et qui ont frayé ce chemin avant moi[^1].

En raisonnant d'abord par fréquence d'utilisation et niveau de compétence, je suis vite arrivé à une distinction entre outils principaux (maîtrisés, d'usage quotidien, indépendants de mon ancrage disciplinaire) et secondaires (importants mais encore en apprentissage, d'usage spécifique, ponctuel ou limité). Ensuite, j'ai effectué des regroupements par type d'activité : m'organiser ; recevoir de la veille et faire des recherches documentaires[^2] ; gérer mes références ; écrire, mettre en forme et convertir ; partager ; expérimenter et documenter. Il y a une exception : les éditeurs de texte, qui sont des entités à part entière pour moi, irréductibles à un processus unique – le mot « éditer » ne me convenait pas.

<figure class="fullwidth">
<figcaption>À gauche, mes principaux outils, consacrés à collecter et produire de l’information. À droite, des outils beaucoup plus spécialisés d’un point de vue disciplinaire. Màj mai 2020 : remplacement de “Markdown” par “Pandoc Markdown”.</figcaption>
<img src="https://www.arthurperret.fr/img/2020-04-08-mes-outils.png">
</figure>

Le cœur de mon métier (et ma principale compétence), c'est d'une part expérimenter et d'autre part écrire de la prose, de la littérature scientifique et pédagogique – principalement texte et image, hypertexte et hypermédia. Je considère aujourd'hui que les outils dédiés à cette seconde activité doivent passer un test d'adéquation à ma pratique ainsi que de productivité, ou bien rester des objets d'étude. Cela inclut les pratiques qui relèvent du bricolage infini, comme positionner des images dans Reveal JS, ou qui sont pensées pour d'autres types de production, comme Asciidoc. Dans mon cas, la difficulté à faire le tri est venue du fait qu'expérimentation et production scientifique se recoupent, puisque mes recherches portent sur l'écriture, l'édition, la documentation et l'épistémologie en information-communication.

J'ai définitivement une double préférence pour l'écosystème du texte brut et le design d'interface homme-machine préconisé par Apple ; j'apprécie particulièrement les logiciels qui fusionnent intelligemment les deux[^3]. Mais certains efforts de développement multi-plateforme arrivent parfois à me convaincre, comme [Zettlr](https://zettlr.com/). Et je ne suis pas complètement armé : je cherche toujours l'éditeur de CSV parfait…

[^1]: Lire par exemple le billet « [Mes outils](https://www.quaternum.net/2019/09/21/mes-outils/) » rédigé par [Antoine](https://www.quaternum.net/a-propos/) dans le cadre de sa formation doctorale. Le brouillon du présent billet avait le même titre, et puis je me suis souvenu que j’avais déjà écrit sur un moment de rationalisation dans [*L’âge de raison (typographique)*]({% post_url 2019-07-18-au-doigt-et-a-l-oeil %}), et j'aime bien créer une série.

[^2]: Je les conçois en chiasme : recherche passive pour la veille et veille active pour la recherche. Mais ce n'est que de l'idiosyncrasie, pas une théorie.

[^3]: Un bon exemple : [Deckset](https://www.deckset.com/).