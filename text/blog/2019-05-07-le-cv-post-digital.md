---
title: Le CV post-digital
date: 2019-05-07
---

Voici une nouvelle étape dans mon petit tour d’horizon des formes documentaires les plus répandues et leur adaptabilité aux techniques d’écriture/édition modulaire basées sur le balisage léger. Aujourd’hui, j’aborde le CV, avec une méthode valable pour tout le monde et un conseil spécifique pour la version académique.

Le titre de ce billet fait écho au livre d'Alessandro Ludovico, *Post-digital print* [@ludovico2013]. L'expression *post-digital* évoque l'idée que rien ne sera jamais plus comme avant pour l'imprimé : le numérique est passé par là et a bouleversé les méthodes de production, de diffusion et de consommation ; l'auteur insiste sur le fait que le numérique n'a pas fait disparaître l'imprimé, ce dernier ayant simplement évolué. C'est dans cette perspective que je voudrais parler d'un CV *post-digital*, c'est-à-dire fait avec des outils numériques mais pensé à la fois pour l'écran et le papier.

# Deux CV pour le prix d'un

Le principe que j’ai adopté est le suivant : rédiger son CV de façon très basique en Markdown et en faire une version HTML mise en forme via deux feuilles de style CSS.

Pourquoi deux CSS ? Pour faire d’une pierre deux coups ! Une CSS sert à afficher le CV sous forme de page web ; l'autre sert à mettre en forme le PDF qui sera produit en imprimant la page dans le navigateur. Elles ne s'appliquent jamais en même temps : la CSS destinée à l'impression prend le relais de celle destinée à l'écran. La version site du CV apporte de la visibilité en ligne et, en tant que page web, hérite des fonctionnalités qu’on apprécie sur ce médium (interactivité, zoom, etc.). La version PDF, qui reste exigée pour la plupart des candidatures, permet de s’affranchir de la connexion internet et de maîtriser l’apparence d’une éventuelle impression. En pratique, il y aura souvent peu de différences entre les règles de mise en forme contenues dans ces deux fichiers CSS. Pour l'impression, on aura tendance à atténuer l'usage des couleurs et à fixer la taille des caractères ; on peut également jouer sur la visibilité avec la propriété `display` afin de faire disparaître certains éléments propres au site web (menus, pied-de-page).

Un seul fichier est à la source des deux versions : c’est tout l’intérêt d’utiliser le balisage. Et le CV se prête particulièrement bien au balisage léger comme le Markdown, dont il n’utilise qu’une fraction des éléments : un ou deux niveaux de titre pour les rubriques (formation, emploi, compétences, etc.), des listes, un peu d’italique ou de gras, des liens. C’est facile à mettre en œuvre, la structuration est plus transparente que dans un traitement de texte et le document est bien plus lisible que du HTML par exemple.

Le style est une question de choix. J’aurais tendance à recommander quelque chose de simple et efficace mais élégamment réalisé. Une belle police, des proportions titres-texte ou marge-colonne qui ne sont pas définies au hasard : les détails comptent, dans un document qui n’a pas forcément vocation à démontrer une compétence graphique — quoique cela dépend des professions et de la personnalité.

# Mise en œuvre

Pour réaliser mon CV, je me suis basé sur [ce billet de blog](https://blm.io/blog/markdown-academic-cv/). En résumé, l’auteur reprend et simplifie une méthode où le Markdown est converti en HTML et affiché avec le navigateur, qui sert aussi à générer le PDF.

Cette solution utilise un générateur de site statique, programme qui prend en charge la création de la page web à partir du Markdown ([et qui propulse mon site](dr-jekyll-et-mr-tex.html)). Si on veut simplement le PDF, il n’y a même pas besoin de mettre la page en ligne : la commande `jekyll serve` permet de construire le site localement et de l’afficher à la volée dans le navigateur (où l’impression est accessible) en entrant l'adresse `localhost:4000`.

Comme j’utilise déjà Jekyll pour mon site, j’y ai intégré le CV. Le fichier source en Markdown est stocké avec les autres fichiers, Jekyll le transforme en page web, comme il le fait avec les billets de blog et la page à propos. La seule différence, c’est que cette page CV a ses propres feuilles de style comme expliqué plus haut : une pour l’écran (`screen`) et une pour l’impression/conversion en PDF (`print`). Les différences portent essentiellement sur les couleurs et la taille du texte.

Il existe bien sûr d’autres solutions basées sur une structuration en Markdown. La différence porte généralement sur les jeux d’écriture. Dans l’exemple que j’ai mentionné, les dates sont traitées comme un extrait de code *inline* ce qui permet de les positionner d’une façon particulière en ciblant l’élément `code` en CSS. Dans [une autre solution](http://there4.io/2012/12/31/markdown-resume-builder/), un agencement particulier des listes permet de disposer des rubriques en colonnes avec un titre et une description. Il existe autant de ces variations que de préférences d’écriture et il est tout à fait possible d’inventer les siennes[^1]. Il n'y a pas que l'éditeur qu'on peut détourner, la syntaxe aussi…

# Côté académique

Pour le CV académique, la rubrique « Publications » est centrale. Plus la carrière du chercheur est avancée, plus le remplissage va être difficile. Mais il n’est jamais trop tard pour mettre en place une approche robuste sur le long terme avec un logiciel de gestion bibliographique approprié (Endnote, Mendeley, Zotero, etc.).

Zotero inclut par exemple une catégorie « Mes publications » (voir [cette page](https://www.zotero.org/support/my_publications) dans la documentation) dans laquelle on peut glisser manuellement les références de ses propres publications à partir de sa bibliothèque. Le suivi est alors facilité : en triant par catégorie de document, il est plutôt simple de générer une bibliographie pour tous les articles, une pour tous les chapitres d’ouvrage, etc. Il n’y a plus qu’à insérer ces différentes bibliographies dans le CV[^2].

![CV version web](https://www.arthurperret.fr/img/2019-05-07-le-cv-post-digital-web.png)

Le CV a temporairement remplacé ma page Publications, qui datait un peu et n'était pas aussi bien structurée. En le rédigeant, je me suis dit que le CV académique était un redoutable outil de suivi de l'activité de recherche. La double exigence d'exhaustivité et de précision associée à ce document fait qu'on y passe un temps fou, mais le rend très utile comme point de référence : c'est un vrai portail à liens, qui pourrait presque se suffire à lui-même pour une présence en ligne — ce qui simplifierait d'ailleurs la publication sous forme de site (pas de menus à harmoniser, de flux RSS à paramétrer…).

![CV version PDF (double page)](https://www.arthurperret.fr/img/2019-05-07-le-cv-post-digital-pdf.png)

J'irais jusqu'à dire que de toutes mes expérimentations (publiées ou non), je recommanderais volontiers le CV aux curieux qui souhaiteraient tester le balisage léger et la modularité écran/imprimé : c'est un petit projet documentaire modeste mais très utile et qui fait son petit effet, tout en ayant une sérieuse marge de sophistication (côté mise en page). Certains étudiants le retrouveront peut-être dans mes prochains cours…

# Référence

[^1]: Si on apprécie la syntaxe Pandoc pour invoquer une classe CSS, on peut très bien l’intégrer au processus. La fonctionnalité *bracketed-spans*, assez simple d’utilisation, permet de faire beaucoup de choses. Ex : `[Petites capitales]{.smallcaps}`

[^2]: Les adeptes de l’automatisation totale peuvent bien sûr imaginer une méthode complètement automatisée, par exemple avec Pandoc… mais l’export et le copier-coller manuel restent très efficaces.
