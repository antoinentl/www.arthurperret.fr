---
title: Cosma, from record to graph
date: 2022-01-30
abstract: "Cosma is a hypertext visualization tool for academic Zettelkasten, now available in English. Here, I present its usefulness, originality, audience and use cases."
lang: en-US
---

In November 2020, I wrote a post entitled “[*De la fiche au graphe*](https://www.arthurperret.fr/fiche-graphe-documentaire.html)” (“From index cards to document graphs”) in which I mentioned that I was working on a visualization tool for interrelated notes (or records). There is no lack of such tools ([Roam](https://roamresearch.com/) and [Obsidian](https://obsidian.md) are among the most popular right now), but there is a niche of users for whom no solution seemed satisfactory at the time: academic writers working in plain text, maybe using [Zettlr](https://www.zettlr.com), probably using [Pandoc](https://pandoc.org), and maintaining a personal documentation they hope would outlive software trends and paid services.

I imagined that this audience, to which I belong, would be very much interested in interoperability, openness, and online sharing, but also in rich metadata and unique identifiers. I started thinking about an alternative to Roam and Obsidian that would include more knowledge organization features (indexing, categories for links and notes, bibliographies) and that would support a creative approach rather than a purely extractive one, i.e. focused on writing and reflexivity rather than on harvesting and analysing external data.

I started working on this idea with my colleagues Guillaume Brioudes, Clément Borel and Olivier Le Deuff. Soon, we had a prototype. And after a year, we released v1.0 of an application called Cosma.

Cosma represents interrelated notes as an interactive network in a web interface; in other words, it is a document graph visualization software, or a hypertext visualization tool. Yesterday, we released an update for Cosma, which is now available in English. I invite you to visit [the Cosma website](https://cosma.graphlab.fr/en/) for more information.

<figure class="fullwidth">
<figcaption>The main interface of Cosma : a menu, a graphe, and some records.</figcaption>
<img src="https://cosma.graphlab.fr/img/screenshot-cosma.png">
</figure>

# What does it do?

Cosma tackles the classic problem of personal knowledge management (PKM) through a very specific approach: note-taking, specifically interrelated notes. Many methods and tools already exist in this space. Cosma is closest to the Zettelkasten method, and to hypertextual objects such as wikis. But the solution we have designed has quirks which make it almost unique[^1].

[^1]: Greaby’s [Telescope](https://github.com/Greaby/telescope/) is a similar project, albeit with major differences (tag-based links instead of wiki-style links). If you know of any similar software in the space of interlinked notes, *tools for thought* or personal knowledge management (PKM), please let me know.

Firstly, **it's not a text editor**. I mentioned Obsidian, but a direct comparison would be flawed, as Obsidian is a full-fledged text editor. Cosma simply reads a directory of text files and generates an interactive visualization of the connections between these files. It is therefore meant to be used alongside a writing tool.

Why didn't we make a text editor? First of all, it was a **pragmatic** decision: we did not have the capacity to develop a text editor, which is an extremely complex thing to make, and which would face steep competition. It was also an **ideological** decision: I have a preference for software that lets me choose my writing tool, like [Deckset](https://www.deckset.com) does for presentations. The pragmatic aspect allowed me to push the ideological preference into the design process, so to speak. But it is also a **strategic** choice: by creating a tool that is not in direct competition with text editors, and is not an online application either, we have entered an extremely small niche, alongside projects such as [Zkviz](https://github.com/Zettelkasten-Method/zkviz) and [Telescope](https://github.com/Greaby/telescope/). The advantage is that Cosma competes with almost no other workflow, and can therefore be of interest to many people. Better still, it represents an alternative to paid sharing services such as [Obsidian Publish](https://obsidian.md/publish).

Secondly, with Cosma **there is an app within the app**.

Let me explain. Cosma (`Cosma.app` or `Cosma.exe`, depending on your operating system) is an application. But its main function is to generate and display an HTML file, which we call a *cosmoscope*, and this file actually contains the whole interface. It includes notes, rendered in HTML, an interactive graph, but also contextualised links and backlinks, an index, a search engine and the ability to filter the display by record type and by keyword occurrence. But most importantly, **this is a standalone file**: it can be exported from Cosma, and used as is. It does not require an Internet connection, and can be shared as you would share any file: by email, messaging, or even FTP'd to a web server, like [our demo](https://cosma.graphlab.fr/demo.html)). Thus, whether you are exploring the cosmoscope from within Cosma on you computer, or navigating an exported cosmoscope someone shared with you, you see the exact same thing, and you have the exact same heuristic capabilities.

<figure class="fullwidth">
<figcaption>The cosmoscope at the heart of Cosma: a standalone HTML file.</figcaption>
<img src="https://hyperotlet.huma-num.fr/cosma/img/cosma-cosmoscope-html.png">
</figure>

# Who is it for?

Cosma is designed first and foremost for knowledge workers: people who accumulate documentation on a topic, and plan to share it or parts of it. Cosma's primary role is to provide a better global view of a collection of interrelated notes, in a form that is easy to share, while still remaining feature-rich.

Doctoral students like myself stand to benefit a lot from such a tool: a PhD is a great opportunity to start a personal documentation from scratch, learning new tools along the way. However, and even though we started from this very specific use case, we ended up with an application that is flexible enough to be useful to different people: researchers, teachers, students, librarians, engineers, journalists… Cosma will be of particular interest to those who want to create and disseminate expert knowledge in an original way[^2].

[^2]: Original but not necessarily new: according to my PhD supervisor, mind map software from the 2000s and 2010s did offer similar sharing functionality (interactive HTML exports).

What kind of knowledge are we talking about? Given the target audience, it will most often consist of densely interrelated descriptions of things, like a wiki. We designed Cosma with two main uses in mind: representing small collections, like a lexicon or a glossary of terms interlinked through their definitions; and representing vast personal documentation, meant to be accumulated over years. These two things do not exclude each other. And in both cases, Cosma can handle anything from a few dozen records to several hundreds or thousands. Most importantly, it allows users to define arbitrary categories of notes and links: this allows people to tweak the application to their specific use case.

# How to begin?

Cosma works particularly well in tandem with a text editor that has both hypertextual and scientific writing features, such as [Zettlr](https://zettlr.com). This is only natural: the starting point of the project was the desire for a graph view similar to Obsidian's but adapted to content created via Zettlr. Cosma and Zettlr inhabit the same technical ecosystem for writing format, identifiers, metadata and citations.

However, you don't have to use Zettlr: any text editor will do, and there are other, similar solutions based on VS Code for example (e.g. [Foam](https://foambubble.github.io/foam/)). You can download [a sample](https://github.com/graphlab-fr/cosma/releases/download/1.0/cosma-fiches-aide.zip) of notes, open them in a text editor and start inserting a few links by hand; check [Cosma's user manual](https://cosma.graphlab.fr/en/docs/user-manual.html) to find out how they should be written. Your choice of editor may be influenced by their support of wikilinks and explicit unique identifiers: links can be fastidious to create manually, or conversely, they can become extremely quick and pleasant to create with the right tool.

# Why “Cosma”?

The name of the application comes from two notable figures in the history of knowledge organization.

First is Cosma Rosselli, author of a *Thesaurus artificiosæ memoriæ* (1579) cited by Umberto Eco [-@eco2010, 121-122], which literally translates as “artificial memory treasure”—a pretty good description for Cosma.

[^paulotlet] And then of course there is Paul Otlet, the subject of my doctoral research. When designing Cosma, we had a diagram of Otlet in mind, in which he represented his Mundaneum, a “machine to think the world”, as consisting of two parts: a “cosmograph” and a “cosmoscope”, respectively allowing knowledge to be recorded and consulted. The architecture of Cosma is based on the same principle: the app is a JavaScript engine, the cosmograph or cosmographer, which builds HTML files, the cosmoscopes. So we borrowed the names from Otlet. Cosma is a tool that allows us to record and visualize a universe of ideas, which is a nod to Otlet's work.

[^paulotlet]: {-} ![](../img/hyperdocumentation-2.jpg) The full diagram is reproduced in a paper titled “[Hyperdocumentation](https://www.arthurperret.fr/hyperdocumentation.html)” which we published in the *Journal of documentation*.

# Cosma's future

Cosma is an experimental project, initially funded by the [HyperOtlet](https://hyperotlet.hypotheses.org) research programme, itself supported by a grant from France's *Agence nationale de la recherche*. This has several implications. First, it is free software under the conditions allowed by the GNU GPL 3.0 licence. However, as the funding was one-off, we cannot guarantee any evolution or even maintenance. So another implication is that Cosma is offered in an open way to a community of curious practitioners, who are hopefully willing to put up with some inevitable bugs that may take some time to be fixed, and to extend the project themselves.

However, this is a major development for my own research, and for [Guillaume](https://myllaume.fr), who develops the application. So we plan to continue working with (and on) Cosma in the coming months and years. Speaking for myself, I will do my best to carry this project a little bit further, because I adhere to [a perspective outlined by Andy Matuschak](https://www.arthurperret.fr/veille/difficile-progression-tools-for-thought.html), a tools for thought expert: we should create tools so that they generate observations that can be generalized in the context of scientific reflection.

# Reference