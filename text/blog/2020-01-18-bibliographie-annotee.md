---
title: La bibliographie annotée
date: 2020-01-18
abstract: "Quelques éléments pour s'approprier la bibliographie annotée, de l'idée à l'édition."
---

Une bibliographie annotée (ou bibliographie commentée) consiste en une liste de références, chacune faisant l'objet d'un commentaire par la personne qui se livre à l'exercice. Comme disait (à peu près) Clint Eastwood, le monde des bibliographies annotées se divise en deux catégories, avec des variables communes (longueur, structure) et quelques différences notables.

Lorsqu'elle précède un travail de recherche, la bibliographie annotée est assimilable à un état de l'art (*literature review*). Elle obéit à une certaine exigence d'exhaustivité et de cohérence. L'expression décrit alors le plus souvent un exercice donné à des étudiants, assez répandu dans le monde universitaire anglo-saxon mais moins en France (à ma connaissance).

À l'inverse, il y a la bibliographie annotée qui est le fruit d'une longue accumulation de savoirs : souvent rédigée par un chercheur à la demande de ses étudiants, collègues ou amis, elle adopte un ton plus informel voire introspectif. Elle est souvent préfacée d'un avertissement au lecteur qui le prévient du caractère très personnel de la sélection. Elle présente alors d'autres vertus : c'est un bon exercice de réflexivité pour son auteur et une ressource intellectuelle d'une plus grande utilité publique.

Le second type de bibliographie annotée constitue l'un de mes objets documentaires préférés et ce pour deux raisons. La première, c'est qu'elle provoque un pur plaisir de bibliophile : on se réjouit de découvrir de nouveaux auteurs et on anticipe avec gourmandise le prochain ravitaillement de bouquins en bibliothèque ou en librairie. L'autre raison est d'ordre sociologique. La bibliographie est un véritable carnet d'adresses – littéralement, si elle contient des liens[^1]. Pour l'auteur, elle ne sert pas seulement à rendre à César ce qui est dû à César mais également à faire valoir un certain cercle social intellectuel, avec des copains, des rivaux, quelques morts illustres, deux ou trois inconnus. Au lecteur, elle fournit des éléments d'interprétation de la personnalité de l'auteur, de son idéologie ou de sa couleur préférée. Bref, j'apprécie cet objet car il permet de *faire connaissance*.

Il ne faut pas craindre les sentiments contradictoires suscités par la bibliographie annotée, à rapprocher du vertige de la liste. En tant que lecteur, je ressens généralement une admiration mêlée de jalousie vis-à-vis du portrait bibliographique d'un grand lecteur – la somme fabrique la sommité. Mais une utilisation raisonnée de l'objet fait vite le ménage là-dedans : la bibliographie annotée est faite pour être moissonnée de manière sélective, au gré de ses besoins, avant de replonger dans le courant de sa propre écriture.

# En pratique

Les gestionnaires de références sont extrêmement utiles pour collecter, mettre en forme, diffuser et exploiter une bibliographie annotée. Il peuvent servir simplement à partager les données : Éric Guichard a publié [en PDF](http://barthes.enssib.fr/outils/Biblio-numerique-Guichard.pdf) une bibliographie annotée sur l'histoire de l’écriture et la philosophie de la technique pour la question du numérique ; il a également transmis le fichier BibTeX à ses collègues du laboratoire Triangle, qui en ont fait [une bibliothèque partagée Zotero](https://www.zotero.org/groups/triangle-enjeux-ecriture-numerique). Mais on peut également créer une bibliographie à partir de conseils de lecture donnés de manière non structurée. Sur Twitter, il est courant de tomber sur un fil qui mériterait d'être transformé en bibliographie. Prenons l'exemple des recommandations d'ouvrage sur la sémiotique par Marc Jahjah [sur Twitter](https://twitter.com/marc_jahjah/status/1218115824445247489) : chaque tweet contient l'appréciation d'un livre et est illustré par une image de sa couverture, ce qui permet de retrouver en une seconde la référence [dans le Sudoc](sudoc.abes.fr/) pour l'importer dans Zotero. Là aussi, cela permet de proposer [une bibliothèque partagée](https://www.zotero.org/groups/semiotique) en complément du fil initial. Mais allons un peu plus loin.

On peut réutiliser le contenu du tweet (auteur, date, contenu, lien vers l'image, etc.)[^2] pour en faire une note attachée à la référence. Zotero permet de générer un « rapport » à partir d'un ensemble de références sélectionnées, sous la forme d'un fichier HTML qui reproduit les métadonnées de chaque référence ainsi que le contenu des notes qui lui sont attachées. Il existe [des ressources](https://www.zotero.org/support/plugins#zotero_reports) pour le customiser via une interface graphique mais l'export est tout à fait gérable manuellement avec un bon éditeur de texte. Le résultat est une page web transformable en PDF via la fonction d'impression du navigateur.

![Rapport Zotero des conseils de lecture de Marc, version HTML. Le titre et les premiers paragraphes sont de moi, le reste est généré par Zotero.](https://www.arthurperret.fr/img/2020-01-18-bibliographie-annotee-1.png)

![Version PDF créée par impression via Firefox. Une pagination correcte repose sur un usage approprié de `@media print` et `page-break-inside: avoid`.](https://www.arthurperret.fr/img/2020-01-18-bibliographie-annotee-2.png)

La feuille de style utilisée pour mettre en page cet exemple est une version combinée et légèrement retouchée des trois CSS utilisées par Zotero pour les rapports. Elle est [en ligne](https://gist.github.com/artorig/4c4fd8c3cb88be75a5a44ff81f1a04d4).

# Allons plus loin

[^edit]Le rapport Zotero est un outil sympathique mais un peu limité. Or ~~la sorcellerie~~ l'ingénierie documentaire devient encore plus efficace quand on lui donne des outils à sa mesure et pleinement pensés pour l'édition de contenus bibliographiques et scientifiques : CSL et Pandoc.

Lorsque j'écris en Markdown (y compris pour générer les pages de ce site), j'inclus des références dans mon texte via leur clé de citation (exemple : `@auteur2019`). Je délègue à Pandoc la conversion de ce fichier « source » en autre chose (page web, PDF, docx…) et les clés de citation sont alors transformées suivant les instructions d'une feuille de style CSL. Le CSL est un format standardisé reposant sur XML et dans lequel peuvent être exprimés des styles très connus comme APA, Chicago ou ISO-690 ainsi que des milliers de variantes. Une [interface visuelle en ligne](https://editor.citationstyles.org/visualEditor/) existe pour chercher et personnaliser ces styles afin de les adapter à une grande variété de situations. Moyennant un peu d'huile de coude, c'est un outil qui permet d'obtenir exactement ce qu'on veut, y compris des choses farfelues. À titre d'exemple, [les citations sur ce site]({{ '/2019/12/17/citations-marginales/' | base_url }}) sont le produit d'un style CSL qui combine note, auteur-date et titre – le résultat ressemble à ceci [@carlino2018 (quitte à citer juste pour faire un exemple, autant glisser une lecture utile)].

Les feuilles de style CSL peuvent utiliser les annotations au même titre que l'auteur, le nombre de pages, l'année ou n'importe quelle autre métadonnée : il suffit d'ajouter `<text variable="…"/>` à l'endroit du fichier CSL qui régit la mise en forme des citations au fil du texte et/ou dans celui qui concerne la bibliographie à la fin. Dans Zotero, les annotations peuvent être renseignées dans le champ « Extra » d'une référence (la variable CSL correspondante est `note`) ou bien se trouver sous forme de note attachée à la référence (la variable est alors `annote`). Pour mon exemple, nous sommes dans le second cas de figure. En modifiant un fichier CSL de la manière suivante, la bibliographie incluera les notes sous forme de paragraphes à la suite de chaque référence :

```xml
<style>
  …
  <bibliography>
    <layout>
      …
      <text variable="annote" display="block"/>
    <layout>
  </bibliography>
</style>
```

Il reste à créer le document que cette feuille de style bibliographique viendra mettre en forme. C'est là qu'intervient mon astuce préférée : en Markdown, le fichier peut se résumer à trois lignes de métadonnées – titre, date et références. En effet, Pandoc a une option `nocite` qui permet d'inclure directement dans les métadonnées une sélection de références que l'on ne compte pas citer au fil du texte mais qu'on veut faire apparaître quand même dans la bibliographie à la fin. Cela signifie qu'on peut créer le fichier, se contenter de renseigner les métadonnées et s'arrêter là : les références listées dans `nocite` constituent l'intégralité du « texte » ![^yaml]

```markdown
 ---
 title: Sémiotique - Bibliographie annotée
 date: 07/02/2020 10:11
 nocite: |
  @ablali2009, @barthes1970, @berthelotguiet2015, @biglari2014,
  @biglari2018, @boutaud2007, @cassin2004, @compagnon1979,
  @deniau2008, @ducrot1972, @eco2001, @fabbri2008, @floch2003,
  @fontanille2008, @fontanille2015, @hebert2007, @henault2004,
  @jeanneret2014, @klinkenberg1996, @kohn2017, @marin1981,
  @marrone2016, @martin1999, @rastier2001, @souchier2003,
  @trifonas2015, @verhaegen2010
 ...
```

![Version docx, générée cette fois-ci avec Pandoc à partir du texte en Markdown ci-dessus.](https://www.arthurperret.fr/img/2020-01-18-bibliographie-annotee-3.png)

L'approche texte brut est assez versatile. Considérons la bibliographie d'Éric, que j'ai mentionnée plus haut. Elle a été conçue très différemment de celle que j'ai concoctée à partir des tweets de Marc : il s'agit d'un texte, avec une introduction et des sections ; l'auteur a utilisé la commande `\fullcite` de LaTeX pour citer chaque référence sous sa forme complète et le commentaire qui suit a été rédigé spécifiquement pour ce document-là (au lieu d'être stocké avec la référence dans un logiciel comme Zotero). On peut tout à fait passer par CSL, Markdown et Pandoc pour imiter cette logique : on utilisera un style CSL dans lequel la citation au fil du texte produit la référence complète (afin d'imiter le comportement de `\fullcite`) et on ajoutera `suppress-bibliography: true` aux métadonnées, car on n'a pas besoin de générer la bibliographie habituelle en fin de document. Il restera à régler la mise en page en fonction du format de sortie. La bibliographie restant par essence une liste, on pourra par exemple jouer sur les différents types d'énumération (ordonnée, non ordonnée, liste de définitions) pour obtenir différents types de rendu :

![Bibliographie annotée en Markdown et son export HTML. Notez la différence entre la syntaxe produisant une liste normale (première section) et celle qui produit une liste de définitions (deuxième section).](https://www.arthurperret.fr/img/2020-01-18-bibliographie-annotee-4.png)

***

Étonnant, non ? La bibliographie annotée est un objet que j'aimerais rencontrer plus souvent. Je pense qu'elle va faire son chemin dans mes enseignements, car elle permet de mobiliser plusieurs petits bouts de savoir-faire (édition en texte brut, mise en page) en contextualisant des notions courantes en SIC (donnée, document, redocumentarisation…).

[^1]: Toute bonne bibliographie se doit de fournir pour chaque œuvre citée l'identifiant de la *manifestation* utilisée (sur ce terme, cf. [modèle IFLA-LRM](https://fr.wikipedia.org/wiki/Mod%C3%A8le_de_r%C3%A9f%C3%A9rence_IFLA_pour_les_biblioth%C3%A8ques)). Les styles (APA, ISO-690…) peuvent ajouter automatiquement le protocole et l'adresse qui feront de cet URI une URL. Exemple : pour un DOI comme `10.1353/lib.2013.0034` le logiciel peut générer un lien formé par ajout du préfixe `https://doi.org`.

[^2]: Ces métadonnées s'obtiennent facilement et proprement via la fonctionnalité d'*embed* de Twitter  ([exemple](https://publish.twitter.com/?hideConversation=on&lang=fr&query=https%3A%2F%2Ftwitter.com%2Fmarc_jahjah%2Fstatus%2F1218152964587696129&widget=Tweet)).

[^edit]: {-} NB: section ajoutée le 10/02/2020 après un accueil plutôt chaleureux sur Twitter, ce qui m’a encouragé à approfondir.

[^yaml]: {-} NB: attention, les trois points à la fin du code ci-contre ne sont pas là pour indiquer une ellipse, il s’agit du délimiteur de fin des métadonnées. Ce que vous voyez constitue l’intégralité du document, au caractère près.

# Référence 