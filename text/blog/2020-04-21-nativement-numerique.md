---
title: Nativement numérique
date: 2020-04-21
abstract: "Une courte discussion de cette expression dont les déplacements ne sont pas anodins."
---

Il y a quelques années, Camille Paloque-Berges a utilisé l'expression « nativement numérique » pour désigner « les sources originaires des outils et usages numériques[^1] ». Elle en a précisé les contours dans un article :

> « Les traces, données, documents, et artefacts issus directement du contexte numérique, et pouvant acquérir une valeur de source pour le chercheur en SHS […] Expression traduite et adaptée de l’anglais “born digital” forgée pour répondre au besoin de nommer les nouvelles productions numériques candidates à la patrimonialisation (UNESCO, 2003) […] Nous distinguons ces sources numériques natives (SNN) des sources issues d’une étape de numérisation ou d’encodage par le numérique de documents existant au préalable[@paloqueberges2016, 1] ».

Cette expression s'applique au patrimoine mais a commencé à susciter un intérêt dans les sciences de l'information au sens large. La revue [*Balisages*](https://publications-prairial.fr/balisages/index.php?id=224) a fait son premier numéro sur les « objets nativement numériques ». Et ce matin, j'ai lu [un tweet](https://twitter.com/kembellec/status/1249676720833277952) de Gérald Kembellec, dont j'attends le mémoire d'HDR avec impatience, dans lequel il qualifie le logiciel Stylo d'« exemple d'écriture scientifique nativement numérique et sémantisable ». Que la référence à Paloque-Berges soit explicite ou non, je constate que « nativement numérique » commence à essaimer. Est-ce le début d'un voyage conceptuel prometteur ? Peut-être, mais il me semble qu'une discussion est nécessaire au préalable.

En effet, l'expression « nativement numérique » a été forgée pour des choses que l'on désigne comme « objets » pour une raison précise : ce sont des objets documentaires, considérés dans une perspective archivistique, et pour lesquels la polysémie du mot « numérique » est réduite à cette seule dimension[^2]. De ce périmètre précis résulte un concept robuste et opérant.

L'application de cette expression à l'écriture déborde totalement du cadre, car il s'agit d'une technique et pas d'un objet documentaire. Pour autant, la problématique d'une écriture et, au sens plus large, d'une technique nativement numérique me paraît tout à fait heuristique. Elle se situe au niveau de ce que Kim Sung-do appelle « un carrefour interdisciplinaire des sciences humaines constitué autour de l’écriture » [@kim2018, 157], où la philosophie de la technique occupe une place importante[^3]. Si la binarité associée aux objets documentaires n'est pas applicable à l'écriture comme technique, on peut identifier différentes distinctions complémentaires : certaines technologies transposent les logiques héritées de l'imprimé, d'autres s'en détachent, d'autres encore mélangent les deux ; sous l'angle du *mode d'écriture* et des interfaces, d'autres délimitations plus ou moins poreuses apparaissent, tout aussi valides. En fonction de la problématique formulée, cela peut constituer une exploration instructive.

Mais entre le concept initial et une problématisation sous l'angle de la technique, il existe une zone intermédiaire qui me semble truffée de chausse-trapes. Le numéro de *Balisages* mentionné plus haut opère une généralisation, en passant de « sources » en « objets » et « productions », mais ne la discute pas. Je crois pourtant que ce glissement conceptuel produit un effet de magnitude similaire à la substitution de « source » par « écriture » : le cadre initial est totalement débordé, et l'article d'Antoine Fauchié [@fauchie2020], le seul à s'engouffrer véritablement dans la brèche, le montre bien.

Il y a donc là une opportunité à explorer mais, à mon humble avis, seulement si on reconstitue autour un cadre théorique adapté au changement de périmètre. Les SIC sont coutumières de ce genre de circulation conceptuelle. Certains déplacements s'avèrent fructueux car la terminologie est originale, et que l'emprunt procède d'une idée forte et fait l'objet d'un investissement intellectuel sur le long terme (voir par exemple « architexte »). D'autres sont plus accidentés, souvent parce que le point de départ utilise des mots courants, vite dilués (« dispositif »). À titre personnel, je trouve que le jeu en vaut la chandelle, et je suis reconnaissant aux gens qui opèrent ces déplacements, car le résultat est toujours stimulant.

# Références

[^1]: Source : <https://calenda.org/267910>

[^2]: La réduction à un statut binaire numérique ou non-numérique n'est pas du tout stérile en documentation, elle a notamment donné le concept de redocumentarisation.

[^3]: Voir par exemple les travaux d'Éric Guichard (cité par Kim) [sur HAL](https://hal.archives-ouvertes.fr/search/index/?q=%2A&authIdHal_s=eric-guichard&sort=producedDate_tdate+desc) et [sur son site](http://barthes.enssib.fr/articles/).