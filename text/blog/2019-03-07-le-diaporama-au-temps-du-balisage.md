---
title: Le diaporama au temps du balisage
date: 2019-03-07
abstract: "Jusqu’à présent, j’ai écrit sur des techniques d’écriture et d’édition appliquées à une famille de communications scientifiques plutôt homogène – article, mémoire, thèse ou livre. Un autre type de communication extrêmement répandue en sciences est la présentation (ou diaporama). Je montre ici comment on peut y appliquer le même genre de processus que dans les billets précédents, avec quelques spécificités vraiment enthousiasmantes. Plutôt qu’un tutoriel, il s’agit de partager à chaud quelques lignes de code et les premières questions soulevées par l’expérience."
---

# Un mot sur PowerPoint

Edward Tufte a écrit une critique acerbe de ce qu’il appelle la pensée PowerPoint dans un essai intitulé [*The cognitive style of PowerPoint*](https://www.edwardtufte.com/tufte/books_pp)[^1]. Selon lui, le célèbre logiciel de Microsoft est conçu comme un miroir de l’entreprise informatique, avec une structure hiérarchique et un style orienté vers le marketing. Il y voit une logique anti-pédagogique qui se met systématiquement en travers du contenu et du spectateur. Il critique particulièrement la liste à puces, qui comprime l’information en effaçant les liaisons entre items. Or ce sont précisément les relations qui contiennent toute la valeur analytique d’un raisonnement. Selon Tufte, les flèches sont préférables à la liste car elles indiquent une causalité ou une séquence ; bien souvent, une phrase avec sujet, verbe et complément sera tout aussi efficace. Explication, raisonnement, découverte, questionnement, contenu, preuve, autorité crédible… indépendamment des qualités de l’orateur, Tufte juge que PowerPoint ne permet pas d’investir pleinement ces ressorts essentiels à une bonne communication.

Ce texte a le mérite d’attirer notre attention sur des mauvaises pratiques qui ne sont pas exclusives à PowerPoint et qu’il est assez facile de reproduire avec d’autres outils. Il faut donc rester vigilant sur la qualité de la communication que l’on élabore, indépendamment de la critique faite sur un outil précis. Ceci étant dit, difficile de retourner à PowerPoint après un brûlot pareil. C’est bien en quête de nouveaux outils qu’on ressort du texte de Tufte.

# Approche alternative basée sur le balisage

Parmi les alternatives à PowerPoint, les outils qui permettent d’écrire une présentation dans un langage à balises constituent une approche très distincte et qui ouvre plusieurs possibilités nouvelles. Il faut avoir goûté au moins une fois dans sa vie au paradis que représentent les diaporamas *écrits*, au sens où ils sont rédigés en texte brut : cela garantit la possibilité de modifier à la volée la mise en page d’un certain type de contenu sur plusieurs dizaines de slides, sans rien faire de plus que modifier la valeur d’un paramètre global ou appliquer une expression régulière très simple, sans se casser les pieds avec le système de *templates* de PowerPoint.

Ainsi pendant un moment, j’ai fait des diaporamas avec Beamer, la classe LaTeX prévue à cet effet. Néanmoins Beamer a de gros inconvénients, comme LaTeX en général : la syntaxe est lourde, toute personnalisation graphique est laborieuse et il n’y a qu’un format de sortie – le PDF.

Le format HTML permet de créer des présentations avec une plus grande souplesse, notamment parce que la mise en page s’adapte à la taille de l’écran mais également parce que la personnalisation en CSS est beaucoup plus accessible et mieux documentée. Le HTML embarque quelques fonctionnalités qui deviennent rapidement indispensables, comme la transclusion[^2]. C’est aussi un environnement qui accueille particulièrement bien certains types de contenus, comme les extraits de code, sans pour autant orienter l’écriture d’entrée de jeu comme le fait PowerPoint avec la liste à puces. Globalement, l’attention peut être davantage portée sur la qualité informationnelle de la communication, afin d’élaborer un support qui facilite l’approche analytique : contenu au centre, images denses, interactivité, etc.

Il est tout à fait possible d’écrire directement en HTML mais pourquoi s’infliger pareille torture quand on peut utiliser un langage plus léger comme Markdown et laisser Pandoc fabriquer le HTML automatiquement ? Voici un exemple qui décrit comment faire un diaporama avec [Reveal](https://revealjs.com/)[^3]. Le processus est le suivant :

1. rédaction en Markdown
2. export en HTML
3. bonus : export en PDF

## 1. Rédaction en Markdown

Reveal permet de disposer les slides suivant une séquence linéaire qui défile horizontalement mais également de positionner des slides verticalement (*nested slides*). Pandoc utilise les niveaux de titre du document Markdown pour établir cette structure. Il faut choisir un niveau qui sera le niveau de référence (par exemple : titre de niveau 3, `h3` en HTML et `###` en Markdown) et s’y tenir. Chaque titre indique le début d’une slide et tout ce qui est rédigé à la suite sera contenu sur cette slide. Si on utilise 2 niveaux de titre, le niveau inférieur est considéré comme celui de référence et le niveau supérieur sert à sectionner le diaporama[^4].

La phase d’écriture est toujours délicate, puisqu’il s’agit de réduire au maximum la dette technologique, c’est-à-dire les petites adaptations, les rustines, béquilles et bricolages qui constituent des solutions raisonnables à court terme mais deviennent un cauchemar lorsqu’il faut migrer plusieurs années de contenus vers un autre environnement technique. Ici, l’auteur a abandonné l’idée d’écrire en Markdown « pur », c’est-à-dire sans parfum et sans mélanges : à certains endroits, il faut écrire du HTML, point. La dette se paiera plus tard.

En dehors des petits caractères habituels en Markdown (`# * _ >` et autres), on trouvera donc de la transclusion :

```html
<iframe data-src="http://abecedaire.enssib.fr" class="stretch"></iframe>
```

Autre exemple qui combine une astuce de mise en forme à base de `span` et l’inclusion d’une option de Reveal (couleur de fond) via une syntaxe spécifique à Pandoc :

```html
# <span class="italic">hyper</span>-texte {data-background="#002b36"}
```

L’idéal reste d’investir du temps dans la mise au point d’une feuille de style personnalisée qui joue sur les éléments HTML et les sélecteurs CSS. À titre personnel, cela se fait au fil de l’eau : de présentation en présentation, j’expérimente de nouvelles choses, révise certains besoins et adapte mon code en conséquence.

## 2. Export HTML

Voici un exemple de commande Pandoc sur le modèle de celles que j’ai partagées précédemment. J’ai inséré un retour à la ligne avant chaque option et je les explique en-dessous.

``` {.numberLines}
pandoc prez.md
-t revealjs
-s
-o prez.html
--filter=/usr/local/bin/pandoc-citeproc
--bibliography /…/biblio.bib
--csl /…/apa-fr.csl
-c /…/reveal-custom.css 
-V revealjs-url=/Users/aperret/Code/reveal.js
-V controls=false
-V progress=false
-V history=false
-V theme=solarized
```

1 : Nom du fichier source  
2 : Format de sortie (Reveal)  
3 : *Standalone* (fichier autonome)  
4 : Nom du fichier à créer  
5 : Filtre Pandoc qui transforme les appels de citation  
6 : Emplacement des références bibliographiques  
7 : Style bibliographique à appliquer  
8 : CSS personnalisée qui s’applique par dessus les styles de Reveal  
*9 et suivants : options de Reveal*  
9 : Emplacement des scripts et styles Reveal  
10 : Masquer les contrôles  
11 : Masquer la barre de progression  
12 : Ne pas enregistrer chaque défilement comme une nouvelle page dans l’historique du navigateur (peut paraître banal mais devrait être désactivé par défaut !)  
13 : Thème graphique appliqué (éventuellement modifié par la feuille de style personnalisée)  

## 3. Export PDF

Là où Beamer est trop orienté PDF, Reveal ne l’est pas assez. La solution référencée dans la documentation de Reveal nécessitait un peu trop d’huile de coude à mon goût, j’ai préféré déléguer entièrement le travail à un petit échafaudage de programmes. Dès qu’on tâte un peu des technologies du web, on se rend vite compte qu’il faut sacrifier à une certaine logique de l’écosystème, avec des bibliothèques de programmes dans lesquels on va piocher en fonction de ses besoins[^5].

**Étape 1 :** capturer chaque slide avec [Decktape](https://github.com/astefanutti/decktape).

```
decktape automatic prez.html prez.pdf --screenshots --size 1920x1080
```

**Étape 2 :** renommer les 9 premières slides avec une numérotation à 2 chiffres (01, 02…).

**Étape 3 :** assembler les captures en PDF avec `convert` ([ImageMagick](http://www.imagemagick.org)).

```
convert * prez.pdf
```

# L’horizon impossible

Un collègue à qui j’exposais le procédé décrit ci-dessus me demandait s’il serait possible de produire, à partir d’un même fichier, la présentation et un support linéaire, ce dernier n’étant pas une simple impression des *slides* avec les notes en dessous mais un authentique document. Ce couple présentation + texte (distribué si possible à l’avance) est d’ailleurs recommandé par Tufte, qui y voit un meilleur moyen de communiquer en dédiant chaque forme de communication aux informations qui s’y épanouissent le mieux. On pourrait imaginer une forme de super-document hybride qui combine le contenu de plusieurs de ces formes, chacune étant balisée de manière spécifique. Une implémentation existe déjà : on peut ajouter des notes de présentation en Pandoc Markdown, qui seront traduites en tant que telles dans l’affichage en mode orateur ; exporter le contenu de ces notes de façon linéaire est l’affaire d’un script.

Pour autant je ne suis pas emballé. Quels formats de sortie peut-on investir à partir d’une même communication ? Et inversement, combien de formes de communication différentes peut-on élaborer sur un même principe de modularité et de balisage ? La double question n’est pas anodine : on est tenté de voir dans toutes ces techniques la promesse d’un Otlet ou d’un Berners-Lee, c’est-à-dire le graphe comme matrice documentaire, une architecture commune à toute communication potentielle – imprimée, numérique, textuelle, graphique, linéaire, arborescente, sous forme de livre ou de diaporama, de listes ou de fiches, de cartes ou de schémas. Du mycélium, la forêt. Matrix *Reveal*-ations !

L’irréductible spécificité de chaque geste éditorial me laisse penser que cet horizon est non seulement impossible mais peut-être même indésirable. C’est dans la diversité de ses incarnations que la technologie intellectuelle se révèle féconde, permettant le jeu entre différents niveaux d’abstraction, différents régimes d’énonciation. J’ai aussi en tête les échecs relatifs de la pasigraphie ou de l’espéranto. Je ne vois pas le balisage comme préambule à une écriture totale. C’est une condition (parmi d'autres) à l’émergence de nouvelles technologies de l’intellect basées sur de l’écriture. Et chaque forme de communication obéit à des logiques très différentes, parfois strictement non-documentaires.

Néanmoins c’est une question sur laquelle je n’ai pas d’avis définitif et qui reviendra sûrement au fil de prochaines expérimentations individuelles ou collectives, peut-être via un article plus rigoureusement documenté. Affaire à suivre !

[^1]: À l’origine, ce texte forme le septième chapitre de [*Beautiful Evidence*](https://www.edwardtufte.com/tufte/books_be). Le texte a été réédité depuis et on peut très bien le parcourir sans avoir lu le livre.

[^2]: La transclusion permet d’intégrer dans un environnement web du contenu présent ailleurs, grâce à son adresse. Au niveau élémentaire, on peut afficher une vidéo hébergée sur YouTube. Mais cela va beaucoup plus loin : on peut très bien afficher une page entière et interagir avec comme si l’on avait ouvert un navigateur internet au beau milieu de la diapositive.

[^3]: Reveal est un programme composé de scripts et de feuilles de style qui permet d’afficher un document HTML sous forme de présentation. C’est *open source*, réutilisable facilement et très ouvert à la personnalisation. Son créateur, Hakim El Hattab, a également lancé une version commerciale, [Slides](slides.com).

[^4]: La [documentation de Pandoc](http://pandoc.org/MANUAL.html#producing-slide-shows-with-pandoc) est beaucoup plus claire que moi. Et le visualiser est encore plus efficace.

[^5]: Je viens d’une culture de la ligne de commande et du copier-coller, avec des lacunes sur des notions fondamentales (architecture client-serveur notamment) qui me poussent à utiliser des solutions intermédiaires.