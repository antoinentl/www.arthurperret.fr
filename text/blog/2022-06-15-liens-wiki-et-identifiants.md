---
title: Liens wiki et identifiants
date: 2022-06-15
description: "Méfions-nous du marketing : les données doivent survivre aux outils."
---

Il y a deux ans, j’ai publié un billet intitulé « [Écrire et éditer](2020-05-22-ecrire-et-editer.html) ». J’y faisais le point sur ma pratique d’écriture : après quelques années à pratiquer le format texte et notamment le langage [Markdown](../cours/markdown.html), je venais de découvrir de nouveaux outils hypertextuels comme [Zettlr](https://www.zettlr.com/).

Ce type d’outils repose sur des liens façon wiki (*wikilinks*), c’est-à-dire des liens internes aux fichiers – par opposition aux liens qui pointent vers le web –, utilisant une syntaxe héritée des wikis, et permettant à terme de créer une base de connaissances interreliées. Exemple : `[[Texte du lien]]`, où le texte du lien peut être le titre du fichier cible, ou bien un identifiant arbitraire. Convaincu par cette forme réticulaire d’écriture, je me suis lancé dans ce que j’appelle aujourd’hui une documentation personnelle hypertextuelle de mon travail de recherche.

Pour faciliter la création de ce genre de liens, les éditeurs de texte incluent généralement une fonctionnalité que j’avais qualifiée dans mon billet de « petit bijou de sorcellerie logicielle » (j’étais très enthousiaste) : en pressant une touche, un champ de recherche s’ouvre au niveau du curseur ; on y tape quelques caractères, le logiciel propose des titres de fichier qui correspondent ; quand on a trouvé le bon, la touche Entrée (ou bien un clic) permet de créer le lien.

J’aime beaucoup adapter mon éditeur de texte favori, [BBEdit](https://www.barebones.com/products/bbedit/index.html), aux évolutions de ma pratique d’écriture. Mais au moment de rédiger mon billet, j’avais un regret : j’aurais aimé reproduire cette fameuse fonctionnalité dans BBEdit mais c’était au-dessus de mes compétences. Eh bien finalement, j’y suis arrivé ! J’avais partagé mon bricolage [sur Twitter](https://twitter.com/arthurperret/status/1273209218049466371) ; aujourd’hui j’en fais un court billet pour que ce partage soit plus pérenne.

# BBBricolage

BBEdit a une fonctionnalité appelée Clippings qui permet d’insérer des fragments textuels prédéfinis (les *clippings*), stockés dans un dossier éponyme. C’est très utile si on utilise souvent les mêmes petits bouts de texte, les mêmes morceaux de syntaxe.

Or cette fonctionnalité se présente sous une forme très similaire à celle qui permet de créer les liens dans un éditeur comme Zettlr : presser une certaine combinaison de touches fait apparaître la liste des *clippings* dans une petite fenêtre, avec un champ de recherche pour les filtrer ; il ne reste plus qu’à appuyer sur la touche Entrée (ou cliquer) pour insérer celui dont on a besoin. Vous voyez donc peut-être venir l’astuce : j’ai créé un script qui fabrique un *clipping* pour chacune de mes fiches Markdown, le *clipping* contenant… le lien !

Ma solution est un script shell, c’est-à-dire un programme interprété par un terminal dans son propre langage. Je l’ai rédigé à l’époque où le terminal de macOS utilisait encore l’interpréteur bash et pas zsh, mais le script est parfaitement rétrocompatible car les différences de syntaxe entre ces interpréteurs est quasiment nulle. Le code est probablement perfectible (les lignes 2 et 9 notamment) car je ne suis pas programmeur professionnel mais le voici :

```bash
#!/bin/bash
exec 2> /dev/null # évite que Bash se plaigne de la ligne 9
for i in /Users/aperret/Fiches/*.md
do
	TITLE="$(/usr/local/bin/gsed -nr 's/^title: "?([^\n"]+)"?$/\1/p' "$i")"
	ID="$(/usr/local/bin/gsed -nr 's/^id: ([0-9]+)$/\1/p' "$i")"
	FILE="/Users/aperret/Library/Application Support/BBEdit/Clippings/IDs/$TITLE"
	CONTENT="[[$ID]] $TITLE"
	printf "%s" "$CONTENT" > "$FILE"
done
```

Petite traduction : dans chaque fichier Markdown de mon dossier *Fiches*, le script fait une recherche avec expression régulière pour capturer le titre et l’identifiant, puis il crée un fichier au nom `Titre` et dont le contenu est `[[identifiant]] Titre`, directement dans le dossier Clippings de BBEdit. Je relance le script à chaque fois que je crée une nouvelle fiche. On pourrait automatiser cela avec un utilitaire comme [fswatch](https://github.com/emcrisostomo/fswatch), qui surveille le dossier contenant les fiches et déclenche le script lorsqu’un nouveau fichier est créé.

Pour insérer un lien, j’ouvre la palette des *clippings* dans BBEdit, je tape le début du nom du fichier, et j’appuie sur Entrée pour insérer le contenu, c’est-à-dire le lien.

<figure class="fullwidth">
<figcaption>Résumé du processus : créer les liens en avance sous forme de fichiers dont il n’y a plus qu’à insérer le contenu.</figcaption>
<img src="../img/2022-06-15-liens-wiki-bbedit.jpeg">
</figure>

Cela fait deux ans que j’utilise ce système et il fonctionne toujours comme un charme, ce qui ne fait que renforcer ma sympathie envers la philosophie Unix et les logiciels extensibles via scripts. Je consigne cette astuce ici pour ma propre mémoire mais peut-être que d’autres personnes y trouveront aussi une inspiration pour bricoler quelque chose qui leur sera utile.

# La question des identifiants

Gordon Brander l’a écrit dans [sa dernière newsletter](https://subconscious.substack.com/p/what-if-links-werent-meant-to-be?s=r) : les liens internes façon wiki doivent fonctionner comme des identifiants uniques. La pratique courante consiste à utiliser le titre de la note ou le nom de fichier mais cela pose un problème : qu’est-ce qui se passe si on change un titre ? Il faut alors mettre à jour tous les liens correspondants. Ça implique soit d’être confiant dans sa capacité à rechercher-remplacer du texte via expressions régulières dans plusieurs fichiers simultanément, soit d’avoir un logiciel qui automatise tout.

J’ai exprimé plusieurs fois mes réserves vis-à-vis de la seconde option. Je l’écris donc ici une fois pour toutes : dépendre d’un logiciel pour une chose aussi basique que la pérennité des liens dans une documentation personnelle censée se développer sur des années, c’est un choix que je refuse de faire. Je préfère que mes données soient robustes indépendamment des fonctionnalités d’un logiciel que je suis susceptible de devoir abandonner pour des tas de raisons.

C’est pour cela que je préfère utiliser un identifiant unique arbitraire qui ne sera jamais modifié. C’est ce que font tous les logiciels qui s’inspirent de la tradition académique de la fiche et notamment de l’exemple donné par Niklas Luhmann avec son célèbre *zettelkasten* (fichier en allemand) – de [The Archive](https://zettelkasten.de/the-archive/) à [Zettlr](https://www.zettlr.com/).

On s’approche alors du dispositif idéal, celui qui permet de distinguer sans ambiguïté les trois choses suivantes :

1. le titre de la fiche, qui peut inclure toutes sortes de caractères, voire carrément constituer une phrase complète si on adopte [les préceptes d’Andy Matuschak](https://notes.andymatuschak.org/Prefer_note_titles_with_complete_phrases_to_sharpen_claims) (exemple : « Le statut épistémique de la fiche évolue avec le temps ») ;
2. le nom du fichier, reprenant le titre mais qu’on peut vouloir mettre sous forme de [*slug*](https://fr.wikipedia.org/wiki/Slug_(journalisme)), car il est alors plus propice aux manipulations par des scripts (ex : `le-statut-epistemique-de-la-fiche-varie-avec-le-temps.md`) ;
3. l’identifiant de la fiche, qui peut être une chaîne de caractères unique, potentiellement générée aléatoirement comme un [UUID](https://fr.wikipedia.org/wiki/Universally_unique_identifier), ou bien suivre une convention quelconque, comme l’horodatage (ex : `20220615094430`).

Si on a cela, on n’est pas loin du meilleur des mondes possibles : identification pérenne qui réduit les risques de liens morts ; manipulation facile des fichiers via leur nom ; affichage du titre dans les interfaces d’écriture et de visualisation (ex : le graphe dans [Cosma](https://cosma.graphlab.fr)). Du moins, si les logiciels font l’effort d’aller dans ce sens. C’est pourquoi je préfère Zettlr à [Obsidian](https://obsidian.md/), parmi des tas d’autres raisons.

# Aïe aïe iA

On pourrait penser que je rame à contre-courant, notamment parce que le logiciel que je viens de mentionner, Obsidian, est aujourd’hui très populaire. L’actualité récente me fournit un autre exemple : le studio [iA](https://ia.net) vient de mettre à jour son éditeur de texte Markdown, [Writer](https://ia.net/writer) ; eh bien la grosse nouveauté de ce iA Writer 6, c’est l’ajout de liens façon wiki… qui utilisent les noms de fichier.

Sur Twitter, Anthony Masure [a sollicité des retours](https://twitter.com/AnthonyMasure/status/1536766934087245825). J’apprécie énormément le travail d’iA, qui est l’un des rares studios à faire attention à l’interopérabilité et aux métadonnées dans ses logiciels. Writer est mon autre éditeur favori, celui que j’utilise quand je n’ai pas besoin spécifiquement de BBEdit. Mais pour les liens wiki, je reste partisan d’identifiants décorrélés du titre ou du nom de fichier, donc cette mise à jour ne me séduira pas.

Antoine [l’a bien résumé](https://twitter.com/antoinentl/status/1536888514905767937) : iA se positionne stratégiquement sur une fonctionnalité populaire, mais d’une manière qui parle finalement plus au grand public qu’aux spécialistes qu’ils disent pourtant cibler. Ce type de solution est séduisante à court terme, mais les travailleurs de la connaissance ont besoin d’une documentation personnelle pensée sur le long terme, dont la finalité n’est pas simplement d’accumuler des matériaux mais de constituer un véritable atelier réflexif. Dans cette perspective, il est crucial d’avoir un système qui ne sacrifie pas la robustesse à l’évolutivité. Pour moi, cela passe par une autonomie des données vis-à-vis des solutions particulières.

Si vous êtes enseignant, chercheur, documentaliste, étudiant, et que vous vous intéressez à ces outils, je vous invite donc à songer en premier lieu à vos besoins et à examiner ensuite les outils disponibles. Le marketing engendre une confusion entre ces deux choses. La rhétorique de l’immédiateté et de la facilité est une arme commerciale : sachons la reconnaître et nous appuyer plutôt sur nos propres critères. Pour nos écrits les plus précieux, privilégions des outils dont la conception reflète une certaine humilité, celle qui consiste à reconnaître que les données doivent survivre aux outils.