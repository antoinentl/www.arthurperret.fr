---
title: Pourquoi tenir un blog scientifique
date: 2022-04-11
description: ""
---

Mark Carrigan, blogueur scientifique depuis 2003, s’interroge à voix haute sur le déclin possible de cette pratique, dans un billet crépusculaire au titre attrape-clic – [« *Are personal academic blogs a thing of the past?* »](https://blogs.lse.ac.uk/impactofsocialsciences/2022/04/11/are-personal-academic-blogs-a-thing-of-the-past/).

Carrigan rappelle sa philosophie, reprise de celle de Cory Doctorow : pour le curieux qui vaque un peu hasard dans les « pâturages de la connaissance » (*knowledge-grazing*), bloguer donne une direction et procure une agréable sensation de récompense. Mais aujourd'hui, notre vétéran est pris par le blues du blogueur.

Carrigan nourrit deux griefs à l'encontre du blog, autrefois dominant sur le Web, aujourd'hui presque ringard. D'abord, construire un lectorat est laborieux et chronophage. Or pour un chercheur, cela rentre en concurrence directe avec d'autres efforts du même type, qui eux font l'objet d'une reconnaissance plus systématique (donner des conférences, écrire des articles et des livres). Et ensuite, les outils de documentation personnelle ayant fait leur apparition ces dernières années (comme Roam et Obsidian) surclassent aujourd'hui la capacité du blog à servir d'aide-mémoire hypertextuel à son auteur. On sent que Carrigan a autant d'admiration pour ces nouveaux instruments que de regret vis-à-vis de la stagnation des outils de blog, qu'il a tant utilisés.

Ces deux arguments semblent légitimes mais n’ont que peu de pertinence dans ma pratique. Et par conséquent, je reste dubitatif face au raccourci (ou à la confusion) que fait Carrigan entre sa fatigue de blogueur et celle du blog en tant que pratique.

# J’écris d’abord pour moi

Premièrement, ce n'est pas parce qu'on tient un blog qu'on est forcément en quête d'un lectorat à analyser pour mieux le fidéliser et le faire croître.

Mark Carrigan explique que pour l'essentiel de sa carrière de blogueur, il n'était pas employé comme chercheur. On peut alors penser que le souci chez lui d'avoir une présence web importante était en partie lié à ses perspectives d'emploi. Il y a même un peu de cynisme dans le fait de mentionner une longue et prolifique pratique du blog, tout en décourageant les nouveaux entrants de s'y essayer. Ça me fait penser à ce sketch des Inconnus, [« Les œils en coulisses »](https://www.youtube.com/watch?v=2dWAaJAC0To), dans lesquels de vieux comédiens de boulevard passent leur temps à surjouer l'hilarité jusqu'au moment où la présentatrice leur demande ce qu'ils pensent de tous les jeunes comiques qui montent – il y a du Didier Bourdon chez Carrigan quand ce dernier souhaite bonne chance à la nouvelle génération, tout en lui savonnant la planche…

Je ne veux pas être de mauvaise foi : pour un doctorant comme moi, tenir un blog scientifique peut constituer un (petit) avantage dans une perspective de recrutement. Mais l'essentiel se situe ailleurs, dans ce que Carrigan lui-même décrit en reprenant Cory Doctorow : l'acte de publication est une récompense en soi ; il y a une satisfaction simple mais fondamentale à compléter une tâche d'écriture. Sinon, pourquoi procrastinerait-on la rédaction de thèse en écrivant des billets de blog ?

Plus sérieusement, je suis convaincu que le blog est autant utile à l'auteur qu'aux lecteurs ; et à moins d'être un blogueur très connu, la balance de l'utilité penche plutôt en faveur de l'auteur. Alors si la question est celle du temps disponible, oui, scruter son lectorat et essayer d'optimiser ses contenus en conséquence me semble être assez peu judicieux, et c'est cela qu'il faut arrêter – pas l'écriture.

# Mon blog périme vite et c'est normal

Deuxièmement, pour moi bloguer c'est d'abord prendre part à une conversation.

En effet, le blog satisfait un besoin d'écrire autrement que dans un article ou un ouvrage. On discute souvent de façon plus détendue autour d'un verre qu'à la tribune d'un colloque ; de la même manière, on peut apprécier l'écriture moins contrainte qu'autorise le blog par rapport à d'autres espaces.

Cette dimension conversationnelle a une conséquence impossible à nier : le blog périme très vite. Certes, il y a ces billets qui refont surface des années plus tard au gré des recherches des internautes, parce que leur pertinence reste intacte. Mais ce sont des appuis, des tremplins ([*stepping stones*, chez Gordon Brander](https://subconscious.substack.com/p/stepping-stones-in-possibility-space)) vers d'autres écrits plus frais, plus pérennes ou plus légitimes à citer.

Il y a un point sur lequel je serai d'accord avec Mark Carrigan : certains types de billets peuvent être remplacés avantageusement par des notes permanentes dans un wiki personnel. Il en va ainsi de mes billets les plus techniques, dans lesquels je partage des façon de pratiquer le format texte : ce type de billet ne m'intéresse plus tellement, et à l'avenir je pense plutôt me consacrer à alimenter des pages pérennes dédiées aux mêmes questions. Je partage également les inquiétudes de Carrigan sur les jardins numériques (*digital gardens*) qui sont souvent soigneusement clos (dans le vocabulaire des communs de la connaissance, on parlerait d'*enclosure*), là où les blogs restent plus sauvagement hypertextuels.

En revanche, j'écris d'autres types de billets : des interventions dans une conversation spécifique, généralement à partir d'un concept mis au débat – [notebook](https://www.arthurperret.fr/blog/2021-06-11-du-notebook-au-bloc-code.html), [nativement numérique](https://www.arthurperret.fr/blog/2020-04-21-nativement-numerique.html), [présentiel et distanciel](https://www.arthurperret.fr/blog/2021-03-15-presentiel-distanciel-critique-fausse-dichotomie.html), [vue graphe](https://www.arthurperret.fr/blog/2022-02-13-a-quoi-sert-une-vue-graphe.html)… Et là, l'idée de « note persistante » ([*evergreen note*, chez Andy Matuschak](https://notes.andymatuschak.org/Evergreen_notes)) serait inappropriée : ce type de billet est au contraire une parfaite « note transitoire », un éclat de voix qu'on va porter sur l'agora des réseaux sociaux, avec tout ce qu'il y a d'imparfait et d'éphémère mais aussi d'essentiel dans cet acte.

***

En résumé, je me sens peu concerné par la question du lectorat (qu'il faudrait toujours soigner) ou de l'actualité de mon propos (qu'il faudrait forcément mettre à jour). Je ne pense pas être le seul dans ce cas. C'est pourquoi je souris quand je lis que le blog serait « une chose du passé ».

Si vous êtes fatigués de scruter vos métriques de lectorat, débranchez-les : vous verrez, l'incertitude est libératrice. Si vous craignez l'obsolescence rapide de vos écrits, lisez un vieux billet d'un bon blogueur : c'est périmé, et pourtant pertinent – les deux n'étant pas contradictoires.

Si cette pratique vous tente mais que vous hésitez, allez consulter quelques blogs actifs dans votre champ d'intérêt. Vous verrez combien les choses sont mouvantes, expérimentales, situées – ce n'est pas inquiétant mais libérateur. Si vous ouvrez un blog, continuez à en lire et à en découvrir. À titre personnel, je regarde périodiquement ce que fabrique Louis-Olivier Brassard dans son [Journal](https://journal.loupbrun.ca) ; vous trouverez d'autres bonnes adresses de ce genre sur ma page [Liens](https://www.arthurperret.fr/liens.html).

Si ce billet vous a plus et que vous voulez en lire un autre sur le même sujet, je vous renvoie vers [« Pour un autre carnet de recherche numérique »](https://www.arthurperret.fr/blog/2019-11-18-pour-un-autre-carnet-de-recherche-numerique.html), où j'aborde d'autres aspects – légitimation, reconnaissance, appropriation de la technique. À moins que vous ne lisiez le billet actuel longtemps après sa publication, auquel cas méfiez-vous : c'est peut-être doublement périmé… mais pas moins pertinent !