---
title: "De la fiche au graphe documentaire"
date: 2020-11-23
abstract: "Point d’étape sur des avancées techniques et terminologiques, entre expérimentation et réflexion."
image: 2020-11-23-fiche-graphe-documentaire-2.png
lang: fr-FR
nocite: |
  @arribe2014
---

[^fig1] En juin dernier, j'ai écrit [un court billet](visualisation-documentation-personnelle-reticulaire.html) sur la mise en place d'un outil de recherche que j'avais alors qualifié de « documentation personnelle réticulaire ». En résumé, j'expliquais avoir adopté la [méthode Zettelkasten](https://organisologie.com/prise-de-note/) pour ma prise de notes, ce qui produit des fichiers interreliés, et j'évoquais l'utilité de visualiser ces notes sous forme de graphe.

[^fig1]: {-} ![](../img/2020-06-25-visualisation-documentation-personnelle-reticulaire-1.png)

Trois changements importants ont eu lieu depuis qui m'incitent à faire un nouveau billet plutôt que de mettre à jour le précédent.

# L'effet Obsidian

D'abord, Obsidian est apparu dans mon paysage tel le monolithe sombre au début de *2001, l'Odyssée de l'espace* – un objet élégant et hypnotique, qui suscite immédiatement la discorde.

![Capture d'écran d'Obsidian issue du site officiel (novembre 2020) qui illustre ses principaux arguments : fenêtres multiples, graphe, rétroliens, etc.](../img/2020-11-23-fiche-graphe-documentaire-1.png)

En effet, ce logiciel de prise de notes qui intègre une visualisation sous forme de graphe ne m'a pas convaincu, à rebours de l'enthousiasme de nombre de mes collègues sur Twitter. La dispute est toujours ouverte. On peut admirer ses jeux d'interface et son graphe, mais je déplore un développement opaque, un modèle financier proche de l'extorsion, et des conventions d'écriture non normées qui verrouillent l'utilisateur dans le logiciel.

Toutefois, il faut bien le reconnaître : le graphe d'Obsidian met la barre très haut en matière de rendu (fluidité et interactivité), dépassant tout ce que j'avais pu trouver du côté des petits outils expérimentaux comme [zkviz](https://github.com/Zettelkasten-Method/zkviz/).

Par ailleurs, tout semble indiquer que le logiciel se développera presque certainement en améliorant sa proposition de base mais sans revenir sur les aspects qui suscitent ma réticence.

Par conséquent, j'ai commencé à réfléchir à une solution alternative pour visualiser des fichiers Markdown interreliés.

# De Charybde en Sigma

L'objectif serait de faire aussi bien (voire mieux) que le graphe d'Obsidian, tout en se démarquant sur des aspects clés : la modularité, la portabilité et la conformité à des pratiques documentaires standardisées déjà mises en œuvre dans plusieurs outils (je pense en particulier à [Pandoc](https://pandoc.org/) et à [Zettlr](https://zettlr.com/)).

Grâce au renfort de [Guillaume Brioudes](https://myllaume.fr/) côté ingénierie, de belles avancées sont en train de voir le jour. Nous avons jeté notre dévolu sur la bibliothèque de visualisation [Sigma](https://medialab.sciencespo.fr/outils/sigmajs/), créée par Alexis Jacomy et Guillaume Plique au Médialab de Sciences Po. Après quelques repérages, nous avons obtenu des premiers résultats encourageants. [^legende]

[^fig3] [^fig4] ![](../img/2020-11-23-fiche-graphe-documentaire-2.png)

[^fig3]: {-} ![](../img/2020-11-23-fiche-graphe-documentaire-3.png)
[^fig4]: {-} ![](../img/2020-11-23-fiche-graphe-documentaire-4.png)
[^legende]: {-} Quelques essais de visualisation avec Sigma sur un répertoire d'environ 270 fiches.

La recherche de lisibilité nous a poussé à nous concentrer d'abord sur le paramétrage de la spatialisation (avec ForceAtlas2), l'interactivité (effets au survol ou à la sélection) et la traduction visuelle de certaines métadonnées.

Avec l'aide de Clément Borel, nous avons également pu percer la barrière de [D3](https://d3js.org/), une autre bibliothèque de visualisation plus difficile d'accès. Ici aussi les expérimentations vont bon train.

![Quelques essais de visualisation des mêmes fiches avec D3.](../img/2020-11-23-fiche-graphe-documentaire-6.png)

Nous ambitionnons de développer une interface complète pour accueillir cette visualisation, sur le modèle de l'[Otletosphère](http://hyperotlet.huma-num.fr/otletosphere/) (moteur de recherche, panneau descriptif, index, filtres).

# On ne se fiche pas de la fiche

En avançant sur ces objets, j'ai aussi changé de mots pour les décrire.

Si le champ anglophone parle généralement de notes [^1], la tradition scientifique francophone dans laquelle je m'inscris consacre plutôt le mot fiche [@bert2017]. Le terme est tombé en désuétude avec le numérique mais il n'a absolument rien de dépassé dans la pratique, et la fiche a une signification particulière dans le cadre du programme ANR HyperOtlet [@ledeuff2019c]. Je commence donc à l'adopter de manière systématique.

[^1]: Voir les travaux d'[Andy Matuschak](https://andymatuschak.org/) et en particulier ses [notes](https://notes.andymatuschak.org/).

Un répertoire de fiches constitue logiquement un fichier. Il faut un peu d'auto-discipline pour éviter la confusion avec les termes informatiques : pour l'ordinateur, mes fiches sont des fichiers, et mon fichier est un dossier !

[^fig5] Ce champ lexical s'étend également à la typologie des fiches. Sur les captures d'écran montrant nos expérimentations avec Sigma, les points bleus correspondent à des fiches de lecture, et les points roses représentent des fiches conceptuelles qui forment mon lexique personnel. La coloration se fait parce que ces fiches contiennent une ligne sur laquelle j'ai écrit `type: lecture` ou `type: lexique`.

On peut tout à fait adopter une autre typologie, à l'instar de mon collègue Ugo Verdi, dont le sujet de thèse nécessite de documenter d'autres catégories d'objets que des concepts ou des références bibliographiques. Sur la capture d'écran ci-contre, qui représente une portion de son graphe documentaire, on remarque des points en jaune : ce sont des fiches décrivant des référentiels de compétence (`type: référentiel`).

[^fig5]: {-} ![](../img/2020-11-23-fiche-graphe-documentaire-5.png)

On passe d'une convention d'écriture très simple au format texte – un bloc de métadonnées en YAML, à la manière de ce que fait [Pandoc](https://pandoc.org/MANUAL.html#extension-yaml_metadata_block) – à un rendu graphique qui permet de naviguer en tenant compte de cette information. Tout l'enjeu de nos développements futurs sera d'accroître cette « capacitation » du texte brut par l'intermédiaire de la visualisation et de l'interface.

# Graphe au logis

Quant à l'expression « documentation hypertextuelle », que j'avais utilisée initialement, elle hérite malheureusement de la polysémie du mot « documentation » lui-même – qui renvoie aussi bien à un objet matériel quantifiable (*une* documentation, *de la documentation*) qu'à une catégorie intellectuelle (*la* documentation comme champ ou discipline).

Il me paraît donc utile de distinguer les deux. Je pense désormais réserver l'expression « documentation hypertextuelle » aux pratiques de documentation qui utilisent des liens hypertexte. Et par ailleurs, je parlerai plutôt de « graphe documentaire » pour désigner l'objet que constitue un ensemble de documents interreliés.

Sur le second point, il y a des précédents : Thibaut Arribe notamment utilise le concept de graphe documentaire pour modéliser les relations entre documents (mais aussi entre fragments au niveau intra-documentaire) [^2]. Pour ma part, il s'agit de consolider mon analyse du modèle épistémologique du réseau dans le contexte de la documentation de Paul Otlet, avec une terminologie qui se clarifie progressivement.

[^2]: On peut consulter directement l'entrée [Graphe documentaire](https://ics.utc.fr/~tha/co/GrapheDoc.html) du glossaire de sa thèse en version web.

On retrouve également le mot « graphe » dans le nom de [graphology](https://medialab.sciencespo.fr/outils/graphology/), la bibliothèque de code qui doit constituer à terme la brique « modélisante » de Sigma, ce dernier s'occupant uniquement du rendu visuel. La grapho-logie, ou l'art de traduire la structure abstraite du graphe documentaire (des fiches reliées entre elles) en un langage permettant sa représentation graphique, son dessin sous forme de réseau.

***

Comme toujours, ces éléments sont appelés à évoluer. Et justement, une petite remarque méta pour finir : avec ce billet (et celui auquel il fait suite), je retrouve le format « carnet de recherche », au sens de la chronique des expérimentations, que j'avais du mal à poursuivre ces derniers temps. De plus, à la différence de certains billets du même type dans lesquels j'explorais des formes documentaires plutôt éloignées de mon sujet de thèse [^3], ici je suis en plein dedans – y compris conceptuellement.

[^3]: Je pense au [CV](le-cv-post-digital.html), aux présentations ([ici](le-diaporama-au-temps-du-balisage.html) et [là](le-diaporama-au-temps-du-balisage-2.html)) ou encore, plus récemment, aux [supports de cours](enseignement-automatisation-pandoc.html).

# Références