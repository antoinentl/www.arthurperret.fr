---
title: Dr Jekyll & Mr TeX
date: 2018-12-17
---

La fragmentation de notre présence éditoriale et le caractère souvent insatisfaisant des plateformes professionnelles conduit certains chercheurs à mettre en place un site à leur façon pour diffuser leurs travaux, ou au moins centraliser l’accès à leur production. C’est ce que j’ai entrepris, en faisant des choix qui me tiennent à cœur et que je souhaitais expliquer en quelques lignes.

# Pourquoi un site web ?

Par essence, le chercheur est dispersé : conférences, articles de revue, chapitres d’ouvrage, billets de blog, cours, gestion de projets, comités scientifiques ou éditoriaux, etc. C’est un métier qui implique une grande diversité de productions écrites ou orales. Ne serait-ce que pour unifier cet ensemble, garantir un point d’accès exhaustif, il est utile de disposer d’un portail faisant le lien vers tous les lieux de savoir numériques ou analogiques où nous travaillons.

À cela il faut ajouter l’importance du site personnel dans la construction d’une identité professionnelle, avec un vécu qui appartient souvent au registre intime. On en parle comme d’une maison, ou d’un corps : Marc Jahjah par exemple fait [un parallèle entre son blog et sa bibliothèque](http://www.marcjahjah.net/2835-portraits-de-mon-corps-1-le-blog), lesquels forment pour lui un continuum vivant avec son propre corps. Paul Otlet serait d’accord pour dire que cette relation va plus loin que le simple prolongement « mobilier » d’une pensée distribuée dans des objets inanimés :

> « Le Livre Universel formé de tous les Livres, serait devenu très approximativement une annexe du Cerveau, substratum lui-même de la mémoire, mécanisme et instrument extérieur à l’esprit, mais si près de lui et si apte à son usage que ce serait vraiment une sorte d’organe annexe, appendice exodermique[@otlet1934, 428] ».

Pour ma part, je suis très attaché à l’acte d’édition et de publication. Jusqu’à présent, je me suis reposé sur l’offre logicielle existante et dominante. Mais à force de travailler sur des technologies plus ouvertes, plus pérennes et plus modulables, j’ai fini par sauter le pas suite à un triple déclencheur : une expérience frustrante sur Hypothèses (Wordpress) ; les conseils d’Antoine Fauchié, qui m’ont motivé à faire plus que simplement lire ses billets ;[^1] le style Edward Tufte et son implémentation dans une myriade d’environnements.

# Intégrer le site à un workflow d’écriture

[Il y a 4 ans](https://www.quaternum.net/2014/04/26/latex-et-jekyll/), Antoine écrivait :

> LaTeX s’apprend, Markdown se pratique, et Jekyll — une fois l’étape parfois délicate du paramétrage — s’utilise tout simplement.

La formulation tient toujours. Autant j’ai souffert sur LaTeX, autant Jekyll est une technologie très accessible et qui répond rapidement à des attentes telles que la légèreté, la modularité, l’intégration à un *workflow* proche du texte. Via Jake Zimmerman, qui a créé les ressources pour intégrer la CSS Tufte à un *workflow* Markdown > Pandoc > HTML, j’ai découvert des modèles de site pour les utilisateurs de Jekyll[^2]. Après un certain nombre d’heures de bidouillage, j’en ai tiré la version que vous consultez actuellement.

[En février 2017](https://blog.jez.io/reach-for-markdown/), Jake écrivait :

> *« . . . when writing we want:*  
> *an open document format (so that our writings are future proof)*  
> *to be using open source software (for considerations of privacy and cost)*  
> *to optimize for the “common case”*  
> *to be able to write for print and digital (PDFs, web pages, etc.) »*

Je continue à apprendre et apprécier LaTeX, mais il m’a fallu dépasser une phase d’adoration, puis une certaine désillusion, avant d’atteindre l’âge de raison : les besoins exposés ci-dessus nécessitent de repenser l’approche que nous avons vis-à-vis de la production de documents. Essayer d’articuler les différents « poids » de balisage (léger comme Markdown et lourd comme LaTeX) m’a fait réaliser que le choix de l’outil doit venir après les principes d’écriture. Or ceux-ci varient d’un type d’objet éditorial à l’autre, et comme je l’ai mentionné au tout début, le chercheur est dispersé : j’édite de courts billets linéaires, des articles remplis de figures, des documents de cours, etc. Je n’ai pas le même rapport au paragraphe, à la note de marge, à la liste ou au tableau selon le canal dans lequel s’engouffrera le document.

Ma pratique de l’édition multi-canal ne correspond donc pas à sortir tout ce que je veux d’un tuyau magique mais à **créer des relations entre chaînes éditoriales** pour faciliter la circulation du texte. Et pour cela, il m’a fallu **décorréler langage et programme**, en investissant les deux de façon distincte.

Sur ce point, Jekyll est *très* satisfaisant. Les substitutions automatiques permettent une très grande souplesse dans le passage d’une syntaxe à une autre, ce qui crucial pour le style Tufte : étant donné qu’il n’y a pas 2 implémentations identiques des notes de marge entre TeX (LaTeX), RMarkdown (RStudio), HTML (Tufte-CSS), Pandoc Markdown ou Liquid (Jekyll), l’automatisation des regex est la bienvenue. C’est un bénéfice du même ordre que le passage entre des *slides* Beamer ou Reveal via Pandoc.

Ce portail est hébergé sur Gitlab Pages. La mise en place est plutôt longue pour quelqu’un qui n’a aucune expérience du développement, mais la gestion des versions couplée au déploiement continu apporte plusieurs bénéfices en matière d’archivage ou de portabilité. Contrairement à un CMS utilisant une base de données, l’architecture technique est cohérente avec mes besoins.

# Remerciements

Je n’ai pas de conclusion, car il s’agit d’un billet de lancement. Voici donc pour finir deux remerciements : à [Antoine](https://www.quaternum.net/a-propos/), dont l’expertise et les pratiques me servent au quotidien, et sans qui ce portail n’aurait pas pu voir le jour tel quel ; et à [Éric](http://barthes.enssib.fr), dont l’inoxydable site montre que les chercheurs affrontaient déjà ces problématiques durant les premières années du Web.

# Référence

[^1]: Voir notamment [son introduction](https://www.quaternum.net/2016/01/09/generateur-de-site-statique-un-modele-alternatif/) aux générateurs de sites statiques dont il va être question ci-dessous.

[^2]: Je vous encourage à consulter [sa page Github](https://github.com/jez) avec les dépôts `tufte-pandoc-css` et `tufte-pandoc-jekyll`, ainsi que [celle de Clay Harmon](https://github.com/clayh53/tufte-jekyll) pour un autre `tufte-jekyll`.