---
title: "Papier, crayon, smartphone"
date: 2022-03-30
description: "Une expérience pédagogique concluante et de nouvelles pages sur mon site : les cours."
---

J'approche les cinq ans d’enseignement à l'Université Bordeaux Montaigne, essentiellement à l'IUT et un petit peu à l'UFR des sciences et territoires de la communication. Au cours de ces cinq années, j’ai construit beaucoup de supports de cours, en puisant dans de nombreuses ressources pédagogiques publiques (des manuels imprimés ou numériques, des sites, des diaporamas, des rapports, des billets de blog…). Il me semble donc juste de rendre un peu de ces emprunts en partageant des ressources à mon tour. Mais sous quelle forme ?

Depuis quelques mois, j'ai commencé à partager aux étudiants des supports de cours au format HTML via mon site, à titre expérimental. C'est en partie une réponse aux contraintes posées par la pandémie de Covid-19 : un support en ligne facilite le travail à distance.

J'ai choisi de publier sur le Web en HTML plutôt que de partager un diaporama, un fichier Google Docs ou un PDF, pour plusieurs raisons. D'abord, j'avais un modèle sympa sous la main, créé pour la documentation de [Cosma](https://cosma.graphlab.fr) ; j'avais passé du temps à le concevoir, et j'avais envie de rentabiliser cet effort. Ensuite, j'avais déjà la chaîne d'outils en place pour mon site, donc l'intégration était triviale. Et enfin, de l'autre côté de l'Atlantique les collègues de Montréal ont montré l'exemple en publiant [de beaux cours en ligne](../veille/2022-01-14-debugue-tes-humanites.html).

L'expérience s'est avérée concluante. Ce qui change tout, c'est la mise en page adaptative : une page HTML bien conçue est lisible de manière confortable quelle que soit la taille de l'écran. Cette différence avec les autres formats peut être utilisée avantageusement dans certaines situations pédagogiques.

Un exemple : j'ai organisé des travaux pratiques durant lesquels les étudiants devaient lire et annoter un texte imprimé. Plutôt que s'encombrer avec des ordinateurs, je les ai encouragés à utiliser leurs téléphones pour consulter le support du cours, chercher des informations sur le Web, et même numériser rapidement leurs notes pour me les envoyer en fin de séance. Papier, crayon, smartphone : la synergie a très bien fonctionné entre ces outils légers mais puissants.

Dans [un billet publié fin 2021](https://www.marieannechabin.fr/2021/12/mon-experience-de-lenseignement-a-distance-merci-corona/), Marie-Anne Chabin suggère qu'il vaut mieux canaliser l'usage des téléphones pour servir la pédagogie plutôt que les interdire. Ma petite anecdote va dans ce sens, et c'est un sujet que j'ai envie de creuser.

***

Suite à ces expérimentations, j'ai décidé d'ajouter une section « [Cours](https://www.arthurperret.fr/#cours) » sur la page d'accueil de mon site. J'y partagerai des ressources pédagogiques modifiées pour devenir des pages permanentes, que je mettrai à jour ponctuellement.

J'ai mis en ligne quelques pages à titre d'essai. Tout est encore en construction mais vous pouvez y jeter un œil par curiosité si vous vous intéressez à des choses comme la [bibliographie](../cours/bibliographie.html) ou le [format texte](../cours/format-texte.html).

Comme mon site est assez artisanal, n'hésitez pas à me signaler les éventuelles erreurs factuelles, coquilles, bugs de mise en page, etc.