---
title: Ancres et liens
date: 2020-01-02
abstract: "Une solution simple pour obtenir des sections citables via hyperlien à partir de Pandoc, en PDF comme en HTML."
image: 2020-01-02-ancres-et-liens.png
---

J'écris ce billet suite à une question de Gérald Kembellec [sur Twitter](https://twitter.com/kembellec/status/1212384006693670913) : comment faire un lien vers un fragment d'un PDF ? Cela s'inscrit dans ses recherches sur le fragment documentaire[@kembellec2017]. Voici une solution parmi d'autres, qui a l'avantage d'être simple. Vous connaissez peut-être déjà la possibilité de charger une page web à un endroit précis en rajoutant un `#` et une certaine chaîne de caractères : c'est ce qu'on va faire ici, en utilisant les identifiants de titres (*heading identifiers*) de Pandoc et les « destinations » (*named destinations*) du format PDF.

L'utilité des destinations est expliquée dans la documentation d'Acrobat : ce sont des régions nommées d'un fichier PDF qui peuvent être invoquées comme paramètre d'ouverture, ce qui signifie qu'elles peuvent être ajoutées à l'URL du fichier pour en modifier l'affichage initial lorsqu'on y accède via un navigateur internet[^1]. Exemple : au lieu d'ouvrir le fichier `doc.pdf` puis faire défiler le document manuellement jusqu'au chapitre 3, je peux saisir `doc.pdf#chapitre3` pour y arriver dès l'ouverture. Encore faut-il avoir créé et nommé ces destinations… C'est là qu'intervient Pandoc.

Un mot sur les [extensions Pandoc](https://pandoc.org/MANUAL.html#extensions) si vous n'en connaissez pas le principe : il s'agit de fonctionnalités optionnelles spécifiques à un langage, que l'on active (`langage+extension`) ou désactive (`langage-extension`) au moment d'invoquer Pandoc pour effectuer une conversion vers un autre langage. Exemple : convertir à partir de Markdown sans notes de bas de page s'écrit en précisant `-f markdown-footnotes`. Dans le cas présent, deux extensions nous intéressent :

1. `auto_identifiers` génère automatiquement des identifiants à partir des titres présents dans la source en Markdown (active par défaut) ;
2. `ascii_identifiers` neutralise tous les caractères spéciaux dans l'identifiant, ce qui permettra d'écrire des URLs dépourvues d'accents (doit être activée explicitement).

Exemple : `# Première partie` aura l'identifiant `premiere-partie`. Dans le fichier de sortie, seul le titre sera visible mais l'identifiant sera bien présent dans le code ; c'est ce qui sert de base à de nombreuses fonctionnalités de navigation. Lorsque vous convertissez du Markdown en HTML, cet identifiant est présent dans un attribut `id` de l'élément titre correspondant. Ceci permet d'écrire un lien `exemple.com/article.html#premiere-partie` qui chargera ou fera défiler la page au bon endroit.

Pour la conversion du Markdown vers le PDF, Pandoc passe par LaTeX. Celui-ci repose également sur un système d'extension, les *packages*. L'un des plus essentiels est le bien-nommé `hyperref`, qui gère les hyperliens mais pas seulement : c'est aussi lui qui crée les signets PDF (*bookmarks*) et leurs cibles (les fameuses destinations). C'est cela qui permet à Acrobat, Evince, Aperçu et les autres d'afficher une table des matières dépliable et cliquable qui renvoie vers les différents chapitres, sections et sous-sections du document. En ajoutant l'option `destlabel=true` à `hyperref`, celui-ci se servira des identifiants de titres générés par Pandoc pour nommer les destinations : on peut alors s'en servir dans l'URL du fichier.

Récapitulatif :

- toujours convertir avec `-f markdown+ascii_identifiers` dans la commande Pandoc ;
- inclure `\usepackage[destlabel=true]{hyperref}` dans le préambule du modèle de document LaTeX pour les conversions en PDF.

Et c'est tout ! Cette petite opération présente un double intérêt. Premièrement, vous pouvez citer une section de PDF avec autant de précision que la structure du document vous le permet. Deuxièmement, la citation suit le modèle des « belles URLs » [dont parle Stéphane Bortzmeyer](https://www.bortzmeyer.org/beaux-urls.html), avec une chaîne de caractères compréhensible et à l'abri d'une re-numérotation ultérieure.

<figure class="fullwidth">
<figcaption>Exemple de fichier PDF ouvert directement à la bonne section via son URL. Le fichier source, utilisé notamment sur ce site, n’a pas eu besoin d’être modifié.</figcaption>
<img src="https://www.arthurperret.fr/img/2020-01-02-ancres-et-liens.png">
</figure>

Je pense à un exemple de document qui pourrait vraiment en bénéficier : les thèses. Elles sont parfois passionnantes mais on les parcourt plus fréquemment dans une logique référentielle que linéaire. Il se trouve qu'elles sont la plupart du temps assez longues et quasiment toujours en PDF. Or une fois sur deux la navigation y est exécrable car le fichier ne comporte pas de table des matières automatisée. On se rabat alors sur celle qui est incluse en début de document, avec une mise en page affreuse, une absence totale de liens hypertexte, et on songe qu'il serait grand temps de ressusciter Umberto Eco pour une sérieuse mise à jour technique de *Comment écrire sa thèse*.

## Référence

[^1]: Cf. [les pages d'aide à l'utilisation d'Acrobat](https://helpx.adobe.com/fr/acrobat/kb/link-html-pdf-page-acrobat.html) (en français) et [sa documentation](http://www.adobe.com/devnet/acrobat/pdfs/pdf_open_parameters_v9.pdf) (en anglais).