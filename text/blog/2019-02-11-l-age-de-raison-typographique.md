---
title: L’âge de raison (typographique)
date: 2019-02-11
---

Ce texte apporte un commentaire sur la typographie du site. La première partie constituait à l'origine la fin du billet de lancement, mais après un mois de démo auprès de collègues ou amis et leurs nombreux retours critiques, j'ai fait des changements et j'ai décidé de consacrer un billet séparé à cette question.

# Computer Modern

Au départ, je cherchais une super-famille de polices avec laquelle je puisse avoir des atomes crochus à la fois sur le plan esthétique et conceptuel. J’ai opté pour le Computer Modern, la fonte emblématique de LaTeX. Elle est au programme de Donald Knuth ce que le Minion est aux logiciels Adobe : un non-choix, car c’est la police par défaut, mais aussi historiquement la solution la moins mauvaise. On est ici dans une philosophie assez Mac : non seulement *it just works* mais qui plus est *it doesn’t suck*[^1].

Le Computer Modern est un beau compromis entre considération **esthétique** (l’italique, qu’on adore ou qu’on déteste, personnellement je l’adore), besoin **pratique** (il contient tous les styles dont j’ai besoin, du serif au sans en passant par le mono) et **symbolique** (LaTeX reste pour moi un modèle de respect envers le travail d’écriture et d’édition scientifique, cette fonte emblématique est donc plus qu’un clin d’œil).

C'est aussi un caractère qui divise. Le savoureux débat ([toujours en ligne](https://news.ycombinator.com/item?id=6954882)) occasionné par la conversion du Computer Modern [pour le Web](https://www.checkmyworking.com/cm-web-fonts/#sans-demicondensed) synthétise, à mon humble avis, la plupart des positions sur les choix typographiques en général :

> `fidotron` — *I swear that a good amount of the TeX love is cargo culting, including any appreciation of Computer Modern, which is a seriously ugly typeface. It’s almost as if adopting it for the web is simply a grasp for mathematical credibility more than any real aesthetic consideration.*
>
> `brennen` — *I think that most of what I appreciate about Computer Modern is that it sends the neighborhood font nerds into fits of white-hot rage every time I use it.*

# Scholar Classic

Le panorama de la typographie numérique a bien changé depuis la première version de LaTeX. Si les mathématiciens n’ont toujours pas trouvé un remplaçant parfait pour le Computer Modern, je n’ai pas la contrainte de devoir afficher des équations sur ce site. Il existe maintenant de nombreuses familles de polices, aussi bien libres que payantes, qui répondent à des besoins et des goûts variés.

Les ressources de grande qualité que j’ai parcourues[^2] recommandent des options disons plus raisonnables que cette didone sans doute un peu prétentieuse, avec son contraste qui fait travailler les yeux, son italique extravagante et sa connotation « sciences exactes » désuète aux yeux de certains. Et les collègues ou amis qui m'ont fait des critiques sont presque tous revenus sur le problème de la lisibilité, qui n'était pas apparent sur le très bon écran de la machine sur laquelle je travaille.

Enfin, j'ai relu attentivement [les discussions](https://www.edwardtufte.com/bboard/q-and-a-fetch-msg?msg_id=0000ld) autour de la mise en page que j'appelle « à la Tufte » mais que lui-même a repris du physicien Richard Feynman et qui s'inscrit dans une longue tradition des usages intelligents de la marge.

Ayant tout ceci en tête, j'ai décidé de revenir sur la typographie du site avec un choix plus classique, c'est-à-dire toujours conforme à mes goûts personnels mais plus respectueux des lecteurs potentiels. Pour le corps du texte, j'ai sélectionné Cardo, de David J. Perry. C'est une fonte livresque élégante et très complète pour les humanités. Elle se rapproche de Bembo, dont elle conserve le trait riche (par opposition à Garamond, souvent trop fine à mes yeux). Pour le reste (caractères sans empattements, chiffres, certains titres, le code), j'ai choisi Gill Sans et Inconsolata pour avoir un style humaniste cohérent de bout en bout.

Le rendu final me satisfait, même s'il n'a pas l'excentricité du Computer Modern. Après quelques semaines d'existence, on peut donc dire que ce site est définitivement en place, en attendant éventuellement de nouvelles rubriques (conférences, cours…) et d'autres défis de mise en page (format de citations…).

[^1]: *It doesn’t suck* est le slogan de BBEdit, redoutable dinosaure de l’édition de code sur OS X.

[^2]: J’ai été impressionné notamment par les livres web de [Richard Rutter](http://webtypography.net), [Donny Truong](https://prowebtype.com) et [Matthew Butterick](https://practicaltypography.com).