---
title: "Présentiel et distanciel : critique d'une fausse dichotomie"
date: 2021-03-15
abstract: "La distance ne suppose pas l'absence. Enseigner, c'est avant tout livrer une performance."
lang: fr-FR
---

Je n'ai jamais pu suivre un MOOC. À chaque fois que j'ai essayé, ça s'est terminé de manière frustrante, avec l'impression de passer à côté de quelque chose, voire un sentiment de culpabilité, d'inaptitude à m'auto-former.

Je réalise maintenant avec l'expérience de l'enseignement à distance que ces tentatives étaient vouées à l'échec, et pour cause : j'ai en fait beaucoup de difficultés avec l'enseignement asynchrone de manière générale. C'était déjà le cas lorsque j'étais étudiant et ça se confirme en tant qu'enseignant.

En fait, je pourrais généraliser ce constat sur l'asynchronicité à d'autres situations, pas seulement l'auto-formation. Si je consulte un document (article, podcast, vidéo) sur une thématique, je peux dire que je me divertis, que je passe le temps, que je réfléchis un peu et éventuellement que certaines idées vont plus ou moins me rester en tête. En revanche, si je parle de cette même thématique avec des gens, il y a de bonnes chances pour que je mémorise plus efficacement les éléments de la discussion. Cela tient probablement à l'illocution, aux différentes choses qui accompagnent le discours : un regard soutenu, une voix qui s'élève, le geste d'aller vérifier une information en ligne pour contredire une affirmation, etc. Le corps est un formidable producteur de métadonnées ; la présence nous donne tout un jeu de coordonnées supplémentaires pour épingler une idée dans le temps et l'espace.

Un MOOC, c'est une sélection de contenus, parfois produite avec un soin remarquable, mais à laquelle il manque cette présence – le flux, la vitalité, l'*anima*. C'est la créature du Dr Frankenstein, sans l'étincelle pour lui donner vie. Je pense que beaucoup de gens ont besoin de cette étincelle et c'est ce qui explique pour moi que les MOOCs aient globalement échoué comme outil d'enseignement *de masse* – il faudrait peut-être les renommer « OOCs ».

# Présentiel et distanciel sont dans un bateau

C'est dans cette perspective et dans le contexte de la pandémie actuelle que je réfléchis aujourd'hui à la dichotomie présentiel-distanciel, avec tout ce qu'elle a de problématique. L'opposé de « distant », c'est « proche » ; l'opposé de « présent », c'est « absent ». L'idée que la distance s'oppose à la présence est donc un non-sens, qui s'est pourtant retrouvé érigé en institution à travers les néologismes de « présentiel » et « distanciel ».

Le problème de ce dilemme fallacieux, c'est qu'il fournit la justification à une logique de désynchronisation totale entre enseignants et apprenants. De nombreux collègues ont fait ou subi ce choix, transformant des cours entiers en contenus consommables de manière asynchrone, en ne construisant que rarement un dispositif du type classe inversée autour de ces contenus. Je ne pense pas que ce choix soit le bon.

D'abord, cette transformation a parfois été présentée comme la seule option viable mais rien ne nous y oblige. Penser cela, c'est méconnaître le fonctionnement même des technologies de l'information et de la communication (TIC), qui servent précisément à compenser l'impossible co-présence du monde à lui-même : nous ne pouvons pas être à la fois ici et là-bas en même temps, donc nous communiquons, et souvent en direct. C'est le paradoxe de la simultanéité [@robert2010, 146, 378]. L'impossibilité de se réunir physiquement ne signifie donc pas qu'il faille supprimer toute simultanéité, au contraire : comme je l'ai dit, je pense que pour un grand nombre de personnes l'éducation nécessite une forme de synchronicité dans les échanges, et l'informatique (entre autres) nous donne les moyens d'en préserver une partie, même à distance.

Ensuite, le risque de cette stratégie est de mobiliser une importante partie des ressources académiques sur la création d'une sorte de version dégradée de YouTube. C'est un mauvais calcul : [les enseignants ne sont pas des producteurs de contenus](https://www.timeshighereducation.com/opinion/academics-arent-content-creators-and-its-regressive-make-them-so) [^1] mais des passeurs.

Dans le mot « passeur », il y a deux composantes : la curation et la performance. Curation, parce que l'essentiel de ce que transmet un enseignant résulte d'une sélection et d'une transformation de contenus existants [^2]. Performance, parce que cette curation a besoin d'être habitée, incarnée à la fois par les étudiants et par les enseignants. Je suis convaincu que c'est la performance qui donne toute sa valeur à la curation. Consulter un document ne suffit pas à faire une éducation. Ça fonctionne peut-être lorsque les contenus s'inscrivent dans une démarche de pédagogie inversée, mais pas lorsqu'on substitue totalement la performance par des formes d'engagement asynchrones. 

C'est pour cette raison que je n'accroche pas du tout à la logique du replay ou du podcast dans le contexte pédagogique, et que je me sens beaucoup plus à l'aise avec la métaphore de la radio ou du *stream*. Attention, je ne suis pas en train de suggérer que nous devions calquer nos pratiques sur celles des streameurs plutôt que celles des youtubeurs. De toute manière, la métaphore radiophonique déplaît également à certains collègues, ce que je peux comprendre. Ce qui compte pour moi, c'est l'idée du « direct ». Fondamentalement, si je n'ai pas changé ma manière de préparer ou de faire cours (c'est toujours un mélange de curation et de performance), c'est parce que le direct reste possible. Évidemment, cela ne va pas sans difficultés mais au bout d'un an de pandémie, nous avons désormais une vision plus claire de ces difficultés et des stratégies pour les affronter.

Ce n'est pas qu'une question de préférence concernant l'enseignement mais aussi de survie de mon activité de recherche : si le temps de préparation de mes cours devait doubler (ou pire) pour que je monte des vidéos, je n'aurais plus une minute pour travailler sur ma thèse.

Et c'est aussi une question qui touche à la manière dont on conçoit l'imbrication de cet enseignement et de cette recherche. Les cours constituent-ils une simple obligation de service, dispensée de la manière la plus détachée possible (au sens littéral), ou bien un moyen de mêler formation par la recherche et recherche par la formation, de faire advenir l'idée de communauté de recherche ?

# Référence


[^1]: Via Antonio Casilli [sur Twitter](https://twitter.com/AntonioCasilli/status/1371356182150057984). J'écris ce billet en réaction à la lecture de l'article.

[^2]: L'essentiel mais pas la totalité. On peut faire intervenir nos travaux de recherche dans notre enseignement, et on fabrique souvent intégralement nos travaux pratiques.