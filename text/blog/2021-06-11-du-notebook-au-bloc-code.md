---
title: Du notebook au bloc-code
date: 2021-06-11
abstract: "Une proposition de terminologie francophone pour *notebook* basée sur la matérialité et l'usage."
lang: fr-FR
---

En informatique, le terme anglais *notebook* est utilisé depuis quelques années pour désigner un nouveau genre d'environnement dans lequel on peut inscrire du code avec des données et de la prose, exécuter le code et visualiser simultanément les résultats. Les *notebooks* sont typiquement utilisés en sciences pour communiquer un dispositif expérimental basé sur du code, sous une forme documentée et interactive : on lit d'abord le document pour comprendre de quoi il retourne, après quoi on peut éventuellement modifier le code et le ré-exécuter pour observer ce que cela change aux résultats.

Les *notebooks* sucitent beaucoup d'enthousiasme en lien avec des questions comme la reproductibilité des travaux de recherche et la didactique des sciences. Toutefois, ils ne sont pas encore acceptés comme format de publication. Par ailleurs, ils ne sont pas interprétables nativement pas les navigateurs web. Enfin, le domaine des *notebooks* n'est pas uniforme d'un point de vue technique ni terminologique, avec des controverses majeures qui sont toujours d'actualité. On est donc face à un outil prometteur mais qui n'a pas encore tout à fait pris sa forme finale.

Récemment, Marin Dacos posait la question suivante [sur Twitter](https://twitter.com/marindacos/status/1403250722473484294) : comment traduire « *Jupyter*[^1] *notebook* » ? J'ai participé à plusieurs discussions sur le sujet, je saisis donc l'opportunité de partager ma réflexion. Je vois trois options pour répondre à la question, la troisième ayant ma préférence. Et comme le titre de ce billet le suggère, j'ai même une proposition concrète : « bloc-code ».

[^1]: [Jupyter](https://jupyter.org) est un environnement de type *notebook* parmi d'autres ; la question s'appliquerait tout autant aux *notebooks* [Observable](https://observablehq.com/) et [R Markdown](https://rmarkdown.rstudio.com) par exemple.

# Option 1 : traduction littérale

La première option pour traduire l'expression « *Jupyter notebook* », c'est « carnet Jupyter ». C'est la meilleure option à court terme. Employer le nom propre du dispositif (Jupyter, Observable, R Markdown…) permet de dissiper l'ambiguïté du nom commun carnet/*notebook*, et la traduction littérale permet de naviguer entre les contextes anglophone et francophone sans trop de frictions.

# Option 2 : expression nouvelle basée sur des mots courants

La deuxième option consiste à créer une expression à partir de noms et d'adjectifs courants, comme par exemple « article exécutable » et « carnet actif ». Les réponses au tweet de Marin Dacos s'inscrivent pour la plupart dans cette catégorie. Je suis assez sceptique vis-à-vis de cette deuxième approche et je voudrais expliquer brièvement pourquoi.

Les noms utilisés renvoient à des supports de communication écrite et des types documentaires avec lesquels nous sommes déjà familiers : carnet, calepin, bloc-notes, essai, article, document… Ces mots évoquent parfois la temporalité du processus d'écriture : l'expression anglaise *scratch pad*, par exemple, est plutôt associée à un usage du *notebook* comme brouillon, avec du code imparfait et éphémère. À l'inverse, le mot essai ([qu'utilise notamment Stephen Wolfram](https://writings.stephenwolfram.com/2017/11/what-is-a-computational-essay/)) renvoie au genre éditorial du même nom, qu'on associe donc plutôt à un travail achevé et publié.

Les adjectifs utilisés, eux, expriment la manière dont le *notebook* se distingue du nom familier auquel on l'a d'abord rattaché. On retrouve notamment numérique, computationnel, programmable, exécutable, actif, interactif… On essaie ici de décrire le *notebook* comme un « document+ », un document « augmenté ». Dans [mon article sur la preuve](https://www.arthurperret.fr/fonction-documentaire-preuve-donnees-numeriques.html) pour DocSoc 2020 par exemple, je parle de document-machine.

Les noms font référence à des pratiques et des traditions d'écriture qui me paraissent toutes valides et globalement interchangeables. En revanche, je ne suis pas convaincu par les adjectifs utilisés et je pense qu'ils affaiblissent le sens des expressions ainsi forgées. C'est notamment la présence de « numérique » (ou de son cousin « digital ») qui met la puce à l'oreille : c'est un bon indicateur d'impasse terminologique ; cela montre qu'on a compris qu'il y avait une différence essentielle dans ce qu'on essaye de décrire mais qu'on stagne dans l'analyse de cette différence. Voir « numérique » apparaître, c'est le signe qu'aucun autre adjectif suggéré jusqu'alors ne décrit de manière adéquate ce qu'on cherche.

Je prendrai juste un exemple : l'adjectif « exécutable », mis en avant aussi bien par Marin Dacos dans son tweet que par le HN Lab durant les Rencontres Huma-Num du mois dernier[^2]. Un fichier docx n'est pas moins exécutable qu'un *notebook* Jupyter : quand on ouvre un docx dans Word, on voit le rendu d'un code source ; on peut éditer ce rendu, ce qui modifie le code source ; on peut définir des variables (les styles) qui propagent des paramètres à de multiples objets ; on peut programmer des actions via des macros ; etc. Et un article de recherche rédigé en Pandoc Markdown dans [Stylo](https://stylo.huma-num.fr) présente toutes les caractéristiques de l'« écriture exécutable », alors qu'il ne s'inscrit pas dans la catégorie des *notebooks*. Donc pour moi, le terme manque de spécificité, et c'est une critique que j'étends pour l'instant à tous ces adjectifs qui dépeignent une sorte de « document+ ».

[^2]: Le [bloc-notes contributif](https://demo.hedgedoc.org/s/kTsJrdJLj) de l'atelier est toujours accessible à l'heure où j'écris.

# Option 3 : néologisme

La troisième option pour traduire « Jupyter *notebook* » consiste à faire un néologisme, comme « artycle » ([suggéré par Alexis Kauffmann](https://twitter.com/framaka/status/1403283804077363205)) ou « bloc-code » (l'idée que je propose et que je vais détailler plus bas).

À mon avis, le néologisme est la solution plus attrayante et la plus convaincante à long terme. C'est ce qui nous permettra de nous débarrasser du mot carnet/*notebook*, qui pour moi est ambigu et inadéquat. Parce que le mot pré-existe à la nouvelle acception qu'on veut lui donner, on ne peut pas parler de « carnet » tout seul sans avoir à définir le terme, et on perd alors tout l'intérêt d'utiliser un mot simple : tant qu'à devoir définir le mot, autant prendre un néologisme dont le sens ne peut être confondu avec autre chose. Une alternative, c'est de toujours accoler le nom du dispositif au mot carnet, comme dans « carnet Jupyter », « carnet Observable », etc. Mais cela nous empêche alors de discuter ce nouveau média qu'est le *notebook* en tant que tel, de manière transversale, afin de généraliser des observations dans le cadre d'une réflexion scientifique.

Je propose donc de tenter l'option du néologisme. Concrètement, il faudrait un effort terminologique collectif, peut-être un groupe de travail : il faut mettre à plat tous les éléments de ce qu'on appelle *notebook* et faire ensuite le tri dedans, entre inventivité et réalisme.

En attendant que cela voie le jour, je livre mes réflexions à titre d'exemple.

# Proposition : du notebook au bloc-code

Voici ma proposition : traduire *notebook* par « bloc-code » (pluriel : des blocs-code, comme des stations-service). Ceci reflète selon moi les trois principales caractéristiques de ce nouveau média : le code comme matériau central ; une structuration en unités souvent appelées « blocs » ; un usage similaire au bloc-notes.

Considérons d'abord le contenu. Un document informatique rassemble des matériaux (ou données) qui font l'objet d'une certaine médiation, laquelle est définie par des interfaces et un ou plusieurs formats. Une spécificité du *notebook*, c'est qu'il rassemble des matériaux différents – prose, données, code en entrée (*input code*) et sortie (résultat de l'exécutation du code, *output*) – exprimés dans différents formats. Les formats sont déclarés implicitement ou explicitement, d'une manière qui varie d'un environnement à l'autre : dans un carnet Jupyter par exemple, le système distingue des blocs de code, des blocs de texte « brut » (*raw*) et des blocs de texte au format Markdown ; à l'inverse, les carnets R Markdown peuvent être définis comme des fichiers rédigés principalement en Markdown et qui contiennent des blocs de code en R (d'où le nom).

Cette diversité de matériaux étant établie, il faut admettre que c'est bien le code qui mène la danse. On pourrait nommer les *notebooks* par une expression traduisant l'hybridation des formats mais je ne crois pas que ce soit suffisamment en phase avec la réalité : les données ne sont incluses que parce qu'il y a du code pour les traiter ; les blocs de texte en Markdown ne servent qu'à commenter les entrées et sorties du code.

Les *notebooks* sont donc centrés sur le code. Si on examine maintenant la manière dont les contenus sont structurés et présentés, on retrouve quelque chose de similaire aux tableurs : dans l'interface de type *notebook*, le fichier est une sorte de classeur (*binder*)[^3] contenant des feuilles (*sheets*) elles-mêmes divisées en blocs (*blocks*) ou cellules (*cells*). Comprendre le fonctionnement des blocs, notamment leur ordre d'exécution, est crucial dans l'utilisation d'un *notebook* ; c'est aussi un point important des controverses entre partisans et détracteurs de ce type d'outil[^4].

[^3]: Aparté : le mot classeur/*binder* paraît désuet mais je le trouve riche de signification. L'utiliser en contexte numérique, c'est continuer à réfléchir à comment on classe, et comment on relie.

[^4]: Lire [« *The First Notebook War* »](https://yihui.org/en/2018/09/notebook-war/), signalé [ici](https://www.arthurperret.fr/veille/la-premiere-guerre-des-notebooks.html) récemment.

Enfin, il faut prendre en compte les usages. Les *notebooks* sont généralement considérés comme des outils de programmation lettrée et d'analyse exploratoire des données, avec des usages concrets qui se partagent en trois catégories (avec des chevauchements) [@kery2018, 4] :

1. bloc-notes, brouillon (le *notebook* comme *scratch pad*) ;
2. code finalisé qui sera extrait du *notebook* pour être incorporé ailleurs en production ;
3. partage ou publication, soit du *notebook* directement, soit d'un export dans un autre format (HTML, PDF…).

Le *notebook* est donc considéré soit comme un bloc-notes, soit comme une source de code réutilisable, soit comme un document. Dans ce dernier cas, il tend à être désigné par le terme approprié au contexte : on parlera d'article, de rapport, de feuille de travail (*work sheet*), etc. Le qualificatif le plus largement applicable c'est donc finalement le premier – brouillon/*scratch pad*. Yihui Xie, principal développeur des carnets R Markdown, propose d'ailleurs qu'on utilise désormais *scratch pad* plutôt que *notebook*.

Résumons : nous avons un format centré sur le code, structuré en blocs, souvent utilisé comme bloc-notes. Pourquoi pas « bloc-code » ?

« Bloc-code » rappelle des objets informatiques familiers, du Bloc-notes de Windows aux *scratch pads* qu'on peut trouver dans les éditeurs de texte ; on peut aussi penser aux *pads* collaboratifs en ligne. On peut utiliser « bloc-code » en complément avec un terme approprié au contexte d'utilisation, suivant qu'on parle d'un cahier de laboratoire, d'un article de recherche ou d'un support de cours (exemple : « Le fascicule de TP se présente cette année sous la forme d'un bloc-code »).

Pourquoi « bloc-code » et pas « carnet de code/programmation » ?

Depuis le début, je trouve que carnet/*notebook* est une analogie assez inadéquate, plombée par le fait que les anglophones utilisent indifféremment le terme pour désigner le logiciel et le fichier. Effectivement, les logiciels comme Jupyter ou R Studio représentent une sorte de fusion entre le cahier de laboratoire (*lab notebook*) et le laboratoire lui-même. En revanche, appeler le fichier *notebook* aussi ne me paraît pas très malin. À l'heure où Huma-Num planche sur [un démonstrateur Jupyter pour les SHS](https://hnlab.huma-num.fr/blog/2021/05/26/callisto-un-demonstrateur-jupyter/), l'ambiguïté ne me semble pas de nature à favoriser l'adoption de ces outils. De la même manière que nous distinguons tableur et feuille de calcul alors que les anglophones mélangent tout sous l'appellation *spreadsheet*, on gagnerait en clarté à parter de logiciels de type bloc-code, et de feuilles de code/programmation pour les fichiers individuels.

Mais même si la confusion logiciel-fichier devait demeurer, « bloc-code » repose sur une petite astuce polysémique qui le rend plus acceptable comme terme fourre-tout : en effet, le bloc est la fois le fragment documentaire (unité de base du contenu) et le support d'inscription (en l'occurrence, le logiciel).

***

Ce billet ne prétend évidemment pas résumer la question terminologique liée aux *notebooks*, ni clore le débat avec une proposition qui se voudrait meilleure que les autres. C'est d'abord et avant tout une invitation à poursuivre la discussion.

# Référence
