---
title: Publication multiformats avec Pandoc et Make
date: 2022-06-22
description: "Une solution pour générer HTML et PDF à partir d’un seul endroit, par exemple pour une thèse."
---

Ça fait un certain temps que je cherche un moyen d'appliquer le principe du *single-source publishing*^[Pour plus d’information sur ce concept, consultez [sa page Wikipédia](https://en.wikipedia.org/wiki/Single-source_publishing) et [cette série d’articles](https://coko.foundation/articles/single-source-publishing.html) d’Adam Hyde.] à ma thèse de doctorat. J'entends par là le fait d’écrire ma thèse dans un seul fichier (ou un seul ensemble de fichiers, si j’écris un chapitre par fichier) et exporter le tout dans plusieurs formats, principalement HTML et PDF. C'est plus difficile qu'il n'y paraît : j'ai des attentes élevées concernant la qualité de ces exports, qui s’inscrivent dans une longue tradition, et il est difficile d’atteindre cela simultanément dans plusieurs formats à partir d'une seule source. J’ai également une préférence pour le logiciel libre et le [format texte](../cours/format-texte.html), ce qui resserre aussi le champ des solutions possibles.

Au cœur de mon processus de travail se trouve [Pandoc](https://pandoc.org/). C'est un outil phénoménal, je ne l’écrirai jamais assez. Je l'apprécie notamment parce qu'il me permet de séparer l'écriture de la mise en forme – la typographie, la mise en page, le design graphique, etc. Je peux bricoler et peaufiner ces aspects en CSS ou en LaTeX sans que cela interfère avec l'écriture ; lorsque je dois me concentrer sur cette dernière, tout le reste s'efface. Grâce à cela, j'ai pu progresser dans mon manuscrit de thèse tout en continuant à travailler sur son design éditorial sans que ces deux tâches ne se cannibalisent l'une l'autre.

Au fil des ans, j'ai trouvé d'excellents exemples de processus automatisés basés sur Pandoc. J'ai fini par en utiliser deux en particulier : [Pandoc-SSG](https://github.com/ivanstojic/pandoc-ssg/) d'Ivan Stojic, pour générer mon site web, et le [Gabarit Abrüpt](https://gitlab.com/cestabrupt/gabarit-abrupt), pour générer ma thèse en PDF. Tous deux sont basés sur [Make](https://www.gnu.org/software/make/), un outil d’automatisation qui est utilisé ici pour organiser un ensemble complexe de commandes Pandoc. Évidemment, ce qui me faisait envie c’est de réunir les deux solutions dans un seul outil.

J'ai perdu pas mal de temps à essayer de fusionner Pandoc-SSG avec le Gabarit Abrüpt mais je n'y suis jamais arrivé. Récemment, j'ai décidé de retenter le *single-source publishing* mais en abordant le problème différemment cette fois : en écrivant mon propre Makefile à partir de zéro. Cette approche s'est avérée bien plus judicieuse. Apprendre les bases de Make n'a pas été long grâce à cet excellent tutoriel : [Makefile tutorial by example](https://makefiletutorial.com). Et j'ai finalement réussi à concevoir un système qui fonctionne pour moi, et que je partage avec vous aujourd'hui: [Pandoc-SSP](https://github.com/infologie/pandoc-ssp), un modèle pour faire de la publication multiformats avec Pandoc et Make.

Je l'ai qualifié de modèle mais ce terme (surtout dans sa version anglaise *template*) peut générer des attentes que je dois désamorcer. C'est un répertoire contenant un Makefile avec des commandes Pandoc, et quelques autres fichiers et sous-répertoires. Mais j'ai délibérément laissé les options Pandoc vides, et à l'exception du Makefile, tous les fichiers sont vides également. Il ne s'agit donc pas d'un modèle fonctionnel que vous pouvez tester tel quel (comme le Gabarit Abrüpt par exemple). Il faut le voir plutôt comme un schéma à redessiner soi-même. Si vous rédigez des documents scientifiques et que vous cherchez à les exporter en HTML et en PDF à partir d'une source unique, mon modèle vous donnera des indications pour le faire – mais seulement des indications.

Autre avertissement : il s'agit d'un petit outil, adapté à des besoins basiques. Vous serez peut-être plus intéressé par un système aussi avancé que [Manubot](https://manubot.org) ou [Quarto](https://quarto.org) (tous les deux basés sur Pandoc). Mais si comme moi vous aimez bien essayer d'abord des outils simples, mon modèle pourrait vous intéresser.

Un dernier mot avant d’entrer dans le détail : si vous ne connaissez pas Make, je vous encourage à lire un tutoriel avant d'utiliser mon modèle. Personnellement, ça m'a fait le plus grand bien et sans ça je serais encore coincé. Encore une fois, celui-ci m’a vraiment plu : [Makefile tutorial by example](https://makefiletutorial.com). Lorsque j'ai commencé à me poser des questions auxquelles il ne répondait pas directement, le tuto m’a renvoyé vers la documentation de Make, où j'ai immédiatement trouvé des solutions. Soyez particulièrement attentifs à ces trois signes cryptiques : `$@`, `$<` et `$^`. Examinez bien mon Makefile : vous verrez qu'ils font le gros du travail concernant Pandoc.

# Comment ça marche ?

L’idée est la suivante : on met tout dans un dossier et on utilise Make pour automatiser l’exécution de quelques commandes Pandoc. Certains fichiers sont utilisés dans toutes les conversions (textes en Markdown, données bibliographiques) ; d’autres ne le sont que pour un format en particulier (les modèles comme `template.html` et `template.tex` notamment).

Toute la magie réside dans le Makefile. Il y a quelques lignes que vous ne voudrez probablement pas toucher et que j’ai adaptées d’une question StackOverflow – celles qui permettent de copier tels quels et de façon récursive tous les fichiers « statiques » (non convertis). Tout le reste peut être adapté ; je ne suis pas allé jusqu’à mettre les chemins de dossier dans des variables mais cela ne freinera pas beaucoup la personnalisation.

Comme je le disais plus haut, ce modèle ne fonctionne pas tout seul. À l’exception du Makefile (qui définit tout le processus), les fichiers et dossiers sont vides, ils ne servent qu’à indiquer l’emplacement des différents éléments. Si vous téléchargez le dossier et que vous lancez la commande `make`, il ne se passera rien. Il faut que vous remplaciez les fichiers vides par les vôtres.

Tel qu'il est écrit, le Makefile utilise les dossiers suivants :

- `text` : les fichiers Markdown, que vous pouvez organiser comme vous le souhaitez (faire des sous-dossiers est utile si vous voulez utiliser différentes commandes Pandoc pour différents groupes de fichiers) ;
- `output` : dossier de sortie ;
- `static` : dossier contenant les fichiers statiques, c’est-à-dire tout ce qui doit être copié tel quel dans l’export (par exemple, pour la version HTML : CSS, polices, images…).

La racine contient :

- le `Makefile` ;
- `metadata.yml` : des métadonnées que Pandoc peut utiliser pour remplir ses modèles ;
- les modèles justement, tels que `template.html` et `template.tex` ;
- `references.bib` : données bibliographiques (qui pourraient être en CSL JSON ou tout autre format supporté par Pandoc) ;
- `style.csl` : style bibliographique ;
- `index.md` : il devient le `index.html` de la version HTML.

À vous de remplacer les différents fichiers par les vôtres et d’ajouter les options Pandoc dont vous avez besoin. Pandoc est merveilleusement bien documenté : n’hésitez pas à consulter [le manuel de Pandoc](https://pandoc.org/MANUAL.html) et [les modèles par défaut](https://github.com/jgm/pandoc-templates).

Voici un diagramme qui illustre le processus de conversion. Ce n’est qu’un exemple, libre à vous de l’adapter. Par exemple, vous pouvez vouloir utiliser le fichier `metadata.yml` dans les deux processus de conversion. Ou bien rajouter un format d’export.

<figure class="fullwidth">
<img src="../img/pandoc-ssp-condensed.png">
<figcaption>Processus de conversion utilisé dans mon modèle Pandoc-SSP.</figcaption>
</figure>

[Cliquez ici](https://www.arthurperret.fr/img/pandoc-ssp.pdf) pour télécharger une antisèche au format PDF avec le diagramme, l'arborescence et le Makefile.

# Comment utiliser le modèle ?

Téléchargez le dossier [Pandoc-SSP](https://github.com/infologie/pandoc-ssp) et **remplacez les fichiers par les vôtres** (à l’exception du Makefile). Ensuite, ouvrez un terminal et déplacez-vous dans le dossier. Exécutez l'une des commandes suivantes pour exporter à la fois le HTML et le PDF : `make` ou `make all`. Exécutez la commande suivante pour exporter du HTML : `make html`. Exécutez la commande suivante pour exporter le PDF : `make pdf`.

***

C’est un dispositif assez simple finalement, surtout quand on le compare à des outils impressionnants comme Manubot et Quarto, déjà mentionnés. Mais ça me plaît bien. J'aime le fait que ce soit aussi simple. J'aime le fait qu’on puisse mettre n’importe quel *template* Pandoc, qu'il s'agisse du *template* HTML par défaut ou d'un fichier LaTeX personnalisé à l’extrême. Et j'aime le fait de partager une solution qu’il faut faire l’effort de modifier avant de voir comment elle fonctionne.

Je n’offrirai pas de suivi ni de support sur ce projet. Alors si vous voyez des possibilités d'amélioration, dupliquez le dépôt git et amusez-vous !
