---
title: Un carré documentologique
date: 2020-08-05
abstract: "Testons les relations entre quelques concepts avec un petit schéma."
image: 2020-08-05-carre-documentologique.jpg
---

J'ai appelé ce diagramme de manière un peu littérale « carré documentologique » parce qu'il est, eh bien, carré, et qu'il renvoie à des travaux en documentologie[^1]. Ce n'est pas le schéma ultime des articulations conceptuelles entre information, communication et documentation : c'est plus un outil de « théorie expérimentale » qu'un système théorique proprement dit. J'ai essayé de le concevoir comme un « générateur de connaissance » [@drucker2014, 105-107] : il y a des mots, des relations, mais la *connaissance* qu'on en retire vient de la circulation active entre ces mots et ces relations, avec une logique combinatoire.

> « Les diagrammes *performent* l'acte de raisonnement, ils ne le représentent pas après coup mais sont le moyen d'en faire fonctionner le processus logique[@drucker2014, 115] ».

![Un carré documentologique. J'insiste sur l'article indéfini : c'est un agencement parmi d'autres. C'est un exercice d'expérimentation sur les concepts, une mise à l'épreuve sans certitude préalable, qui est appelée à évoluer.](https://www.arthurperret.fr/img/2020-08-05-carre-documentologique-1.jpg)

Comment lire ce schéma ? Prenons un exemple avec une médiation documentaire classique, une personne plongée dans un texte en quête de connaissance.

[^fig2] J'intitule ce lecteur « récepteur » car il est impliqué dans un processus de communication et que mon diagramme essaye de ne pas séparer information et communication. Mais comme ici mon prisme est plus info-documentaire que communicationnel, je me préoccupe d'abord de ce avec quoi il interagit, à savoir un document. D'où ce point de départ : le lecteur-récepteur établit la **documentarité** d'un objet ; celui-ci « fait document » ou pas *à ses yeux*. C'est un processus d'interprétation, d'analyse, de jugement.

[^fig2]: {-} ![Documentarité](https://www.arthurperret.fr/img/2020-08-05-carre-documentologique-2.png)

[^fig3] Depuis le document, on peut alors (ou pas) remonter « à la source » (l'émetteur). Lorsque j'ai réalisé ce diagramme, je travaillais sur la fonction documentaire de preuve, c'est pourquoi j'ai utilisé le mot **fiabilité** pour désigner cette relation entre document et source. À partir du document toujours, on peut aussi remonter au niveau des données, si la structure du document le permet : on évalue alors son **intégrité**.

[^fig3]: {-} ![Fiabilité, intégrité](https://www.arthurperret.fr/img/2020-08-05-carre-documentologique-3.png)

[^fig4] L'information en tant qu'objet, c'est-à-dire le document ou les données qu'il contient, *désignent* une information, laquelle constitue une connaissance potentielle. La relation entre document ou donnée et connaissable est une relation d'**indexicalité**, c'est-à-dire un rapport métonymique entre ce qui est inscrit et ce qui est, tout court. Comme pour une métaphore, arriver à comprendre cette relation suppose une compétence d'écriture et de lecture, la maîtrise un jeu de langage particulier : il faut connaître une langue naturelle, peut-être un langage construit, une convention ou une norme, voire une bizarrerie propre à l'auteur ou aux aléas du temps.

[^fig4]: {-} ![Indexicalité](https://www.arthurperret.fr/img/2020-08-05-carre-documentologique-4.png)

Vous aurez peut-être compris que les cinq rectangles – émetteur, récepteur, document, donnée, connaissable – constituent des *positions* et pas des entités fixes. Une entité peut occuper différentes positions : par exemple, un humain peut être considéré comme un document [Cf. le fameux texte de @ertzscheid2009; cf. également l'indexation des existences selon @ledeuff2015a], ou bien être impliqué dans une communication en tant qu'émetteur, ou que récepteur. Les relations entre ces positions font référence à des théories de l'information-communication (sauf fiabilité et intégrité, qui sont utilisés dans leur sens courant et ne renvoient pas à une référence particulière) :

- modèle de la communication de Shannon [@shannon1948] ;
- typologie de l'information de Buckland [@buckland1991; @gorichanaz2018] ;
- indexicalité chez Ron Day [@day2016] ;
- documentarité chez moi (à partir d'autres auteurs) [@perret2019c].

Pour moi, le graphe n'est pas dirigé *a priori*, c'est-à-dire que les relations ne vont pas dans une direction donnée par l'auteur (moi) mais construite par le lecteur (vous et moi, à un instant t). En effet, la fonction de ce diagramme est d'éprouver à la fois les cadres théoriques qu'il mobilise et la tentative d'agencement qui en est faite. Pour cela, il faut poser des questions concrètes, comme « comment sont reliés le lecteur et ce qu'il peut connaître ? » et cheminer. En fonction de cela, certaines relations du graphe peuvent constituer la somme d'autres relations : des « pliages » deviennent possible. Par l'intermédiaire du document, la communication entre émetteur et récepteur peut être vue comme la somme de la documentarité et de la fiabilité, et l'expérience de l'information (relation entre un récepteur et un connaissable) peut être considérée comme la somme de la documentarité et de l'indexicalité.

Bien sûr, ces chemins n'en excluent pas d'autres. Et tout ceci reste un chantier ouvert. Je présenterai ce diagramme à la conférence [Document numérique et société](https://docsoc2020.sciencesconf.org/) le mois prochain. Ce sera l'occasion d'essayer un autre mode de démonstration (diaporama) et d'entendre quelques critiques constructives.

# Références

[^1]: Documentologie : études scientifiques (et plus particulièrement théoriques) sur la documentation, en SIC et LIS. Le mot est devenu désuet, ce qui est dommage.

