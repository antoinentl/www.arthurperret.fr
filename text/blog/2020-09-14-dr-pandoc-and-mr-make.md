---
title: Dr Pandoc & Mr Make
date: 2020-09-14
abstract: "Ce n'était qu'une question de temps : j'ai refait tout mon site avec Pandoc. Et ça valait le coup."
---

# Une saison en enfer

Ce site est généré à partir de textes rédigés en Markdown. Jusqu'ici, j'utilisais [Jekyll](https://jekyllrb.com/), mais la maintenance était devenue plus que pénible. D'abord, Jekyll est écrit en Ruby. Or la version de Ruby installée par défaut sur macOS n'est pas la plus récente et il est recommandé de ne pas y toucher. Installer Jekyll nécessite donc de gérer deux versions distinctes de Ruby, de préférence avec un programme dédié. À cela il faut ajouter que l'écosystème de plugins Ruby (les « gemmes ») se gère via un autre programme, et que les dépendances d'une application Ruby (par exemple un blog géré via Jekyll) se gèrent via un autre programme encore.

Cette organisation peut vite devenir infernale pour une personne comme moi. En effet, je n'ai pas été formé au développement logiciel ; je n'ai donc jamais articulé tout à fait correctement ces différents programmes. De plus, mon rythme de publication est relativement lent (environ une fois par mois), plus lent que celui des mises à jour logicielles. Je me suis donc vite retrouvé à devoir corriger un problème à chaque fois que je voulais ajouter du contenu sur mon site, et j'ai dû gérer un gros plantage tous les six mois environ depuis le lancement de celui-ci. La goutte d'eau est tombée il y a deux semaines : j'ai voulu publier un billet, et mon installation s'y est refusée catégoriquement. J'ai senti qu'il était temps de passer à autre chose.

Quelle solution ? Changer de CMS ? Compliqué : j'ai besoin de pouvoir utiliser pleinement Pandoc si je veux conserver une même convention d'écriture partout dans mon travail. Or les alternatives à Jekyll ne me le permettent pas encore (exemple : Hugo) ou alors sont beaucoup trop complexes (exemple : Hakyll). Autre piste : déléguer la mécanique – fabrication, déploiement – à un service comme Netlify. Difficile : il faut que les programmes impliqués dans cette mécanique soient mis à disposition du service sous une forme particulière (conteneur, ou image). Or si les auteurs de Pandoc s'en occupent pour la partie principale du programme, j'utilise aussi des éléments qui sont en dehors de ce périmètre, en particulier une version modifiée du filtre [pandoc-sidenote](https://github.com/jez/pandoc-sidenote) créé par Jake Zimmerman, qui n'existe que sur ma machine.

# Simplex sigillum veri

J'ai fini par trouver une solution tout récemment après avoir consulté le site du chercheur américain Jeff Huang et son billet intitulé *« This page is designed to last »* [^1]. Dans ce texte, il offre quelques suggestions pour améliorer la durabilité des contenus que nous publions sur le web. En particulier, il décrit exactement la situation que j'ai vécue avec Jekyll, et avertit du risque que cela entraîne à moyen terme. Par ailleurs, son site a une structure très simple : il n'y a qu'une page principale avec des informations sur l'auteur et des liens vers les différents textes publiés sur son site – lesquels sont tous au même niveau.

[^1]: L'[original](https://jeffhuang.com/designed_to_last/) est en anglais, je l'ai découvert via [sa traduction](https://framablog.org/2020/08/24/pour-une-page-web-qui-dure-10-ans/). Antoine a publié [une réaction](https://www.quaternum.net/2020/09/01/la-rouille-des-fabriques/) sur son carnet, avec des critiques assez justes.

La juxtaposition du propos et de l'exemple de site m'a laissé une forte impression. J'ai décidé d'essayer une approche simplificatrice. Sur le papier, j'ai imaginé mon site sur le modèle de celui de Jeff Huang et j'ai listé les éléments dont j'aurais besoin pour le remplir. J'ai été surpris de constater que la liste est très courte : hormis les pages elles-mêmes (HTML, CSS, images, polices de caractères, intégration d'Hypothesis), seul le flux RSS me tient vraiment à cœur. Pas de commentaires, de moteur de recherche, de nuage de mots-clés… Or en simplifiant une architecture déjà basique, on peut envisager de se passer d'un CMS classique mais aussi d'un générateur de site statique comme Jekyll ou Hugo : ce dont j'ai besoin peut être réalisé par des scripts très simples, des outils qui n'évoluent pas ou presque (les scripts *shell*, Make). C'est ce que j'ai choisi d'appliquer à mon site. Vous avez le résultat sous les yeux !

Je précise que je ne connaissais pas Make, mais que, par expérience, un fond de culture numérique permet généralement de se repérer assez vite dans un nouvel outil à partir d'exemples existants. Pour automatiser la création des pages, j'ai donc utilisé le Makefile du projet [pandoc-ssg](https://github.com/ivanstojic/pandoc-ssg/) [^2], en l'adaptant un peu. Idem pour le flux RSS : je l'ai généré grâce à une version légèrement personnalisée d'un script *shell* existant, [pandoc-rss](https://github.com/chambln/pandoc-rss).

[^2]: L'auteur a publié un [billet explicatif](https://ordecon.com/2020/07/pandoc-as-a-site-generator.html) qui fait mention d'une certaine lassitude vis-à-vis de Jekyll. Coïncidence ?

Le déploiement du site suit cette logique simplificatrice : j'ai opté pour GitHub Pages, ce qui règle d'un coup le contrôle de version, le stockage en ligne et la publication web. C'est la solution de facilité. J'utilise Netlify pour certains travaux, j'expérimente d'autres processus plus ou moins compliqués, et je commence à être familier du protocole ftp, mais je crois que j'avais besoin de redescendre sur une solution simple, pratique, fiable… et gratuite.

**Mise à jour du 22/02/2021 :** j'ai finalement sauté le pas et loué un serveur chez mon hébergeur. Ceci me permet de mettre en ligne toutes sortes de choses, notamment des mini-sites développés dans le cadre de mes différentes activités. N'ayant plus besoin de GitHub Pages (le ftp n'a plus de secrets pour moi), j'ai migré le stockage des fichiers de ce site depuis GitHub vers [GitLab](https://gitlab.com/infologie/www.arthurperret.fr).

# Ultime (?) ravalement de façade

Les personnes ayant déjà parcouru mon site connaissent peut-être les ingrédients de sa mise en page, que j'ai commentée à plusieurs reprises [^3] : la disposition du texte en colonne, avec une marge contenant de nombreuses notes et quelquefois des figures, un jeu sur les polices et les couleurs qui permet de distinguer le corps du texte de l'appareil critique (notes, légendes). J'aime décrire ces choix, parce que cela me permet de vous faire voir le site comme je le vois [^4].

[^3]: Pour l'esprit général, voir [ce lien](https://www.arthurperret.fr/semantique-et-mise-en-forme.html#organiser-lespace). Sur la mise en forme des notes, suivre [ce lien](https://www.arthurperret.fr/citations-marginales.html#le-rouge-et-le-gris).

[^4]: Marc [a mis un mot](https://twitter.com/marc_jahjah/status/1207631701838569477) sur cela : la *querencia*. Lisez [son billet](http://www.marcjahjah.net/2966-portraits-despaces-2-la-querencia) sur le sujet.

L'œil aiguisé de certains lecteurs me permet régulièrement d'améliorer certains détails [^5] mais globalement je me sens bien dans cette esthétique et je ne compte pas procéder à des changements majeurs ; le dernier que j'ai réalisé est le bouton soleil/lune au sommet qui permet basculer entre thème clair et thème sombre.

[^5]: Voir [ce tweet d'Anthony](https://twitter.com/AnthonyMasure/status/1301137160830099456), à ranger dans la rubrique « Pan ! sur le bec ».

Toutefois, retaper son site c'est comme déménager : on en profite toujours pour vider des cochonneries. Et j'en ai découvert une belle… Pour la mise en forme, j'étais parti non pas de [Tufte-CSS](https://edwardtufte.github.io/tufte-css/) mais du fichier css inclus dans le thème Tufte-Jekyll. Or ce dernier contenait quelques rajouts que je n'avais jamais remarqués, et notamment deux déclarations qui activent le [lissage des polices](https://developer.mozilla.org/fr/docs/Web/CSS/font-smooth) pour les navigateurs compatibles avec cette option. Mauvaise idée : cela amaigrit considérablement la police [ET Book](https://github.com/edwardtufte/et-book/), que j'avais fini par choisir après pas mal de tergiversations [^6], et qui perd complètement ses belles rondeurs avec ce traitement. Ce n'est qu'en refaisant les styles du site en repartant de zéro que j'ai découvert la boulette, et que j'ai enfin obtenu l'apparence que vous voyez en ce moment. L'esthétique de ce site reposant quasiment exclusivement sur le texte et la typographie, c'est presque un changement de peau…

[^6]: J'avais écrit [un billet](https://www.arthurperret.fr/l-age-de-raison-typographique.html) là-dessus il y a deux ans mais il faudrait un véritable *changelog*…

# The road goes ever on

Ce nouveau site est donc en ligne, mais il n'est pas tout à fait complet. Il me reste à réintégrer la mise en page adaptée au mobile qui était présente jusqu'ici, reprise de Tufte-CSS. Je voudrais aussi compresser les images, qui gonflent inutilement le poids de certaines pages. J'aimerais bien intégrer des métadonnées détectables par les logiciels de gestion bibliographique. Je me pose toujours la question de l'ISSN et de l'indexation par les moteurs de recherche académiques [^7].

[^7]: Voir [le billet](pour-un-autre-carnet-de-recherche-numerique.html) que j'ai consacré au carnet de recherche.

Cette feuille de route ne me servira pas seulement de prétexte à esquiver la rédaction de thèse pendant un après-midi ou deux (!) mais aussi et surtout à donner une direction à l'atelier permanent qu'est ce site, lequel, de version en version, me permet d'en apprendre toujours un peu plus sur l'information scientifique et la publication numérique.