---
title: "Mot compte triple"
date: 2019-09-12
abstract: "On m'a fait découvrir le mot numérotique et je suis tombé immédiatement sous le charme de cette petite curiosité linguistique. Voici en quelques lignes l'explication de sa construction particulière et des pistes pour en trouver d'autres."
---

C'est un [mot-valise](https://fr.wikipedia.org/wiki/Mot-valise) particulier puisqu'il s'agit d'une haplologie – fusion de deux mots grâce à une ou plusieurs syllabes communes, en l'occurrence les mots numéro et érotique (par apocope du premier ou aphérèse du second, au choix). Mais le fait qu'on y entend en plus le mot numérique l'élève au rang de véritable « mot compte triple ». Apparemment, c'est intentionnel :

> « Dans une conférence, Wolfgang Ernst, théoricien majeur de l’archéologie des media, forgeait le mot de “numérotique” en conflagrant joyeusement nombre et numéro, numérique et érotique[@citton2015, 178] ».

Les haplologies sont parfois voisines du calembour, surtout si on s'amuse à les introduire d'abord par une définition inventée de toutes pièces. Exemple : « Longues bottes de cuir traditionnellement portées par les pêcheurs bretons. Réponse : cuissardines ». Mais dans la perspective de l'écriture scientifique il s'agit avant tout d'un procédé intéressant pour rendre son argument plus marquant, sans forcément prétendre forger un concept opérant. Si j'écris un jour quelque chose sur la fragilité des ressources numériques, je ne manquerai pas de placer le très évocateur « numérosion[^1] ». Un autre exemple : le mot « architexture » [@caws1981, 10], dans lequel on retrouve à la fois architexte et texture.

Toutefois, il est assez difficile de créer un mot à triple sens sur le mode de l'haplologie : non seulement on a besoin de deux mots compatibles mais il faut en plus faire émerger un troisième signifiant sur un coup de chance ou de génie. Or tout le monde n'est pas lexicographe. Alors que si on prend la chose à l'envers, en disant par exemple que numérotique est créé à partir de numérique par épenthèse – c'est-à-dire en rajoutant des lettres au milieu[^2] –, cela me paraît plus accessible : on part d'un seul terme qui nous donne à la fois les lettres du début et de la fin, ainsi que la thématique recherchée. Je n'ai pas encore réussi à trouver quoique ce soit d'aussi astucieux que numérotique, mais je ne désespère pas !

# Références

[^1]: J'en profite pour souligner qu'on a rarement la primauté d'un terme : j'ai trouvé au moins une autre occurrence de numérosion sur le Web. Par ailleurs, l'antériorité de la découverte réserve parfois des surprises : ainsi numérotique apparaît-il dans le titre d'un ouvrage sur le calcul datant des années 1950.

[^2]: Ici, je ne suis pas sûr qu'un authentique grammairien valide l'usage du terme mais c'est ce que j'ai trouvé de plus proche dans les lexiques à ma disposition.