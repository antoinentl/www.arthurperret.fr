---
title: What is the point of a graph view?
date: 2022-02-13
abstract: "A long post about links, documents and graph screenshots."
lang: en-US
---

You may have heard about “interrelated note-taking tools” lately. I use quotes because there is no consensus on terminology: some people call them “tools for thought”, to emphasize what they're used for rather than what they do.

The defining characteristic of this group of tools is that they use hyperlinks to organize notes. Indeed, links aren't just navigation tools: they can be used to organize documents (in reticular fashion) just as folders and tags can be used to create hierarchies and apply taxonomies.

Linking of course existed before hyperlinks and computers. In Diderot and D'Alembert's *Encyclopaedia*, for example, entries are organized alphabetically but also by cross-references. These small indications at the end of a text send readers on a non-linear path to other texts, behaving like a proto-hypertext. And these links are not random nor trivial. Writing on the occasion of a digital edition of the *Encyclopaedia*, Philippe Roger underlines how discreetly subversive they are: “the cross-references create an allusive and polemical subtext […] they thwart the dogmatic trap inherent to fixing meanings”.[@roger2001, 14] In the *Encyclopaedia*, linking is used to build an alternative knowledge organization, in subtle opposition to the established order of things.

Today, linking is obviously at the heart of the Web. Links are so important that we could legitimately call the Web a reticular information system. And the semantic Web (or linked data) shows us how far the logic can be taken. However, most people don't think of hyperlinks as a way of organizing knowledge. Discourse around the Web is rather sociological, economic or political: “linking” doesn't refer to documents but to “communities” and “platforms”. And the semantic Web remains fairly confidential.

The rise of interlinked note-taking tools could change the situation somewhat. They show that hyperlinks are not reserved to things such as web pages or wikis but that they can be used for personal notes as well. And so links enter the toolbox of personal knowledge management (PKM).

In practice though, many of those who use these new tools say that they don't use links very much. And if you browse through various blogs, forums and social spaces, you will quickly notice that people's enthusiasm for software like [Roam](https://roamresearch.com) or [Obsidian](https://obsidian.md/) isn't based on links at all.

# Mobile and search before links

First, people are tired of existing tools and their uses. The popularity of applications that dominated the market for years, such as Evernote, is on the decline, for a host of reasons. Some blame the apps themselves, especially when promised evolutions were never delivered. But a lot of people have also given in to what Christian Tietze, developer of The Archive, calls [“the collector's fallacy”](https://zettelkasten.de/posts/collectors-fallacy/), which is the uncontrolled accumulation of notes, most of which are never opened more than once. And so the time is ripe for a new generation of tools, bringing a breath of fresh air and the illusion of a clean slate. These don't necessarily put links at the forefront—[Notion](https://www.notion.so/) is a good example of this, emphasizing databases, tables and workspaces instead.

Secondly, links aren't the users' top priority. The spot is occupied by mobile and sync: the most popular tools will be the ones to offer a mobile version and the ability to synchronize files hassle-free. For most people, linking between notes is far less important than being able to capture information on the go.

And third, information retrieval practices have changed dramatically in the past decade, centering more and more around search. In my opinion, this is where the biggest problem lies, and I need to elaborate a bit.

If you apply traditional documentation processes to personal knowledge, you start by choosing ways to classify your personal knowledge, and then you implement it through naming, tagging and filing conventions. When, some time later, you want to find some information, you then navigate this well-thought documentary structure depending on how you classified information: chronology, alphabetical order, themes, document types, etc.

As I mentioned earlier, links are a way of organizing knowledge; they complement other methods, such as hierarchies and taxonomies. If you organize by folders and keywords, and add links between texts, you essentially multiply the paths to find any given information. Combining knowledge organization techniques is very efficient from a documentation standpoint.^[Documentation is used throughout the post to refer to the theory and use of documents, a branch of information science with European roots. See Michael Buckland's [“Document Theory”](https://www.isko.org/cyclo/document) in ISKO's Encyclopedia of Knowledge Organization.]

Moreover, links have their own special uses. You can add backlinks to effectively cross-reference documents. Links can be qualified or contextualized, to clarify their meaning and accelerate information retrieval. And these things combine: with contextualized backlinks, the power of linking is increased tenfold, as is the number of paths to information.

Now, the issue: these traditional documentation techniques have been marginalized by search. Everything has a search bar now, from file systems to email clients. In some applications, even menus can only be accessed through search—I'm referring to the command palette, which is spreading among text editors and which I personally dislike with a passion. Obviously, note-taking tools are no exception: they focus heavily on search, leaving links, tags and metadata fall by the roadside. As we will see later, search cannot do everything, so this is problematic.

To summarize, new note-taking tools have emerged in a context where the appearance of novelty takes precedence over tried-and-true documentation techniques, and they prioritize needs that have been shaped by several years of smartphone and search engine domination. That last point is actually a general trend: I recommend reading *The Verge*'s [“File not found”](https://www.theverge.com/22684730/students-file-folder-directory-structure-education-gen-z), from last September, which investigates the subject in the context of education.

# The graph's usefulness

All this brings me to the question of the graph. The term refers either to the abstract structure formed by interrelated entities, or to its graphical representation. The question I want to address, that pops up all the time and served as a starting point for this post, uses the word “graph” in the second meaning.

Here is the question: what is the point of a graph view in interrelated note-taking software?^[It's most often asked about Obsidian, because it's the most popular tool in this category at the moment, but it applies to all the tools which offer the same functionality, and in the last section it is mentioned in the context of Zettlr.]

There are direct answers to this, answers which are interesting to discuss. First of all, you're not aware of what you're making until you see it. And so the first use of the “graph” in the sense of a visualization, is to show you the existence of the other “graph”, the abstract structure.

Now, this abstract structure, the very idea of a graph, is a conceptual tool, and so it has cognitive implications: it affects the way you think. Scholars use the word “text” as a concept to think about literature in a certain way. The words “graph” and “hypertext” are concepts as well, and here what they allow us to do is reflect about note-taking: the way we write, and what it says about the things we're writing about.

Such questions allow us to become aware of the very notion of interrelation: to see the graph of your notes is to realize that you're *writing in the shape of a network*—reticular writing. It helps you realize that the things you write about are interrelated, before you even see the links. This realization reorganizes the way you write. From now on, you pay attention to connections, contradictions, and all other kinds of associations. You start looking for patterns, not just in the things you write about but also in the writing technique itself: what kind of notes am I writing, what kind of links am I making, etc.

Another answer to the question of the usefulness of the graph concerns its visual role. Linking between notes can produce very complex structures, because of both the fragmentation of information and the amount of relationships between fragments. Visualization brings an overall view which helps to embrace the complexity and navigate it, whether it's through visual reading or interaction. It's easier to identify patterns (regularities, irregularities) visually with a graph than by reading files. In turn, this helps you decide how the notes should be modified: locating isolated notes, which you may want to link to the rest of the graph; identifying an emerging cluster of notes, which you may want to study further; etc.

And finally, a third answer: a graph helps you find relevant information that you are not looking for. All note-taking tools allow you to find information through full-text search. But not all of them complement this with related information, things which you were not looking for but may be relevant. This is extremely useful, and the best information systems are those that include mechanisms for evidencing information, not simply retrieving it.

Links are a way to implement such mechanisms. This can be a list of incoming and outgoing links for a note, or this can be a graph. The point is to remind you of the existence of connections to things you search for, present them and re-present them, even when they are not directly searched for. For a connection could lead you to augment, refine, complement, contradict, compare, etc. A graph is a memory aid that prevents siloed thinking.

# From the graph to the index card

I think graphs are useful, and so I've tried to outline some of their usefulness. But I'm also wondering why their usefulness is questioned so much in the context of note-taking tools. And here too, I have some ideas.

The reason I started this post with a rather long development about links and knowledge organization, is to introduce the following idea: a graph view will be most useful if you have a strong methodology for note-taking, with an emphasis on linking, and if your note-taking tool strongly encourages the use of documentation techniques.

Let's elaborate. First, you can't really understand the usefulness of graphs if you don't situate them in the long history of knowledge work. For this reason, I will now make a dramatic (!) semantic shift from “note” to “index card” (or card for short). In the scholarly tradition, the card is an important instrument. Now, wherever there are instruments, there are methods. And at the heart of many card-based methods lies interrelation: cards become trees, trees devolve into networks, and soon we're talking about graphs again.

Paul Otlet, the Belgian pioneer of documentation, worked according to a “monographic principle” (one card, one idea): the idea was to atomize books into conceptual units, which could then be linked through a universal classification.[@otlet1934, 256, 286] Niklas Luhmann, a very prolific German sociologist, used a *Zettelkasten* (card box): a huge set of cards serving as a writing partner, prompting him with suggestions thanks to the methodical interrelation of cards according to shared themes.[@luhmann1992] More recently, Andy Matuschak has written about [“evergreen notes”](https://notes.andymatuschak.org/Evergreen_notes): cards which help develop insight, because they are atomic, concept-oriented and, most importantly, densely linked. And if you read Gordon Brander, well, [“all you need is links”](https://subconscious.substack.com/p/all-you-need-is-links): they are immensely expressive and so they can do almost anything—keywords, hierarchies, comments, recommendation, semantics, topic modeling, etc.

I chose these examples because the authors have all produced remarkable conceptual work, with great inventiveness and impressive productivity. I belive this is due in particular to their methodical conception of note-taking, which they treat as long-term personal documentation. And the hyperlink is the star of the show. In this respect, a graph is not just a decorative thing: it is a knowledge worker's tool, relying on links and extending them, giving them another shape, allowing use to use them in a different way. It's not just an image but a tool for methodical exploration.

Obviously, the role of software is decisive in achieving this. There is no point in categorizing files if the software does not allow you to filter the display by category. There's no point in putting links everywhere if you don't have backlinks, contextualization, or even the ability to categorize links. And a graph becomes really useful when it leverages all this: when it visually translates categories with different colors and strokes; when you can filter its display by these same categories; etc.

To sum up: graphs show us what hypertext is, conceptually and practically, which influences the way we write and think about interrelation; and the more it taps into all the documentary work that we apply to cards, the more it becomes useful.

And so if we recall what I said before, that the rise of new note-taking tools has little to do with hypertext and documentation, it's a bit clearer why so many people wonder what the point of a graph view is.

# The shape of graphs to come

Last night Hendrik Erz [announced](https://twitter.com/zettlr/status/1492614979970863106) the imminent addition of a graph view in Zettlr. It was met with a lot of enthusiasm, but also with a few sneers. One person said that after over a year spent with note-taking tools, all they could say about graphs was that they looked nice pretty, and that finding a use for them was another matter.

The announcement itself had a weird ring to it: I had to read it a couple times to make sure it did say “Here, have a graph view”, with that dejected undertone—it's almost as if he's saying “Happy now?”. Later, Erz clarified his position, confirming my interpretation: [he called](https://twitter.com/zettlr/status/1492866680602738688) the graph a “nice toy” but inferior to note-taking techniques and full-text search.

Reading this made me want to share my point of view. Personally, I don't think the graph is the be-all and end-all of hypertext tools. But it is a very important tool, certainly not a toy in my opinion. Hence this post, in which I have tried to defend its usefulness.

But before I wrap this up, I would like to share my intuition concerning this negative perception that some people may have of the graph view. In [“Rhizome Blues”](../articles/2020-12-03-rhizome-blues.html), my colleagues and I used the word “monstration”, the imperative of showing something; documents have their roots in monstration, the urge to record monstrous and extraordinary things, to display them. I think graphs exemplify the ambiguity of monstration, through two very different sides of their visual nature.

In this post, I've said that graphs are helpful because they present and re-present information, show hypertext, and leverage the work of categorization through graphic features. Graphs belong to visual epistemology, and this is the positive side of “monstration”, showing something to help thinking about it.

But there's another side. I'm thinking of these screenshots of extraordinarily massive graphs, proudly shared on Twitter by people who boast about the enormity of their Obsidian vaults. This is the other, less positive side of “monstration”.

Mathieu Jacomy defines [“storyletting”](https://reticular.hypotheses.org/1783) as the act of publishing a visualization while pretending it doesn't tell anything, or letting it tell its own stories. He uses this concept to discuss [what people do with graphs they export from Gephi](https://reticular.hypotheses.org/1866). I think about this when I come across screenshots on Twitter showing graphs of tens of thousands of notes. But after reflecting on it today, I think they're on the edge of “storyletting”, and not entirely there. These graphs don't tell stories like Gephi's, because that's not their function: they are visual memory aids connected to hypertext, not a tool for data analysis.

If these images tell us anything, it's the fascination attached to networks. It's a meme, and it doesn't help anyone think seriously about the graph view; it just propagates a cliché among the designers of interrelated note-taking software, and also among users. Because it's superficial, it gets tiresome, and I'm a little worried that people like Hendrik Erz, who could do phenomenal things with graphs, may not give them the attention they deserve, because they're being nagged by users who just want the feature for a screenshot.

And so you have the whole story. A long post—but I really felt like it was a case of a thousand words being better than one picture here.

::: box
*This blog post has a sequel:* [“Analyze, synthesize, visualize: about cards, links & graphs”](2022-02-17-analyze-synthesize-visualize.html)
:::

# References