---
title: "L’impensé des formats : pourquoi écrit-on des articles ?"
date: 2021-12-28
---

Toute profession a des outils. Et à profession intellectuelle, technologie intellectuelle. En tant que travailleurs de la connaissance, nos technologies intellectuelles de prédilection sont le langage et l'écriture. Nous les pratiquons assidûment, et nous développons des savoir-faire autour d'elles. Mais tout ceci ne se produit pas dans le vide : nos pratiques prennent place dans des cadres, et les formats en sont les vecteurs matériels. Ce sont les formes plus ou moins stabilisées de nos habitudes, progressivement instituées en normes [Je reprends de manière schématique @demourat2020].

Si on considère les formats dans une perspective un peu plus concrète, plus individuelle aussi, on peut dire que les formats satisfont des besoins. J'emploie volontairement le mot besoin pour son caractère polysémique : écrire, par exemple, est un travail au sens littéral d'effort, et on a besoin de s'exercer pour bien travailler ; mais écrire procure aussi du plaisir, et c'est cela qui motive le choix d'une profession basée sur l'écriture, car on a besoin de trouver une certaine satisfaction dans notre travail.

L'écriture scientifique se décline ainsi en de nombreux formats, qui sont autant de prétextes pour s'aiguiser l'esprit. Chaque format peut être considéré comme un exercice permettant de travailler de manière privilégiée tel ou tel aspect de l'écriture (décrire, synthétiser, argumenter…). Mais la diversité de formats sert aussi à accommoder la diversité des auteurs et de leurs sensibilités : certains préfèrent écrire des livres, d'autres des articles, ou encore des billets de blog, des supports de cours, des articles de presse, etc.

L'article a un avantage inhérent. Avec sa longueur moyenne – entre le tweet de 200 caractères et le pavé de 700 pages –, il représente un format pratique à la fois pour les auteurs et les lecteurs : c'est en effet un compromis acceptable pour beaucoup de projets d'écriture en ce qui concerne la longueur, et par ailleurs ce compromis facilite la revue systématique des publications sur un sujet donné.

Évidemment, l'article a aussi des défauts inhérents. Le principal que je vois est justement lié à cet aspect de compromis : l'auteur voudrait souvent faire plus long ; à l'inverse, le lecteur se demande parfois si l’article n’aurait pas pu être plus court. Ceci explique sans doute en partie que, dans les revues et les conférences, l'article soit décliné en plusieurs sous-formats définis par la longueur (en nombre de pages, de mots ou de signes).

Et bien sûr, l'article a une connotation négative liée à sa place dans l'écosystème académique. En effet, ce format a été mis au centre des pratiques d'évaluation, notamment à des fins de recrutement. Or il est relativement aisé de produire un article, plus en tout cas qu'une thèse ou qu'un livre : c'est moins long, généralement moins complexe à structurer, et souvent écrit de manière collective, ce qui permet de se démultiplier. Tout ceci facilite la production rapide d'articles, et c'est une pente glissante vers la corruption des pratiques de publication (surproduction, falsification, duplication…).

***

J'écris ce billet en réaction à [un tweet](https://twitter.com/DamienPetermann/status/1475514274269302788) de Damien Petermann :

> « Pourquoi écrit-on des articles scientifiques ? En vrai, pas dans l'idéal hein. Non, sérieusement, plus me j'interroge sur le sujet, moins je comprends le fonctionnement de la recherche et le sens de tout ça. En nombre, en utilité, en pertinence… »

Voici quelques-unes des réponses :

> [Laurent](https://twitter.com/ljegou/status/1475522797086597120) : « Parce qu’on croit encore dans la recherche basée sur les échanges, la discussion, la diffusion des connaissances. »
> 
> [Sophie](https://twitter.com/EurydiceSophie/status/1475751505798078465) : « Pour faire avancer son champ, pour faire connaître ses objets d’études. Pour interroger des catégories aussi. »
> 
> [Quentin](https://twitter.com/moreau1_quentin/status/1475514955059322881) : « Pour avoir des publications, puis des citations et enfin un poste ? »
> 
> [Pierre](https://twitter.com/pierre_bat/status/1475541007844397073) : « Perso, j'écris les articles que j'aimerais lire. C'est ma principale boussole. »
> 
> [Joan](https://twitter.com/Jojo848329021/status/1475515371822227463) : « Parce qu'on ne sait plus faire que ça au bout d'un moment ! »
> 
> [Camille](https://twitter.com/CamMortelette/status/1475556297458307078) : « Il n'y a pas vraiment de réponse à ta question… Tu auras autant de réponses que de personnes je pense. »

Ce petit échantillon montre la richesse et la complexité de la question, que je ne peux évidemment pas prétendre résumer avec un billet de blog. Mais cela m'a quand même donné envie d'écrire, ou de m'exercer à écrire^[Tout ceci est très méta…].

Dans la discussion qui s'ensuit, Damien [précise sa question](https://twitter.com/DamienPetermann/status/1475522749670084620) et le désenchantement qu'il ressent :

> « Combien d'articles sont réellement lus ? Nos bibliographies ne cessent de s'allonger, les PDF non lus s'empilent dans les dossiers, etc. Je questionne la pertinence et le sens de cette usine à publications. »

La question est très différente de celle initialement posée, et que j'ai traitée comme un petit exercice de réflexion synthétique. Ici, Damien pointe un aspect de la publication académique qui, effectivement, n'est pas franchement enthousiasmant.

Il est indéniable que la prolifération des références complique la recherche d'information. Mais c'est un problème classique, qui revient de manière cyclique depuis l'invention de l'imprimerie. L'apparition de la documentation, notamment autour du travail de Paul Otlet et ses collègues au tournant du 20^e^ siècle, répondait à une prolifération similaire à celle qu'on connaît aujourd'hui avec l'informatique en réseau. Nous avons des outils pour aborder ce nouveau cycle, que ce soit sur le plan pratique avec le progrès des systèmes d'information, ou sur le plan théorique avec les travaux sur l'intelligence collective.

Il est également indéniable que la corruption des pratiques de publication est dommageable pour le progrès des connaissances scientifiques. Mais je crois que nous avons de bonnes raisons d'espérer voir une amélioration dans les années qui viennent, notamment grâce à l'innovation dans les techniques d'édition, les pratiques d'évaluation et l'accès aux publications. Évidemment, le problème c'est que l'édition scientifique professionnelle oppose une formidable capacité de résistance à toute réforme du marché, car elle est jalouse de sa rente. Mais je préfère rester optimiste.

Ceci étant dit, la difficulté posée par la prolifération des articles est que nous ne sommes pas égaux face au problème. D'abord, il y a une part de caractère : certains aiment lire énormément, d'autres préfèrent lire peu. Les premiers risquent la noyade ; les seconds doivent faire feu de tout bois pour trouver du sens, au risque de tomber dans le non-sens ou la vacuité. Ce sont deux écueils très différents. Ensuite, le sujet de nos recherches définit en partie le volume de la bibliographie. Enfin, notre parcours (études, encadrement doctoral, échanges informels…) dessine une trajectoire très spécifique, surtout en France. Tout ceci contribue à individualiser le ressenti de la prolifération des articles scientifiques.

Ainsi, je n'ai pas le même ressenti que Damien : je lis de manière extrêmement sélective et je crois que cela me prémunit du désenchantement qu'il décrit. C'est d'abord par inclination : je fais partie de ceux qui ont peur de se disperser. Mais c'est aussi une conséquence de mon sujet de thèse : je dois pratiquer une lecture serrée d'un petit nombre de textes théoriques très denses. Cela fait donc quatre ans que je travaille avec un corpus bibliographique restreint, et cela façonne en partie mes habitudes, même pour d'autres sujets de recherche. Enfin, les enseignants qui m'ont influencé de manière durable m'ont plutôt conseillé de lire moins et mieux, et d'écrire, toujours écrire.

Dès lors comment intervenir dans la discussion initiée par Damien, et proposer une réponse qui ne sonne pas comme venant de la planète Mars ? Si je lui dis « Tu n'as qu'à lire moins d'articles ! », ça marche dans mon cas mais est-ce applicable à sa situation ? Et puis, il y a cet autre problème encore évoqué par Damien : l'article scientifique est le plus souvent produit par des universitaires, or l'insertion professionnelle des jeunes docteurs dans cet univers-là est plus compliquée que jamais. Dès lors, comment se projeter avec confiance dans la lecture et l'écriture d'articles, en plus de toutes les questions déjà soulevées plus haut ?

***

Concluons sur une note d'optimisme. De la même manière que nous ne parlons pas tous exactement le même français, ni le même vocabulaire scientifique, nous travaillons et sommes travaillés différemment par les formats comme l'article. L'enjeu est donc d'en parler entre nous : enrichissons-nous de l'introspection, du partage d'expérience et du débat. Cela nous permet de défaire progressivement l'impensé[@robert2020], puis de faire évoluer les formats et les pratiques, dont rien ne dit qu'elles doivent être figées.

# Bibliographie