---
title: Cosma, de la fiche au graphe
date: 2021-09-04
abstract: "Une année de travail porte ses fruits. Nous avons créé un logiciel de visualisation de graphe documentaire, appelé Cosma. Dans ce billet, je présente rapidement le problème auquel répond le logiciel, l'originalité de la solution proposée, le public visé et quelques autres informations utiles sur le projet."
lang: fr-FR
---

En novembre 2020, j'ai publié un billet intitulé « [De la fiche au graphe documentaire](https://www.arthurperret.fr/fiche-graphe-documentaire.html) » dans lequel j'expliquais avoir entrepris la conception d'un outil permettant de visualiser des fiches interreliées. Ce ne sont pourtant pas les outils de ce genre qui manquent (Roam et Obsidian s'imposent parmi les plus connus) mais il existe une niche d'utilisateurs pour lesquels aucune solution ne me paraissait alors satisfaisante : les auteurs de textes scientifiques travaillant avec le format texte, utilisant peut-être Zettlr, connaissant probablement Pandoc, et qui cherchent à accumuler une documentation personnelle dont la durée de vie soit supérieure à celle du dernier logiciel à la mode et des services payants qui vont avec.

Pour ce public, auquel j'appartiens, j'imaginais l'intérêt que pourrait avoir un logiciel fonctionnant indépendamment de l'outil d'écriture, reposant sur des formats ouverts, bâti pour le partage des connaissances via le Web. Un logiciel qui ne repose pas sur le tout-automatisé mais promeuve plutôt l'utilisation de métadonnées explicites et d'identifiants uniques. Un logiciel qui inclue des fonctionnalités d'organisation des connaissances (indexation, classification) mais dans une logique créative plutôt qu'extractive, c'est-à-dire axée sur la réflexivité dans une pratique d'écriture plutôt que sur le moissonnage et l'analyse de données externes.

Je suis heureux de partager le fait qu'après un an de travail, ce logiciel existe enfin ! Nous l'avons appelé Cosma. Il permet de représenter des fichiers interreliés sous la forme d’un réseau interactif dans une interface web ; en d'autres termes, c'est un logiciel de visualisation de graphe documentaire. Je vous invite à visiter [le site dédié à Cosma](https://cosma.graphlab.fr) pour plus d'informations. Vous pouvez essayer l'essentiel des possibilités offertes par le logiciel via une démo accessible sur la page d'accueil du site.

<figure class="fullwidth">
<figcaption>L'interface de Cosma, dans laquelle on reconnaît l'ADN de l'Otletosphère : un menu, un graphe, des fiches.</figcaption>
<img src="https://cosma.graphlab.fr/img/screenshot-cosma.fr.png">
</figure>

# En quoi ça consiste ?

Cosma s'attaque à un problème classique qu'est la gestion des connaissances personnelles (*personal knowledge management*, PKM), décliné dans un domaine bien particulier : la prise de notes interreliées. Dans ce domaine, il existe déjà de nombreuses méthodes et outils. Cosma est proche notamment de la méthode Zettelkasten, et des objets hypertextuels comme les wikis. Mais la solution que nous avons conçue n'a pas vraiment d'équivalent si on considère ses deux principales spécificités[^1].

[^1]: Si toutefois vous connaissez un logiciel similaire – dans le domaine des notes interreliées, des *tools for thought*, de la gestion des connaissances personnelles (PKM) –, n'hésitez pas à me le signaler.

Premièrement, **ce n'est pas un éditeur de texte** – *exit* donc la comparaison directe avec un programme comme Obsidian. Cosma lit un répertoire de fichiers et génère une visualisation interactive des connexions entre ces fichiers. C'est donc un logiciel qui s'utilise en complément d'un outil d'écriture.

Pourquoi ne pas avoir fait un éditeur de texte ? C'est d'abord une décision **pragmatique** : nous n'avions pas la capacité de développer un éditeur de texte, chose extrêmement complexe, et qui livrer un résultat suffisamment convaincant pour attirer des personnes ayant déjà investi beaucoup de temps dans la maîtrise d'autres outils. C'est aussi une décision **idéologique** : j'ai une préférence pour les logiciels qui me laissent choisir mon outil d'écriture, comme le fait par exemple [Deckset](https://www.deckset.com) pour les présentations. Le critère pragmatique m'a pour ainsi dire permis de pousser cette préférence dans le processus de conception. Et puis c'est aussi un choix **stratégique** : en créant un outil qui ne se positionne pas en concurrence directe des éditeurs de texte mais qui n'est pas non plus une application en ligne, nous avons investi une toute petite niche, qui inclut par exemple [zkviz](https://github.com/Zettelkasten-Method/zkviz). L'intérêt, c'est que l'application ne concurrence aucun *workflow* existant, et peut donc intéresser beaucoup de monde. Mieux, elle représente une alternative à des services de partage payants comme  [Obsidian Publish](https://obsidian.md/publish).

Deuxièmement, avec Cosma **il y a une application dans l'application**.

Je m'explique : Cosma est une application pour ordinateur, mais son fonctionnement principal consiste à générer et afficher un fichier HTML, que nous surnommons un *cosmoscope*, et c'est ce dernier qui représente la véritable interface d'exploration. Le cosmoscope contient à la fois le texte (mis en forme) des fiches, le graphe interactif mais aussi des liens et rétroliens contextualisés, un index, un moteur de recherche et la capacité de filtrer l'affichage par type de fiche et par occurrence de mots-clés. Mais surtout, **ce fichier est autonome** : il peut être utilisé hors de l'application Cosma, ne nécessite pas de connexion à Internet, et peut être partagé comme bon vous semble (par mail, messagerie, transfert de fichier, ou même mis en ligne sur un serveur, comme [notre démo](https://cosma.graphlab.fr/demo.html)). Ainsi, que vous soyez la personne qui élabore les fiches ou celle qui consulte le cosmoscope qui en résulte, vous disposez des mêmes capacités heuristiques.

<figure class="fullwidth">
<figcaption>Le cosmoscope au cœur de Cosma : un fichier HTML autonome.</figcaption>
<img src="https://hyperotlet.huma-num.fr/cosma/img/cosma-cosmoscope-html.png">
</figure>

# À qui s'adresse Cosma ?

Cosma est pensé d'abord et avant tout pour les travailleurs de la connaissance, les personnes qui accumulent de la documentation sur une thématique donnée, avec souvent une communication de ces connaissances sous une forme ou une autre. La fonction première de Cosma est de fournir une meilleure vision globale sur un ensemble de documents interreliés, sous une forme qui soit facile à partager tout en restant intéressante d'un point de vue heuristique.

Ce sont les doctorants comme moi qui ont le plus à bénéficier d'un tel outil sur le long terme, car la thèse est le meilleur prétexte pour amorcer une pratique de documentation personnelle. Cependant, et bien que nous soyons effectivement partis de ce cas d'usage très précis, nous avons finalement abouti à un logiciel suffisamment souple pour être utile à différentes personnes – chercheurs, enseignants, étudiants, bibliothécaires, ingénieurs, journalistes… Cosma intéressera particulièrement celles et ceux qui souhaitent créer puis diffuser des connaissances expertes d'une façon originale [^2].

[^2]: En fait, d'après mon directeur de thèse, la modalité de partage que représente Cosma faisait partie des possibilités offertes par certains logiciels de cartes mentales dans les années 2000 et 2010. On peut donc parler de résurgence.

Ces connaissances peuvent avoir une forme variable mais étant donné le public visé on peut imaginer qu'il s'agira le plus souvent de fiches descriptives densément interreliées, à la façon d'un wiki. Nous avons d'ailleurs conçu Cosma avec deux principaux usages en tête : la représentation d'un lexique spécialisé, qu'il soit personnel (idiotexte) ou lié à une communauté (vocabulaire contrôlé) ; et l'élaboration d'une documentation personnelle sur le long terme. Les deux pratiques peuvent d'ailleurs se recouper. Dans les deux cas, Cosma encaisse aussi bien quelques dizaines de fiches que plusieurs centaines ou plusieurs milliers, et permet d'utiliser n'importe quelle typologie de fiches et de relations. Quant au partage sous forme de cosmoscope, il peut appuyer une démarche scientifique, pédagogique, médiatique, interprofessionnelle…

# Comment se lancer ?

Cosma fonctionne particulièrement bien en tandem avec un éditeur de texte spécialisé dans l'écriture scientifique et hypertextuelle comme [Zettlr](https://zettlr.com). Rien de plus normal, étant donné que le point de départ du projet était d'obtenir une vue graphe similaire à celle d'Obsidian mais adaptée aux contenus créés via Zettlr. Cosma et Zettlr habitent le même écosystème technique en ce qui concerne l'écriture, les identifiants, les métadonnées et les citations.

Toutefois, rien ne vous oblige à utiliser Zettlr : n'importe quel éditeur de texte fera l'affaire pour tester rapidement Cosma. Téléchargez les fiches de démonstration [sur le dépôt GitHub](https://github.com/graphlab-fr/cosma/releases/download/1.0/cosma-fiches-aide.zip), ouvrez-les dans un simple bloc-notes et insérez quelques liens à la main ; vous pouvez vous aider de [la documentation utilisateur de Cosma](https://graphlab-fr.github.io/cosma/fr.html) pour savoir comment faire. À terme, ce sont bel et bien les fonctionnalités liées à l'insertion des liens qui peuvent vous inciter à adopter tel ou tel éditeur : Cosma repose sur l'interrelation des fiches, ce qui peut être laborieux à faire entièrement à la main, ou à l'inverse devenir extrêmement fluide avec le bon outil.

# Mais au fait, pourquoi « Cosma » ?

Le nom du logiciel est lié à deux personnages notables dans l'histoire de l'organisation des connaissances. Il y a d'abord Cosma Rosselli, auteur d'un *Thesaurus artificiosæ memoriæ* (1579) cité par Umberto Eco [-@eco2010, 121-122], titre qui se traduit littéralement par « trésor de mémoire artificiel » – un qualificatif qui s'applique très bien à notre logiciel.

[^paulotlet] Et puis il y a évidemment Paul Otlet, mon sujet de thèse. Nous avions en tête un schéma dans lequel il représente son Mundaneum, « machine à penser le monde », comme composé d'un « cosmographe » et d'un « cosmoscope » permettant respectivement d'enregistrer et de consulter la connaissance. L'architecture de Cosma est basée sur le même principe, nous avons d'ailleurs repris les noms. C'est un outil qui permet d'inscrire et de visualiser un univers intellectuel : un cosmographe autant qu'un cosmoscope.

[^paulotlet]: {-} ![](../img/hyperdocumentation-2.jpg) Le schéma entier est reproduit dans notre article « [Hyperdocumentation](https://www.arthurperret.fr/hyperdocumentation.html) ».

# Le futur de Cosma

Cosma reste un logiciel expérimental, qui a été produit dans le cadre d'une démarche de recherche-conception financée par le programme ANR HyperOtlet. Ceci a plusieurs implications. D'abord, Cosma est mis à disposition gratuitement, et son code est dès à présent réutilisable librement sous les conditions permises par la licence GNU GPL 3.0. Néanmoins, le financement étant ponctuel, nous ne pouvons pas envisager une maintenance ou une évolution constantes. L'autre implication, c'est donc que Cosma est proposé de manière ouverte à une communauté de praticiens curieux, qu'on espère prêts à supporter quelques bugs inévitables qui pourront mettre un certain temps à être corrigés.

Comme il s'agit d'une réalisation majeure pour ma thèse, j'espère pouvoir continuer à travailler avec (et sur) Cosma dans les mois, voire les années qui viennent. Je ne prends pas d'engagement, car il s'agit d'une réalisation collective, située, qui est le fruit de circonstances particulières. L'avenir à l'université est aussi incertain qu'ailleurs. Mais je ferai mon possible pour que ce logiciel porte ses fruits, [dans la perspective que traçait Andy Matuschak](https://www.arthurperret.fr/veille/difficile-progression-tools-for-thought.html), un spécialiste des *tools for thought* : créer des outils pour qu'ils génèrent des observations généralisables dans le cadre d'une réflexion scientifique.

# Référence