---
title: "Recommandation : Zettlr"
date: 2019-09-06
---

Je profite de la mise à jour d'un logiciel que j'aime bien pour faire un coup de projecteur dessus. [Zettlr](https://zettlr.com/) est un chouette éditeur de texte taillé pour la recherche, qui ne se contente pas de rendre accessible mes techniques favorites mais propose par-dessus une myriade de fonctionnalités agréables à l'usage. La version 1.4 vient de sortir et c'est le meilleur moment pour l'essayer.

En préparant ces lignes, je cherchais quelques arguments bien tournés pour appuyer ma recommandation. J'avais le souvenir d'un billet intitulé [*En finir avec Word !*](https://eriac.hypotheses.org/80) et en le relisant voilà que je tombe sur cette phrase : « les traitements de texte sont une mise au service de l’ordinateur d’une technologie pourtant ancienne, qui est l’imprimerie ».[@dehut2018] Les personnes qui consultent ce blog savent que j'aime beaucoup l'expression « imprimé post-numérique », traduction libre de l'ouvrage éponyme d'Alessandro Ludovico[@ludovico2013]. Par association d'idées, cette phrase m'a fait songer à l'expression « numérique post-imprimé » : une façon pour moi 1/ de faire un jeu de mots à peu de frais et 2/ de désigner ce moment dans lequel nous sommes et qui voit se consolider une pratique d'écriture scientifique dédiée aux écrans tout en étant capable de cohabiter productivement avec l'édition papier.

J'ai fini par trouver les arguments que je cherchais, et qui expliquent bien l'enjeu de cette pratique. Julien Dehut, l'auteur d'*En finir avec Word*, présente les bénéfices de l'usage de Markdown ou de LaTeX en sciences (et la pratique des langages informatiques en général). Il souligne notamment que :

- cela permet d'être libre du choix de son logiciel ;
- dans ce que nous écrivons, ce qui est interprétable par la machine reste également lisible par l'humain ;
- cette pratique conduit à une meilleure compréhension de notre outil de travail.

Cela passe par une opération de démolition en règle : celle de Word, rhabillé pour l'hiver, mais également d'une certaine terminologie. Julien Dehut mentionne par exemple la critique du terme « application » par Ted Nelson ; on pourrait y ajouter celle du terme « utilisateur », comme chez Johanna Drucker[@drucker2011a].

Mais revenons au contexte. Si je déterre ce billet, c'est pour y faire un petit ajout — non pas dans la bibliographie, colossale et encore tout à fait d'actualité, mais dans la liste intitulée « Quelques ressources pour aller plus loin avec le Markdown et Pandoc », située en toute fin d'article. Les bases (les fondations devrais-je dire) sont bien là, avec la trinité syntaxe (Markdown) / conversions (Pandoc) / références (Zotero + Better BibTeX). C'est une recommandation de logiciel que je voudrais y ajouter.

Julien Dehut mentionne plusieurs noms de logiciels spécialisés dans l'écriture en Markdown mais c'est un logiciel généraliste, l'éditeur de code Atom, qui semble avoir sa préférence : il le mentionne en premier et liste quelques extensions ou astuces destinées à en faire un outil taillé sur mesure pour ses besoins. Je suis moi-même un adepte de BBEdit : comme je l'expliquais [en mars dernier](et-a-la-fin-ecrire.html), mon outil d’écriture idéal n'est pas celui qui prétend proposer des fonctionnalités adaptées à tous mes besoins mais bien celui qui est suffisamment modulable pour que je les y intègre moi-même.

Toutefois, je suis également d'accord avec Julien Dehut lorsqu'il écrit :

> « La production des documents à partir du Markdown en conjonction de Pandoc demande dans un premier temps un certain investissement, en tout cas une motivation quotidienne, et peut-être quelque chose d’une résolution indéfectible[@dehut2018] ».

Il existe aujourd'hui au moins un logiciel qui devrait se hisser au sommet des recommandations de ce genre d'articles, précisément parce qu'il facilite cet investissement. Si la réappropriation de l'écriture informatique se fait au quotidien, il faut une solution qui s'y intègre en répondant à plusieurs besoins de façon attractive ; c'est ce que j'ai trouvé dans Zettlr.

Zettlr combine deux choses : les fondations que j'ai mentionnées (Markdown + Pandoc + biblio) mais aussi des fonctionnalités qui relève de la « qualité de vie » dans l'expérience utilisateur. C'est un excellent compromis entre la technicité des programmes sous-jacents et les apports indéniables d'une bonne interface.

Premièrement, ce logiciel constitue un *wrapper* pour Pandoc : il encapsule le processus de conversion en nous épargnant le passage par la ligne de commande mais nous laisse la main sur les détails via une petite interface de paramétrage. C'est une bonne façon de rendre accessible au grand public ce que des gens comme moi font en mode usine à gaz (cf. [BBPandoc](https://github.com/jrgcmu/BBpandoc)) ou ce que des personnes plus compétentes en informatique proposaient déjà pour certaines conversions spécifiques (cf. [DocDown](https://github.com/lowercasename/docdown)).

Deuxièmement et peut-être de façon plus significative, l'expérience utilisateur de Zettlr semble avoir été pensée suivant les principes de l'amélioration progressive : de multiples fonctionnalités sont proposées sans rien enlever à la proposition fondamentale. Cela inclut des options liées à l'apparence (mode sans distraction, mode nuit, thèmes, personnalisation de l'interface en CSS), la rédaction (traduction, analyse de la lisibilité), la documentation (tags, liens entre documents, insertion automatique des clés de citation), l'organisation du travail (liste de tâches, minuteur Pomodoro), etc. Beaucoup de ces options sont à plusieurs étages : en un clic, l'utilisateur novice peut tester une fonctionnalité ; via des contrôles supplémentaires, l'utilisateur avancé peut s'approprier pleinement l'outil.

Je ne veux pas transformer ce billet en une critique complète du logiciel : restons-en à une vive recommendation. Zettlr est un excellent outil pour écrire de la recherche, circuler dans ses articles, ses notes et les exporter. Étant donné qu'il ne crée pas son propre répertoire de fichiers ni ses propres bizarreries syntaxiques, l'essayer est sans conséquence ; qui plus est, cela le rend complémentaire de logiciels comme par exemple StackEdit pour éditer du Markdown dans le *cloud*.

Un dernier mot si vous débutez dans cet univers. Ne faites pas la moue devant l'installation pré-requise de Pandoc ou de LaTeX : vous n'avez pas besoin d'interagir avec, Zettlr les prend en charge pour vous. Prenez le temps d'essayer l'outil régulièrement pendant quelques temps : au-delà du petit zêta de Zettlr, c'est tout un champ de techniques et d'outils qui peut vous intéresser. Alors allez-y !

# Références