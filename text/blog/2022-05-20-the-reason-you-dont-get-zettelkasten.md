---
title: "The reason you don’t ‘get’ Zettelkasten"
date: 2022-05-20
description: "Have you thought about why you might need it? Let’s ask Umberto Eco."
lang: en-US
---

::: box
*This post is a translation of* [“Et toi, qu’est-ce que tu fiches ?”](2022-05-20-et-toi-qu-est-ce-que-tu-fiches.html)
:::

This week, there was an online conference on linked notes, the [Linking Your Thinking Conference](https://www.linkingyourthinking.com/conference). The event was put under the umbrella of personal knowledge management (PKM). Many authors took the opportunity to share their thoughts on the subject and I saw a bunch of articles on different methods: how to take notes, make links, manage writing projects, etc.

Reading through some of these, I thought about two things.

First, I think that most of these methods can be merged. They touch on such fundamental issues that, essentially, their differences come down to marketing. David Allen's *Getting Things Done* method outlined the elements of task management in such a definitive way (capture, process, create) that innovation now happens mostly at the margins. Any new, supposedly definitive method seems to me like it tries to reinvent the wheel.

Second, I've read the following idea several times: people enthusiastically discover interrelated note-taking tools but quickly become disillusioned, because they struggle to make sense of them. In [“The essence of the Zettelkasten method, demystified”](https://feeei.substack.com/p/the-essence-of-the-zettelkasten-method) for example, author Fei-Ling Tseng complains that the aforementioned method does not come with instructions – hence the need to “demystify” it. In her case, the struggle produced something useful and clever: she imagined the Idea Compass, a series of prompts for coming up with ways to link your notes. But underlying her initial remark, I see a problem.

People look at a tool and ask themselves, “What can I do with it?”. I'm not saying that nothing interesting can come out of this line of questioning, but it seems like a designer question, something to wonder about when your job is to push the limits of what can be done. For most of us, I think looking at the tool and asking how to use it is twice misguided: it’s the wrong question, and it formulates the problem the wrong way around.

The first question to ask is “what do I want/need to do?”. The second is “what tool can help me?”.

# A link to the past

A master's or doctoral student in the humanities could read Umberto Eco's *How to Write a Thesis* today and start using a tool like Zettlr or Obsidian right away without any problem; the only prerequisite would be to learn Markdown, which can be done [in ten minutes](https://commonmark.org/help/).

Why? Because Eco's book starts with the first question: “what do I have to do?”. In this case, the answer is: write a thesis. Eco explains the exercise perfectly, including how note-taking fits into it, which gives note-taking meaning and purpose. Management experts try to build meaning *ex nihilo* for their ready-made methods but it can’t make any sense to you if you haven’t considered the extrinsic needs that drive your note-taking practice.

I mention *How to Write a Thesis* because its advice is especially timeless. This may not seem apparent depending on the edition you read.

In the one I have, the translator states that the passage of time “has rendered many of Eco's tips rather obsolete [@cantagrel2018, 332]”. This may be true for the very last part, which concerns the writing, layout and printing of the manuscript, and where Eco explains that you should underline to indicate italics when you use a typewriter. But his advice on note-taking has not aged a bit. Translators for the 2015 MIT Press edition got it much better:

> « We have preserved Eco’s handwritten index card research system in all its detail, precisely because it is the soul of *How to Write a Thesis*. Obviously the card catalog of the small town library is primitive compared to today’s online research systems, but the research skills that Eco teaches are perhaps even more relevant today [@mongiatfarina2015, xvii] ».

What Eco says about index cards (in French: *fiches*) is entirely applicable today, especially if you use fancy hypertextual personal documentation tools.

He explains that cards, being mobile units, form the basis of a powerful and flexible system:

> “You can move the index card, merge it with the others, place it before or after another card in the file.” [@eco2015, 117]

Eco suggests that we create different types of cards according to the work we need to do: readings, themes, quotations… Each can have its own type of card. These types are applicable to any subject, because they are related to the organization of knowledge and not to the knowledge itself.

> “Depending on the context, we can create index cards of various types: connection index cards that link ideas and sections of the work plan; question index cards dealing with how to confront a particular problem; recommendation index cards that note ideas provided by others, suggestions for further developments, etc.” [@eco2015, 118]

These form the connective tissue of the file. They literally serve to make connections between ideas written on other cards.

Finally, Eco advocates a rigorous documentary approach: make use of metadata in your cards, and make it so that the file is complete and homogeneous. This is what allows you to apprehend your work as a whole, a role fulfilled today by indexes, lists of keywords, and [graph views](2022-02-13-what-is-the-point-of-a-graph-view.html).

> “Always work on homogeneous material that is easy to move and handle. This way, you will know at a glance what you have read and what remains to be read.” [@eco2015, 119]

Eco’s advice is still perfectly applicable today. Students who need to take notes can take it word-for-word, including when using interrelated note-taking tools.

# Choosing the right tool

This is where the second question comes in: “which tool can help me?” It is much easier to select the appropriate tool when you know what you need to do with it. If you’re a student who needs to compile a bibliography, take notes on your readings and synthesize your ideas, all to help with a scientific writing project, you will quickly filter out tools and find the one that can help you accomplish all this. In the case of linked notes for research, [Zettlr](https://www.zettlr.com/) comes to mind, along with [Cosma](https://cosma.graphlab.fr/en/).

This applies to other types of knowledge work. You already have goals that include the need to take notes. Knowing what they are helps you find a tool and a method that makes sense. It might be Zettlr and the Zettelkasten method. It may be an outliner like [Roam](https://roamresearch.com/) or [Logseq](https://logseq.com/). It may be a toolbox like [Obsidian](https://obsidian.md/) or [VS Code](https://code.visualstudio.com/), loaded with plugins.

Where it gets complicated is when you haven't thought about your goals beyond note-taking. Then, sure, you may feel a bit helpless when discovering Zettelkasten—just the name alone is enough to confuse you if you don’t know German…

What’s strange, is that the underlying principles of interrelated note-taking tools should be familiar to us. It’s hypertext, which we see everytime we browse the Web. It’s association, the very mechanism that Vannevar Bush said is the basis of the way our mind operates [@bush1945, 106].

But perhaps this is why it’s difficult to grasp. It’s too generic. If you bury it under jargon, it becomes even more difficult to ‘get’. And without purpose, it makes even less sense. But put it in the context of a broader objective and then it becomes much clearer.

# References