---
title: "History and information: notes from a workshop"
date: 2022-06-22
description: "Notes from a workshop with Michael Buckland and Paul Duguid."
lang: en-US
---

I attended a workshop at [IUT Bordeaux Montaigne](https://www.iut.u-bordeaux-montaigne.fr), organized by the [E3D team from the MICA laboratory](https://mica.u-bordeaux-montaigne.fr/e3d/). As part of a project supported by the [France-Berkeley Fund](https://fbf.berkeley.edu), Olivier Le Deuff & Rayya Roumanos went to Berkeley a few weeks ago, and this week they welcomed Michael Buckland (Professor Emeritus) and Paul Duguid (Adjunct Professor) for a return visit. Their research collaboration centers on the work of [Robert Pagès](https://people.ischool.berkeley.edu/~buckland/pages.html), a document theorist and contemporary of [Suzanne Briet](https://people.ischool.berkeley.edu/~buckland/briet.html). His 1947 article on document theory was translated in English and published as [a special issue](https://ideaexchange.uakron.edu/docam/vol8/iss1/) of the Proceedings of the Document Academy.

I shared notes live [on Twitter](https://twitter.com/arthurperret/status/1542049217052655616), with screenshots of the presenters’ slides. I re-publish them here slightly revised, without the images but with proper references for those who want to follow up on the literature.

# Michael Buckland on Robert Pagès

Buckland presented Pagès’s work. According to Pagès, “documentation is to culture what machinery is to industry”.[@pages2021, 1] He challenges Descartes’ dichotomy between lived experience and bookish knowledge, arguing that new media combines these to create quasi-experiences.

Pagès describes mass documentification, and discusses its political implications, which are well understood, taking us all the way to “alternative facts” and fake news. According to Buckland, the economic impact of documentification is less discussed but just as real: for instance, falsification, dubious documents played a part in the 2008 Great Recession.

Pagès can help us with current issues in information science. For instance, Buckland argues (with Wayne De Fremery) that context (personal, cultural) should be emphasized more when defining relevance, significance.[@defremery2022] This was Pagès’ view as well. Another example: Pagès’ distinction between particular and specimen can be criticized to fuel discussions on copy theory.[@defremery2022a]

Apart from Pagès, Buckland currently works on library catalogs. According to him, the library catalog obeys to an obsolete rationale—it is limited to what is accessible in the library. There is more material outside than inside the library now, and so library catalog design should be challenged.

***

Clément Borel asked where we should look for other pioneers, and Michael Buckland pointed to the “golden age” of documentation post-World War II (with the figures of Briet, Pagès, De Grolier…). This period, neglected by the French founders of information and communication sciences (SIC), is now being rediscovered. Olivier Le Deuff pointed to an edited book of Fidelia Ibekwe[-@ibekwesanjuan2014] and added that the history of this period is complicated by interpersonal conflicts between the pioneers.

Rayya Roumanos asked whether Pagès was influent during his time. Not really, according to Buckland. His indexing language CODOC, much like others from the same period (works by Jean-Claude Gardin, James Perry), was powerful but complex, and ultimately was replaced by much simpler techniques. But it could come back, supported by increased computing power and artificial intelligence.

# Paul Duguid on history and information

With Ann Blair, Anja-Silvia Goeing and Anthony Grafton, Paul Duguid recently co-edited a massive book[-@blair2021] that looks at information through the lens of history and vice-versa.

Duguid reminded us that the “golden age” of documentation mentioned by Buckland was very much distinct from what was going on at the same time in the Anglo world, influenced by Shannon’s mathematical theory of communication and Wiener’s cybernetics, and which ultimately became hegemonic.

As Duguid explains, history has absorbed the concept of information very slowly. Elizabeth Eisenstein mentions it 50 times in the 800 pages of her history of the printing press [-@eisenstein1979], whereas Paul Dover uses it 50 times in the first 8 pages of his 2021 book on the same topic[-@dover2021].

Work on the *Companion* began with difficult historical questions—the what and when of information. But the biggest question was: should we even bother with history? Don’t computers make the past irrelevant? Duguid opposes this: we should be critical of the idea of technological revolution, of a “coupure” (Foucault). After all, some authors from the 17th century called their own times an “age of information”!

Beyond explicit references to information, there have also been information-based ideas throughout history. Info-optimism for instance, from Shakespeare, Milton, Adam Smith’s invisible hand, to Jürgen Habermas’s public sphere. For Duguid, info-optimism verges on naivety, and this is also illustrated by more contemporaneous examples, such as Google Books (Duguid recalls an exchange with someone involved which went like this: “Which books are you going to select? — Oh, all of them”).

Other information issues that appear throughout history: quantification, evaluation, circulation, extraction… often along info-optimism (especially in the case of Paul Otlet).

# Trust and context

A major theme emerged during the workshop: trust (with regards to information) and the importance of context in establishing that trust. Buckland stresses that we can’t know everything, so we need to choose to trust or not; and that choice is entirely contextual. He alluded to a book on this topic by Patrick Wilson[-@wilson1983], who had a rather pessimistic view. With regards to more recent times, Rayya Roumanos mentioned some of the trends in journalism that try to address the crisis of trust, from over-simplification (e.g. add bullet points before the lede) to over-explanation (e.g. include a making-of).

A great event, and a lot of literature to digest!

# References

