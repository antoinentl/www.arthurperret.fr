---
title: "L'impensé des formats : réflexion autour du PDF"
date: 2021-03-07
abstract: "Une analyse de la relation compliquée entre format et pratique du texte académique en régime numérique."
lang: fr-FR
---

Le format PDF m'occupe beaucoup ces jours-ci. Il s'impose dans ma pratique individuelle, car je fais beaucoup de vérifications de littérature dans l'optique de la rédaction de ma thèse ; dans des problématiques de gestion documentaire au sein de différents collectifs ; et dans l'actualité des outils que j'utilise, Zotero venant d'[annoncer](https://www.zotero.org/support/pdf_reader_preview) un lecteur de PDF + annotateur qui fait frémir d'excitation un grand nombre d'utilisateurs.

Cette omniprésence a fait remonter des réflexions que j'ai pu lire ou entendre ces derniers mois, et qui dormaient sagement dans mes notes en attendant leur heure.

# Quand l'information résiste à l'extraction

Les 26 et 27 novembre 2020, l'EPFL a organisé [un atelier](https://dhcenter-unil-epfl.com/en/event/epfl-unil-workshop-on-digital-research-practices/) sur le thème « Traces numériques, parcours de navigation et mobilités intellectuelles : comment les chercheurs naviguent dans les bibliothèques et les plateformes en ligne ».

[Marc Jahjah](http://www.marcjahjah.net/a-propos) faisait partie des intervenants, avec une conférence intitulée « Annoter extraire exploiter : vers une pratique imaginale du savoir »[^1]. Durant sa présentation, il a évoqué la problématique du corps au travail à travers les conséquences douloureuses de la logique extractiviste : sa pratique intensive des logiciels d'annotation comme [Liquid Text](https://www.liquidtext.net) [^2] l'a en effet marqué physiquement.

Marc a fait un parallèle qui m'a frappé, entre l'extraction du savoir par l'industrie du travail intellectuel et l'extraction des ressources naturelles par l'industrie lourde. Nous savons que la Terre réagit violemment à l'extractivisme – tremblements de terre, coulées de boue, effondrement de mines. Mais je n'avais jamais considéré la résistance que peut opposer le savoir à l'extraction sous ce même angle, notamment lorsque le format PDF est impliqué. Bien sûr, l'échelle est très différente (je ne m'apprête pas à blâmer le PDF pour le dérèglement climatique…) mais le phénomène est bien là.

En effet, le parallèle établi par Marc m'a immédiatement fait penser aux nombreuses difficultés que je rencontre lorsque j'essaye de copier des fragments dans un PDF : sélection qui ne suit pas le flux du texte et capture le paratexte ; diacritiques qui deviennent des caractères autonomes et se superposent aux lettres ; césures inutiles et erreurs de reconnaissance optique qu'il faudra modifier ; etc.

Dès que le texte est un peu ancien ou marginal, dès que l'OCR a été fait un peu vite ou avec une technologie non aboutie, c'est la galère pour relever des citations et constituer une fiche de lecture. Si la métaphore n'était pas déjà réservée à un autre usage, je parlerais volontiers de *text mining*, avec ce sentiment désagréable que sur beaucoup de filons les pioches cassent et les galeries s'écroulent. Cela m'est arrivé plus d'une fois, et je n'ai rien d'un archiviste : il suffit de remonter au 20^e^ siècle pour constater, documents à l'appui, que la redocumentarisation est un chantier complexe.

L'appropriation de certains textes constitue un véritable calvaire, mais peut-être faut-il questionner la démarche. Est-ce que cela fait encore sens d'accumuler laborieusement quelques maigres pépites de capital textuel aussitôt remisées dans un silo individuel et précaire ? Est-ce qu'on ne pourrait pas plutôt entreprendre une démarche collective de redocumentarisation décentralisée et exposée via le Web, à la croisée des chemins entre encyclopédie collaborative, plateforme d'édition et archive ouverte ? Les technologies existent, c'est une question de vision. Peut-être Paul Otlet m'influence-t-il par-delà la tombe… En tout cas, on ne fonderait probablement pas ce système sur l'utilisation du PDF.

# Un format incontournable ?

Je viens d'évoquer des problèmes spécifiques à l'extraction de texte mais le PDF suscite d'autres critiques plus importantes encore, et ce depuis longtemps. Pour les nouveaux modes de consultation, via les petits écrans notamment, le PDF offre une lisibilité médiocre (nécessité d'agrandir, réduire, faire défiler). Il pose des problèmes d'accessibilité à certains publics, et s'avère par nature difficile à améliorer sur ce point. Et puis, les mauvaises pratiques abondent. J'ai déjà pesté ici contre le fait que la plupart des fichiers PDF n'incluent pas de signets permettant aux logiciels censés les lire d'afficher une table des matières automatique, car les utilisateurs ne savent pas paramétrer cela via leurs logiciels de création de documents.

Pourtant, le PDF reste un format incontournable dans nos métiers, peut-être le seul qui le soit véritablement. Le docx de Microsoft Word, par exemple, peut assez facilement être remplacé par ses concurrents propriétaires (Google Docs) ou libres (LibreOffice Writer). Pour le PDF, *there is no alternative*. Comment en est-on arrivé là ? Il faut reconnaître à Adobe d'avoir élaboré une stratégie particulièrement maligne pour établir l'hégémonie du PDF. Mais, dans le monde académique en tout cas, celle-ci s'explique aussi par d'autres facteurs.

Songeons à la manière dont le format PDF est défendu. Il s'agit presque toujours de louer la fiabilité qu'il apporte à nos transactions documentaires. C'est la solution « qui marche », « universelle », et surtout, « stable ». Généralement, cela va de pair avec un rejet des formats bureautiques, jugés (à raison) peu fiables, ainsi que des nouveaux modèles éditoriaux reposants sur le Web (lien hypertexte, page unique qui défile). Face à une bureautique qui plante une fois sur deux, et à un flux qui élimine tous repères, le PDF serait un parangon de robustesse.

Cette stabilité du PDF me semble être un leurre, et en tant que tel, participer d'un impensé.

# Derrière la question du format, une rupture épistémologique

Le format PDF est défini par son indépendance vis-à-vis du contexte matériel et logiciel. Sa raison d'être initiale était de préserver la forme d'un document en vue de l'impression. Ce n'est qu'a posteriori qu'il a été érigé en un format standard pour le partage et le stockage des textes scientifiques, autour du fait qu'il fige la mise en page.

Or pourquoi faut-il figer la mise en page ? Parce qu'elle remplit une fonction cruciale : en sciences, elle fonde notre pratique de **localisation** des citations.

C'est la pagination qui fournit le moyen de localiser les extraits et ainsi d'asseoir nos arguments. Si elle devient dynamique, alors tout bascule : une assertion comme « (Goody, 1979, p. 60) » n'a plus aucune fiabilité ; autant écrire « (Goody, 1979) », et dans ce cas-là bonne chance pour retrouver l'extrait en question. Si on ne dispose pas d'une version numérique avec fonctionnalité de recherche, ou d'un index pour nous mettre sur la piste dans l'exemplaire papier, c'est fichu. Les dépendances rhétoriques sont perdues ; l'algorithme argumentatif se bloque ; c'est tout le système de la preuve qui s'écroule.

Le rôle de la localisation dans le façonnage de nos pratiques est immense. C'est elle qui nous oblige à jongler entre les formats en fonction des étapes de notre travail : on peut lire confortablement un ouvrage au format EPUB sur une liseuse, ou bien éplucher rapidement les articles d'une revue au format HTML dans un navigateur web, mais lorsque vient le moment de citer, il faut se tourner vers le PDF (ou l'imprimé). Certains chercheurs font tout simplement une croix sur les formats à la mise en page dite adaptative, considérant que leur pratique est contre-productive.

À l'ère de la pré-publication, des archives institutionnelles et de l'évaluation par les pairs ouverte, la pagination constitue pratiquement la dernière valeur refuge pour l'édition scientifique commerciale. Dès lors, et pour reprendre la terminologie de Pascal Robert [@robert2020], la défense du PDF comme solution qui marche, universelle et stable apparaît comme un moyen de soustraire la question de nos pratiques de localisation à l'examen critique, à l'épreuve de justification. C'est un leurre qui participe à construire un impensé des formats d'écriture et d'édition dans le monde scientifique.

En effet, la littérature scientifique n'échappe pas aux transformations des pratiques de lecture : elle se diffuse désormais principalement via le Web et se consomme de plus en plus sur mobile.  Pourtant, la science n'a pas opéré la bascule vers un autre modèle que la page imprimée et le PDF : la page reste notre seule unité de référence pour la localisation, alors qu'elle est *de facto* fragilisée.

Ce n'est pas seulement la faute de l'édition scientifique, même si cette industrie ancienne et rétive au changement a sa part de responsabilité. Il faut aussi admettre que la bascule n'est pas simple, car il s'agit de négocier une véritable rupture épistémologique : par quoi remplace-t-on la page ?

# Forger une nouvelle unité intellectuelle dans l'architexte

Pour rappel, la page est une division matérielle avant d'être une division intellectuelle.

Au début du 20^e^ siècle, un certain nombre de travaux se sont penchés sur la possibilité de faire coïncider les deux divisions : on peut citer notamment l'institut [Die Brücke](https://hyperotlet.huma-num.fr/otletosphere/12) de Wilhelm Ostwald, dont le travail sur les *monos* inspirera probablement Paul Otlet et son principe monographique (une idée, une fiche).

Le problème du paradigme actuel, c'est qu'on ne peut pas retrouver une division matérielle fixe au niveau du support : on ne peut pas tailler l'équivalent numérique du A4 dans un continuum de tailles d'écran et de fenêtres qui va du smartphone au vidéoprojecteur.

Il faut en fait changer la façon dont on pense le support lui-même. En régime numérique, l'écran et les signes qui s'y déploient sont articulés par un architexte, une écriture de l'écriture : c'est dans ce matériau qu'il nous faut forger une division qui sera la fois matérielle et intellectuelle, architecturale et sémantique.

Conceptuellement, le travail est déjà en chemin : @kembellec2017 ont proposé un modèle théorique qui définit une unité de sens appelée « péricope ». Mais à quelle division matérielle peut-elle correspondre ? À quel fragment de document ?

Le paragraphe ? On se retrouverait à citer régulièrement des nombres à 4 ou 5 chiffres. La section ? Et si la personne n'en utilise pas, ou trop peu ? Compliqué. Un découpage a posteriori, en utilisant le traitement automatique des langues [@kembellec2017, 20] ? On risque de perdre en pertinence au niveau micro et en cohérence au niveau macro. Personnellement, je penche aujourd'hui pour le paragraphe (faute de mieux) : il a déjà un symbole, « § » et il est facile à numéroter de manière automatique. Mais cela n'a rien d'évident.

# Après moi, le dégel

Ce qui m'apparaît évident en revanche, c'est qu'à l'instar des polices de caractères, les PDF font un peu figure de glaçons dans un océan, et que le dégel est inévitable [^3]. Pour les caractères, c'est la standardisation des fontes variables qui a créé les conditions d'une bascule. Pour le texte scientifique, on attend encore. Mais un manque de curiosité ou de culture technique n'est plus une excuse pour esquiver la question : les formats doivent évoluer.

On sait depuis longtemps qu'on peut combiner plusieurs mécanismes de localisation, par exemple en indiquant la pagination de l'édition originale dans la marge d'une édition critique ou d'une réédition. Les plateformes comme OpenEdition, Cairn ou Persée ont montré que la numérotation des paragraphes fonctionne à une échelle industrielle. On commence à avoir des exemples de thèses publiées en HTML [^4] qui offrent une expérience de lecture satisfaisante et des moyens robustes de se référer à un extrait précis. Et des sites comme le mien montrent que le Web nous permet de combiner des techniques appartenant à des espace-temps très différents (les *marginalia* avec le mode nuit, par exemple), sans que la dimension expérimentale nécessite de sacrifier la qualité du produit éditorial.

***

Au début de ce billet, j'ai évoqué mon expérience du format PDF : alors que je veux simplement moissonner quelques citations, trop souvent le texte se dérobe sous mes clics, transformant ce qui devait être un après-midi de sérendipité en une séance de torture pour nettoyer les extraits dans mon éditeur de texte.

Je voudrais conclure sur un exemple qui, je pense, illustre assez bien la réflexion que j'ai essayé de développer ici. Prêtez-vous à l'exercice suivant :

- choisissez une phrase sur cette page qui contient un mot trop long en fin de ligne et qui est donc coupé par un tiret ;
- copiez la phrase en entier ;
- collez-la quelque part : la césure n'apparaît pas.

Tout est normal. Copier le même texte depuis un PDF aurait embarqué la coupure mais le format HTML fonctionne différemment : au lieu de figer le fond dans une certaine forme, il distingue les deux, et permet au lecteur de ne capturer que le premier.

Alors évidemment, il faut que le fond soit correctement façonné. Vous aurez peut-être remarqué que si vous copiez sur cette page une phrase contenant un appel de note, le contenu de la note en question se retrouve attrapé avec, de façon peu élégante – la faute à un mécanisme qui privilégie la simplicité d'écriture à la propreté.

Cet exemple montre deux choses. Premièrement, qu'on ne perd rien à abandonner la stabilité tant vantée du PDF. Deuxièmement, qu'un format n'est pas a priori meilleur ou pire mais qu'il incarne un certain nombre de savoir-faire, de choix et d'ancrages, dans un vacillement entre démarches situées et normes stabilisées [@demourat2020, 466]. Nos formats racontent ce que nous avons été, ce que nous sommes, et ce que nous aspirons à devenir. C'est ce qui rend leur impensé si regrettable, et leur pratique si enrichissante.

# Références


[^1]: Le titre diffère de celui indiqué sur le programme suite à un changement de dernière minute.

[^2]: À ne pas confondre avec l'excellent logiciel [Liquid](http://liquid.info/liquid.html) de Frode Hegland pour macOS.

[^3]: La métaphore est de Nick Sherman. Pour en savoir plus, voir mon article pour Ecridil, [« Sémiotique des fluides »](semiotique-des-fluides.html).

[^4]: J'en ai compilé quelques-unes dans [une collection Zotero publique](https://www.zotero.org/groups/2617207/thesesweb/library).