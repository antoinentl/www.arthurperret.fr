---
title: "Wordless 2 : négocier la réécriture"
date: 2019-06-05
abstract: "Passer de Markdown ou Asciidoc à Word, c'est simple. Rapatrier sans accroc les changements effectués sous Word dans son fichier d'origine, c'est moins simple. Je liste ici (ne serait-ce que pour ma propre mémoire) quelques éléments à prendre en compte pour bien négocier les allers-retours entre le balisage léger et le traitement de texte bureautique, avec Pandoc bien entendu."
---

En dehors de l'auto-édition qui est un cas spécifique, l'élaboration d'un texte à des fins de publication fait généralement l'objet d'un certain nombre d'allers-retours entre auteur et éditeur. Mon cas pratique, l'édition scientifique, ne déroge pas à la règle : les articles font l'objet de commentaires par des relecteurs, et les monographies sont corrigées en détail, que ce soit directement ou sous la forme de modifications suggérées. Tout ceci se fait très souvent avec Word, qui intègre un puissant outil de révision permettant de suivre les changements effectués. Prenons le cas de figure suivant :

1. j'écris un article en Markdown, que je convertis en docx avec Pandoc ;
2. l'éditeur utilise le mode révision de Word pour effectuer ou proposer des changements et me renvoie le document ;
3. je le convertis à nouveau en Markdown, toujours via Pandoc.

Comme je disais au début, l'étape 1 est simple. Elle correspond typiquement à ce genre de commande :[^1]

```
pandoc texte-initial.md -o conversion-word.docx
  --reference-doc feuille-style.docx
  --bibliography biblio.bib
  --csl style-biblio.csl
```

Les étapes 2 et 3 impliquent de la réécriture, en partie humaine et en partie machine[^2]. Les changements effectués par l'humain (l'éditeur) sont plutôt simples à gérer, tandis que ceux effectués par la machine nécessitent un peu plus de vigilance.

# 1. Réécriture humaine

Le mode Révision de Word ou Libreoffice permet de passer en revue les modifications suggérées pour les accepter ou refuser, soit au cas par cas, soit en bloc.

![Le mode Révision dans LibreOffice.](https://www.arthurperret.fr/img/2019-06-06-review.png)

Dans certains cas, par exemple après une relecture destinée à effectuer de simples corrections orthographiques, le document comporte des dizaines de micro-changements qu'on souhaite intégrer en bloc. Étant donné qu'on prévoit de revenir au Markdown, on peut faire d'une pierre deux coups en ajoutant `--track-changes=accept` à la commande Pandoc, ce qui intègre tous les ajouts et suppressions signalés par le mode Révision (sans les commentaires) :

```
pandoc conversion-word.docx -o reconversion-markdown.md
  --track-changes=all
```

# 2. Réécriture machine

Les changements effectués par la machine en re-convertissant vers le Markdown sont assez variés. Pour comparer le fichier d'origine et celui produit par re-conversion, j'ai utilisé un outil de visualisation des différences (*diff* en anglais). Attention : certains *diff* basiques fonctionnent ligne par ligne, or un paragraphe en Markdown occupe une seule « ligne » de code. Donc une différence d'un seul caractère suffit à marquer tout un paragraphe comme différent. On privilégiera donc un *diff* capable de fonctionner au mot par mot.

Parmi les **ajouts**, on remarque d'abord la multiplication des caractères d'espacement. La conversion tend à rajouter des espaces à certains endroits, dans la syntaxe des listes notamment. Des sauts de ligne sont introduits systématiquement pour maintenir la longueur de ligne en-dessous de 80 signes. Les caractères pouvant avoir une fonction syntaxique en Markdown mais utilisés de manière normale sont protégés avec une barre oblique inverse ; c'est le cas par exemple des crochets droits utilisés pour indiquer une modification dans une citation : `\[`.

Les **suppressions** sont plus rares et concernent a priori les éléments non interprétés. C'est par exemple le cas des commentaires en texte brut `<!-- comme ceci -->` qui sont perdus dans le processus de conversion / re-conversion. Idem pour certains délimiteurs facultatifs comme la barre droite `|` dans l'écriture des tableaux.

Quant aux **modifications**, elles interviennent à plusieurs endroits. On remarque la substitution de certains caractères typographiques : guillemets courbes `“` en guillemets droits `"`, tirets longs `—` en tirets courts successifs `---` etc. Certaines chaînes de caractères interprétables sont transformées : les clés de citation passent par exemple de `@auteurdate` à (Auteur, date). Enfin, des variations dans la syntaxe peuvent survenir en fonction du dialecte Markdown utilisé par l'auteur : niveaux de titre style ATX `#` ou Setext `==`, astérisques `*` ou tirets du bas `_` pour l'italique, etc[^3].

# Leviers de négociation

Comment gérer ces changements ? Une partie d'entre eux peuvent être anticipés au niveau de la re-conversion : en effet, le comportement de Pandoc est réglable via une foule d'options, dont certaines empêchent des modifications indésirables. Reprenons notre commande de re-conversion initiale et modifions ses options :

- `wrap=none` pour prévenir l'introduction de sauts de ligne intempestifs ;
- `atx-headers` pour forcer l'utilisation du symbole `#` dans les niveaux de titre ;
- `-t markdown-smart` pour désactiver l'extension *smart*, responsable des interventions typographiques indésirables.

```
pandoc -s conversion-word.docx -o reconversion-markdown.md
  --wrap=none
  --atx-headers
  -t markdown-smart
```

On peut ensuite utiliser un *diff* pour comparer les deux versions du document Markdown, avant et après passage sous Word. Les *diff* proposent des fonctionnalités intéressantes, qui me les rendent presque préférables au suivi des modifications via traitement de texte.

![Le gestionnaire de différences de BBEdit.](https://www.arthurperret.fr/img/2019-06-06-difftool.png)

Le comparateur de BBEdit illustré ci-dessus me permet d'ignorer tout ou partie des différences dues aux caractères d'espacement (dont les sauts de lignes), lignes vides, guillemets, etc. C'est une alternative (ou un complément) aux options de Pandoc, même si je préfère utiliser ces dernières afin d'introduire le moins possible de variations par rapport à mon document d'origine. Mais surtout, le *diff* me permet de traiter chaque changement dans les deux sens : on peut aussi bien rapatrier les modifications dans son fichier d'origine que rétablir les contenus de ce dernier d'origine dans le nouveau fichier. Ceci s'intègre donc aussi bien avec une gestion rigoureuse des versions—faite à la main ou automatisée avec un programme comme git—qu'avec une économie documentaire plus rapide.

Autre illustration de cette souplesse : on essaye la plupart du temps de ne pas utiliser deux environnements différents pour faire la même chose, or le mode Révision et l'utilisation du *diff* peuvent paraître redondants. Les options de Pandoc comme `--track-changes`, combinées au *diff*, simplifient beaucoup les choses : elles me permettent d'atteindre mon objectif principal—gérer des versions successives, intégrer sélectivement des changements, maintenir le format texte brut—en me laissant toutefois la possibilité d'intervenir via un traitement de texte en amont le jour où j'en ai besoin.

***

Que Word soit installé ou non sur sa machine, on peut coexister pacifiquement et productivement avec les traitements de texte WYSIWYG. On recommandera tout de même de conserver à portée de clic un concurrent *open source* tel LibreOffice pour une souplesse absolue. On voit en tout cas avec ce genre d'expérience que le robuste mode Révision de Word n'est pas sans alternative et que l'édition de texte brut, pour peu qu'on y réfléchisse un tantinet, est totalement intégrable à des processus traditionnels tels que la navette auteur-éditeur en contexte scientifique. Étant donné que les universitaires consacrent une part non négligeable de leur temps à ces activités (d'un côté ou de l'autre du texte), il est bon de savoir que les technologies sont là, souhaiterions-nous les mobiliser dans des contextes éditoriaux à (re)définir.

[^1]: Je rajoute ici des sauts de ligne pour une meilleure lisibilité mais la commande exécutée n'en comporte pas.
[^2]: J'en exclus toutes les modifications régies par une feuille de style (typographie, mise en page, format des citations et de la bibliographie) et qui sont donc extérieures au document.
[^3]: C'est l'un des gros défauts de ce langage : en l'absence d'une spécification unique, les « parfums » se sont multipliés, ce qui nous oblige à une certaine gymnastique de conversion pour accomoder leurs différences d'écriture.