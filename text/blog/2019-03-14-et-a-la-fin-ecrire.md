---
title: Et à la fin, écrire…
date: 2019-03-14
---

L’outil d’écriture idéal est-il celui qui comporte toutes les fonctionnalités dont vous avez besoin, ou bien celui qui est suffisamment modulable pour que vous les intégriez vous-même ? Je crois que la réponse à cette question peut conditionner l’usage et éventuellement l’adoption d’un logiciel. Ce n’est pas avant d’y avoir répondu que j’ai pu entrevoir les *conditions de l’écriture* pour ma thèse.

# Dichotomies

Pour chacun de ses avatars, l’écriture convoque différentes écoles de pensées. Ainsi en programmation, ce qui s’écrit d’une seule façon en Python peut généralement s’écrire de dix façons en Perl ; on oppose alors simplicité et souplesse, comme deux façons d’atteindre l’efficacité. En bureautique classique, la popularité d’un logiciel se jauge notamment sur l’interface, qui peut être très dense ou au contraire minimaliste.

Une autre dichotomie qui m’intéresse est celle-ci : exhaustivité et adaptabilité. Certains logiciels affichent une liste impressionnante de fonctionnalités, attirant les utilisateurs qui aiment une solution clés en main. À l’inverse, on trouve des logiciels plus ou moins modulaires, au sens où leurs fonctionnalités se composent en partie à la carte. La personnalisation peut alors se faire en piochant dans une offre pré-établie de modules (ou extensions, ou *plugins*…) mais également en les aménageant soi-mêmes à partir de fonctions prévues à cet effet (intégration de programmes externes, paramétrage de raccourcis ou de macros, réglages d’interface).

Ces différentes lignes de partage se retrouvent dans les logiciels d’écriture en texte brut. J’entends par cette appellation les logiciels qui *révèlent* l’architexte, c’est-à-dire la structure qui balise le texte et en régit l’énonciation[^1]. Cela recouvre à la fois la pratique du balisage lourd (XML, HTML, LaTeX…) et celle du balisage léger (Markdown, Asciidoc…) : il ne s’agit donc pas de programmation au sens strict, ou alors de [programmation éditoriale](http://barthes.enssib.fr/cours/atelier-programmation-editoriale/) si on pense l’écriture comme faisant partie d’un processus d’édition comprenant de la programmation.

Pour écrire en Markdown, j’ai utilisé pendant plus de 2 ans une solution simple, minimaliste et clé en main. En multipliant mes expérimentations éditoriales, j’ai réalisé qu’il me fallait plusieurs changements. J’ai conservé la simplicité d’usage – ou devrais-je dire la tranquillité d’esprit – qui va de pair avec un logiciel payant stable et documenté. Mais j’ai sacrifié une certaine perfection dans le minimalisme à des besoins de navigation et d’automatisation plus importants. Et surtout, j’ai définitivement embrassé la modularité, en investissant plus activement les possibilités de personnalisation fonctionnelle. En résumé, je suis passé d’un éditeur Markdown élégant mais rigide à un éditeur de texte plus polyvalent.

# Balisage

Pour autant, il ne s’agit pas d’un outil infusé de bout en bout par la culture du développement Web, comme Sublime, Atom, Brackets et autres populaires éditeurs orienté programmation/code. Car pour moi, coder c’est peut-être écrire mais écrire ne se résume pas à coder.

Je voudrais éviter la métonymie qui consiste à parler de balisage pour décrire l’écriture dans sa globalité. Il faudrait étudier comment le balisage se manifeste dans la pratique, dans le geste, en tant que rythme. Intuitivement, je pense que le balisage a moins à voir avec la ponctuation qu’avec les diacritiques : je reviens sur un mot pour l’enchâsser dans des accolades, des chevrons ou des apostrophes inversées, comme je reviendrais sur une lettre pour y ajouter un accent. C’est une modification de notre usage de la *scripturation*[^2], à l’invitation du curseur. Par l’emploi de ces mots, je signale ce que je crois être une nécessité : inscrire l’écriture informatique dans l’histoire longue de l’écriture tout court.

Ceci étant dit, je considère aujourd'hui que le balisage est l’une des clés de la compréhension de ce qu’est devenue l’écriture en régime numérique. On pressent que le balisage ne peut se réduire ni à la computation ni à la littérature. Mais c’est une question à tiroirs puisqu’elle est alimentée par la problématique des objets informationnels (qu’est-ce qu’un document, une donnée, qu’est-ce que le texte, le script est-il un texte, etc.) dont les sciences exactes ont assez peu à faire et à laquelle les humanités peinent à apporter une réponse. Le travail est devant nous.

# Et à la fin…

Ce moment de transition technique est à peine passé que déjà il me semble avoir joué un rôle critique dans mon travail. En effet, j’ai embarqué dans ce changement d’outil bon nombre de choses expérimentées récemment sur le plan éditorial : intégration des métadonnées, gestion des versions (avec Git), automatisation, maîtrise du rendu… Or je l’ai fait en mettant totalement de côté la question du processus éditorial. Je me suis concentré sur les *conditions de l’écriture* ; ce faisant, je suis passé d’une réflexivité construite dans l’immédiat à la *perspective* d’une réflexivité. Je veux dire par là qu’une certaine éditologie instantanée, certes productive, peut-être utile à quelques lecteurs (?) doit désormais laisser la place à une pratique d’écriture. Un de mes professeurs appellait cela l’épreuve, au sens de l’action d’éprouver le concept sur le terrain et réciproquement. Cela me semble nécessaire pour poursuivre mon travail d’élaboration théorique.

En d’autres termes, l’injonction à l’écriture que j’entends depuis le début de ma thèse coïncide enfin avec un besoin véritablement ressenti d’écrire celle-ci.

[^1]: Contrairement à certaines interprétations de sa définition initiale par Jeanneret et Souchier, je ne crois pas que l’architexte se résume à un logiciel. Cette illusion s'est dissipée le jour où le `.doc` est devenu `.docx`, révélant le tour de passe-passe accompli par Word, qui n’est qu’une interface d’édition de l’architexte.

[^2]: Expression de Roger Laufer. Voir ses articles [*Du ponctuel au scriptural*](https://doi.org/10.3406/lfr.1980.5267) et surtout [*L’énonciation typographique*](https://doi.org/10.3406/colan.1986.1762).