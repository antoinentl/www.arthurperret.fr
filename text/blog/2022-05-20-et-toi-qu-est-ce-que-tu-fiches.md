---
title: "Et toi, qu’est-ce que tu fiches ?"
date: 2022-05-20
description: "Rien compris à la méthode Zettelkasten ? Pas de panique : Umberto Eco est là pour vous aider."
---

Cette semaine a lieu une conférence en ligne sur la prise de notes interreliées, [Linking Your Thinking Conference](https://www.linkingyourthinking.com/conference). L’évènement s’inscrit dans le domaine de la gestion des connaissances personnelles (*personal knowledge management*, PKM). C’est l’occasion pour de nombreux auteurs de partager leurs réflexions sur le sujet ; j’ai ainsi vu passer des articles sur différentes approches méthodologiques : comment prendre des notes, faire des liens, gérer des projets d’écriture, etc.

En parcourant rapidement ces textes, je me suis fait deux réflexions.

D’abord, je crois que bon nombre de ces méthodologies pourraient être fusionnées. Elles touchent à des questions tellement fondamentales que leurs différences relèvent essentiellement du marketing. Le cadre général ayant été posé quasiment une fois pour toutes avec la méthode *Getting Things Done* de David Allen, l’innovation se fait désormais surtout aux marges, et toute nouvelle méthodologie prétendument définitive me semble réinventer la roue. Mais il faut bien que les consultants gagnent leur vie…

Et ensuite, j’ai vu passer plusieurs fois la chose suivante : des gens qui ont découvert avec enthousiasme les outils de prise de notes interreliées mais ont rapidement déchanté, soi-disant parce que rien n’explique comment les utiliser. Dans [« The essence of the Zettelkasten method, demystified »](https://feeei.substack.com/p/the-essence-of-the-zettelkasten-method?s=r) par exemple, l’auteur Fei-Ling Tseng regrette que la méthode en question ne soit pas accompagnée d'un mode d'emploi - d'où la nécessité de la « démystifier ». Elle en a tiré quelque chose d'utile et d'intelligent : le Compas à Idées, une méthode qui aide à trouver comment relier ses notes quand on peine à voir comment faire. Mais derrière sa remarque initiale, je vois un problème.

Les gens regardent l’outil et se demandent « qu’est-ce que je peux faire avec ? ». Je ne dis pas qu’il n’y a rien d’intéressant à tirer de cette interrogation mais je pense que cela concerne surtout les concepteurs des outils, dont le travail est de repousser le champ des possibles. Pour la plupart d’entre nous, regarder l’outil et se demander comment l’utiliser revient non seulement se poser la mauvaise question mais aussi à prendre le problème à l’envers.

La première question à se poser, c’est « qu’est-ce que je veux/dois faire ? ». La seconde, c’est « quel outil peut m’aider ? ».

# On ne se fiche pas de la fiche

Un étudiant de master ou de doctorat en sciences humaines pourrait lire aujourd’hui *Comment écrire sa thèse* d’Umberto Eco et se mettre à utiliser un outil comme Zettlr ou Obsidian dans la foulée sans aucun problème ; le seul pré-requis serait d’apprendre Markdown, ce qui peut se faire [en dix minutes](../tutomd/index.html).

Pourquoi ? Parce que le livre d’Eco part de la première question : « qu’est-ce que je dois faire ? ». En l’occurrence, la réponse c’est : rédiger un mémoire ou une thèse. Eco explique parfaitement cet exercice, y compris la manière dont la prise de notes vient s’y intégrer. Il donne ainsi un ancrage et un sens extrinsèque à une démarche que les consultants en gestion essaient de constituer *ex nihilo* à l’intérieur même de leurs méthodes.

Si je cite Eco parmi tous les auteurs ayant écrit sur ce sujet, c’est parce que ses conseils sont particulièrement intemporels. Mais en fonction de l’édition que vous avez sous la main, cela peut être plus ou moins évident.

Dans l’édition que j’ai de *Comment écrire sa thèse*, le traducteur a ajouté une postface où l’on peut lire que, selon lui, le passage du temps « donne à bien des conseils d’Eco une allure passablement obsolète [@cantagrel2018, 332] ». C’est vrai pour la toute dernière partie, qui concerne la rédaction du manuscrit, sa mise en page et son impression (les explications portent sur l’utilisation de la machine à écrire). En revanche, ce qui concerne la prise de notes n’a pas pris une ride. Les traducteurs de la version anglaise de 2015, eux ont compris ça :

> ^[{-} *“We have preserved Eco’s handwritten index card research system in all its detail, precisely because it is the soul of* How to Write a Thesis*. Obviously the card catalog of the small town library is primitive compared to today’s online research systems, but the research skills that Eco teaches are perhaps even more relevant today”*] « Nous avons restitué le système de fiches manuscrites d'Eco dans tous ses détails, précisément parce qu'il est l'âme de *Comment écrire sa thèse*. Il est évident que le catalogue sur fiches de la bibliothèque d'une petite ville est primitif par rapport aux systèmes de recherche en ligne d'aujourd'hui, mais les techniques de recherche enseignées par Eco sont peut-être encore plus pertinentes aujourd'hui[@mongiatfarina2015, xvii] ».

Ce que Eco dit de la fiche s’applique mot pour mot aux nouveaux outils de documentation personnelle hypertextuelle dans le contexte de la recherche. Il explique que la fiche, étant une unité mobile, forme la base d’un système flexible et puissant :

> « La fiche peut être déplacée, fusionner avec d’autres fiches, être placée avant ou après une autre, etc. [@eco2018, 194] ».

Eco nous invite à créer différents types de fiches en fonction du travail à accomplir : lectures, thématiques, citations, synthèses… Ce sont des types applicables à tout sujet, car ils sont liés à l’organisation des connaissances et pas aux connaissances elles-mêmes.

> « Fichier bibliographique […] fichier de lecture de livres ou d’articles, fichier thématique, fichier par auteurs, fichier de citations, fichier de travail […] Le nombre et la nature des fichiers dépend du travail de thèse » [@eco2018, 196-197].

Eco insiste sur ce qu’il appelle fiches de travail et qui constitue le tissu connectif du fichier. Les fiches « de raccord » notamment servent littéralement à faire des liens entre des idées inscrites sur d’autres fiches :

> « Fiches de travail de différentes sortes : fiche de raccord entre idées et sections du plan, fiches de problèmes (comment traiter telle ou telle question ?), fiches de suggestions (recueillant des idées que d’autres vous ont données, des développements possibles), etc. [@eco2018, 196] ».

Enfin, appliquer une démarche documentaire rigoureuse au fichier, avec des métadonnées riches et homogènes, permet d’avoir un outil de vision globale du travail. C’est le rôle que remplissent toujours les index, les listes de mots-clés et (désormais) [les graphes interactifs](2022-02-13-a-quoi-sert-une-vue-graphe.html).

> « Il est essentiel qu’un fichier donné soit complet et homogène […] D’un simple coup d’œil, vous saurez ce que vous avez lu et ce qui vous reste à consulter [@eco2018, 197] ».

Tous ces conseils orientaient déjà parfaitement les étudiants italiens des années 1980, qui devaient s’atteler à leur *laurea*^[Une sorte d’intermédiaire entre nos mémoires de master et de thèse.], et qui se demandaient comment prendre des notes. Ils restent parfaitement applicables aujourd’hui, y compris pour les outils de prise de notes interreliées.

# Le bon outil

Dès lors, il reste la seconde question : « quel outil peut m’aider ? ». Il est beaucoup plus simple de se repérer dans la jungle des outils existants quand on sait ce qu’on doit faire. Un étudiant qui doit faire de la bibliographie, prendre des notes sur ses lectures et faire des notes de synthèse, le tout pour nourrir un projet d’écriture scientifique, sélectionnera un outil qui peut l’aider à accomplir tout cela. En l’occurrence, [Zettlr](https://www.zettlr.com/) me vient évidemment à l’esprit, avec [Cosma](https://cosma.graphlab.fr) en complément.

On peut appliquer ce raisonnement à des activités voisines. Documentation, veille, édition… Les travailleurs de la connaissance poursuivent des objectifs dans lesquels s’insère déjà la nécessité de prendre des notes. C’est en connaissant ces objectifs que le choix d’outil prend du sens. Et chaque situation a ses spécificités, qui jouent au moment de sélectionner le bon outil : là où Zettlr peut s’imposer pour un étudiant en SHS, [Obsidian](https://obsidian.md/) sera peut-être préférable pour le documentaliste qui alimente un wiki.

Là où ça se complique, c’est quand on n’a pas réfléchi aux objectifs qu’on poursuit au-delà de la prise de notes. Dans ce cas-là, évidemment qu’on se sent un peu désarmé face à la méthode Zettelkasten – rien que le nom, quand on n’est pas germanophone…

La logique fondamentale des outils de prise de notes interreliées nous est pourtant familière. C’est l’hypertexte, dans lequel nous baignons à chaque fois que nous naviguons sur le Web. On peut même monter en abstraction et dire que tout cela repose sur l’association, ce mécanisme dont Vannevar Bush disait qu’il fonde notre mode de pensée [@bush1945, 106].

Eh bien justement, c’est peut-être parce que la logique réticulaire est si générique qu’on a du mal à la saisir en tant que telle. Mise au service d’un objectif plus large, elle fait sens. Mais si on l’examine sous cloche, décorrélée de ce qu’elle doit servir, alors tout cela peut paraître très confus.

# Références