---
title: Une micro-chaîne éditoriale multicanal pour l’écriture scientifique
date: 2018-10-18
---

Collecter, lire, annoter, expérimenter, rédiger, publier, discuter… Presque toutes les étapes du travail scientifique impliquent des formes d’écriture. Les outils qui permettent d’appréhender et de créer du texte sont aujourd’hui extrêmement nombreux, pour notre plus grand bonheur mais aussi notre plus grande confusion : comment trouver chaussure à son pied parmi la galaxie de logiciels, de formats et de normes existantes, tout en articulant ses propres pratiques avec celles des autres ?

L’une des réponses consiste à faire bon usage de la conversion entre formats de fichier. Un bon utilitaire comme Pandoc permet de passer rapidement et efficacement du traitement de texte à une publication web ou bien un PDF optimisé pour l’impression. À l’échelle individuelle, c’est une gymnastique éditoriale intéressante, qui permet de diffuser son travail de plusieurs façons ; dans le cadre d’un travail collectif, cela change totalement la donne, puisque cela permet de relier des continents d’usagers isolés dans leurs outils.

En juillet, Antoine Fauchié a fait remonter dans sa veille un article décrivant un *workflow* pour produire des documents Word lorsqu’on écrit plutôt en Markdown ([*Markdown et Word en milieu académique*](https://www.quaternum.net/2018/07/24/markdown-et-word-en-milieu-academique/)).

Si au départ j’étais simplement curieux, ma mâchoire s’est progressivement décrochée au fil de la lecture : bien que limitée en apparence à une certaine plateforme (Mac) du fait des exemples pratiques, la recette que présente Raphael Kabo est absolument géniale dans le principe. Avec humour, il désigne son montage de la façon suivante :

> *« I have named this system *Zotero/BetterBibTeX/zotpick-applescript/Service*, because that is what it is made of and I’m sorry, I can’t be called upon to be imaginative on demand ».*

Pour moi, sa démonstration s’apparente à un embryon de micro-chaîne éditoriale multicanal à échelle auctoriale – la formulation est tout aussi indigeste, mais elle en suggère peut-être mieux le potentiel.

Les outils mobilisés sont les suivants :

1. un éditeur de texte ;
2. un gestionnaire de références bibliographiques ;
3. un convertisseur de formats de fichier ;
4. un script qui automatise l’articulation des 3 items précédents.

Raphael Kabo utilise respectivement Ulysses, Zotero, Pandoc et Automator (l’utilitaire de script fourni avec macOS), mais en pratique, on peut remplacer Ulysses par n’importe quel logiciel concurrent, propriétaire ou libre ; quant à l’automatisation, j’imagine qu’elle se pratique aussi bien sur Linux et Windows que sur Mac. Même Zotero et Pandoc pourraient théoriquement être substitués, même si personnellement je suis d’accord avec lui sur le caractère incontournable de ces deux outils en recherche.

Ce qui compte, c’est l’idée : un format d’écriture unique qui sert de pivot pour produire d’autres formats.

L’article en fait la démonstration pour le passage du Markdown à Word. En changeant quelques éléments, on peut reproduire exactement la même méthode pour fabriquer une page HTML. Il suffit de changer les options de la commande Pandoc et de remplacer le gabarit .docx par un fichier CSS.

Voici le code de départ. Il permet d’automatiser la conversion d’un document en Markdown au format docx avec Pandoc, en traitant au passage automatiquement les références bibliographiques suivant la norme de son choix. C’est un simple script Shell qui, sauvegardé sous la forme d’une application, évite de retaper cent fois la même commande dans le Terminal ; un simple glisser-déposer suffit.

```bash
title=$(basename "$@" .md)
echo -e "\n\n# Bibliography" >> "$@"
/usr/local/bin/pandoc -s --filter=/usr/local/bin/pandoc-citeproc --bibliography /PATH/TO/BIBTEX/LIBRARY/Zotero.bib --csl /PATH/TO/CSL/FILE/FILENAME.csl --reference-doc /PATH/TO/REFERENCE/DOC/reference.docx -f markdown+smart -t docx -o /PATH/WHERE/YOU/WANT/TO/EXPORT/"$title".docx "$@"
open /PATH/WHERE/YOU/WANT/TO/EXPORT/"$title".docx
```

(Pour référence, l’article d’origine détaille très rigoureusement ce que fait chaque commande et pourquoi.)

En modifiant simplement les paramètres (et pas les commandes elles-mêmes), on peut faire exactement la même chose pour fabriquer une page web mise en forme suivant une CSS donnée (comme [celle-ci](https://gist.github.com/killercup/5917178)) :

```bash
title=$(basename "$@" .md)
echo -e "\n\n# Bibliography" >> "$@"
/usr/local/bin/pandoc -s --filter=/usr/local/bin/pandoc-citeproc --bibliography /PATH/TO/BIBTEX/LIBRARY/Zotero.bib --csl /PATH/TO/CSL/FILE/FILENAME.csl -c style.css -f markdown+smart -t html -o /PATH/WHERE/YOU/WANT/TO/EXPORT/"$title".html "$@"
open /PATH/WHERE/YOU/WANT/TO/EXPORT/"$title".html
```

Cela permet de mettre un orteil dans le grand bain de la publication web indépendamment de tout écosystème ultra-sophistiqué.

En suivant cette logique, on peut également modifier le script pour créer du PDF, que Pandoc fabrique en utilisant LaTeX sous le capot. Plutôt que du docx, on peut également fabriquer un odt. On pourrait même créer un EPUB.

Je parlais de micro-chaîne éditoriale multicanal à échelle auctoriale (ouf !).

- **micro** : le système pèse à peine quelques kB, se conçoit en quelques minutes, tient en une ou deux icônes sur le bureau et nous épargne plusieurs dizaines de clics ;
- **chaîne éditoriale** : on récupère des références dans Zotero, on les inclut dans son text rédigé en Markdown, on crée un document mis en forme qu’on peut partager, mais le tout *dans une seule interface* (l’éditeur de texte), notamment grâce aux raccourcis Zotero décrits par Raphael Kabo dans son article ;
- **multicanal** : différents formats permettent une meilleure accessibilité des documents à plusieurs types d’interlocuteurs, sans avoir à utiliser autant de logiciels d’édition ;
- **échelle auctoriale** : peut-être le plus important pour moi, la faisabilité à l’échelle individuelle. Jusqu’à présent, je voyais ce principe de format pivot comme une pratique désirable mais réservée en pratique aux professionnels de l’édition, dans un cadre d’équipe. En expérimentant ce système, il s’avère que le coût technique n’est peut-être pas si élevé que je pensais pour un chercheur qui souhaiterait optimiser son propre *workflow*.

Ce dernier point est celui qui m’enthousiasme le plus.

Je fabrique d’habitude des documents avec LaTeX, dont l’intégration avec la gestion bibliographique me semblait jusque-là imbattable. Pour la première fois, j’ai expérimenté un *workflow* qui ne passait pas par LaTeX, avec une gestion sans faute de la bibliographie et offrant une porte d’entrée beaucoup plus fiable sur la publication web. C’est une très bonne chose, puisque cela permet d’être outillé de façon plus pertinente en fonction des documents à produire.

Mais ce n’est pas tout. Raphael Kabo conclut en félicitant de façon un peu sarcastique le lecteur qui aurait reproduit jusqu’au bout son tutoriel :

> *« Well done. You are academia god. We all bow down ».*

Je vais être honnête : j’ai effectivement ressenti un sentiment d’euphorie en testant ce micro-système sur un texte récent. Pourquoi ? Après tout, son postulat de départ, très concret, était simplement d’éviter une prise de tête sur les formats de fichier avec la personne qui relit vos chapitres de thèse. Et pour la plupart des lecteurs de cet article, sûrement déjà familiers avec les outils dont il parle, la manœuvre n’est finalement pas si complexe.

Toutefois si quelqu’un tentait de réaliser l’ensemble du paramétrage décrit dans son article à partir de zéro, sans expérience de Zotero ou de la ligne de commande par exemple, je pense que cela pourrait se révéler assez difficile. D’où l’importance de bien former les étudiants qui veulent faire de la recherche aux outils informatiques ; il ne faudrait pas laisser le bénéfice de ces innovations dans l’organisation du travail intellectuel aux seuls initiés. Et par ailleurs, je pense réellement que ce petit système est très puissant, comme tout ce qui favorise l’autonomie et l’adaptabilité – deux pôles qui s’articulent au quotidien dans l’univers académique.