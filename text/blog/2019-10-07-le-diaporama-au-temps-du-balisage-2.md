---
title: Le diaporama au temps du balisage (2)
date: 2019-10-07
abstract: "Vous trouvez la gestion des images dans Reveal.js un peu limitée ? Je recommande une pincée de flex en suspension dans une goutte de Markdown parfum Pandoc. (L'abus de div est dangereux pour la santé. Pas d'utilisation prolongée sans avis médical.)"
---

[Dans un précédent billet](le-diaporama-au-temps-du-balisage.html), je décrivais le principe d'une présentation écrite en Markdown et transformée en HTML via Pandoc. Pour résumer l'intérêt de cette alternative à PowerPoint, Keynote et Google Slides :

1. **pérennité** : vos données sont dans un format ouvert et lisible par les humains ;
2. **autonomie** : vous accroissez votre agilité informatique, ce qui vous permet de choisir vos dépendances logicielles plus librement ;
3. **automatisation** : en texte brut, le contenu comme sa mise en page peuvent être manipulés précisément et reproduits rapidement ;
4. **fonctionnalités** : le HTML permet une interactivité intéressante avec par exemple la transclusion (inclusion d'une page web directement dans la présentation).

Je prolonge le billet initial avec une courte note sur les images. Ce sont des éléments incontournables, qu'on les utilise comme contenu dans l'argumentation, pour une simple illustration ou dans la composition graphique. Pour un outil de création de présentations, la gestion des images est donc un élément important.

Sur ce point, l'export Reveal.js à partir de Pandoc m'avait d'abord déçu. En Markdown, une image s'écrit `![Texte](fichier.jpg)`. S'il y a du texte entre crochets, la conversion en HTML par Pandoc génère un élément `figure` qui contient une image `img` et une légende `figcaption`. S'il n'y a pas de texte, l'export ne contient qu'un élément `img`. Par défaut pour les présentations, le cas des images multiples est assez insatisfaisant : si on sépare chaque inclusion d'image par des sauts de ligne, elles s'empilent verticalement en débordant de l'écran ; si on les sépare par un espace, Pandoc les place côte-à-côte mais sans légende, qu'il y ait du texte entre crochets ou non. Heureusement en tâtonnant, j'ai fini par découvrir un moyen assez simple de faire très vite beaucoup mieux.

Une présentation en HTML se style naturellement en CSS. Pour appliquer des propriétés graphiques particulières à des segments manuellement définis d'une présentation, on peut utiliser des éléments `div` et `span` avec des classes définies dans une feuille de style à part. Il est tout à fait possible de mélanger du Markdown et du HTML. Par défaut c'est assez intrusif mais Pandoc permet de le faire sous une forme un peu raccourcie[^1], qui vient plus facilement sous les doigts que les balises HTML dans leur intégralité, ce qui fait qu'on perd moins en lisibilité. Il y a des inconvénients qui se discutent[^2] mais les avantages sont conséquents : la capture d'écran ci-dessous montre un exemple simple d'utilisation du modèle [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) pour aligner plusieurs images de façon satisfaisante sans perdre leurs légendes.

<figure class="fullwidth">
<figcaption>Trois figures alignées horizontalement.</figcaption>
<img src="https://www.arthurperret.fr/img/2019-10-07-le-diaporama-au-temps-du-balisage-2.png">
</figure>

La liste que j'ai faite plus haut pour tenter de vous convaincre de l'intérêt de cette pratique pourrait être encore augmentée d'une remarque : étant donné qu'il s'agit de CSS, les compétences de mise en page qu'on apprend pour styler une présentation en Reveal.js sont transférables à toute autre tâche qui utilise les technologies du Web. Je trouve cet argument crucial à l'heure où notre principal concurrent dans un contexte professionnel et pédagogique n'est plus vraiment la suite Office (Word, Excel, PowerPoint) mais la suite Google (Docs, Sheets, Slides). Or elle a beau vivre dans le cloud, cette dernière ne vous apprendra strictement rien sur la fabrique du Web. Reproduire les contraintes de la bureautique WYSIWYG dans un navigateur, il fallait le faire ! Par comparaison, ce qu'on peut réaliser avec quelques lignes de texte brut me paraît bien plus intéressant dans une perspective de culture numérique.

[^1]: <https://pandoc.org/MANUAL.html#divs-and-spans>

[^2]: Cela renforce le fait que les présentations en Markdown ont leur propre régime d'écriture, de mise en forme et d'interaction, au détriment d'une certaine universalité.