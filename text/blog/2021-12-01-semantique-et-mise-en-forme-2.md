---
title: "Sémantique et mise en forme (2) : consolider les fondations"
date: 2021-12-01
abstract: "Simplifier l'écriture et améliorer la structure : chronique de l'évolution d'un site web fabriqué avec Pandoc."
---

Il y a un peu plus d'un an, j'ai remanié mon processus de fabrication pour ce site. Vous pouvez retrouver les explications dans [ce billet](dr-pandoc-and-mr-make.html). En résumé, l'idée était de simplifier à la fois l'outillage en coulisses et la présentation du contenu en façade : le site serait désormais entièrement généré avec Pandoc (le convertisseur qui me permet de rédiger dans un format texte avec des fonctionnalités académiques comme les citations) et serait organisé autour d'une page d'accueil unique renvoyant vers toutes les autres pages du site.

Cette semaine, j'ai procédé à quelques améliorations sur le site. Et comme précédemment, je voudrais les commenter brièvement à travers un billet de blog. Bricolage, puis retour d'expérience écrit : c'est désormais un rendez-vous annuel, la visite guidée épisodique de ma *querencia* éditoriale, [dans les mots de Marc Jahjah](https://twitter.com/marc_jahjah/status/1207631701838569477). Et pour cette nouvelle livraison, j'ai décidé de reprendre le titre d'[un vieux billet](semantique-et-mise-en-forme.html)[^1] qui parlait déjà de Pandoc, d'Edward Tufte et de technologies du Web.

[^1]: 2018, pour moi, c'est il y a une éternité, surtout à l'échelle du blog (qui périme vite, comme je l'ai écrit [ici](pour-un-autre-carnet-de-recherche-numerique.html)).

# Mange ta soupe (de tags)

Au moment de la refonte il y a un an, j'étais conscient qu'un aspect était largement perfectible : le fichier source de ma page d'accueil, écrit en Pandoc Markdown.

La page d'accueil d'un site peut être assez complexe. Suivant la façon dont on veut répartir les choses dans l'espace, et anticiper l'affichage sur différentes tailles d'écrans, c'est tout un échafaudage d'éléments HTML et de déclarations CSS qu'il faut construire. Dans cette perspective, le conteneur générique `div` est assez populaire. Il permet de sectionner arbitrairement le contenu, tout en attribuant à chaque unité un identifiant (unique) et des classes (partagées) qui facilitent l'application des règles de mise en forme en CSS. Cette possibilité de découpage arbitraire est tellement pratique qu'on peut vite abuser des `div`, le code tournant à la « soupe d'éléments » ([*tag soup*](https://en.wikipedia.org/wiki/Tag_soup)), au lieu d'exprimer la structure intrinsèque du contenu par des éléments dits sémantiques, c'est-à-dire dont le nom correspond vraiment à ce qu'ils sont censés représenter (comme `article`, `section`).

Pour ma page d'accueil, j'avais fait le choix de ne pas écrire en HTML mais en Pandoc Markdown, comme sur le reste du site, et de générer automatiquement le HTML. Normalement, il n'y a pas d'équivalent de `div` en Markdown. Mais la variante de Markdown propre à Pandoc propose une syntaxe équivalente. Ainsi, l'extrait de Pandoc Markdown suivant :

```
::: .groupeA #bloc1
Cette section sera transformée en div en HTML.

Et je peux *toujours* écrire en Markdown.
:::
```

Donnera le résultat suivant en HTML :

```html
<div class="groupeA" id="bloc1">
  <p>Cette section sera transformée en div en HTML.</p>
  <p>Et je peux <em>toujours</em> écrire en Markdown.</p>
</div>
```

L'intérêt est qu'on a le découpage arbitraire du contenu tout en pouvant continuer à écrire en Markdown à l'intérieur. L'inconvénient est qu'on s'éloigne de la lisibilité et de la simplicité de Markdown pour se rapprocher d'autres langages de balisage léger comme AsciiDoc ou reStructuredText, dont la syntaxe est plus verbeuse. En bref, ça fonctionne mais on est dans un entre-deux un peu rugueux entre deux formes de balisage.

J'ai expliqué cette fonctionnalité de Pandoc dans [un billet](enseignement-automatisation-pandoc.html) publié peu de temps après la refonte du site. J'y explique aussi ce que sont les filtres Pandoc, une fonctionnalité encore plus avancée et qui peut fonctionner en tandem avec la précédente[^2]. Mais au moment de conclure le billet, c'est finalement sur quelque chose de beaucoup plus simple que je m'arrête : comment paramétrer la conversion avec Pandoc via les métadonnées (en-tête YAML) du document. C'est un mécanisme très simple (on ajoute une ligne en haut du fichier) mais qui permet d'actionner des fonctionnalités de mise en page très complètes. Cela permet par exemple de solliciter toute la puissance de LaTeX sans en écrire une ligne. Exemple : mettre `documentclass: scrartcl` pour générer un PDF plus adapté aux conventions typographiques européennes que le modèle LaTeX par défaut, américano-centré.

[^2]: Dans le cas de mon site, le sectionnement via des *divs* natifs permet de préparer le contenu pour le mettre en forme via CSS. Mais Pandoc peut tout à fait exploiter ce sectionnement dans les autres formats de sortie. On peut notamment écrire des filtres Lua qui traduiront le sectionnement par du code *ad hoc* en fonction du format d'export. J'en donne un exemple pour LaTeX dans [le billet pré-cité](https://www.arthurperret.fr/enseignement-automatisation-pandoc.html). 

J'ai repensé récemment à ce retour d'expérience, à ces différentes fonctionnalités, et au confort de l'en-tête YAML, si simple mais polyvalent et puissant. Cela m'a fait réfléchir à ma page d'accueil : n'y aurait-il pas moyen d'en simplifier l'écriture, tout en conservant la capacité de mettre en page le contenu de façon complexe ?

La réponse est oui, grâce à la bonne combinaison de fonctionnalités de Pandoc : l'option [`section-divs`](https://pandoc.org/MANUAL.html#option--section-divs) et l'extension [`auto_identifiers`](https://pandoc.org/MANUAL.html#extension-auto_identifiers). Dans Pandoc, une option s'invoque soit dans l'en-tête du document, soit au moment de lancer la commande ; une extension, elle, s'invoque au moment de préciser le format d'export désiré.

Prenons un exemple. Le document ci-dessous :

```
---
title: Mon document
section-divs: true
...

# Introduction

Dans ce document, nous parlerons de…

# Première partie

```

Converti de cette manière :

```
pandoc mon-document.md \
  -t markdown+auto_identifiers \
  -o mon-document.html
```

Donnera le résultat suivant en HTML :

```html
…
<article>
  <section id="introduction" class="level1">
    <h1>Introduction</h1>
    <p>Dans ce document, nous parlerons de…</p>
  </section>
  <section id="première-partie" class="level1">
    <h1>Première partie</h1>
  </section>
</article>
…
```

Cette simple combinaison de fonctionnalités permet d'obtenir une structure HTML claire et plus sémantique via les niveaux de titres, tout en conservant la possibilité de cibler facilement des éléments grâce à des identifiants et des classes générées automatiquement.

J'ai donc réécrit le fichier Pandoc Markdown de ma page d'accueil de manière beaucoup plus légère : plus de *divs* natifs à tout va mais de simples titres desquels découle automatiquement une structure HTML facilement manipulable en CSS. Ainsi, mettre un titre de niveau 1 `# Articles` en Markdown ouvre la possibilité de positionner précisément la section correspondante, par exemple dans une grille, sans avoir à écrire le HTML :

```css
article {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: auto auto auto;
  gap: 1px 40px;
}
section#articles {
  grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  grid-row-end: 2;
}
```

# Le diable est dans l'élément `details`

Il y a quelques mois, j'ai commencé à partager mon activité de veille. Dans [le billet](https://www.arthurperret.fr/une-nouvelle-etape-dans-mon-travail-de-veille.html) qui présente cette nouveauté, j'explique que la veille a sa propre page, ce qui est une entorse au principe d'une page d'accueil unique pointant vers toutes les autres. Sur le moment, c'est une solution que j'estimais inévitable. En effet, dès le lancement, la liste des entrées de veille était déjà beaucoup plus longue que celle des articles et des billets de blog. Or je ne voulais pas déséquilibrer la page d'accueil. J'ai donc décidé de n'y afficher que les cinq dernières entrées de veille, et de lister l'intégralité des entrées sur une page dédiée.

Malheureusement, ce fonctionnement s'est avéré un peu pénible. En effet, et contrairement à un générateur de site clé en main (comme [Jekyll](https://jekyllrb.com/), [Hugo](https://gohugo.io) ou [Eleventy](https://www.11ty.dev/)), mon processus de fabrication n'inclut pas la génération automatique de listes de contenus. Les seules étapes complètement automatisées sont la conversion en HTML et le traitement des citations ; tout le reste, des listes d'articles au flux RSS, est fait à la main. Par conséquent, pour les entrées de veille, je devais créer manuellement les mêmes liens sur deux pages différentes. C'est une petite redondance, mais qui a fini par me lasser[^3].

[^3]: Il faut dire aussi que l'intégration d'une entrée de veille sur le site n'est pas moins longue que celle d'un billet ou d'un article, quand bien même ces derniers seraient dix fois plus volumineux. Donc toute perte de temps est ressentie de manière plus aigüe.

Dans le cadre du développement de [Cosma](https://cosma.graphlab.fr), [Guillaume Brioudes](https://myllaume.fr) m'a fait découvrir l'élément HTML `details`. Cet élément permet de créer une section dont le contenu est masqué et qu'on peut révéler en cliquant sur un bouton. Dans Cosma, nous nous en servons pour créer des sections repliables dans la barre d'outils. Lorsque j'ai entrepris la réécriture de ma page d'accueil, j'ai pensé à `details` pour résoudre mon problème de veille. Et voici la façon dont je l'utilise :

```html
# Veille

- [Quarto](quarto.html)
- [Tree-query : prémices d’une évolution ?](tree-query.html)
- [Treeverse : le fil, l'arbre et le réseau](treeverse.html)

<details>
<summary>Voir les entrées précédentes</summary>

- [Ordinateurs et créativité](veille/ordinateurs-et-creativite.html)
- …  

</details>
```

Ceci me permet de n'afficher qu'une partie des liens, sans avoir de page séparée à maintenir et de copier-coller à faire. Un gain de temps modeste, mais un gain de temps tout de même.

Rapatrier ma veille en page d'accueil me permet aussi de mieux la valoriser. La nouvelle mise en page, en trois colonnes, donne un meilleur aperçu des rythmes d'écriture qui coexistent sur ce site – articles longs, billets plus compacts, entrées de veille très courtes – tout en soulignant une continuité éditoriale.

# La qualité des défauts

Si vous vous promenez un peu sur ce site, et notamment que vous faites défiler un article jusqu'à la section Références, vous constaterez vite mon affection pour le retrait négatif (en anglais *hanging indent*). C'est l'inverse de l'alinéa : on renfonce tout le paragraphe, sauf la première ligne. Visuellement, on peut dire que cela fonctionne comme une liste à puces, sauf qu'il y a du texte à la place de la puce.

Le retrait négatif, c'est un peu l'équivalent typographique des onglets dans un agenda papier, un classeur, ou même un navigateur internet. Cela remplit exactement la même fonction. Dans une liste dense mais ordonnée, comme une bibliographie classée par ordre alphabétique, mettre un retrait négatif c'est créer une petite affordance qui facilite la recherche d'information.

J'aime beaucoup cette façon de présenter les listes de contenus, à tel point que je l'avais appliquée à ma page d'accueil : présentation bibliographique pour mes articles, et retrait négatif partout, y compris pour la liste des billets et celle des entrées de veille. Mais en revisitant cette page d'accueil, j'ai réalisé que ces choix n'étaient pas très judicieux.

D'abord, pour les articles, donner toutes les informations bibliographiques alourdit la liste ; c'est aussi redondant, car j'indique déjà ces informations sur la page de chaque article. Donc j'ai allégé la liste des articles en ne laissant que le titre et l'année, ainsi que les co-auteurs pour les articles écrits à plusieurs.

Et ensuite, de manière générale, le retrait négatif fonctionne mal pour cette page d'accueil, car elle est faite de paragraphes courts dans des colonnes étroites. Dans ce contexte, le retrait négatif provoque des retours à la ligne fréquents pour très peu de mots, ce qui crée une trop grande irrégularité visuelle.

J'ai donc supprimé les lignes de CSS qui appliquaient un retrait négatif à ces listes, et c'est là que j'ai redécouvert les vertus de la liste à puces. C'est la forme que prend par défaut l'élément HTML `ul` dans la plupart des navigateurs, et le fait que ce choix par défaut fonctionne bien m'a surpris.

Il faut dire que j'ai une certaine méfiance pour les listes à puces, qui remonte à ma lecture d'un pamphlet d'Edward Tufte sur PowerPoint [@tufte2003]. Selon Tufte, PowerPoint nous pousse à sectionner le propos via des listes à puces, alors qu'elles ont souvent le défaut de gommer l'enchaînement entre différentes étapes d'un raisonnement. C'est un argument qui m'a marqué.

Par ailleurs, j'ai aussi un préjugé contre les réglages par défaut, qui remonte lui à mon apprentissage du format EPUB. Comme le disait alors Jiminy Panoz, l'enfer, c'est les autres… feuilles de styles : le code CSS embarqué dans les systèmes de lecture (liseuses, applications) est parfois un véritable obstacle à la conception d'une bonne expérience de lecture. Dans la même logique, sur le Web, il est courant de voir un fichier CSS dont les premières lignes sont consacrées à un *reset* – le fait d'écraser préventivement les réglages du navigateur pour repartir sur des bases de design saines.

D'où ma surprise lorsque j'ai constaté que, pour une fois, c'était bien un réglage par défaut qui fonctionnait le mieux, de surcroît quand ce choix par défaut était celui d'une liste à puces. Mais quant des choses basiques s'avèrent appropriées et efficaces, il n'y a pas forcément besoin d'aller chercher la complexité. Et au passage, c'est un aspect de moins à régler manuellement.

***

J'ai toujours aimé passer du temps sur des détails mais j'ai de moins en moins la possibilité de le faire. Alors, des options miraculeuses de Pandoc aux nouveaux éléments HTML5, en passant par les feuilles de styles par défaut des navigateurs, j'apprécie tout ce qui peut me faire gagner du temps quand j'ai une nouvelle idée d'édition.

Le site ayant eu droit à sa révision annuelle, il est temps pour moi de retourner écrire – principalement ma thèse, mais aussi, périodiquement, quelques textes qui viendront allonger petit à petit les trois colonnes de ma nouvelle page d'accueil.

# Référence

