---
title: Citations marginales
date: 2019-12-17
abstract: "Un *making-of* de mes citations/notes de marge, entre filtres Pandoc et choix typographiques."
---

Je l'avais dit dans un précédent billet, j'essaye depuis le début d'implémenter un style « auteur-date-titre dans une note de marge » sur ce site. Tel le docteur Frankenstein, je suis heureux de pouvoir envoyer les faire-part de naissance du monstre : le problème sur lequel je butais est résolu. Ce billet fait en quelque sorte suite à [*Au doigt et à l'œil*](au-doigt-et-a-l-oeil.html) et avant cela au billet inaugural du site, [Dr Jekyll & Mr TeX](dr-jekyll-et-mr-tex.html), il y a exactement un an.

# Faire des notes de marge avec Pandoc

J'écris en [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), c'est-à-dire la syntaxe Markdown basique augmentée d'éléments propres à Pandoc, comme par exemple les notes de bas de page. En convertissant le code ci-dessous, Pandoc produira un document avec la note et son appel numérotés automatiquement :

	Du texte avec un appel de note [^identifiant].

	[^identifiant]: Le contenu de la note.

Pandoc est aussi un logiciel modulable grâce à son système de **filtres**. Ce sont des programmes que l'on peut ajouter en option à la conversion et qui effectuent un traitement supplémentaire.

Un exemple est [pandoc-citeproc](https://pandoc.org/MANUAL.html#citations). Ajouter l'option `--filter pandoc-citeproc` permet de convertir les clés de citation présentes dans le code en vraies citations, avec une bibliographie récapitulative en fin de document. Par exemple, la chaîne de caractères `[@goody1979, 11]` deviendra « (Goody 1979, p. 11) » dans le corps du texte ou bien « Goody, Jack. *La Raison graphique : la domestication de la pensée sauvage*. Paris : Les Editions de Minuit, 1979 » dans une note de bas de page, selon le style bibliographique choisi.

Il y a quelques années, le développeur américain Jake Zimmerman a créé [pandoc-sidenote](https://github.com/jez/pandoc-sidenote). Tout est dans le nom : c'est un filtre pour Pandoc qui déplace les notes de bas de page dans la marge. Il y a bien sûr une condition : il faut que le modèle de document à la sortie soit pensé pour. Ce filtre est inspiré du « style Edward Tufte », populaire en sciences de l'information et en visualisation de données. Il a été implémenté [en LaTeX](https://www.ctan.org/pkg/tufte-latex), [dans RStudio](https://github.com/rstudio/tufte) et [en CSS](https://edwardtufte.github.io/tufte-css/), notamment pour les blogs générés avec Jekyll – ce qui est le cas de ce site.

On peut combiner ces filtres mais l'ordre dans lequel les faire intervenir est important. Si vous souhaitez citer des documents dans un style bibliographique de type « note » et faire apparaître les notes dans la marge, il faut d'abord fabriquer les citations, puis les « marginaliser ». Il faudra donc mentionner d'abord pandoc-citeproc puis pandoc-sidenote. Pour Jekyll, les paramètres de génération du site sont contenus dans un fichier de configuration `config.yaml` dans lequel il faut indiquer les deux filtres dans le bon ordre :

	pandoc:
	  extensions:
		- filter: "pandoc-citeproc"
		- filter: "pandoc-sidenote"

# Le rouge et le gris

Comment mettre en page les notes ? Par respect pour les anciens, je suis d'abord allé consulter le chapitre qui y est consacré dans le manuel de Jan Tschichold. Petit florilège :

> « Les chiffres supérieurs au commencement des notes en bas de page doivent être relégués dans l'enfer de la typographie . . . Il est inexplicable que l'on persiste à placer à gauche au-dessus des notes en bas de page un filet long de 4 cicéros[^1], cet ornement est encore plus inutile que l'appendice pour l'être humain . . . L'habitude profane de séparer les notes par une interligne de quelques points donne un aspect typographique inarticulé, extrêmement confus, sans rythme et donc laid. On obtient le contraire d'une forme : une difformité[@tschichold2018, 134-135] ».

Après avoir remis mes côtes en place (je ne me souviens pas avoir ri autant sur les manuels de statistique en biologie ou en master info-com), j'ai tourné la page et je suis tombé là-dessus :

> « Placer des notes en bas de page est la méthode la plus tardive et la plus employée. Les notes marginales exigent une marge même là où elle ne serait pas nécessaire ; il n'est pas toujours simple de trouver ces notes, quand par hasard l'une d'elles est très longue et éloigne beaucoup la suivante du chiffre d'appel. C'est une forme depuis longtemps désuète[@tschichold2018, 136] ».

Aïe ! Heureusement, beaucoup de flux numérique a coulé dans les tuyaux de l'Internet depuis que Tschichold a écrit cela (1929). La variabilité des supports numériques a conduit à l'émergence de la mise en page adaptative, qui apporte de nouvelles solutions au problème des notes – j'en cite quelques exemples [dans mon précédent billet sur le sujet](au-doigt-et-a-l-oeil.html).

Pour bien différencier les notes du texte, j'ai combiné plusieurs effets. D'abord, les numéros de note sont doublement mis en valeur, avec un renfoncement inversé (*hanging indent*) comme pour la liste des références bibliographiques et l'application d'une couleur différente (rouge), qui indique notamment le fait qu'ils sont cliquables lorsque la mise en page rétrécit sur les petits écrans. Ensuite, je me suis inspiré du travail de certaines maisons d'édition comme Transcript (repérée grâce à une recherche sur les utilisations de la police Bembo [via le site Fonts In Use](https://fontsinuse.com/uses/16434/bildung-anfuehren-1)) ou bien comme B42 sur [*La typographie moderne*](https://editions-b42.com/produit/la-typographie-moderne-2/) [@kinross2019] : les designers de ces livres ont créé visuellement deux plans informationnels – le texte et l'appareil critique – via une différence de police. N'ayant pas le courage de me lancer en quête d'une nouvelle police à accorder au reste, j'ai réutilisé Inconsolata, mais en gras. Cela affecte du coup ce que les typographes appellent [le gris](https://fr.wikipedia.org/wiki/Gris_typographique) : troisième paramètre de différenciation.

Pour différencier les notes entre elles, j'ai tenté de suivre certaines préconisations de Tschichold, ce qui s'est révélé être le détail le plus infernal à paramétrer de toute l'histoire. Contrairement aux paragraphes du texte qui sont séparés par une interligne supplémentaire car cela fonctionne bien pour la lecture web, il est préférable de resserrer les notes, pour ne pas  trop les éloigner de leur appel lorsqu'elles se succèdent vite. C'est aussi pour cela que j'ai conçu ce fameux style bibliographique auteur-date-titre, qui donne trois informations mais dans une note compacte : même en enchaînant rapidement les citations, elles occupent peu de place dans la marge. Les instructions laissées par Jake Zimmerman sur son dépôt m'ont permis de créer une version parallèle (*fork*) de son filtre dans lequel j'ai enlevé les sauts de ligne excessifs codés en dur qui m'empêchaient de contrôler l'interlignage simplement en CSS. La manœuvre était tellement alambiquée sur le papier que j'ai été surpris qu'elle fonctionne absolument sans aucun accroc.

***

Je suis content d'avoir résolu cette question, notamment parce que j'en suis débarrassé dans la perspective de la rédaction de thèse. En effet, j'aimerais en livrer aussi une version HTML et j'ai entendu trop de récits d'imprévus éditoriaux de dernière minute pour me passer définitivement l'envie de gérer les marges entre la conclusion et les remerciements…

Vous aurez peut-être remarqué que tout à droite, il y a aussi Hypothesis qui s'est rajouté, grâce aux bonnes recommandations de la twittosphère. Je n'ai pas grand-chose à en dire : l'intégration est d'une grande simplicité, il y a bien des options de personnalisation (*branding*) mais je ne me suis pas encore penché dessus. Ce sera pour un prochain billet, même si j'ai l'impression que l'excellent travail d'exploration effectué ailleurs comme [chez Louis-O par exemple](https://labs.loupbrun.ca/hypothesis/) contient déjà pas mal de réponses.

# Références

[^1]: Environ 1,8 cm. Cf. l'entrée « [cicéro](https://fr.wikipedia.org/wiki/Cic%C3%A9ro_(unit%C3%A9)) » de Wikipédia.
