---
title: Écrire et éditer
date: 2020-05-22
abstract: "Dans mon bilan sur mes outils, j’ai distingué écriture et édition. On ne les démêle pourtant pas facilement… Explications."
---

# Éditer en écrivant

J’avais mentionné la polysémie du verbe éditer dans une conférence au congrès de la SFSIC (mon premier article publié !) :

> « Si la langue anglaise fait la distinction entre l’édition en tant que filière (*publishing*) et l’élaboration effective du document (*editing*) […] le terme français d’édition a acquis une certaine polysémie. […] Éditer, c’est orchestrer l’élaboration du document. Dans le contexte de cet article c’est la seconde dimension qui nous intéresse : éditer au sens de créer et manipuler des fichiers ; éditeur au sens de logiciel d’édition ; édition au sens de pratiques d’écriture, entre conception, rédaction et programmation ». [@perret2018, 2, article consultable [en HTML ici-même](matiere-a-pensees.html).]

Dans la dernière phrase, on voit que je ramène toutes les activités d’édition à l’écriture. Et c’est normal : déjà à l’époque, j’étais influencé par ma pratique, qui se passait essentiellement dans un écosystème textuel.

Aujourd’hui, elle s’y inscrit totalement. Je balise mon texte avec des signes de ponctuation et de scripturation [Scripturation : ensemble des marques d’énonciation, manuscrites et typographiques. Cf. @laufer1986, 75] qui me permettent de le structurer, de le sémantiser, d’anticiper sa mise en forme. J’interviens sur le texte pour le réorganiser ou faire des substitutions, en cherchant et en remplaçant des chaînes de caractères (normales ou expressions régulières). Je conçois la mise en forme pour mes exports et je l’exprime dans différents langages informatiques (CSS, CSL, LaTeX…). Enfin, je mets en œuvre la fabrication par des commandes textuelles déléguées à des scripts.

En somme, tout autant que la rédaction, c’est aussi l’édition du texte, le design éditorial [Sur cette expression, lire le n°8 de *Sciences du design* : @bourassa2018] et la programmation éditoriale[^1] qui relèvent de l’écriture dans ma pratique. Et le fait que ces activités soient mises en écriture les rapproche ; ainsi le passage au texte brut n’est pas une négation ou un rejet du design, ainsi que l’explique Daniel Allington :

> « Les auteurs qui choisissent LaTeX le font pour une raison opposée à celle, supposée, qu’ils préfèrent se concentrer sur le contenu et oublier le design […] Loin de croire qu’il vaut mieux laisser le design aux designers, ils utilisent LaTeX précisément parce qu’ils veulent s’occuper eux-mêmes du design de leurs documents[*“Authors choose LaTeX for the opposite reason to the stereotypical one about focusing on content and forgetting about design . . . They do not believe ‘that it is better to leave document design to document designers’: in fact, they are using it precisely because they want to have a go at being designers, which is in turn because they ‘worry about the appearance of their documents’.”* @allington2016] ».

Je recommande vivement la lecture du billet dont j'ai extrait le passage ci-dessus. L’auteur y reprend point par point les forces de LaTeX comme outil d’édition et ses faiblesses comme outil d’écriture, et j’ai trouvé la démonstration remarquable, car elle montre que l’écriture et l’édition ne se confondent pas : si leur superposition est indéniable, leurs contours n’en sont pas moins définissables.

# Eierlegende Wollmilchsau

Cette distinction entre écriture et édition, je la retrouve dans [un billet récent d’Antoine Fauchié](https://www.quaternum.net/2020/05/21/fabriques-de-publication-stylo/) à propos de l'application Stylo. Antoine a rejoint la Chaire de recherche en écritures numériques où il contribue notamment à une recherche-développement autour de cet outil d’édition scientifique. Dans ce billet, qui fait partie d’une série intitulée *Fabriques de publication*, il livre son analyse personnelle de l’outil et fait cette remarque :

> « La priorité n’est pas encore de proposer un outil d’écriture. Stylo a d’abord été mis en place pour rédiger et gérer des articles scientifiques dans le cadre éditorial d’une revue ».

Stylo permet pourtant d’écrire, en plus de renseigner des métadonnées, générer une bibliographie automatiquement, annoter à plusieurs et exporter dans divers formats. Mais la sélection des composants d’un système et la conception de son interface définissent la manière dont il répond à nos besoins d’écriture et d’édition. Et ces paramètres sont eux-mêmes déterminés en amont par le temps, les connaissances et les moyens disponibles pour développer l’outil.

Par conséquent, il faut faire des choix, et un prototype de recherche comme Stylo penche souvent d’un côté ou de l’autre de la distinction entre écrire et éditer. Stylo facilite l’édition via l’interfaçage des composants, la collaboration et l’exportation aux normes des éditeurs scientifiques. On peut le comparer à Zettlr (et Antoine a prévu de le faire), qui se concentre lui sur l’écriture, avec des fonctionnalités classiques mais indispensables (raccourcis clavier, boutons, menus) ainsi que d’autres plus spécifiques (liens internes, indexation par mots-clés). Ces ensembles de fonctionnalités ne se superposent pas, ce qui rend les deux outils potentiellement complémentaires.

Inversement, un logiciel développé dans un contexte commercial doit souvent se positionner de manière complète sur les deux plans. Le risque d’accoucher d’un mouton à cinq pattes est grand, ce qui explique peut-être le succès du modèle de développement open source pour les éditeurs de texte, y compris ceux portés par une grosse entreprise : les efforts de développement sont distribués sur une large communauté et ceux de rentabilité sont concentrés sur les services.

L’outil idéal existe-t-il ? Si oui, il s’agit probablement d’un cochon laineux qui donne du lait et pond des œufs[^2], armé d’un couteau-suisse – c’est-à-dire qu’il fait tout et qu’en plus il utilise Pandoc.

# Les conditions de l'écriture

Dans [un billet qui date d’il y a un an](et-a-la-fin-ecrire.html), j’avais évoqué une première fois l’importance des outils d’écriture et d’édition dans mon travail. À ce moment-là, j’étais passé d’un logiciel d’écriture taillé pour le Markdown mais un peu trop minimaliste et dirigiste (iA Writer) à un éditeur de texte polyvalent, doté de fonctionnalités expertes (BBEdit). L’un des objectifs était de rationaliser un peu mes expérimentations éditoriales en les faisant migrer vers mon environnement d’écriture : il s’agissait par exemple d’intégrer mes scripts de prévisualisation ou de conversion directement à l’interface de l’éditeur, en escamotant l’intermédiaire que constituait la ligne de commande. J’annonçais alors que cela me permettait de me pencher sur les *conditions d’écriture* de ma thèse – j'avais l'intuition que l'expression renvoyait à un sens complexe et pas encore bien compris.

Nous sommes un an plus tard et j'ai bien avancé sur ces conditions. Les voici en deux mots : des identifiants et des liens. Mon système d’écriture repose entièrement sur la capacité à identifier de manière unique un objet et à utiliser cet identifiant pour le relier à d’autres objets[^3].

Je pourrais théoriquement tout faire à la main mais trois outils m’épargnent cette galère, en automatisant la gestion des identifiants et la création des liens : Zotero, Pandoc et Zettlr. Je stocke mes références dans Zotero. Le plugin Better BibTeX leur attribue des clés de citation de type auteur-date[^4]. Je crée mes fichiers texte dans Zettlr, qui leur attribue un ID sous forme d’horodatage compact (exemple : `202005221321`). Dans ces fichiers, je mentionne les clés de mes références pour créer des citations, et les ID de mes fichiers texte pour créer des liens entre eux[^5]. Ils renvoient ainsi les uns aux autres, ainsi qu'aux références. Exemple :

```
---
title: Biblion
id: 202005221321
---
Le concept de biblion est de Paul Otlet [@otlet1934, 12].
Cf. [[202005101602]] Documentologie
```

Comment ce système d’écriture s’articule-t-il avec ma pratique d’édition ? J’utilise toujours BBEdit, car même si Zettlr permet de manipuler le texte (expressions régulières) et d’intégrer de la programmation éditoriale (personnalisation de la commande Pandoc), il est plus limité et un peu moins efficace. En revanche, porter des fonctionnalités dédiées à l’écriture dans BBEdit est à la fois compliqué et révélateur du statut de l'éditeur. Voici deux exemples.

Le premier, c’est la coloration syntaxique, c’est-à-dire la coloration dans l’interface des éléments spécifiques au langage informatique utilisé. Le Pandoc Markdown inclut des éléments n’appartenant pas à la syntaxe Markdown de base, et qui sont au cœur de ma pratique (les citations déjà mentionnées, mais également les notes). Leur coloration serait une aide visuelle appréciable. Zettlr est une instance de l'éditeur de texte CodeMirror, qui lui-même ne colore que le Github-Flavored Markdown. Zettlr colore les citations, mais uniquement comme effet collatéral de l'utilisation de `citeproc-js`, qui est responsable de leur rendu dans l'interface.

Pour CodeMirror comme pour BBEdit, il faudrait créer un module de coloration syntaxique spécifique au Pandoc Markdown mais c'est complètement hors de ma portée. En revanche, certains éditeurs *open source* et très modulaires ayant une plus large communauté d'utilisateurs, comme Vim, Emacs ou encore Atom, disposent d'un tel module.

Le second exemple concerne un petit bijou de sorcellerie logicielle. Il s’agit de l’insertion des identifiants. Écrire `@` (pour les clés de citations) ou `[[` (pour les ID) ouvre un sélecteur en forme de liste, qui permet de filtrer rapidement la liste des documents en tapant quelques caractères, puis d’insérer l’identifiant correspondant au document sélectionné.

![Le sélecteur de clé pour les citations et le sélecteur de fichier pour les liens internes dans Zettlr (NB : c’est un montage, les deux ne peuvent pas apparaître en même temps).](https://www.arthurperret.fr/img/2020-05-22-ecrire-et-editer.png)

Là aussi, certains éditeurs disposent de modules qui remplissent ces fonctions. Le sélecteur de citations est une fonctionnalité populaire du fait de l’écosystème [CiteProc](https://en.wikipedia.org/wiki/CiteProc). Et le système des liens internes façon wiki est également implémenté ça ou là. Mais pas dans BBEdit, et j'aurais du mal à fabriquer une telle fonctionnalité moi-même.

Cette dernière remarque m'amène vers ma conclusion. À l'évidence, toute ma pratique d’édition peut être facilement ramenée à de l’écriture mais ma principale activité d’écriture (rédiger de la prose scientifique), elle, n’est pas aisément transposable dans n’importe quel éditeur. Voilà une différence importante, et qui s'explique par l'existence de plusieurs jeux d'écriture.

Les différents composants de ma pratique d'édition existent dans un écosystème textuel que je peux investir sans trop de difficulté : modifier un style CSL, adapter un modèle LaTeX, écrire mon propre filtre Lua pour Pandoc, etc. C'est le régime du déjà-écrit, du prêt-à-coder. Tout ceci est documenté, enseigné, discuté à un niveau qui m'est (relativement) accessible. En revanche, l'éditeur, c'est-à-dire le programme qui me permet d'écrire et d'éditer, donc détermine mes *conditions d'écriture*, lui est codé suivant un autre jeu d'écriture, qui m'est inaccessible. Je n'ai tout simplement pas les compétences informatiques (en programmation, notamment) pour percer le niveau architextuel suivant.

Alors en attendant, je prends mes quartiers dans celui-ci, dont les défis ne sont pas tous résolus…

::: box
**Mise à jour du 15 juin 2022.** Trois mois après avoir écrit ce billet, j’ai fini par percer les mystères de BBEdit et j’ai réussi à reproduire ces fameuses fonctionnalités. Mais il aura fallu deux ans de plus avant que je me décide à écrire un billet là-dessus. Il est désormais publié : [Liens wiki et identifiants](2022-06-15-liens-wiki-et-identifiants.html).
:::

# Références

[^1]: Cette expression-ci a probablement été inventée par Éric Guichard, au moins pour ce qui est du français – à vérifier.

[^2]: *Eierlegende Wollmilchsau*, expression découverte chez les collègues germanophones.

[^3]: Ce modèle en réseau constitue une partie de mon sujet de thèse.

[^4]: La syntaxe Better BibTeX exacte est : `[auth:lower:nopunctordash][year]`. Cf. [la documentation du plugin](https://retorque.re/zotero-better-bibtex/installation/preferences/citation-keys/).

[^5]: La syntaxe des liens internes choisie par Zettlr est typique des wiki. On la retrouve dans d'autres logiciels de Zettelkasten. En revanche, la syntaxe pour les citations est spécifique au [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown).
