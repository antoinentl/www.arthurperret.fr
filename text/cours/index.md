---
title: Cours
---

- [Bibliographie](bibliographie.html)
- [Écriture académique au format texte](ecriture-academique-format-texte.html)
- [Expressions régulières](expressions-regulieres.html)
- [Format texte](format-texte.html)
- [Markdown](markdown.html)
- [Pandoc](pandoc.html)
- [Publication web](publication-web.html)
- [Recherche d’information et veille](recherche-information-veille.html)
- [Sérialisation de données au format texte](serialisation.html)
