---
title: "Techniques d’écriture et d’édition : évaluation"
author: Arthur Perret (Université Bordeaux Montaigne)
date: DUT Infonum 2021-2022
abstract: "Évaluation du cours Techniques d'écriture et d'édition."
css: 2021-2022-techniques-ecriture-edition.css
---

Téléchargez [le fichier du TP](2021-2022-techniques-ecriture-edition-evaluation.odt){download="2021-2022-techniques-ecriture-edition-evaluation.odt"}.

Ouvrez-le dans LibreOffice.

# Expressions régulières

Pour chaque énoncé ci-dessous, trouvez une expression régulière de recherche unique et une expression régulière de remplacement unique également.

1. Remplacez les espaces normales par des espaces insécables entre les guillemets `« »`, ainsi que devant les deux-points `:`, les points-virgules `;`, les points d'interrogation `?` et les points d'exclamation `!`.
2. Modifiez `TP1`, `TP2` et `TP3` en `TP n°1`, `TP n°2` et `TP n°3`.
3. Changez le format des dates de `YYYY-MM-DD` en `DD/MM/YYYY`.

Une fois les modifications faites, écrivez vos expressions régulières de recherche et de remplacement directement dans le document, à la toute fin, dans la section prévue à cet effet.

<details>
<summary>Correction (cliquer pour afficher)</summary>

**NB :** le symbole `␣` est utilisé pour indiquer une espace insécable.

1. Rechercher : `(«) | ([»:;?!])`  
Remplacer : `\1␣\2`
2. Rechercher : `TP([123])` (également : `TP(\d)`, `TP(1|2|3)`…)  
Remplacer : `TP n°\1`
3. Rechercher : `(\d{4})-(\d{2})-(\d{2})`  
Remplacer : `\3/\2\1`

</details>

# Bureautique avancée

1. Appliquez des styles au contenu d'après le tableau ci-dessous. Pour savoir quels paragraphes correspondent à des titres, et de quel niveau, vérifiez sur cette page.

élément | style
---|---
titre du document | Titre principal
auteur | author
diplôme et année | date
titres de niveau 1 | Titre 1
titres de niveau 2 | Titre 2
titres de niveau 3 | Titre 3

2. Modifiez les éléments stylés avec les styles indiqués ci-dessous colonne de gauche, pour leur appliquer à la place les styles indiqués colonne de droite. (Conseil : utilisez la fonction Rechercher & remplacer.)

style à remplacer | style de remplacement
---|---
Definition Term Tight | Definition Term
Definition Definition Tight | Definition Definition
Note en marge | Note

3. Ajoutez des champs dans l'en-tête et le pied-de-page conformément au indications ci-dessous :

zone | champ à insérer
---|---
en-tête | Utilisateur (votre nom, renseigné dans les préférences de LibreOffice)
pied de page | Numéro de page

**Attention !** Ces informations doivent être insérées automatiquement sous forme de champ, pas écrites manuellement.

4. Modifiez les styles indiqués ci-dessous conformément aux paramètres demandés :

style | paramètres à appliquer
---|---
Style de paragraphe par défaut | Traitement des veuves et des orphelines (2)
Titre 2 | Style de caractère : Normal
Titre 3 | Taille : 12 pt
Légende | Taille : 10 pt
Texte préformaté | Espacement au-dessus du paragraphe : 0,15 cm
Source_text | Surlignage : Couleur (gris clair 5 / `rgb(238,238,238)` / `#eeeeee`).
En-tête et pied de page | Alignement : Centrer

# Rendu

Cliquez sur *Fichier* › *Enregistrer* et nommez votre fichier **en respectant la forme suivante** : `NomPrénom.odt` (pas d'espaces).

Cliquez sur *Fichier* › *Exporter vers* › *Exporter au format PDF…* puis dans *Général* › *Structure*, cochez *Exporter le plan et autre éléments de structure*. Cliquez sur *Exporter*.

Envoyez les deux fichiers (odt et pdf) à [arthur.perret@u-bordeaux-montaigne.fr](mailto:arthur.perret@u-bordeaux-montaigne.fr).