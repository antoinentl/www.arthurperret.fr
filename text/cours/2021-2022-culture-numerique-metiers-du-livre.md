---
title: Culture numérique et métiers du livre
author: Arthur Perret (Université Bordeaux Montaigne)
date: BUT Métiers du livre 2021-2022
toc-depth: 2
abstract: "Le cours de Culture numérique que je donne en DUT Métiers du livre s'appuie sur le manuel *Culture numérique* de Dominique Cardon. Sur cette page, je livre un éclairage complémentaire, spécifique à la filière Métiers du livre. Pour cela, je reprends deux cours : *Économie du document* de Benoît Epron et Jean-Michel Salaün (Enssib/ENS-Lyon, 2016-2017) ; *Théories de l'édition numérique* d'Antoine Fauchié (Université de Montréal, 2020-2021). Je remercie particulièrement ce dernier d'avoir mis à ma disposition ses supports."
updated: 11/04/2022
---

# Sur le Web

## Entre attention et orientation

Avec l’avènement du Web, l’attention et l’orientation ont remplacé la matérialité comme ressorts de l'économie du document.

Dans un contexte d'abondance de l'information et de stagnation du pouvoir d'achat, l'attention est rare. Pour les marques, elle est liée à la nécessité d'attirer des investissements. Pour les personnes, c'est la recherche du quart d'heure d'attention.

L'attention est faite de deux dimensions :

1. La durée est une forme de reconnaissance qui permet une bonne orientation. Elle correspond à l'attention habituelle, automatique.
2. L'intensité est la forme ponctuelle de l'attention : elle éveille la mémoire immédiate en se détournant des habitudes.

Ces deux dimensions travaillent en opposition, sauf dans les situations d'immersion (perceptive, narrative ou sociale). Avec le numérique, on constate un glissement de notre point de vue, de la perspective vers l'immersion. Cela renforce considérablement l'importance de l'attention.

L'enjeu de l'économie est de mesurer et piloter l'attention. Du Klout aux altmetrics, les nombreuses métriques développées dans cette optique se concentrent sur les traces laissées par les utilisateurs. Le marché de l'attention numérique est un oligopole au sein duquel les mêmes acteurs (GAFAM) produisent, captent, commercialisent et analysent ces traces. Ils dessinent la société numérique : une forme d'état civil en ligne (Google, Facebook), des goûts et préférences (Apple, Amazon), la vie professionnelle (Microsoft).

L'orientation est l'autre ressort-clé de l'économie du numérique. Dès les origines du web, elle a fait l'objet d'une compétition entre annuaires et moteurs de recherche, que ces derniers ont fini par remporter. La force du moteur de recherche réside dans le fait que l'utilisateur lui reconnaît une forme d'objectivité. En réalité, les résultats présentés dépendent de critères spécifiques qui constituent une politique des savoirs. Cette doxa tend à favoriser l'autoréférence et l'optimisation (SEO), mais les moteurs s'y opposent, cherchant plutôt à « être absent des intentions des internautes » (expression de Dominique Cardon).

La qualité et la puissance du système Google posent un certain nombre de problèmes : opacité, dépendance, rapports de force, effondrement du pluralisme. Ce dernier point est important car l'orientation repose à la fois sur l'expertise distante et l'expérience ordinaire, une dichotomie qui se retrouve dans l'opposition entre ontologies et folksonomies.

Conserver une forme de pluralité sur le web consiste à combiner différentes politiques d'orientation : familiarité, exploration, usage de plans (annuaires), usage de mots-clés (moteurs de recherche), navigation sociale.

## Entre attention et biens communs

Quel modèle de média pour le Web ? Les médias se sont constitués en différents modèles économiques. Une typologie nous permet de mieux saisir leur émergence, à travers les trois dimensions de l’économie du document proposées par Jean-Michel Salaün dans *Vu, lu, su* :

| Dimension | Exemple | Type de bien | Modèle |
|:--|:--|:--|:--|
| Forme (vu) | Codex, exemplaire | Bien rival | Édition |
| Texte (lu) | Titre, œuvre | Accès non rival | Bibliothèque |
| Médium (su) | Lecture, attention | Espace-temps rival | Spectacle |

Chaque dimension opère suivant une logique différente des deux autres. Suivant la façon dont on raisonne, des modèles totalement opposés peuvent donc en émerger.

L’industrialisation de ces modèles correspond à une séparation entre univers marchand et non-marchand :

1. Le premier comprend l’édition, dont le modèle se stabilise autour de la vente des biens, ainsi que la presse et la télévision, dont le modèle tourne autour de la captation de l’attention. La télévision est un flux ; la presse combine le modèle de l’édition et celui de la télévision, à mi-chemin entre flux et vente de biens.
2. Le second univers comprend la bibliothèque et les biens communs.

Le Web se situe a priori à mi-chemin entre les modèles de l’attention et des biens communs. En fait, il combine tous les modèles mais modifie leurs modalités. C’est ainsi que les différents secteurs économiques se retrouvent tous bouleversés par le Web, de l’édition à la bibliothèque.

# Sur le livre

## Définir le livre

Définir le livre est une entreprise périlleuse et subjective, sans critères légaux et économiques. Sur quels éléments pouvons-nous nous baser ? Matériel (un objet) ? Physique (du papier ou des octets) ? Contenu (ce qu'il comporte) ? Économique (quel régime fiscal, quel lieu de vente) ?

Deux pistes :

1. Lois sur le livre : nombre de pages (reliées), genre, publications en série, etc.
2. Dépôt légal : nombre de pages (reliées), signes, non périodiques.

On constate le flou et la difficulté de normaliser quelque chose qui nous échappe.

Proposition d'Antoine Fauchié : le livre est un objet numérique ou papier, produit via un processus d'édition, dans l'objectif de diffuser des contenus dans un espace économique. Il faut qu'il y ait production, ainsi qu'édition. Il fait le choix ici de déplacer la définition du processus d'édition de Benoît Epron et Marcello Vitali-Rosati vers le livre. Les termes suivants sont importants :

objet
: quelque chose tangible (le numérique est tangible, certes différemment du papier), quelque chose de manipulable et de partageable ;

production
: il y a un geste, c'est un objet qui résulte d'une fabrication. « Fabriquer, cela signifie d’abord manipuler et détourner quelque chose qui fait partie du donné, le changer en artefact et le tourner vers l’application pratique » (Vilém Flusser, *Petite philosophie du design*) ;

diffuser
: il y a une intention de rendre accessible un contenu, il y a une intention de faire voir ou lire (sentir ?) un contenu choisi et organisé ;

contenus
: il y a un propos dans le livre (en plus du propos que représente l'existence du livre lui-même) ;

économie
: il ne s'agit pas forcément d'une économie financière, il peut s'agir d'une économie de la connaissance. Il faudrait trouver un terme plus générique, il s'agit d'un écosystème dans lequel les interactions concernent le livre.

Malgré tous les efforts possibles pour trouver une définition qui nous convienne et sur laquelle nous pourrions faire consensus, il y aura toujours des cas à part. Littérature jeunesse : quelques pages colorées et reliées sans texte constituent-elles un livre ? Fanzine : une brochure en un seul exemplaire est-elle un livre ? Site web : à partir de quel moment pouvons-nous considérer un site web comme un livre ? Une lettre d'information ? etc.

## Définir le livre numérique

Définir le livre numérique n'est pas moins périlleux ni subjectif que définir le livre tout court. Mais on peut là aussi se pencher sur la définition légale et économique du livre numérique pour tenter de trouver un appui.

### Définition légale du livre numérique (en France)

> « Ouvrage édité et diffusé sous forme numérique, destiné à être lu sur un écran. Note : le livre numérique peut être un ouvrage composé directement sous forme numérique ou numérisé à partir d'imprimés ou de manuscrits ; le livre numérique peut être lu à l'aide de supports électroniques très divers ; on trouve aussi le terme “livre électronique”, qui n'est pas recommandé en ce sens. Voir aussi : liseuse. Équivalent étranger : e-book, electronic book ». Journal officiel du 4 avril 2012 <https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000025627105>

Cette définition arrive tardivement, en 2012, alors que l'arrivée du livre numérique peut être datée à 2006-2007. Cela signifie que pendant plus de 6 ans il n'y a eu aucun encadrement légal du livre numérique en France.

Quelques points saillants :

- « sur un écran » : aucune précision n'est faite concernant un dispositif particulier. Que ce soit un écran de télévision ou une montre connectée, il doit s'agir avant tout d'un écran. La liseuse n'est par exemple pas évoquée. D'ailleurs, qu'est-ce qu'une liseuse ? Voir aparté ci-dessous ;
- nativement numérique ou issu d'une numérisation ;
- supports électroniques très divers : encore une fois ce n'est pas la liseuse qui est plébiscitée ;
- distinction entre le dispositif de lecture et le livre (le fichier).

Aparté : la liseuse à encre électronique. La liseuse, ou *e-reader*, est un appareil mobile principalement destiné à lire des livres numériques, et équipé d'un écran à encre électronique. Voici ses particularités :

- encre électronique : énergie nécessaire uniquement pour changer l'état de l'écran ;
- confort de lecture ;
- grande autonomie ;
- grande capacité de stockage ;
- dispositif → système d'exploitation → logiciel de lecture → fichiers EPUB

### Définition économique du livre numérique

En Europe, le débat porte sur la définition fiscale du livre numérique : bien (acquisition) ou service (accès temporaire). C'est un autre débat qui a débuté au moment où la question fiscale semblait réglée en France et dans d'autres pays européens. Le livre, en France, est considéré comme un bien particulier, c'est pour cela qu'il dispose d'une TVA (taxe) spécifique de 5,5%, ainsi que d'un prix unique (via la loi Lang de 1981). Si le livre numérique est un bien, sa TVA réduite peut être conservée, en revanche s'il s'agit d'un service alors sa TVA passe à 20%. Ce débat était donc purement économique, mais derrière se jouait un enjeu plus intéressant pour nous : pourquoi pourrait-on considérer qu'un livre numérique est un service plutôt qu'un bien.

La question portait donc également sur les possibilités d'accès au livre numérique, et notamment le sujet des mesures techniques de protection des fichiers, ou DRM en anglais. Les DRM sont un moyen de protéger un fichier d'éventuels pillage, dans les faits c'est principalement un frein aux usages et au développement économique. Concrètement, soit vous parvenez à faire fonctionner le DRM et vous n'y pensez plus (une fois votre liseuse connectée à votre compte Adobe par exemple), soit vous ne réussissez pas à utiliser ce qui apparaît comme une complication de plus. Essayez d'ouvrir un « vieux » livre numérique (acheté il y a quelques années) : on ne retrouve pas son mot de passe Adobe, on n'arrive pas à le modifier… Bref, tout cela pour dire que si vous ne pouvez pas être en possession du fichier du livre numérique, alors vous n'achetez pas un bien mais un service. Dans le cas d'Amazon et de son écosystème Kindle, c'est encore pire.

Pour prolonger la réflexion :

- [Le livre numérique est-il un service ?](https://opee.unistra.fr/spip.php?article321)
- [Bien ou service, faut-il choisir ?](https://www.livreshebdo.fr/article/bien-ou-service-faut-il-choisir)
- [Un livre électronique verrouillé par un DRM ne peut être comparé à un livre imprimé](https://www.plateforme-echange.org/Un-livre-electronique-verrouille-par-un-DRM-ne-peut-etre-compare-a-un-livre)

## Émergence du livre numérique

### Le premier livre numérique

Le premier livre numérique est inventé en 1971. À cette date, l'informatique est déjà née ; en revanche la micro-informatique n'en est qu'à ses débuts et Internet permet de connecter quelques ordinateurs seulement. Le 4 juillet 1971, jour de la fête nationale aux États-Unis, le jeune étudiant qu'est Michael Hart décide de taper au clavier la Déclaration d'indépendance des États-Unis pour la partager via des moyens informatiques. Le texte est saisi sur le clavier d’un terminal du Materials Research Lab, il s'agit du laboratoire informatique de l'Université de l'Illinois. Fait notable, le texte est saisi en caractères majuscules, les caractères minuscules n’existent alors pas encore. Michael Hart envoie le fichier texte de quelques kilo-octets par Internet à plusieurs de ses contacts, pour fêter ce 4 juillet.

Pourquoi cet événement est important ? Tout d'abord parce qu'il s'agit d'une entreprise d'encodage, aussi minimaliste soit-elle, et sans être la première. En allant regarder [une version plus récente du fichier sur le site du projet Gutenberg](http://www.gutenberg.org/cache/epub/1/pg1.txt), il y a une tentative de structuration de l'information – qui ressemble à un langage de balisage léger. Quand on parle d'encodage, il s'agit bien d'inscrire un texte dans l'objectif de le diffuser sous différentes formes, et pas d'une prise de vue photographique qui serait alors une numérisation. La différence est très importante, et vous pouvez la faire vous-même : prenez n'importe quel type de document comme un courrier papier de l'université et convertissez-le en numérique, vous avez alors la possibilité de le prendre en photo ou de le re-taper à la main sur votre ordinateur en utilisant par exemple un traitement de texte. Dans le premier cas vous obtiendrez une prise de vue, une image avec laquelle vos interactions seront limitées (sauf si vous faites un traitement type OCR ou reconnaissance de caractères) ; de l'autre vous avez un texte manipulable, un encodage qui est une conversion de ce document imprimé, vous passez de l'analogique au numérique.

C'est également un événement important car ce travail d'encodage est réalisé dans une optique précise : le diffuser via le réseau Internet. Le geste de Michael Hart n'est pas seulement un geste de conservation ou d'encodage dans l'objectif de travailler le texte, mais bien de l'encoder pour ensuite pouvoir le publier, le diffuser.

Ce format en plein texte, ou texte brut, et plus précisément le format TXT, ne permet pas une structuration très précise de ce document qui le mériterait pourtant. En 1971 les limites sont telles que le [codage UTF-8](https://fr.wikipedia.org/wiki/UTF-8) n'existe pas encore, le texte est inscrit en majuscules, ce qui ne facilite pas sa lecture. Mais malgré tout il s'agit d'un format à la fois « robuste, portable, commode et durable ». On détourne ici les 4 qualificatifs utilisés par Anthony G. Mills, et cités par Alberto Manguel dans son chapitre « La forme du livre » extrait de son livre *Une histoire de la lecture*, pour définir le livre. Robuste, car ce format est encore lisible 50 ans plus tard (même si le fichier actuel diffère légèrement du fichier original) ; portable, car le fichier ne pèse que quelques kilo octets (ce qui est peu même pour des supports limités comme les disquettes) ; commode, car il est lisible sur tout type de dispositif informatique ; et enfin durable, car ce format est pérenne et pourra toujours être lisible.

Pourquoi ce premier livre numérique bouleverse le travail d'édition ? Tout d'abord, on ne dit pas que cette expérimentation a été remarquée dans les années 1970 comme les prémisses de l'édition numérique ou même comme le premier livre numérique, cela n'a pas du tout été le cas. Nous constatons seulement après coup un bouleversement parce que cette entreprise implique une modification des pratiques. L'encodage d'un livre avec des moyens informatiques dans un objectif de diffusion numérique – certes assez rudimentaires à l'époque.

### L'essor économique du livre numérique

L'essor du livre numérique est différent de l'invention du livre numérique. C'est un événement majeur dans l'histoire du livre, survenu en 2006-2007, et lié à l'accumulation des trois phénomènes suivants :

1. la standardisation d'un format pour le livre numérique, et plus spécifiquement le format EPUB, sous l'égide de l'IDPF puis du W3C ;
2. deuxièmement la constitution de catalogues de livres numériques du côté des maisons d'édition, permettant aux lecteurs et lectrices de disposer d'un choix conséquent ;
3. et enfin troisièmement la mise sur le marché d'une liseuse à encre électronique offrant un réel confort de lecture tout en étant relativement bon marché.

## Lecture numérique

Qu'est-ce qu'une disposition de lecture ? Ce n'est pas lire sur un écran immobile. De façon non scientifique : le test du canapé (lire dans n'importe quelle position, le dispositif numérique doit suivre le lecteur au moins aussi bien que le livre imprimé). Voir le texte de François Bon, *Après le livre*.

La lecture numérique homothétique consiste à imaginer que l'écran remplace la page imprimée. On répète des pratiques de lecture issues du livre imprimé, mais avec des dispositifs numériques. Est-ce qu'il existe en revanche une lecture nativement numérique ? Voir le texte de Craig Mod, [« *The “Future Book” Is Here, but It’s Not What We Expected* »](https://www.wired.com/story/future-book-is-here-but-not-what-we-expected/).

## Littérature numérique

Écriture numérique. Il s'agit ici de mentionner l'usage de l'informatique et de logiciels pour écrire. Voir le livre de Matthew Kirschenbaum, *Track changes*, une enquête sur les pratiques d'écriture des auteurs et des autrices de littérature. C'est une histoire des traitements de texte sous l'angle de la littérature, et on y constate que l'outil a une grande influence sur la façon d'écrire. Un des exemples est le cas de George R. R. Martin et son utilisation d'un programme appelé WordStar, via un ordinateur qui n'est pas connecté à Internet. L'écriture en réseau c'est soit le fait d'écrire en ligne, donc potentiellement à la vue de tous, soit le fait d'écrire à plusieurs.

Littérature numérique. La production d'un artefact par des auteurs, autrices. Informatique + Internet + le Web : écriture assistée par les ordinateurs, produisant un hypertexte dans un réseau intelligent à la périphérie. Ceci permet de voir émerger une littérature numérique. Le numérique a amené de nouvelles possibilités, c'est-à-dire produire des objets jusqu'ici impossible à réaliser avec le papier. Hypertexte et interactivité. Mais aussi une nouvelle dimension : le fait de maîtriser de bout en bout la chaîne éditoriale, sans dépendre d'un éditeur. Pour le meilleur et pour le pire : notamment le fait que beaucoup d'œuvres sont restées inconnues, et que la grande majorité des expérimentations du début des années est désormais perdue, ou peut-être archivées quelque part. L'utilisation massive de Flash fait que certains sites (s'ils sont encore en ligne) seront très bientôt illisibles.

La littérature numérique ? Quelques exemples :

- [*The Shape of Design*, Frank Chimero, 2012](https://shapeofdesignbook.com)
- [*Koya Bound, Eight Days on the Kumano Kodo*, Craig Mod & Dan Rubin](https://walkkumano.com/koyabound/)
- [*Seed*, Joanna Walsh, 2017](https://seed-story.com)
- [*L'incendie est clos*, Otto Borg, Abrüpt, 2019](https://www.antilivre.org/lincendie-est-clos/)

# Sur l'édition

L'édition numérique est une évolution de l'édition dite classique, avec différents événements que nous pouvons identifier et analyser. L'édition numérique est influencée par l'informatique : les outils, les formats et les protocoles. L'édition numérique n'est pas une transposition de l'édition avec quelques outils numériques, mais un changement de paradigme. Elle est, d'après Antoine Fauchié, multimodale et hybride.

## Des contraintes venues d'ailleurs

Cet événement, pris parmi d'autres, nous permet de constater que les choix de diffusion ont une influence sur les modalités de production du livre. Le travail d'édition ne peut pas s'effectuer de la même façon si un livre doit être envoyé par message électronique ou s'il prendra la forme d'un imposant volume imprimé. L'édition est habituée depuis longtemps à utiliser de nombreuses contraintes techniques, notamment en lien avec les exigences de production imprimée, que ce soit la qualité du papier, le détail typographique, le temps d'impression ou la circulation du livre. Ce que nous apprend ce premier livre numérique, c'est que les contraintes techniques de création et de production peuvent provenir d'ailleurs, dans le sens où elles ne sont pas forcément spécifiques au domaine du livre ou de l'édition. C'est ce qu'expliquent Benoît Epron et Marcello Vitali-Rosati dans le chapitre « Enjeux stratégiques de la structuration des documents » de leur livre *L'édition à l'ère numérique* :

> « Il s’agit d’un renversement important, puisqu'une part incontournable de l’activité éditoriale devient de fait conditionnée par des choix techniques issus de secteurs d’activité parfois très éloignés de l’édition » (p. 35).

Comment cela se traduit-il concrètement ? Par exemple par le fait que les maisons d'édition ont dû appréhender un format comme l'EPUB pour la production de leurs livres numériques. Certaines d'entre elles ont parfois sous-traitées à outrance cette production, mettant en danger leur savoir-faire dans le cas où le sous-traitant produisait des fichiers de mauvaise qualité, ou quand il a fallu mettre à jour des milliers de livres en raison d'une modification des standards. L'introduction de ces contraintes fortement techniques, accompagnées de protocoles spécifiques, nous amènent donc à la question de la maîtrise des formats.

## La maîtrise de nouveaux formats

Prolongeons l'exemple du format EPUB, en continuant d'utiliser le texte de Marcello Vitali-Rosati et de Benoît Epron. Les éditeurs ont donc eu deux possibilités pour s'adapter à ce nouveau format et au nouvel environnement qu'il impose : former les équipes ou s'entourer de spécialistes (des prestataires ou des sous-traitants). Le paradigme est très différent de l'apprentissage de nouveaux logiciels, car ici il s'agit d'acquérir une culture numérique, et non de savoir utiliser quelques fonctionnalités d'un énième logiciel. Pour le dire autrement, c'est comme si on vous disais que l'édition numérique se résume à WordPress, alors qu'il ne s'agit que d'une fabrique de publication [parmi beaucoup d'autres](https://www.quaternum.net/fabriques). Par contre WordPress pose des principes que partagent nombre de chaînes de publication. A contrario, un logiciel comme InDesign, aussi précis soit-il pour produire un format PDF destiné à être imprimé, n'apporte pas de solution au-delà de ce but.

Enfin, concernant la nécessité de maîtriser de nouveaux formats pour les maisons d'édition, il faut prendre en compte la différence entre des formats propriétaires qui n'apportent aucune plus-value en terme de connaissances, et des formats ouverts qui obligent à comprendre comment tout cela fonctionne, sans pour autant devenir développeur ou développeuse. Les formats propriétaires sont rigides, ils enferment leurs utilisateurs et utilisatrices dans des pratiques normalisés – au sens péjoratif du terme – à défaut de proposer une interconnexion entre des pratiques diverses. Ce qui nous amène à la question de l'interopérabilité, un concept central dans l'édition numérique.

## L'interopérabilité comme condition

Comme on vient de le voir, il ne s'agit pas simplement d'intégrer un nouveau format parmi une panoplie. Le format EPUB a eu l'avantage d'introduire un concept que les éditeurs et les éditrices n'avaient pas forcément pris en compte jusque-là : l'interopérabilité, « capacité à communiquer de deux logiciels différents » – pour reprendre [les mots](https://framablog.org/2019/06/12/cest-quoi-linteroperabilite-et-pourquoi-est-ce-beau-et-bien/) de Stéphane Bortzmeyer. C'est un concept encore mal compris dans le monde de l'édition, qui a segmenté toute sa chaîne. Cette segmentation est telle qu'une fois qu'une maison d'édition confie le travail de mise en page du texte à un graphiste qui va utiliser InDesign, personne ne sera en capacité de modifier le fichier et donc le texte, à part ce graphiste. L'interopérabilité permet de connecter différents outils, ou plutôt différentes phases d'édition, et ainsi différents humains. Si l'interopérabilité est mise en place, alors une certaine horizontalité est permise au sein de l'instance éditoriale, et les différentes phases d'édition peuvent s'articuler sans être des étapes hermétiques les unes par rapport aux autres. L'édition numérique doit être poreuse.

Car bien souvent ce qui peut sembler être de l'édition numérique est en fait de l'édition numérique homothétique. Mais qu'est-ce que cette expression d'édition numérique homothétique veut-elle bien dire ? Il s'agit simplement de calquer les pratiques de l'édition classique en ajoutant quelques gadgets numériques comme de nouveaux logiciels ou des applications. L'arrivée de l'informatique dans le monde de l'édition n'a pas engendrée automatiquement la constitution d'une nouvelle activité qui serait l'édition numérique. Il a fallu un changement de paradigme, une modification des pratiques, une nouvelle relation au texte et aux différentes façons de produire des artefacts. 

## Prendre en compte les usages

Parlons des usages de lecture. Le format EPUB est un déclencheur dans l'édition, mais d'autres formats numériques existent. L'attention se porte aujourd'hui moins sur un format spécifique que sur des modes d'accès au texte. L'EPUB est un bon moyen de diffuser et de vendre des livres au format numérique, mais il ne s'agit ni d'un format source (le texte originel qui permet de produire des formats de sortie), ni du seul format de sortie. Dans le domaine de l'édition numérique le format le plus plébiscité est l'EPUB pour le livre numérique, et les formats HTML et PDF pour l'article.

Antoine Fauchié propose l'hypothèse suivante : l'édition numérique serait par définition multimodale, et hybride. Une instance éditoriale numérique produirait plusieurs artefacts numériques et non une seule forme. Plutôt que de se limiter au format EPUB, la condition pour répondre à la dénomination « édition numérique » serait au moins deux formats différents. Et, en plus de cela, ces productions numériques s'articuleraient avec des productions imprimées, c'est-à-dire qu'il doit y avoir une hybridation des formats, qu'ils soient imprimés ou numériques – Alessandro Ludovico a largement détaillé cette idée dans *Post-Digital Print*.

Dans l'édition aujourd'hui nous pouvons noter deux comportements :

- le premier concerne un travail qui vient s'ajouter sans perturber les étapes d'édition classiques : le texte est enrichi pour qu'il puisse être convenablement intégré dans les plateformes de diffusion. Il s'agit essentiellement d'un balisage plus fin et d'ajout de métadonnées, que l'auteur voir même l'éditeur ne réalise pas, et qui permet ensuite de référencer correctement l'article ou l'ouvrage. Ou alors un nouvel intervenant sera chargé de produire une nouvelle version du livre, par exemple le format EPUB pour le livre numérique. Nous sommes ici dans ce qu'on peut qualifier d'édition numérique homothétique : il y a une conversion numérique sans proposer de dimension nouvelle ;
- le second comportement est plus rare : il s'agit d'une reconfiguration des étapes d'édition en prenant réellement en compte le numérique et ses possibilités. Cela se traduit par une horizontalité des échanges et une perméabilité des phases d'édition. Les réviseurs peuvent par exemple travailler en même temps que les personnes chargées de la mise en forme du texte, la chaîne ne s'articule plus autour des outils mais autour des fonctions nécessaires à la réalisation de tâches définies.

Le point commun de ces deux comportements qui prennent en compte le numérique, c'est la volonté de permettre une éditorialisation : le texte va être inscrit dans un espace numérique, des interactions vont naître, et pour cela il faut préparer le document. Pour que les lecteurs et les lectrices puissent s'emparer du texte, le partager, l'annoter, le conserver, etc., il faut qu'un certain nombre de critères soient remplis.

# Sur la bibliothèque

La bibliothèque est une entreprise de service basée sur le partage, la non-rivalité et la singularité des biens informationnels. Elle génère de la valeur ajoutée de trois façons :

- Gains de mutualisation (distribution d'information plus économique)
- Gains d'opportunité (découverte d'information accélérée, sérendipité)
- Gains en potentialité (conservation de l'information)

Par ailleurs, la bibliothèque sert trois échelles de communauté :

- Les usagers l'emploient comme un puissant dispositif d'écriture qui décuple les capacités de mémoire et de raisonnement.
- La collectivité auxquels les usagers appartiennent représente le financement principal du capital documentaire et le bénéficiaire du retour sur investissement.
- L'intérêt général, au sens de la constitution d'un patrimoine.

Le modèle se résume ainsi :

| Destinataire | Base arrière   | Face avant   | Produit   |
|--------------|----------------|--------------|-----------|
| Individu     | Collection     | Accès        | Mémoire   |
| Collectivité | Information    | Action       | Capital   |
| Société      | Patrimoine     | Transmission | Empreinte |

Web et bibliothèque, même modèle ? On peut arguer du fait que le web représente la principale innovation dans les dispositifs d'écriture depuis le livre imprimé mais qu'il s'apparente plutôt au modèle des bibliothèques. Or, s'il nous semble bouleverser un certain nombre de codes, c'est qu'il a hérité en partie de leur modèle économique, un modèle à la fois ancien et singulièrement moderne, qui paraîtrait presque iconoclaste au regard de l'économie capitaliste mondialisée.

L'exemple des moteurs de recherche est parlant. Ils reproduisent le schéma d'une bibliothèque : acquisition, indexation, catalogage, recherche, lecture. Le web lui-même ne peut pas atteindre le degré d'organisation d'une bibliothèque, mais les moteurs de recherche contournent le problème : ils permettent de trouver des ressources sans avoir besoin d'une base de données centrale ou d'une arborescence. Par ailleurs, ils s'inspirent d'outils bibliométriques : l'algorithme PageRank de Google s'inspire du facteur d'impact des revues scientifiques.

En revanche, les moteurs de recherche se distinguent par des possibilités de monétisation inaccessibles aux bibliothèques. En effet, les firmes du web possèdent une expertise en matière de calcul et stockage de données qui leur permet d'exploiter la plasticité de l'information et d'investir dans l'économie de l'attention à travers la publicité.

Pour illustrer cette proximité, il est utile de se pencher sur la façon dont on peut quantifier la valeur ajoutée des bibliothèques. Métriques et statistiques offraient jusqu'ici un aperçu imparfait de cette mais de nouveaux indicateurs ont fait leur apparition :

- Mesures quantitatives rendues possibles par l'informatisation des ressources
- Enquêtes sur les usages et la sociologie des bibliothèques

Les enjeux diffèrent en fonction du type de bibliothèque et donc d'usagers. Les bibliothèques académiques se basent sur la production scientifique des chercheurs et la réussite scolaire des étudiants. Les bibliothèques publiques s'inscrivent dans une démarche plus globale et politique : ce sont des acteurs majeurs de la démocratisation de la littératie, ce qui lie leur économie à celle de la société toute entière. Cette dimension se retrouve dans le web : les services représentent une valeur bien plus importante que celle, mesurable, des transactions commerciales.

Une étude du retour sur investissement d'une base de donnée anglaise nous montre comment enquêter sur la valeur générée dans le modèle économique de la bibliothèque et donc du web. L'étude décrit une construction de la valeur en plusieurs temps :

1. Valeur de l'investissement et de l'usage (coût de mise en place)
2. Valeur contingente (propension à payer/accepter)
3. Bénéfice pour l'utilisateur
4. Valeur nette (différence du bénéfice utilisateur et des coûts)
5. Gain d'efficacité pour les utilisateurs
6. Augmentation du retour sur investissement

On retrouve bien la logique du modèle évoqué précédemment, notamment dans les différentes échelles de communauté.

# Conclusion

Ce cours contient beaucoup d'outils et de références nécessaires pour nourrir une analyse éclairée de l'information à l'ère numérique. En particulier, l'angle économique permet d'informer une connaissance préalable et d'ouvrir des questionnements nouveaux. L'objectif était d'aider à comprendre les dynamiques anciennes de ce secteur, ainsi que son évolution récente.

Les acteurs technologiques entretiennent un certain impensé autour de leur univers, que ce cours s'emploie à défaire en promouvant le temps long, les tendances et le regard critique. L'ère numérique est caractérisée par une amplification de l'instabilité liée aux technologies. En mobilisant conjointement les principes économiques et l'histoire des techniques, ce cours avait pour objectif de redonner de la stabilité à votre perception de cet univers.

Enfin, et c'est peut-être son apport le plus significatif, ce cours réintroduit l'aspect politique et stratégique de l'information dans une perspective individuelle. Cela passe par l'éveil à des problématiques liées aux personnes, comme l'usage des données. C'est aussi la découverte des conflits stratégiques discrets autour de l'infrastructure réseau, qui concernent pourtant tous les internautes. Mais cela réside également dans le choix des mots : Google est un système documentaire, Facebook est un éditeur ; ou bien Google et Facebook sont des régies publicitaires. Les mots sont importants : ils changent notre perception, nous donnent des armes pour réfléchir plus librement. Dans la perspective de ce cours, l'information est souvent associée à des régimes de pouvoir ; il est important de conclure en réaffirmant qu'elle est synonyme de liberté.