---
title: Markdown
author: Arthur Perret (Université Bordeaux Montaigne)
abstract: "Cette page explique ce qu'est Markdown, le plus populaire des langages de balisage léger, et renvoie vers un tutoriel interactif en français."
wip: true
updated: 30/03/2022
---

# Qu'est-ce que Markdown ?

[Markdown](https://fr.wikipedia.org/wiki/Markdown) est un [langage de balisage léger](https://fr.wikipedia.org/wiki/Langage_de_balisage_léger) inventé par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) avec l'aide d'[Aaron Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz) en 2004. C'est à la fois une syntaxe pour structurer des contenus au [format texte](format-texte.html){.cours}, et un programme de conversion vers HTML de ces contenus structurés.

::: note
Un langage de balisage dit « léger » correspond à un langage dont la syntaxe est plus économe en caractères, donc potentiellement plus facile à écrire et à déchiffrer, que des langages comme HTML, XML ou encore LaTeX.
:::

La syntaxe de Markdown s'inspire des nombreuses formes d'écriture utilisées durant les débuts du Web, notamment dans les forums et les canaux de messagerie directe. Ces écritures utilisaient les caractères du clavier (comme `-`, `>`, `*`) pour symboliser différentes choses comme des listes, des citations ou de l'emphase.

D'autres langages de balisage léger existaient avant Markdown, et continuent à exister. Mais Markdown est le plus populaire d'entre eux. Il est notamment utilisé sur de nombreuses plateformes web dans lesquels les internautes peuvent écrire des messages : on écrit en Markdown, et le rendu se fait en HTML.

Un immense écosystème d'outils est apparu autour de Markdown, si bien qu'il existe probablement une solution pour rédiger en Markdown quel que soit votre type d'environnement logiciel préféré – éditeur dédié et minimaliste, environnement de développement intégré, application en ligne, terminal…

# Variantes et spécification

Markdown est une syntaxe simple, un sous-ensemble de HTML. C'est sa force (facile à prendre en main, lisible, parfaitement adapté à la rédaction pour le Web) mais c'est aussi sa limite. C'est ainsi que de nombreuses variantes (*flavors*) de Markdown sont apparues, ajoutant leurs propres spécificités à la syntaxe de base. Ceci complique l'interopérabilité des fichiers en Markdown.

John Gruber n'a jamais rédigé de spécification pour Markdown et a toujours refusé d'adouber les efforts allant dans ce sens. C'est pourquoi une initiative indépendante pour spécifier Markdown existe mais sous un autre nom : [CommonMark](https://commonmark.org).

# Tutoriel

La communauté CommonMark a créé un tutoriel interactif en ligne, que j'ai traduit en français. [Cliquez ici](https://www.arthurperret.fr/tutomd/) pour le consulter.

Si vous rencontrez des problèmes durant le tutoriel et si vous en avez le temps, merci de me le signaler [via un ticket sur GitHub](https://github.com/infologie/tutomd/issues).

# Et ensuite ?

Il n'y a pas besoin d'épiloguer sur Markdown, qui reste un format extrêmement simple. De plus, le Web regorge de contenus permettant d'aller plus loin (variantes, outils, cas d'usage…). Je m'en tiens donc là pour les généralités.

En revanche, si votre travail porte sur la connaissance, si vous rédigez dans un contexte académique, ou si vous vous interrogez sur la manière de passer de Markdown à d'autres formats (HTML, PDF, DOCX…), je vous renvoie vers mon cours sur [Pandoc](pandoc.html){.cours}, le convertisseur couteau-suisse du format texte, qui inclut une variante de Markdown pensée justement pour ces besoins.