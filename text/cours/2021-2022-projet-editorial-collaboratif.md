---
title: Projet éditorial collaboratif
author: Arthur Perret (Université Bordeaux Montaigne)
date: DUT Infonum 2021-2022
abstract: "Ce cours combine travail individuel et travail de groupe autour d'un projet éditorial. Le travail se déroule en deux temps. D'abord, il s'agit de définir une charte éditoriale, avec des rôles (rédaction en chef, rédaction, conception graphique…) et une ligne éditoriale (contenus, formats) du projet. Ensuite, il s'agit de créer la publication, de la gérer et de l'alimenter périodiquement sur plusieurs semaines. Six séances en présentiel sont consacrées à l'apprentissage des notions utiles ainsi qu'à l'accompagnement des projets. En dehors de ces heures, chaque groupe fait avancer son projet en s'organisant de manière autonome pour le mener à bien."
wip: true
css: 2021-2022-projet-editorial-collaboratif.css
updated: 07/02/2022
---

# Infos pratiques

## Modalités d'évaluation

Vous devez constituer des groupes de 3 à 5 personnes. La composition des groupes est libre.

Le cours est évalué de deux manières :

1. une note collective sur deux rendus : la publication avec ses contenus, et un tableau Trello avec son historique d'activité ;
2. une note individuelle sur la participation en cours, complétée par l'historique du tableau Trello, et un bilan individuel écrit à rendre à la fin du projet.

La note finale est une moyenne de ces deux notes.

### Publication (évaluation collective, rendu n°1)

**La consigne :** chaque groupe doit créer une publication numérique en ligne, périodique et collective. Ceci implique de définir des paramètres éditoriaux (sujet, public, ton, type d'information, type de média, longueur, modalités de diffusion, identité visuelle), puis créer des contenus et les publier en respectant ces paramètres.

Quelques précisions :

- Aucun paramètre n'est défini a priori : que ce soit le sujet, le volume des contenus, les modalités de diffusion ou encore la fréquence de publication, les aspects choix devront être faits collectivement au sein de chaque groupe, avec une étape de validation par l'enseignant.
- Les contenus peuvent être créés exprès pour le projet, ou avoir été déjà produits dans le cadre de la formation et être adaptés pour ce projet. Dans tous les cas, vous ne devez pas utiliser des contenu extérieurs sur lesquels vous n'avez pas travaillé.
- **Le calendrier est très court**, à peine deux mois. Il faut donc traiter l'exercice comme une campagne éclair, avec un feuilletonnage sur quelques semaines, et ne surtout pas prendre de retard.

**Critères d'évaluation :**

- rigueur dans la définition de la ligne éditoriale en début de projet ;
- originalité de la ligne éditoriale ;
- adéquation entre la ligne éditoriale et la publication réalisée ;
- professionalisme de l'expression (qui dépend de la nature des contenus : expression écrite, design graphique, montage…).

### Gestion du projet (évaluation collective, contrôle continu + rendu n°2)

**La consigne :** vous devez gérer le projet éditorial en utilisant une méthodologie (tableau de type kanban) et un outil (Trello) imposés. Le but de cet exercice est de vous faire travailler la méthodologie en question, très répandue dans le monde professionnel, que vous soyez débutant ou que vous la connaissiez déjà.

**Critères d'évaluation :**

- clarté du tableau (respect des principes d'organisation qui auront été vus en cours) ;
- implication collective dans l'utilisation du tableau ;
- mise à profit des fonctionnalités avancées (étiquettes, commentaires, mentions, pièces jointes…).

### Travail individuel (évaluation individuelle, contrôle continu + rendu n°3)

**La consigne :** vous devez vous impliquer individuellement tout au long du projet, et rédiger un bilan du projet qui présente votre travail individuel, ainsi que votre perception du travail collectif. L'évaluation du travail individuel se fait en observant votre participation lors des séances en présentiel, en examinant le tableau Trello, et en lisant votre bilan.

**Critères d'évaluation :**

- participation à l'élaboration collective de la ligne éditoriale ;
- accomplissement du rôle prévu ;
- implication individuelle dans l'utilisation de l'outil de gestion (Trello) ;
- qualité du bilan personnel.

Le bilan doit présenter les caractéristiques suivantes :

- fichier PDF ;
- entre 1 et 5 pages ;
- identification claire (titre, auteur, date, groupe, etc.) ;
- document de qualité professionnelle (lisible, clair, navigable) ;
- propos factuel, précis et argumenté.

## Calendrier

08/02
: Présentation du cours. Constitution des groupes. Notion de ligne éditoriale, avec exercice pratique et début d'élaboration d'une ligne éditoriale dans chaque groupe.

15/02
: Initiation à la méthode kanban et à Trello. Validation de la ligne éditoriale.

07/03
: Point d'étape après les premières publications.

21/03
: Suivi des projets.

28/03
: Suivi des projets.

11/04
: Bilan des projets. Je ferai un retour à chaque groupe sur le travail effectué, et je solliciterai vos retours sur le cours, notamment sur la méthodologie de gestion de projet imposée.

***

# Ligne éditoriale

## Paramètres éditoriaux

### Sujet

Sur quoi on communique ?

### Public

À qui on s'adresse ?

### Ton

Comment on s'exprime ?

### Type d'information

- officielle (information légale)
- scientifique
- journalistique
- opinion
- divertissement
- référence (encyclopédies, recettes, tutoriels…)

### Type de média

- texte
- hypertexte
- image
- son
- vidéo
- hybride (mélange des types de média précédents)

### Longueur

Micro, court, moyen, long, etc.

Comment définir/mesurer la longueur :

- temps de production nécessaire
- nombre de signes (texte)
- durée (son, vidéo)

### Diffusion

- modèle de diffusion : domaine propre, plateforme tierce, POSSE (*Publish on your Own Site, Syndicate Elsewhere*), PESOS (*Publish Elsewhere, Syndicate to your Own Site*)
- fréquence, périodicité de la publication : quotidien, hebdomadaire, mensuel, etc.
- créneau de publication (a une influence sur la visibilité) : moment de la journée (matin, midi, soir), heure précise

### Identité visuelle

- polices (pour du texte)
- couleurs
- logo
- illustrations (photo, art…)
- etc.

L'identité visuelle est souvent récapitulée dans un document appelé charte graphique.

## Catégories de contenus

Une catégorie de contenus constitue une combinaison de tous les paramètres (sujet, public, ton, type d'information, type de média, longueur, diffusion, identité visuelle) pour définir un certain contenu éditorial.

Lorsqu'on a plusieurs catégories de contenus, on peut les matérialiser par un format (ou rubrique). Un format possède une identité propre, qui peut inclure un nom et une charte graphique. Exemple : « Brèves », « L'instant détente », « Page Opinions », etc. C'est un élément fondamental dans les publications de type *magazine* (terme qui ne désigne pas seulement un périodique imprimé, cela peut aussi être une émission de télévision).

## Architecture de la publication

### Paratexte

- titre
- auteur(s)
- date de publication
- mots-clés
- description
- informations sur la publication (exemple : page « À propos »)
- etc.

### Mise en page des contenus

- liste
- tableau
- grille (ou galerie)
- etc.

## Identité éditoriale

- nom de la publication
- rôles
- ligne éditoriale : la ou les catégories de contenus

L'identité éditoriale est généralement récapitulée dans un document appelé charte éditoriale. C'est la qu'on précise la fameuse *ligne éditoriale*, à savoir l'ensemble des choix effectués quant aux paramètres éditoriaux et l'architecture de la publication.

## Exercice : identifier une ligne éditoriale {.exercice}

Voici quelques objets éditoriaux :

- [Bulletin](https://bulletin.fr)
- [Brut](https://www.brut.media/fr)
- [Entre les lignes](https://www.francetvinfo.fr/replay-radio/entre-les-lignes/)
- [Les Jaseuses](https://lesjaseuses.hypotheses.org)
- [NextInpact](https://www.nextinpact.com)
- [Quotidien](https://www.tf1.fr/tmc/quotidien-avec-yann-barthes)

Choisissez une publication et décrivez-la en utilisant [le modèle ci-joint](2022-02-08-analyse-ligne-editoriale.txt){download="2022-02-08-analyse-ligne-editoriale.txt"}. Chaque personne du groupe doit choisir une publication différente.

# Gestion de projet via Trello

Origine : méthode kanban.

Structure : tableau, colonnes, cartes.

## Principes à suivre

### Structurer le tableau en colonnes de manière efficace et pertinente

Les colonnes reflètent souvent une logique temporelle dans l'organisation des tâches. Exemple d'organisation générique : colonnes « À faire », « En cours », « Fait ». Exemple plus spécifique pour un projet éditorial : « Rédaction », « Relecture », « Publication ».

Un bon tableau combine organisation générique et spécifique. Ceci peut être accompli de différentes manières en fonction des possibilités de l'outil : ajouter des colonnes, utiliser des étiquettes…

### Nommer les tâches avec des verbes d'action

Ceci permet de clarifier ce qu'il faut faire. Exemple : « Créer un rapport » plutôt que « Rapport ».

### Attribuer les tâches à des personnes

Ceci est indispensable pour éviter la confusion sur le rôle de chacun et s'assurer que les tâches soient prises en charge.

### Définir des échéances

Définir une date et éventuellement une heure limite favorise l'accomplissement d'une tâche. Sans échéances, l'exécution des tâches traîne en longueur, jusqu'à provoquer un embouteillage en fin de projet, ce qui conduit à laisser des tâches inachevées.

