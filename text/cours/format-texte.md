---
title: Format texte
author: Arthur Perret (Université Bordeaux Montaigne)
abstract: "Cette page explique ce qu'est le format texte et donne des arguments en faveur de son utilisation. Elle ouvre vers d'autres ressources pour explorer l'écosystème du format texte."
wip: true
updated: 17/05/2022
---

# Qu'est-ce que le format texte ?

Vous avez probablement déjà vu un fichier dont le nom finit par `.txt`. Vous avez peut-être aussi ouvert un jour un logiciel comme Bloc-notes (Windows) ou TextEdit (Mac). Ce que vous avez croisé à ces occasions, c'est le format texte.

::: note
Je traduis l'anglais « *plain text* » par « format texte » et pas par « texte brut », comme c'est souvent le cas, ni par « format texte brut » comme on le trouve parfois, et encore moins par « texte simple » ou « texte pur ». Je n'aime pas les connotations de ces adjectifs. Je préfère mettre en avant le mot « format », beaucoup plus important malgré (ou à cause de) sa polysémie. En effet, ce mot est lié à une tradition en SHS : l'analyse et la critique des logiques de formatage, cadrage, contrôle, etc. C'est un petit cheval de Troie qui embarque potentiellement la question de l'aliénation et de l'émancipation.
:::

Un fichier au format texte est un fichier dont le code binaire (suite de 0 et de 1) se traduit par des caractères textuels uniquement (par opposition à des pixels ou des sons par exemple). Dit de manière encore plus courte : c’est un fichier qui ne contient des caractères.

Voici un exemple extrait d'un fichier au format texte :

```
LE VICOMTE:
Personne ?
Attendez ! Je vais lui lancer un de ces traits !...
(Il s'avance vers Cyrano qui l'observe, et se campant devant lui d'un
air fat):
Vous... vous avez un nez... heu... un nez... très grand.

CYRANO (gravement):
Très !

LE VICOMTE (riant):
Ha !

CYRANO (imperturbable):
C'est tout ?...

LE VICOMTE:
Mais...

CYRANO:
Ah ! non ! c'est un peu court, jeune homme !
On pouvait dire... Oh ! Dieu !... bien des choses en somme...
```

Le fait qu'un fichier soit au format texte ne signifie pas qu'il ne contienne que du texte au sens littéraire. Avec des caractères, on peut exprimer toutes sortes de choses.

Voici un autre exemple :

```
Date,Open,High,Low,Close,Volume,Adj Close
2018-06-01,569.16,590.00,548.50,584.00,14077000,581.50
2018-05-01,584.90,596.76,522.18,577.73,18827900,575.26
2018-04-02,601.83,644.00,555.00,583.98,28759100,581.48
```

Si vous utilisez régulièrement un tableur, vous aurez peut-être reconnu un fichier au format CSV (*Comma-Separated Values*). Ici, le format texte est utilisé pour exprimer des données tabulaires.

Voici un autre exemple, toujours extrait d'un fichier au format texte mais cette fois écrit dans un langage de balisage :

```
<figure>
  <img src="cyrano.jpg" alt="Portrait de Cyrano">
  <figcaption>
    Cyrano de Bergerac interprété par Gérard Depardieu.
  </figcaption>
</figure>
```

Ici, on a un extrait d'un fichier HTML. Les caractères alphabétiques côtoient d'autres caractères comme `<`, `=` et `/`. Si on ouvre ce fichier texte dans un navigateur, on verra une image (le fichier `cyrano.jpg`) avec une légende.

Un dernier exemple :

```
<svg style="display: none;">
	<symbol viewBox="-3 -3 30 30" id="soleil">
		<circle cx="12" cy="12" r="5"></circle>
		<line x1="12" y1="1" x2="12" y2="3"></line>
		<line x1="12" y1="21" x2="12" y2="23"></line>
		<line x1="4.22" y1="4.22" x2="5.64" y2="5.64"></line>
		<line x1="18.36" y1="18.36" x2="19.78" y2="19.78"></line>
		<line x1="1" y1="12" x2="3" y2="12"></line>
		<line x1="21" y1="12" x2="23" y2="12"></line>
		<line x1="4.22" y1="19.78" x2="5.64" y2="18.36"></line>
		<line x1="18.36" y1="5.64" x2="19.78" y2="4.22"></line>
	</symbol>
</svg>
```

Ici, on reconnaît le mot « soleil » mais le reste ressemble à de la géométrie (`line`, `circle`, coordonnées). Si on ouvre ce fichier dans un logiciel de dessin vectoriel, ou dans un navigateur, on verra l'image d'un soleil. Il s’agit du format SVG (*Scalable Vector Graphics*), qui sert à exprimer des images.

***

Ces quelques exemples montrent bien la nature du format texte. Ce n'est pas un format au sens du format PDF, ou du format DOCX, ou des noms de formats cités ci-dessus (CSV, HTML, SVG). Ces formats sont spécifiques, là où le format texte est générique : il est défini par le fait que les fichiers ne contiennent que des caractères textuels ; on peut l'utiliser pour créer des fichiers dans toutes sortes de formats spécifiques. Le format texte est donc un méta-format, une matérialité à lui tout seul, un mode d'existence de l'information, transversal à de nombreuses catégories d'objets et d'environnements logiciels.

De cette transversalité découle un avantage : les outils qui permettent de manipuler du format texte sont par définition capables de manipuler tout ce qui est exprimable avec le format texte – littérature, hypertexte, données chiffrées, images vectorielles et bien d'autres choses. C'est pourquoi je parle d'écosystème du format texte pour désigner l'ensemble des formats, outils et pratiques qui s'y rattachent.

# Caractères, codage

Un fichier au format texte ne contient que des caractères. J’entends par là les caractères d'imprimerie – chiffres, lettres, caractères d'espacement – mais aussi potentiellement des symboles, des emoji, etc. Ce qu'on inclut là-dedans dépend en fait du codage de caractères.

Un codage est une norme qui définit quels sont les caractères utilisables dans un fichier. Toutes les normes ne codent pas les mêmes caractères. Le codage [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange), par exemple, est une norme américano-centrée et assez ancienne qui ne code que 128 caractères, excluant non seulement les caractères accentués mais également tous les alphabets non latins (arabe, cyrillique, hébreu, idéogrammes…). À l'inverse, le codage UTF-8 est une norme internationale récente qui code tous les [caractères Unicode](https://unicode-table.com/fr/), incluant les caractères codés par l'ASCII et bien d'autres (alphabets non latins, symboles, emoji…).

Vous avez sûrement consulté un jour une page web à l'apparence un peu étrange, avec des mots comme « numÃ©rique » et « caractÃ¨res ». C'est un problème de codage : la page est codée suivant une certaine norme (par exemple UTF-8) mais le navigateur essaye de la décoder suivant une autre norme (par exemple Latin-1) qui n'inclut pas certains caractères ; ces derniers sont alors mal affichés.

L’ASCII, avec ses 128 caractères, est le plus petit dénominateur commun des codages pour le format texte. Il est toujours utilisé, ce qui garantit une compatibilité *a minima* des systèmes actuels avec les documents et logiciels plus anciens. L’UTF-8, avec ses millions de caractères, est à l’inverse le plus grand dénominateur commun. Avec la globalisation des échanges, il tend à remplacer l’ASCII comme norme « universelle ». Les 128 premiers caractères de l’UTF-8 sont les mêmes que ceux de l’ASCII et sont codés de la même manière : tout texte en ASCII peut donc aussi être considéré comme un texte en UTF-8.

Sauf contre-indication spécifique, c’est le codage UTF-8 que je recommande d’utiliser dans la plupart des situations.

# Format, langage, extension

On mélange parfois le mot « format » et le mot « langage ». Par exemple, on dit qu’un fichier `truc.xml` est au format XML mais on parle du langage XML. Quelle est la différence ? Et quel est le lien avec l’extension de fichier `.xml` ?

## Format

Un format est une série de conventions, de règles formelles, qui définissent comment structurer, comment donner forme à quelque chose.

La notion de format est antérieure à l’informatique. Le format A4, par exemple, est un format pour les feuilles de papier. Il définit un rectangle de 21 par 29,7 cm, soit un ratio de 1√2 entre la longueur des côtés. Si on a une grande feuille de papier et qu’on veut y tailler une feuille au format A4, on prend une règle, un crayon et des ciseaux, on mesure, on trace et on découpe.

Autre exemple : la méthode Cornell, un format de prise de notes sur papier. Cette méthode consiste à définir une organisation spatiale de la feuille pour structurer la prise de notes, avec des zones dans lesquelles on doit écrire différentes choses (métadonnées, notes prises au fil de l’eau, synthèse). Si on veut prendre des notes en suivant la méthode Cornell, il faut prendre une feuille et réserver un cadre en haut, un cadre en bas et une marge à gauche. Comme avec le format A4, on peut s’aider d’une règle et d’un crayon. En revanche, les règles formelles sont moins strictes que pour le format A4 : les zones doivent respecter des proportions relatives plutôt que des dimensions exactes.

## Langage

Un langage informatique, c’est un format mis en œuvre par l’écriture, c’est-à-dire que les règles formelles se traduisent par des règles d’écriture. Cette notion de langage repose sur un parallèle avec les langues naturelles. Un langage informatique a lui aussi une grammaire : ce sont ses règles de syntaxe. Et il a également un vocabulaire : ce sont les termes et caractères ayant une signification spéciale dans le contexte de son utilisation. La règle, le crayon et les ciseaux de mes exemples précédents sont remplacés par des instructions écrites, formulées suivant une syntaxe précise.

Prenons un exemple. Voici un peu de texte :

```
Un format est une série de conventions, de règles formelles, qui définissent comment structurer, comment donner forme à quelque chose.
```

Transformons-le en HTML. Pour cela, on utilise la syntaxe de HTML, c’est-à-dire une syntaxe qui combine des caractères (comme `<`, `>` et `/`) et des termes (`p` pour paragraphe, `strong` pour une forte emphase) :

```html
<p><strong>Un format</strong> est une série de conventions, de règles formelles, qui définissent comment structurer, comment donner forme à quelque chose.</p>
```

Pour décrire ce que je viens de faire, je peux dire que j’ai *mis mon texte au format HTML*, et que j’ai *écrit en langage HTML*. Les deux expressions sont vraies. Dans les deux cas, HTML reste HTML. Simplement, les mots reflètent les deux dimensions de ce qu’est un langage : un format, mis en œuvre par l’écriture.

Tout format informatique n’est pas nécessairement associé à un langage. Par exemple, je peux dire qu’un fichier `truc.txt` est au format TXT ; en revanche il n’existe pas de langage TXT.

## Extension de fichier

L’extension de fichier est la partie du nom du fichier située après le point. Par exemple, dans `truc.txt`, l’extension est `txt`.

Une extension de fichier est purement déclarative : la changer ne change pas le format du fichier. Seul le contenu du fichier détermine son format. L’extension sert uniquement à suggérer à la machine un programme pour ouvrir le fichier, si l’extension est connue et qu’un programme par défaut lui est associé.

Une extension peut être n’importe quelle chaîne de caractères. Ainsi `truc.txt` pourrait s’appeler `truc.text` ou `truc.chose`. Une machine ne peut donc pas connaître toutes les extensions possibles. C’est pourquoi l’extension sert aussi à suggérer la nature du fichier aux humains.

L’extension `.txt` a une signification particulière : on l’utilise souvent pour indiquer que le fichier est au format texte, sans la présence d’un langage particulier. La plupart des machines reconnaissent cette extension et ouvrent les fichiers correspondants avec un type de logiciel bien particulier : un éditeur de texte.

# Éditeur de texte

De la même manière qu'on ouvre une image dans un logiciel fait pour afficher des médias audiovisuels, un PDF dans un lecteur de PDF, ou une page web dans un navigateur, le format texte a son type d'environnement dédié : les éditeurs de texte.

Il ne faut pas confondre éditeurs de texte et traitements de texte. Les éditeurs de texte (*text editors*) sont capables d'afficher le contenu de fichiers au format texte uniquement. C'est une catégorie large, qui inclut des utilitaires, des logiciels pour développeurs, des environnements conçus pour l'écriture, etc. Les traitements de texte (*word processors*), eux, appartiennent plutôt à la bureautique. Ils sont capables d'afficher des fichiers au format texte mais sont principalement utilisés pour ouvrir des formats comme DOCX ou ODT.

<!-- Extensions de fichiers :

Unlike Word documents, which mostly come as .docx files, plain text files can have thousands of different extensions. In all cases, the file can be read by a simple text editor. What the extension does is tell you about the purpose of the file. Beyond the .txt file, there’s a huge range of plain text extensions. Most of them relate to computer programming. There’s the .py extension for python files, the .R extension for R files, the .cpp extension for C++ files, and so on. The extension doesn’t change the fact that the file is in plain text. Rather, it tells the computer what software to use when interpreting the file.

Source : https://economicsfromthetopdown.com/2020/12/10/why-and-how-i-write-scientific-documents-in-plain-text/

Via [Plain Text Project](https://plaintextproject.online/articles/2022/03/29/links.html)

 -->

# Arguments en faveur du format texte

La page que vous consultez actuellement existe parce que le format texte souffre d’un déficit de notoriété. Or cette notoriété est inverse à son utilité, et notamment à son potentiel éducatif et émancipateur. Mais alos, s’il est si utile, pourquoi est-ce qu’il est si peu connu ? Eh bien le problème, c’est qu’il est difficile de faire de l’argent avec. Le format texte fait partie de ces choses en informatique qui sont à la fois simples, efficaces et peu coûteuses. Or c’est précisément parce que ça ne coûte rien de l’utiliser que peu de gens sont susceptibles d’investir du temps et des moyens pour le « vendre ». D’où l’importance pour moi de le faire connaître dans le cadre de mes recherches.

Voici mes principaux arguments en faveur du format texte, synthétisés d'après quelques sources détaillées à la fin de cette page [@descy2021; @sivers2022; @stoehr2022].

## Simplicité, légèreté

Le format texte peut être caractérisé objectivement comme simple (complexité faible) et léger (volume réduit). De ces deux caractéristiques découlent déjà quelques avantages.

Parce qu’il est léger, il est performant : toutes les actions sont plus rapides (ouverture, édition, sauvegarde…). Il est également portable, car rapide à copier, économe en espace de stockage, et utilisable tel quel sur n’importe quel système d'exploitation.

Parce qu’il est simple, il est facile à prendre en main : à format simple, interactions simples. Mais simple ne veut pas dire limité : le format texte est très expressif. Pour la machine, cette simplicité fait qu’il est également facile à manipuler. En particulier, il est facile à sauvegarder et à synchroniser entre différents appareils ; il est notamment parfait pour la gestion des versions.

## Logiciel libre

Le format texte n'est pas propriétaire : on peut librement l'utiliser, le diffuser, connaître son fonctionnement et le modifier (les 4 libertés fondamentales du logiciel libre). Les codages de caractères les plus utilisés, comme ASCII et UTF-8, sont des normes internationales adoptées par l’ISO et l’IETF, utilisables librement. Le format texte relève donc du logiciel libre et ceci a plusieurs conséquences qui avantagent les utilisateurs.

D'abord, cela favorise la libre concurrence dans l’offre logicielle. Il en résulte une grande diversité d’outils, ce qui permet une plus grande liberté de choix.

Ensuite, cela accroît la durée de vie des données. Il y aura toujours un outil libre et gratuit pour remplacer un outil propriétaire. Ceci diminue les risques d'être enfermé hors de ses données parce que le seul outil qui permettait de les interpréter devient payant ou bien disparaît.

## Stabilité, fiabilité

L’ASCII existe depuis les années 1960. L’UTF-8 depuis les années 1990. Toutes choses égales par ailleurs, le format texte est une norme plus stable, donc plus fiable que les alternatives propriétaires plus récentes. Un fichier texte créé dans les années 1970 est consultable aujourd’hui dans les mêmes conditions qu’à l’époque ; si tout va bien, le fichier que j’écris actuellement le sera tout autant dans les années 2070.

## Polyvalence

Le format texte est incroyablement flexible dans les usages qu'il permet. J’ai déjà mentionné quelques exemples en introduction mais il y en a beaucoup d’autres. C’est un peu comme XML, avec tous ses langages dérivés : on a une grande expressivité à partir d'une technique générique. Et le format texte facilite les conversions entre formats spécifiques.

## Stoïcisme

Faisons un pas de côté avec Derek Sivers, qui propose de considérer le format texte dans une perspective stoïciste. Selon lui, il est souhaitable d'accroître notre autonomie vis-à-vis de la technique, en favorisant notamment les formats plutôt que les fonctionnalités, et en disposant d’outils qui permettent de travailler hors ligne.

Selon Sivers, nous sommes volontiers dépendants de logiciels particuliers, notamment parce qu’ils proposent des fonctionnalités spécifiques que nous jugeons indispensables. En réalité, de nombreux besoins peuvent être couverts par des techniques simples mais expressives, donc polyvalentes. Il est tout à fait viable d'utiliser des techniques génériques plutôt que des outils précis : ainsi le format texte vient remplacer Word, Evernote ou Notion.

Autre exemple : de plus en plus d’outils sont proposés sous forme de logiciels-services (serviciels ; *software-as-a-service*, SAAS), utilisables uniquement en ligne. Mais Sivers rappelle qu'on ne dispose pas toujours d’une connexion internet, et surtout que la déconnexion peut s’avérer souhaitable dans certains contextes, car elle favorise la concentration. Il est donc utile de disposer d’outils compatibles avec la déconnexion. Là aussi, le format texte s’inscrit dans cette logique, du fait de sa simplicité.

## Textualité

L’ultime qualité que je souhaite mentionner par rapport au format texte prend la forme d’un truisme : il est utile à toute activité basée sur le texte. Il est ainsi tout indiqué dès qu’il faut prendre des notes, commenter, documenter, transcrire… Et il facilite également des opérations comme la recherche basée sur le texte.

Cela n’a rien d’anodin dans la mesure où le texte est omniprésent dans nos activités. Essayez de faire une liste de toutes les actions que vous faites dans une journée et qui impliquent de lire ou d’écrire du texte : la liste a toutes les chances d’être longue.

Et alors, considérez la question suivante : pour tous ces gestes qui passe par le texte, est-ce qu’il vous serait utile de connaître une technique universelle simple, légère, performante, portable, gratuite, pérenne, pouvant rendre toutes sortes de services ? Une sorte de *lingua franca*, de plus petit dénominateur commun de la textualité version numérique ? Songez à la versatilité du couple papier-crayon pour toutes les tâches d’écriture ; transposez-la à l’informatique : vous obtenez le format texte.

# Liens utiles

- Ma page [Écriture académique au format texte](ecriture-academique-format-texte.html)
- Page [Liens](https://plaintextproject.online/links.html) du Plain Text Project

# Bibliographie

