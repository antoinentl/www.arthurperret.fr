---
title: Écriture académique au format texte
author: Arthur Perret (Université Bordeaux Montaigne)
abstract: "Cette page donne des arguments en faveur de l’utilisation du format texte pour rédiger des documents scientifiques."
wip: true
updated: 17/05/2022
---

# Introduction

## Inconvénients des logiciels de traitement de texte

En sciences humaines et sociales, les logiciels de traitement de texte, comme LibreOffice Writer, Microsoft Word et Google Docs, sont utilisés par la majorité des auteurs et des éditeurs. Ce sont des outils polyvalents, puissants, capables de gérer toutes les étapes de la chaîne éditoriale. Mais leurs avantages sont contrebalancés par une série d’inconvénients plus ou moins problématiques.

D’abord, et du fait de leur polyvalence, ces logiciels tendent souvent vers l’usine à gaz. Leur interface est notoirement encombrée, pleine de distractions. Et ils sont gourmands en ressources (énergie, mémoire) ; or malgré cela, ils gèrent mal les documents longs et complexes.

Ensuite, ce sont des logiciels ancrés dans le paradigme de l’imprimé, qui sont donc fondamentalement inadaptés à la communication via le Web. Dans un monde où le papier et le numérique sont appelés à cohabiter durablement, c’est une lacune considérable.

Autre problème : leur modèle économique (en tout cas pour les logiciels propriétaires) est de moins en moins avantageux pour le consommateur. La tendance est à l’abonnement : payer pour accéder à ses propres données, sans aucune garantie sur la longévité du service.

Enfin, leurs formats sont soit complètement fermés (`.doc`), soit difficiles à utiliser via d’autres outils (`.docx`). Les logicels de traitement de texte verrouillent les auteurs dans un écosystème propriétaire dans lesquels les choix sont réduits. Certes, LibreOffice existe ; mais la compatibilité entre formats a des limites, en partie entretenues par les éditeurs commerciaux qui n’en tirent aucun profit.

## Le format texte comme alternative

Le format texte constitue une alternative au traitement de texte, sans pour autant le remplacer totalement. Il constitue un compromis différent, avec ses propres avantages et inconvénients.

Un fichier au format texte est un fichier ne contenant que des caractères. Il se manipule avec des éditeurs de texte. Un fichier au format texte est toujours exprimé dans un certain codage, qui définit les caractères utilisables dans le fichier. Et il peut également être rédigé dans un langage particulier, c’est-à-dire en respectant des règles d’écriture qui se traduisent par des fonctionnalités lorsqu’on ouvre le fichier dans un logiciel approprié. En fonction du langage utilisé, on peut exprimer différentes choses : pas seulement du texte au sens littéraire, mais aussi toutes sortes de données et de médias.

Le format texte a des avantages intrinsèques. Sa simplicité et sa légèreté se traduisent par des gains de performance et de portabilité. Il appartient au logiciel libre, ce qui se traduit par une grande diversité d’outils, donc une plus grande liberté de choix, et par une plus grande pérennité des données. Enfin, c’est une technique expressive qui sert de base à de nombreux formats et donc de nombreux usages, tout en garantissant une certaine interopérabilité et en facilitant les conversions.

Pour plus de détails, voir ma page [Format texte](format-texte.html).

Le format texte a également des avantages plus spécifiques dans la perspective de l’écriture académique.

Je l’ai dit, il existe une plus grande diversité dans le choix d'interfaces de travail parmi les outils conçus pour le format texte. On trouve ainsi des interfaces bien mieux pensées pour l’écriture que celles des logiciels de traitement de texte, avec notamment des fonctionnalités qui favorisent la concentration et d’autres qui facilitent la manipulation de la structure des documents.

Le format texte donne de plus grandes capacités d’automatisation. Il facilite notamment la gestion des figures, beaucoup moins laborieuse que dans un traitement de texte. Mais surtout, il rend l’automatisation reproductible par simple copier-coller.

Enfin, le format texte encaisse parfaitement les longs documents ; certains langages comme LaTeX sont même pensés pour ça.

# Langages

Le format texte est une technique générique, qui se décline en différents langages. Chaque langage a ses propres fonctionnalités, qui se traduisent par des règles d’écriture, et qui lui donnent une utilité propre.

Voici quelques langages largement répandus dans le monde académique et qui permettent de créer des documents :

HTML
: HTML (*HyperText Markup Language*) est le langage de balisage conçu pour représenter les pages web. Il permet d’écrire de l’hypertexte, de structurer sémantiquement la page, d’inclure des ressources multimédias et de créer des documents interopérables et accessibles. Tout contenu scientifique publié sur le Web est en HTML.

XML
: XML (*eXtensible Markup Language*) est un méta-langage qui permet de créer des langages de balisage ayant des vocabulaires différents mais une grammaire commune. Parmi les langages basés sur XML et utilisés dans le monde académique, on trouve par exemple TEI (*Text Encoding Initiative*, pour les manuscrits), JATS (*Journal Article Tag Suite*, pour les périodiques) et DocBook (pour la documentation technique). Certains de ces langages servent d’intermédiaire pour exprimer le document dans un autre langage (comme HTML) ou un autre format (comme PDF).

LaTeX
: LaTeX est un langage permettant d’utiliser le système de composition TeX afin de créer des PDF de qualité professionnelle. TeX a été inventé par un mathématicien qui désirait retrouver la qualité typographique des ouvrages de mathématiques imprimés à l’époque de la composition au plomb ; LaTeX est un ensemble de macros (formes condensées) qui allègent l’écriture des commandes TeX et les rendent ainsi plus accessibles aux auteurs. LaTeX est principalement utilisé en mathématiques et en sciences de la nature.

Markdown
: Markdown est un langage de balisage dit « léger », c’est-à-dire dont la syntaxe est plus économe en caractères, donc potentiellement plus facile à écrire et à déchiffrer, que des langages comme HTML, XML ou encore LaTeX. Markdown a été conçu pour faciliter l’écriture de contenus destinés au Web et a depuis été adapté à des processus d’écriture académique en sciences, à travers différentes variantes comme R Markdown et Pandoc Markdown.

Il existe d’autres types de langages, qui servent à exprimer des données au format texte. Or celles-ci sont susceptibles d’intervenir dans la création de documents. Voici donc également une liste de quelques noms qui donnent une idée des usages possibles :

- données bibliographiques : BibTeX, CSL JSON ;
- données tabulaires : CSV ;
- images vectorielles : SVG.

# Outils

## Markdown et Pandoc

Pandoc est un programme de conversion entre formats de balisage. Il reconnaît et sait transformer des formats comme HTML, XML et LaTeX mais aussi les formats pour traitement de texte, présentations, bibliographies, livre numérique, wikis et bien d’autres.

Parmi les formats reconnus par Pandoc, Markdown occupe une place particulière. Pandoc permet d'utiliser une variante de Markdown qui lui est propre, et qui ajoute des fonctionnalités liées à l'écriture académique : tableaux, références bibliographiques, listes de définitions, formules mathématiques, notes de bas de page. Il est alors possible (et encouragé) d'utiliser Markdown comme « format d'entrée universel » pour Pandoc : cela consiste à écrire au format texte avec une syntaxe simple et de générer à la volée des documents dans tous les formats que peut fabriquer Pandoc.

Concrètement, écrire en Markdown via Pandoc donne accès à l'essentiel des possibilités de LaTeX et HTML (entre autres), sous une forme beaucoup plus simple, et en conservant un seul fichier source dans un format pérenne.

Ceci est particulièrement intéressant dans le cas de LaTeX, dont les bénéfices sont très intéressants mais qui est lourd à écrire et difficile à maîtriser. Pandoc permet de créer des PDF via LaTeX sans écrire de LaTeX, ce qui permet – passez-moi l’expression – d’avoir le beurre et l’argent du beurre.

Comme l’écrit Blair Fix [-@fix2020], Markdown a transformé le paysage de l’écriture au format texte :

::: colonnes
> « Markdown a transformé le paysage de l'écriture au format texte. Il permet d'utiliser une syntaxe simple pour rédiger des documents complexes. Et contrairement aux outils plus anciens comme LaTeX, un document Markdown peut être transformé facilement dans n’importe quel format. C'est le rêve de tout auteur. »

> *“Markdown has transformed the landscape of plain-text writing. It allows you to use simple syntax to write complex documents. And unlike older tools like LaTex, your Markdown document can be rendered seamlessly in any format you want. It’s a writer’s dream.”*
:::

## Éditeurs de texte

Les principales fonctionnalités qu'on attend d'un bon logiciel d'écriture académique sont :

- structurer de longs documents et faciliter la navigation ;
- insérer des citations et générer des bibliographies ;
- exporter les fichiers dans différents formats.

[Zettlr](https://www.zettlr.com/) est un exemple d'éditeur de texte Markdown avec des fonctionnalités académiques, basé sur Pandoc, qui répond à ces critères.

# Liens utiles

Mes autres pages :

- [Format texte](format-texte.html)
- [Markdown](markdown.html)
- [Pandoc](pandoc.html)

# Bibliographie

