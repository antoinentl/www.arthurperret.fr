---
title: Pandoc
author: Arthur Perret (Université Bordeaux Montaigne)
abstract: "Cette page constitue une introduction à l'utilisation de Pandoc, le convertisseur couteau-suisse du format texte, ainsi que sa variante de Markdown pensée pour l'écriture académique."
wip: true
updated: 30/03/2022
---

# Qu'est-ce que Pandoc ?

Pandoc est un programme de conversion entre formats de balisage. Il reconnaît et sait transformer des formats comme `html`, `xml`, `latex` mais aussi les formats pour traitement de texte (`docx`, `odt`), présentations (`pptx`, `html`), bibliographies (`bibtex`, `csl json`), livre numérique (`epub`), wikis…

# Utilisation

Pandoc est un programme qui s’utilise en ligne de commande : il s’exécute en saissant des instructions textuelles dans un terminal. La syntaxe est la suivante :

```
pandoc fichier-d’entrée options
```

Les options sont précédées de deux tirets : `--option`. Les plus utilisées peuvent également être rédigée sous une forme raccourcie, avec un seul tiret et une seule lettre. Exemple : `-o` au lieu de `--output`.

::: note
Lorsque je mentionne les options qui possèdent une forme raccourcie, je reprends la convention d’écriture du manuel de Pandoc qui consiste à présenter les deux formes séparées d’une barre oblique. Exemple : `-o/--output`. Cela ne veut pas dire qu’il faut taper `-o/--output` mais bien `-o` ou `--output`, au choix.
:::

Exemple de conversion d’un fichier `mon-fichier.txt` en `mon-fichier.html` (on précise le nom du fichier de sortie avec l’option `-o/--output`) :

```
pandoc mon-fichier.txt -o mon-fichier.html
```

::: note
Dans le manuel de Pandoc, le nom du fichier d’entrée est toujours mis en dernier dans la commande, après les options :

```
pandoc options fichier-d’entrée
pandoc -o mon-fichier.html mon-fichier.txt
```

Mais en réalité, Pandoc permet de mettre le nom du fichier d’entrée en n’importe quelle position. Or je trouve plus intuitif de mettre le nom du fichier en premier, comme si on disait « Pandoc, convertis mon fichier en appliquant les options suivantes ». Sur cette page, j’ai donc choisi de toujours mettre le nom du fichier en premier.
:::

Pandoc peut déduire le format d’un fichier à partir de son extension. On peut également expliciter le format d’entrée avec `-f/--from` et le format de sortie avec `-t/--to`.

Pandoc utilise le codage UTF-8 en entrée et en sortie.

Avec l’accumulation des options, les commandes Pandoc deviennent vite très longues. Pour faciliter leur lecture et la modification des options, il peut être utile de séparer les options par des retours à la ligne, à condition de faire précéder ces derniers d’une barre oblique inverse `\`. Ceci permet de noter la commande dans un fichier pour ne pas l’oublier, et le moment venu la copier-coller telle quelle dans un terminal.

Exemple :

```
pandoc mon-fichier.txt \
-o mon-fichier.html \
--standalone \
--css=styles.css \
--citeproc \
--bibliography=references.bib \
--csl=apa-fr.csl
```

::: note
J’utilise cette façon d’écrire dans la plupart de mes exemples pour des raisons de lisibilité, notamment sur mobile, afin que la commande soit visible entièrement sans avoir à faire défiler horizontalement le code. Si vous voulez reproduire un de ces exemples mais avec une commande en une seule ligne, retirez simplement toutes les barres obliques inverses `\` et tous les retours à la ligne.
:::

# Pandoc Markdown

Parmi les formats reconnus par Pandoc, il y a aussi les formats de balisage dit légers. Ce sont des syntaxes simplifiées, allégées, pensées pour pouvoir générer facilement des formats lourds à écrire. Exemple : écrire en AsciiDoc au lieu de DocBook XML, ou bien en Markdown au lieu de HTML.

Markdown est particulièrement populaire. C'est un format de balisage léger pensé pour faciliter la rédaction de contenus dans le cadre du Web 2.0 (blogs, forums, commentaires). Il est très basique, et il existe de nombreuses variantes qui lui rajoutent des fonctionnalités. Je vous renvoie vers [mon cours sur Markdown](markdown.html) pour une introduction rapide et un tutoriel.

Markdown sert la plupart du temps à fabriquer du HTML, mais Pandoc permet d'utiliser une variante de Markdown qui lui est propre, et qui ajoute des fonctionnalités liées à l'écriture académique : tableaux, références bibliographiques, listes de définitions, formules mathématiques, notes de bas de page. Il est alors tout à fait possible d'utiliser Markdown comme « format d'entrée » universel pour Pandoc : ceci permet d'écrire dans un format texte simple, léger et pérenne, et de générer à la volée des documents dans tous les formats que peut fabriquer Pandoc.

Je donne un exemple de la syntaxe de Pandoc pour les citations [dans mon cours sur la bibliographie](https://www.arthurperret.fr/cours/bibliographie.html#rédaction).

# Modèles

Lorsque l'option `-s/--standalone` est utilisée, Pandoc utilise un modèle pour ajouter les éléments d'en-tête et de pied de page nécessaires à un document autonome. Les modèles permettent de définir une architecture, une mise en page, et de l’appliquer à plusieurs documents. C’est un outil puissant d’automatisation.

Les modèles contiennent des variables, qui permettent d'inclure des informations arbitraires à n'importe quel endroit du fichier. Les variables peuvent être définies dans la commande Pandoc en utilisant l'option `-V/--variable` ou dans les métadonnées du document (qui peuvent être définies en utilisant les blocs de métadonnées YAML ou avec l'option `-M/--metadata`). Pandoc donne à certaines variables des valeurs par défaut.

Les modèles par défaut de Pandoc sont hébergés dans le dépôt [pandoc-templates](https://github.com/jgm/pandoc-templates) sur GitHub.

Un modèle personnalisé peut être spécifié en utilisant l'option `--template`.

## Exemple

Voici un exemple de document `mon-document.md` rédigé en Markdown, avec un en-tête en YAML contenant des métadonnées au début, et le corps du texte ensuite :

```
---
title: Mon document
date: 2022-03-30
---

Je rédige un super document pour expliquer ce qu’est
[Pandoc](https://fr.wikipedia.org/wiki/Pandoc).
```

Voici un exemple de template Pandoc pour le format HTML `mon-modele.html5` qui définit l’emplacement de différentes variables notées `$variable$` (on peut également écrire `${variable}`) :

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>$pagetitle$</title>
    <meta name="dcterms.date" content="$date$" />
  </head>
  <body>
  	$body$
  </body>
</html>
```

Si on exécute la commande suivante (on ne précise pas de chemin pour le modèle, cela implique qu’il est situé dans le même répertoire que le fichier d’entrée) :

```
pandoc mon-document.md \
--standalone \
--template=mon-modele.html5 \
-o mon-document.html
```

On obtient le fichier suivant :

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Mon document</title>
    <meta name="dcterms.date" content="2022-03-30" />
  </head>
  <body>
  	<p>Je rédige un super document pour expliquer ce qu’est
  	<a href="https://fr.wikipedia.org/wiki/Pandoc">Pandoc</a>.</p>
  </body>
</html>
```

Le même modèle peut être réutilisé avec un autre document. Le contenu des variables changera alors en fonction de leur valeur dans le nouveau document.

## Règles conditionnelles

Dans un modèle, on peut conditionner l’inclusion d’un certain passage à la présence d’une variable.

Reprenons l’exemple donné précédemment. Il contient la ligne suivante :

```html
<meta name="dcterms.date" content="$date$" />
```

Tous nos documents ne sont peut-être pas datés. Modifions cette ligne pour qu’elle ne soit incluse en sortie que si la date est présente dans le fichier d’entrée :

```html
$if(date)$<meta name="dcterms.date" content="$date$" />$endif$
```

***

Pour aller plus loin → [Conditionals – Pandoc User’s Guide](https://pandoc.org/MANUAL.html#conditionals)

# Liens utiles

[**Manuel utilisateur de Pandoc** (Pandoc User’s Guide)](https://Pandoc.org/MANUAL.html) (John MacFarlane et les contributeurs de Pandoc, licence GPL version 2 ou ultérieure).

Pandoc fait l’objet d’une documentation remarquable. Certains éléments sur cette page sont traduits directement du manuel. La plupart des réponses s’y trouvent. En cas de bug, [les tickets sur le dépôt GitHub de Pandoc](https://github.com/jgm/pandoc/issues/) sont également une source fiable pour trouver des solutions (qu’ils soient ouverts ou résolus). Le seul obstacle est celui de la langue : la documentation n’est disponible qu’en anglais. Mais sa traduction nécessiterait probablement un effort monumental.

Cours en français :

- [Élaboration et conversion de documents avec Markdown et Pandoc](https://www.jdbonjour.ch/cours/markdown-Pandoc/) – Jean-Daniel Bonjour (EPFL)
- [Markdown et vous](https://infolit.be/md/) – Bernard Pochet (Université de Liège)
- [Rédaction durable avec Pandoc et Markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-Pandoc-et-markdown) – Dennis Tenen et Grant Wythoff (Université de Columbia)

Le mot de la fin :

> « Pandoc is, without any doubt and exaggeration, one of the best tools I’ve ever used. It does exactly what it promises to, its documentation is stellar, it’s actively and carefully maintained and never once let me down. If I would have to shorten this post to one word, it would be “Pandoc”. » (Thorsten Ball, [The Tools I Use To Write Books](https://thorstenball.com/blog/2018/09/04/the-tools-i-use-to-write-books/))
