---
title: Recherche d’information et veille
author: Arthur Perret (Université Bordeaux Montaigne)
date: 2021-2022
abstract: "Ce cours porte sur les notions de recherche d'information et de veille informationnelle, ainsi que sur les méthodes pour mettre en place une stratégie de recherche et de collecte d'information en adéquation avec les besoins informationnels d'une organisation. Il s’agit d’une initiation aux questions suivantes : mettre en place un questionnement préalable à une recherche d'information ; prendre en main les outils de recherche les plus courants ; repérer les sources pertinentes en fonction des objectifs informationnels, en définir la qualité et analyser les résultats obtenus ; restituer ces résultats de manière éclairée dans un format adapté."
updated: 16/05/2022
---

# Généralités

## Définitions

Recherche d’information
: Ensemble d’opérations qui permettent de rassembler des données ou des informations structurées, pertinentes et sûres, et de les livrer à une échéance convenue.

Veille
: Processus itératif, manuel ou automatisé, qui permet d’échelonner la recherche d’information dans le temps ou de la poursuivre au-delà de la livraison d’un dossier de recherches en vue de son actualisation permanente.

## Méthodologie

La recherche d'information et la veille font appel à des **savoir-faire** communs :

- questionner besoins et attentes ;
- connaître ou identifier les sources de référence du domaine de recherche grâce à des outils de recherche et de veille ;
- traiter les informations (valider, sélectionner, résumer, synthétiser, analyser) ;
- livrer des résultats de qualité et réutilisables.

Elles sollicitent également une **éthique professionnelle** commune :

- pluralité des sources ;
- citation explicite des auteurs et des contextes d’énonciation ;
- respect de l’intégrité du contenu ;
- respect de la propriété intellectuelle en vigueur et de la licéité des informations.

Elles obéissent enfin à une **méthodologie** commune :

- analyser le besoin d'information ;
- définir une stratégie de recherche ;
- mener la recherche ;
- livrer les résultats.

Des différences existent toutefois entre recherche d'information et veille en ce qui concerne les outils utilisés et la forme des livrables.

# Analyser le besoin d’information

Pour envisager une stratégie de recherche, il faut analyser le besoin d'information, c'est-à-dire ce qui pousse un individu ou une organisation à s'engager dans une activité de recherche d'information. Cela peut être la sensation d'une lacune dans ses connaissances, ou bien d'une incertitude ; le ressort de cette sensation peut être d'ordre conceptuel (on souhaite comprendre) ou décisionnel (on doit décider). Le besoin d'information se précise et se concrétise dans l'interaction itérative avec un système d'information.

Analyser un besoin d'information revient à identifier clairement :

- le commanditaire ;
- l’objectif poursuivi ;
- les pré-requis de connaissance du destinataire ;
- le type de documentation recherchée : donnée, information, analyse, rapport, étude… ;
- le niveau d’expertise ;
- la langue.

# Définir et mener une stratégie de recherche

## Sources

On appelle source l'origine d'une information.

Une stratégie de recherche commence souvent par l'identification des sources de référence dans le domaine concerné. Il s'agit de déterminer si la question a déjà été clarifiée et contextualisée. Ces sources de référence renvoient elles-mêmes vers d'autres sources, qu'il s'agit de consulter à leur tour, et ainsi de suite.

### Types de sources

Source-auteur, source-émetteur
: Personne physique ou personne morale.

Source-document
: Support d'information.

Ressource
: Production documentaire qui regroupe diverses sources-documents. Ex : site, base de données. Les bases de données documentaires (BDD) et autres catalogues sont des aides à la recherche, des documents et des sources déjà sélectionnées et validées par des professionnels.

### Degré des sources

Source primaire
: Correspond à la première publication relatant, traitant ou produisant une information originale et jusque-là inédite.

Source secondaire
: Utilise des sources primaires pour construire une synthèse ou une analyse. Elle ne comporte pas d’information inédite.

Source tertiaire
: Présente des informations issues de sources secondaires. Les encyclopédies comme Wikipédia sont considérées comme des sources tertiaires.

### Types d'émetteurs, types d'information

Chaque type de source-émetteur est associé à un type d'information.

Sources officielles
: Les institutions publiques sont une source de l'information légale. En France : [DILA](https://www.dila.premier-ministre.gouv.fr) (Légifrance, Service public, Vie publique), Assemblée nationale, ministères, collectivités locales…

Sources académiques
: Les chercheurs sont une source d'information scientifique. Ressources : plateformes d'édition (ex : OpenEdition Journals, Cairn, Persée), catalogues de bibliothèques universitaires (ex : Babord+), moteurs de recherche spécialisés (ex : Worldcat, Sudoc, Isidore, BASE, Google Scholar, etc.), archives (ex : HAL, TEL).

Presse, audiovisuel
: Les titres de presse et les médias audiovisuels sont une source d'information journalistique. Attention toutefois à ne pas confondre les faits (censés être rapportés le plus fidèlement possible) des analyses (qui peuvent être débattues) et des chroniques ou éditoriaux (qui expriment une opinion plus ou moins éclairée).

Acteurs
: Les entreprises et associations sont une source d'information diverse. Cette dernière peut être généraliste ou spécialisée, blanche (publiée) ou grise (non publiée), objective ou partisane.

### Validation des sources et des informations

Exactitude
: Il faut savoir repérer des sources fiables : qui est l’auteur ? Quelle est son expertise, sa notoriété auprès de ses pairs ? Quels sont ses buts et ceux de son canal de diffusion ? A-t-il des partis pris (politiques, économiques) ou parle-t-il en toute objectivité ? Il faut également savoir vérifier l’information : est-elle déjà “contrôlée” ? Faut-il la croiser ? Est-elle à jour (un fait “établi” peut être rectifié par de nouvelles découvertes) ? La citation est-elle fidèle à l’original ? 

Traçabilité 
: Non sourcée, une information n’est pas utilisable.

Licéité
: L’information doit être obtenue dans le respect des contraintes légales : respect du secret (médical, industriel), respect de la propriété intellectuelle. Exemple : France Archives propose [un service en ligne](https://francearchives.fr/@docs/) permettant de connaîtr la licéité de la communication des différents documents administratifs.

## Outils de recherche

Pour trouver des sources et *in fine* des informations pertinentes, différents outils peuvent être utilisés :

- outils généralistes (dictionnaires ; encyclopédies ; moteurs de recherche grand public comme [Google](https://www.google.fr/advanced_search) ou [Qwant](https://www.qwant.com/)) ;
- outils spécialisés (BDD, catalogues, moteurs de recherche, portails divers) ;
- outils de veille (suivi, collecte) ;
- utilitaires (logiciels, programmes, applications).

### Outils généralistes

Les dictionnaires et encyclopédies sont les premières sources à consulter lors de la définition d'un sujet de recherche. Les dictionnaires en ligne permettent parfois de réaliser des recherches rapidement et facilement via l'URL. Exemple :

`http://www.cnrtl.fr/definition/mot` (remplacer « mot » par le mot cherché)

Ensuite, les moteurs de recherche connus du grand public indexent les ressources disponibles sur le Web. Ils offrent souvent deux modes de recherche :

Recherche simple
: Mode de recherche qui n'utilise qu'un seul champ de saisie. En fonction du moteur, il est possible d'utiliser des symboles et des mots spéciaux pour affiner la recherche. Voir par exemple [cette page de l'aide Google](https://support.google.com/websearch/answer/2466433?hl=fr&ref_topic=3081620). Parmi les outils les plus répandus on trouve l'expression exacte `""`, l'exclusion `-` ou encore les opérateurs booléens comme `OR` et `AND`.

Recherche avancée
: Mode de recherche qui utilise plusieurs champs, définis en fonction des métadonnées connues par l'outil. Exemple : [recherche avancée Google Scholar](https://scholar.google.com/#d=gs_asd).

Dans chacun de ces modes, il est souvent possible d’utiliser des techniques de recherche dite experte : opérateurs logiques (booléens), recherche de toutes les formes d'un radical (via l'utilisation du *wildcard* `*`), filtres restreignant les résultats à un type de contenu (ex : `filetype:pdf` sur Google), etc.

Enfin, des utilitaires comme [Liquid](http://www.liquid.info/liquid.html) (macOS) permettent de centraliser plusieurs outils de recherche dans une barre de recherche unique pour gagner du temps.

### Outils spécialisés

On parle d'outil spécialisé pour désigner des outils de recherche d'information dédiés à un domaine particulier. Ils ont les mêmes caractéristiques que les outils généralistes. Les outils spécialisés sont souvent répertoriés sur des portails (annuaires de liens).

Prenons l'exemple de la recherche d'information scientifique, abordée notamment dans le tutoriel [CERISE](http://urfist.chartes.psl.eu/cerise/). Parcourir la littérature scientifique sur un sujet donné consiste à faire un état de l'art. La première règle de l'état de l'art, c'est d'utiliser celui des autres, on va donc se tourner vers des ressources précises :

- articles de type état de l'art (*literature review*) ;
- livres introductifs (ex : collection *Que sais-je ?*) ;
- manuels et autres supports de cours ;
- thèses et mémoires.

Pour cela, il existe des moteurs de recherche spécialisés :

- [Google Scholar](https://scholar.google.fr/)
- [BASE](https://www.base-search.net/)
- [Isidore](https://isidore.science/) (SHS, *open access*)
- [TEL](https://tel.archives-ouvertes.fr/) (thèses de doctorat et mémoires de HDR)
- [Dumas](https://dumas.ccsd.cnrs.fr/) (mémoires de master)
- [Portail du SCD](http://scd.u-bordeaux-montaigne.fr/contenus/tout)

D'autres ressources spécialisées (archives, BDD, catalogues) permettent également de trouver des informations pertinentes :

- [Monoskop](https://monoskop.org/Monoskop)
- [ADIT](http://www.adit.fr/fr/adit/presentation)
- [arXiv.org e-Print archive](http://arxiv.org/)
- [Directory of Open Access Journals](http://doaj.org/)
- [Europe PubMed Central](http://europepmc.org/)
- [Scholar](https://scholar.google.fr/)
- [MemSIC Mémoires de master](http://memsic.ccsd.cnrs.fr/)
- [OpenDOAR Directory of OA Repositories](http://opendoar.org/)
- [Persée : Portail de revues](http://www.persee.fr/web/guest/home)
- [Revues.org : open electronic publishing](http://www.revues.org/)
- [Roar Registry of OA Repositories](http://roar.eprints.org/)
- [Scimago Journal &amp; Country Rank](http://www.scimagojr.com/)
- [TermSciences - Terminologie Scientifique](http://www.termsciences.fr/)
- [Thèses.fr](http://www.theses.fr/)
- [Web of Science](http://apps.webofknowledge.com/)
- [Bibnum Education](http://www.bibnum.education.fr/)

De nombreux outils de recherche spécialisés sont payants, comme par exemple les BDD de presse Europresse et Factiva. [Le portail des BDD disponibles à l'université Bordeaux Montaigne](https://scd.u-bordeaux-montaigne.fr/contenus/tout) permet d'accéder à certaines ressources via votre login universitaire.

Enfin, des logiciels comme [Publish or Perish](https://harzing.com/resources/publish-or-perish) (macOS, Windows, Linux) permettent d'automatiser les requêtes sur les moteurs de recherche académiques et d'extraire les résultats.

### Outils de veille

La veille utilise les mêmes outils que la recherche d'information, plus quelques outils qui lui sont spécifiques. On les regroupe grosso modo en deux catégories :

- outils de suivi permettant d'être notifié de l'apparition de nouvelles sources et publications d'après une recherche enregistrée (alertes via moteurs de recherche) ou un abonnement à un flux (flux RSS et Atom) ;
- outils de collecte permettant de réaliser une curation (sélection de contenu) publique ou non (ex : enregistrement de textes via Pocket, signets sociaux via Twitter et Diigo).

La veille étant une activité itérative, elle nécessite des outils de gestion, qui prenne souvent la forme d'un tableau de bord. Des tableaux de bord spécifiques existent pour certains réseaux sociaux (ex : [Tweetdeck](https://tweetdeck.twitter.com) pour Twitter). Des tableaux de bord de veille génériques permettent d'agréger différentes sources et d'obtenir des informations sur la veille, comme des statistiques (ex : [Netvibes](https://www.netvibes.com/fr), [Hootsuite](https://www.hootsuite.com/fr/)). Des outils de gestion plus génériques encore permettent de gérer les tâches associées à la veille, comme n'importe quel projet (ex : [Trello](https://trello.com/)).

# Livrer les résultats de la recherche

La livraison se négocie, il faut convenir des modalités à l'avance et les respecter.

Types de contenu
: Panorama de presse, synthèse documentaire, résumés informatifs de documents, bibliographie accompagnée ou pas de résumés indicatifs, sitographie, bulletin d’information, infographie, visualisation de données, sources réutilisables dans le cadre d'une veille, etc.

Sélection du contenu
: L’information doit être pertinente, c'est-à-dire qu'elle doit répondre au besoin initial de l’utilisateur : conforme au sujet, utilisable, issue de sources appropriées (langue, niveau d’expertise, nature). Il faut aussi maîtriser le volume de l'information : éviter une sélection redondante, veiller à la complémentarité et au traitement des divers aspects ou angles de vue.

Modalités de transmission
: Via un service tiers (plateforme sociale, partage de fichiers), sous forme de documents (traitement de texte, PDF, tableur, fichier OPML…).

Caractéristiques documentaires des livrables
: Page de garde, sommaire automatique, pagination, nommé de manière archivable, etc.

## Bibliographie

Une bibliographie est une liste de sources mise en forme suivant une certaine convention. La bibliographie permet de répondre aux impératifs éthiques et méthodologiques de la recherche d'information : c'est un répertoire méthodique des sources, et la garantie intellectuelle des différents points de vue consultés. Pour être exploitable, elle doit donner un maximum d'informations permettant d'accéder aux sources, et être normée de manière homogène.

La méthodologie de la bibliographie comprend deux points principaux :

Sélection
: On ne peut pas tout lire. La bibliographie est donc un travail sélectif, qui passe par une pratique de repérage et d'organisation des sources.

Organisation
: On ne peut pas se souvenir de tout ce qu'on a lu. Il faut donc organiser les sources sélectionnées, en les décrivant via des métadonnées et en les classant. Les métadonnées garantissent l'exploitabilité des sources. Le classement permet notamment de visualiser les lacunes d'une recherche d'information en cours.

Normes
: On doit présenter ce qu'on a lu de manière normée et homogène. Pour citer des références et constituer une bibliographie dans un document, il convient de respecter les normes pertinentes dans le cadre d'une recherche d'information donnée. Il existe des normaes internationales, suivies par les bibliothèques en particulier, telle ISO 690 ou encore ISBD (International Standard Bibliographic Description). En sciences, les règles varient d’une discipline à une autre, et souvent également au sein d’une même discipline.

L'utilisation d'un gestionnaire de données bibliographiques tel que [Zotero](https://www.zotero.org) est fortement conseillée. [Cliquez ici](https://boiteaoutils.info/2015/03/formations-bibliographie/) pour obtenir des conseils sur comment vous former à ces outils.

Les fonctionnalités d'un logiciel de gestion bibliographique sont souvent les suivantes :

- collecter (récupération automatique de références) ;
- organiser (stockage de références et enrichissement des métadonnées, annotation, gestion, import, export, recherche, etc.) ;
- conserver les documents (fichiers PDF, images, audio, vidéo, captures de pages Web, etc.) ;
- citer (générer une bibliographie et faire des citations) ;
- synchroniser et partager (travail collaboratif, stockage en ligne).

Consultez ma page de référence [Bibliographie](bibliographie.html) pour plus d’informations.

## Document de synthèse

Un document de synthèse restitue les informations repérées dans le corpus signalé en bibliographie pour leur donner sens en lien avec les attentes du commanditaire. On extrait des informations pertinentes et complémentaires des divers documents sélectionnés, et on signale la source de ces informations par un renvoi vers la bibliographie ou par une note en bas de page.

Exemple d'appels bibliographiques au format auteur-date :

> Les éoliennes contribuent à la production électrique renouvelable. Ainsi une statistique officielle établit que le développement de cette source d’énergie, aujourd’hui 4,5 % de la production totale d’électricité, permet à la France de se rapprocher des objectifs de l’accord de Paris (Ministère de l’environnement, 2019). Cependant, une étude montre que l’énergie éolienne rencontre une opposition marquée de la part des populations (Smith et al., 2018). Par ailleurs, d’autres aspects préoccupants sont signalés : dégradation des paysages, bruit et intermittence de la production (Durepaire, 2018).

Exemple d'appels bibliographiques au format note :

> Les éoliennes contribuent à la production électrique renouvelable. Ainsi une statistique du Ministère de l’environnement établit que le développement de cette source d’énergie, aujourd’hui 4,5 % de la production totale d’électricité, permet à la France de se rapprocher des objectifs de l’accord de Paris. Cependant, une étude montre que l’énergie éolienne rencontre une opposition marquée de la part des populations^1^. Par ailleurs, on liste d’autres aspects préoccupants : dégradation des paysages, bruit et intermittence de la production.
>
> ^1^ MISSELWITZ, Benjamin, 2018. L’énergie éolienne : étude des impacts environnementaux,  Science et énergie  [en ligne]. 18 septembre 2015. pp. 8‑20. [Consulté le 12 octobre 2018]. DOI 10.3390/nu7095380. Disponible à l’adresse : http://www.mdpi.com/2072-6643/7/9/5380/

Un bon document de synthèse doit être navigable de manière efficace. Au minimum, il faut numéroter les pages. Pour les documents de plus de 3 pages, il est indispensable de produire un document avec sommaire ou table des matières. Pour les longs documents (plusieurs dizaines de pages), il est important d'utiliser des en-têtes et pieds de page comportant des métadonnées utiles au repérage des sections (titre courant).

## Sources réutilisables

Les livrables qui mentionnent des sources devraient toujours être accompagné de fichiers de données réutilisables.

Les applications de lecture de flux RSS/Atom permettent d'exporter les flux enregistrés sous la forme d'un fichier unique dans un format de données standard (privilégier OPML).

Les gestionnaires de références bibliographiques permettent également d'exporter des collections sous la forme de fichiers dans des formats standards (privilégier BibTeX et CSL JSON).

# Exercice 1 : traitement de texte

Ce TP porte sur les fonctionnalités de traitement de texte qui permettent de produire un livrable de bonne qualité. Le TP requiert LibreOffice Writer. Dans un premier temps, il va s'agir d'appliquer quelques styles simples à un texte dépourvu de styles, et de mettre à jour les styles après avoir modifié certains paramètres typographiques. Dans un second temps, le TP aborde des fonctionnalités plus avancées (numérotation des titres, génération d'une table des matières, titres courants).

Ce TP nécessite LibreOffice Writer. Si vous n'avez pas déjà installé LibreOffice, [cliquez ici](https://fr.libreoffice.org/download/telecharger-libreoffice/), choisissez votre système d'exploitation et cliquez sur « Télécharger ». Au premier lancement de LibreOffice, si l'interface du logiciel est en anglais, [cliquez ici](https://download.documentfoundation.org/libreoffice/stable/7.1.4/mac/x86_64/LibreOffice_7.1.4_MacOS_x86-64_langpack_fr.dmg) pour télécharger l'interface traduite en français, puis installez-la. **Avertissement pour les utilisateurs de macOS :** lancez bien une première fois LibreOffice avant d'installer l'interface traduite.

Le texte que nous allons utiliser est un article du Site du Zéro, un site de tutoriels qui est l'ancêtre du site de cours en ligne OpenClassrooms. Récupérez le fichier [orthotypographie.txt](orthotypographie.txt){download=orthotypographie.txt} et ouvrez-le avec un éditeur de texte comme Bloc-notes, Notepad++ (Windows), ou TextEdit, BBEdit (macOS). Copiez le contenu du fichier puis lancez LibreOffice, créez un nouveau document intitulé « Nom Prénom IRI TP traitement de texte.odt » et collez le contenu du presse-papier.

## Introduction aux styles

### Appliquer des styles

Ouvrez le panneau des styles : vous pouvez soit choisir Styles › Gérer les styles, soit utiliser le raccourci clavier indiqué pour cette action, soit cliquer sur l'icône des styles dans la barre latérale droite. Vérifiez que le mode d'affichage réglable en bas du panneau est bien sur « Hiérarchie ».

Appliquez le style « Titre principal » au titre du texte, le style « Titre 1 » aux titres de sections et le style « Titre 2 » aux titres de sous-sections. [Cliquez ici](http://sdz.tdct.org/sdz/l-orthotypographie-bien-ecrire-pour-bien-etre-lu.html) pour consulter le texte tel que présenté dans l'archive en ligne du Site du Zéro : ceci vous permettra de repérer quels sont les titres de sections et de sous-sections.

### Modifier des styles

Faites clic droit › Modifier sur le style « Titre » (il s'agit du style sous lequel sont regroupés tous les autres styles dont le nom commence par « Titre… »). Dans l'onglet Police, sélectionnez « Source Sans Pro », « Gras » puis cliquez sur OK.

> **Question :** pourquoi les paragraphes stylés avec « Titre principal », « Titre 1 » et « Titre 2 » se retrouvent modifiés alors que vous avez modifié seulement le style « Titre » ? Indice : faites clic droit › Modifier sur l'un de ces styles et consultez l'onglet Gestionnaire.

Faites clic droit › Modifier sur le style « Style de paragraphe par défaut ». Dans l'onglet Retraits et espacement › Espacement, réglez Sous le paragraphe sur « 0,20 ». Dans l'onglet Police, sélectionnez « Source Serif Pro ». Cliquez sur OK.

### Numéroter les titres

Cliquez sur Outils › Numérotation des chapitres › Numérotation.

1. Sélectionnez le niveau 1. Réglez le champ Nombre sur « 1, 2, 3… ». Vérifiez que Style de paragraphe est réglé sur « Titre 1 ».
2. Sélectionnez le niveau 2. Réglez le champ Nombre sur « 1, 2, 3… » et le champ Afficher les sous-niveaux sur « 2 ». Vérifiez que « Style de paragraphe » est réglé sur « Titre 2 ».

Cliquez sur OK.

### Générer une table des matières automatique

Effacez la table des matières présente au début du fichier, y compris le titre « Table des matières ». Il ne doit rien rester entre le titre principal et le premier titre de section.

Placez le curseur à la toute fin du document. Cliquez sur Insertion › Table des matières et index › Table des matières, index ou bibliographie.

1. Dans Type › Type et titre › Titre, saisissez « Table des matières ».
2. Dans Type › Créer un index ou table des matières, réglez « Évaluer jusqu'au niveau » sur « 1 ».

Cliquez sur OK.

## Mise en page avancée

### Enchaînements

Modifiez les styles de paragraphe suivants d'après les indications :

Style de paragraphe par défaut
: Enchaînements › Options, cochez les cases qui permettent d'activer le traitement des lignes veuves et orphelines, réglez-les sur 2.

Titre 1
: Enchaînements › Sauts › Insérer, type « Page », position « Avant », avec le style de page « Première page ».

Titre de table des matières
: Enchaînements › Sauts › Insérer, type « Page », position « Avant », avec le style de page « Première page ».

Modifiez les styles de page suivants d'après les indications :

Première page
: Gestionnaire › Style › Style de suite : « Page gauche ».

Page de gauche
: Gestionnaire › Style › Style de suite : « Page droite ».

### En-tête et pied de page

Définissez le titre du document : Fichier › Propriétés › Description › Titre, valeur « Tutoriel : L'orthotypographie : bien écrire pour bien être lu ».

Remplissez les en-têtes :

1. Cliquez dans la zone d'en-tête d'une page de gauche (ni une première page, ni une page vide, ni une page de droite). Cliquez sur Insertion › Champ › Titre.
2. Cliquez dans la zone d'en-tête d'une page de droite (ni une première page, ni une page vide, ni une page de gauche).  Cliquez sur Insertion › Champ › Autres champs… › Document, type « Chapitre », format « Nom de chapitre ».
3. Modifiez le style de paragraphe « En-tête » : centré, en italique, 11 pt.
4. Modifiez les styles de page « Page droite » et « Page gauche » : réglez En-tête › Hauteur sur 1 cm.

Remplissez les pieds de page :

1. Cliquez dans la zone de pied de page d'une page de gauche. Cliquez sur Insertion › Numéro de page. Faites de même dans une page de droite.
2. Modifiez le style de paragraphe « Pied de page » pour qu'il soit centré, 11 pt.
3. Modifiez les styles de page « Page droite » et « Page gauche » : réglez Pied de page › Hauteur sur 1 cm.

Vérifiez que les pages ayant le style « Première page » n'ont ni en-tête ni pied de page.

### Empagement

Modifiez les marges des styles de page suivants (dans les réglages de chaque style, Page › Marges) :

Style de page | Gauche | Droite | Haut | Bas
--------------|---|---|---|---
Première page | 2,8 | 4,2 | 3,5 | 4,9
Page droite | 2,8 | 4,2 | 2,5 | 3,9
Page gauche | 4,2 | 2,8 | 2,5 | 3,9

::: note
Les valeurs de base de l'empagement sont celles utilisées pour le style « Première page ». Mais les en-têtes et pieds de page faussent les calculs, car ils ne sont pas compris dans les marges. La solution consiste alors à définir manuellement la hauteur des en-têtes et pieds de page, et retrancher cette hauteur aux marges hautes et basses sur les styles de page où ils sont présents.
:::

## Export

Cliquez sur Fichier › Exporter › Exporter au format PDF pour ouvrir la fenêtre Options PDF. Dans cette fenêtre, cochez les options suivantes :

- Général › Archive PDF/A
- Structure › Exporter le plan et autres éléments de structure
- Structure › Exporter les pages vides insérées automatiquement

Cliquez sur Exporter.

::: note
Un avertissement concernant l'accessibilité des liens peut apparaître. Pour le moment (LibreOffice 7) il n'y a pas de solution pour traiter cet avertissement, donc il faut l'ignorer et cliquer sur OK.

LibreOffice Writer mémorise ces réglages au niveau du document. Une fois que vous les avez définis pour le document, vous pouvez cliquer sur Fichier › Exporter › Exporter directement au format PDF pour passer directement au choix du nom et de l'emplacement. Pour tout nouveau document, pensez à visiter ces réglages avant d'exporter.
:::

Déposez les fichiers « Nom Prénom IRI TP traitement de texte.odt » et « Nom Prénom IRI TP traitement de texte.pdf » dans le dossier du TP sur le serveur de l'IUT.

# Exercice 2 : bibliographie

Ce TP a pour objectif de vous apprendre les bases de la gestion de données bibliographique et les bonnes pratiques en matière de citation. La première partie du TP porte sur l'utilisation d'un outil, Zotero. La seconde partie porte sur l'ajout de citations et de bibliographies à un document.

Zotero est un logiciel de gestion bibliographique. Il s’installe localement en version dite *standalone*, disponible sur Windows, macOS et Linux. Des extensions facultatives dites *connectors* sont également proposées pour automatiser la collecte de références directement depuis un navigateur. [Cliquez ici](https://www.zotero.org/download/) pour télécharger Zotero et éventuellement l'extension adaptée à votre navigateur.

De nombreux tutoriels existent pour prendre en main Zotero, notamment via les sites web des bibliothèques universitaires. [Cliquez ici](https://formadoct.doctorat-bretagneloire.fr/zotero) pour consulter un excellent guide proposé par l'Université de Bretagne Loire dans le cadre du tutoriel Formadoct. Ce TP commence par une visite guidée de Zotero qui reprend des éléments du guide Zotero de Formadoct :

- [Installation](https://formadoct.doctorat-bretagneloire.fr/c.php?g=491561&p=3362530)
- [Premiers pas](https://formadoct.doctorat-bretagneloire.fr/c.php?g=491561&p=3362579)
- [Créer un compte Zotero](https://formadoct.doctorat-bretagneloire.fr/c.php?g=491561&p=3362553)
- [Collecter](https://formadoct.doctorat-bretagneloire.fr/c.php?g=491561&p=3362404)
- [Organiser](https://formadoct.doctorat-bretagneloire.fr/c.php?g=491561&p=3362628)
- [Créer des bibliographies](https://formadoct.doctorat-bretagneloire.fr/c.php?g=491561&p=3362652)
- [Collaborer](https://formadoct.doctorat-bretagneloire.fr/c.php?g=491561&p=3362429)

## Gérer des données bibliographiques

Dans Zotero, créez une nouvelle collection « 2021 TP Zotero ». Saisissez les références ci-dessous dans cette collection.

- L'édition de 2003 du livre d’Elizabeth Eisenstein *La révolution de l’imprimé*, disponible au CRM (la bibliothèque de l'IUT).
- Le chapitre de Bruno Latour aux pages 605-615 de l'ouvrage collectif *Lieux de savoir* (tome 1).
- L'article ayant ce DOI : `10.3406/colan.1986.1762`
- Cette thèse : <https://these.nicolassauret.net/>
- Le billet intitulé « En finir avec Word ! » sur *L'Atelier des savoirs*.

## Utiliser des données bibliographiques

### Insérer des citations et une bibliographie

Dans LibreOffice, créez un fichier « Nom Prénom TP Zotero.odt ».

Copiez le texte ci-dessous et collez-le dans le fichier.

> Les définitions contemporaines du document reposent sur sa dimension fonctionnelle, en l’occurrence l’enregistrement ou l’inscription en vue d’une transmission. Cette finalité était déjà la principale préoccupation d’Elizabeth Eisenstein dans son ouvrage sur la révolution de l’imprimé [source]. Mais la manière dont cette fonctionnalité se décline concrètement n'a pas que des aspects techniques : elle influence aussi nos modes de réflexion. Ainsi, comme l'affirmait Roger Laufer, « il y a une indéniable corrélation entre l'évolution de la pensée et celle de l'organisation du livre » [source, p. 78]. La remarque dépasse même le livre et le document pour englober les dispositifs et lieux de savoir, ainsi que l'a montré Bruno Latour à propos des bibliothèques en particulier : la pensée n'est pas *retenue* dans le seul cerveau humain mais *distribuée* dans le réseau des acteurs, lieux et instruments du savoir [source]. D'où l'intérêt de réfléchir aux implications de nos choix d'outils dans une perspective globale, comme le proposent par exemple Julien Dehut sur les traitements de texte [source] et Nicolas Sauret sur la manière dont nous éditorialisons nos communautés [source].

En utilisant le style ISO-690 auteur-date français sans résumé, citez les références de votre collection Zotero aux emplacements marqués [source], puis ajoutez une bibliographie complète en fin de document.

### Mettre en forme le document

Dans LibreOffice, vérifiez que votre prénom et votre nom sont correctement renseignés dans Préférences › LibreOffice › Données d’identité. Ce paramétrage est global, vous n'avez besoin de le définir qu'une seule fois.

Dans Fichier › Propriétés › Description, saisissez le titre suivant : « Note sur le rapport entre outil et pensée ».

Dans le document, ajoutez les métadonnées suivantes au début du texte en utilisant Insertion › Champ :

- sur la 1^ère^ ligne, insérez le champ Titre avec le style « Titre principal » modifié (taille 18 pt) ;
- sur la 2^ème^ ligne, insérez le champ Premier auteur, avec un nouveau style intitulé « Auteur » (corps de texte + centré) ;
- sur la 3^ème^ ligne, insérez le champ date, avec un nouveau style intitulé « Date » (corps de texte + centré + espacement en dessous de 1 cm).

Au-dessus de la bibliographie, saisissez « Références » avec le style « Titre 1 » modifié (taille 14 pt).

Modifiez le style « Bibliographie 1 » en appliquant un retrait avant le texte de 0,5 cm et un retrait de première ligne de -0.5 cm.

## Rendu

Exportez la collection Zotero « 2021 TP Zotero » au format BibTeX, avec le nom « Nom Prénom TP Zotero.bib ».

Exportez le fichier « Nom Prénom TP Zotero.odt » au format PDF, avec le nom « Nom Prénom TP Zotero.bib ».

Déposez les trois fichiers (odt, pdf et bib) sur le serveur de l'IUT.

# Exercice 3 : synthèse et veille

## Consignes

Voici un sujet de recherche d’information :

- Sujet : les librairies sont-elles un commerce essentiel ? Panorama de la controverse autour de l'ouverture des librairies durant le confinement.
- Commanditaire : Emmanuel Laurentin, présentateur de l'émission *Le Temps du débat* sur France Culture.
- Objectif du commanditaire : se préparer à un débat radiophonique entre trois intervenants (Anne Martelle, présidente du Syndicat national de la librairie française ; Claude Poissenot, enseignant-chercheur en sociologie, spécialiste des pratiques culturelles et de la lecture ; Frédéric Worms, philosophe, professeur à l'ENS).

Seul ou en groupe (2-3), analysez le besoin d’information et proposez une synthèse documentaire (20 lignes maximum) avec une bibliographie (5 références) et une préconisation de veille (entre 3 et 5 sources).

- une synthèse documentaire de 20 lignes maximum (texte avec citations et bibliographie, format PDF) ;
- un fichier de données bibliographiques contenant 5 références (format BibTeX, exporté depuis Zotero par exemple) ;
- un fichier contenant entre 3 et 5 sources préconisées pour une veille sur le sujet (format OPML, voir tutoriel ci-dessous).

## Générer un fichier OPML : tutoriel

Voici deux exemples d'outils qui permettent de générer un fichier OPML à partir d'une liste d'abonnements à des flux RSS/Atom.

### OPML Generator (service en ligne)

[OPML Generator](https://opml-gen.ovh) est un script très simple, qui ne nécessite pas d’installer d’outil ni de créer un compte.

Collez simplement dans la zone de saisie une liste des URL des flux RSS que vous souhaitez compiler (une URL par ligne).

### Feedly (service en ligne)

Créez un compte sur [Feedly](https://feedly.com).

Pour vous abonner à une source, visitez d'abord la source et copiez son URL. Puis, sur la page d'accueil de Feedly, cliquez sur le bouton en forme de +. Collez l'URL dans le champ de recherche qui s'affiche au centre de la page. Parmi les suggestions, identifiez la source et cliquez dessus. Puis, cliquez sur *Follow*.

Pour exporter vos abonnements au format OPML, rendez vous sur la page d'accueil de Feedly. Cliquez sur l'icône en forme de personnage en bas à gauche de l'interface, puis sur *Organize Sources*. En haut à droite de la page *Organize Sources*, cliquez sur le bouton en forme de flèche. Puis, sur la page *OPML Export*, cliquez sur *Download your Feedly OPML*.

### NetNewsWire (application macOS)

Téléchargez [NetNewsWire](https://netnewswire.com).

Pour vous abonner à une source, visitez d'abord la source et copiez son URL. Puis, dans NetNewsWire, cliquez sur le bouton en forme de +, puis sur *New Web Feed*. Dans la boîte de dialogue qui s'affiche, collez l'URL dans le champ correspondant et donnez éventuellement un titre à la source. Puis cliquez sur *Add*.

Pour exporter vos abonnements au format OPML, cliquez sur sur le menu Fichier puis sur *Export Subscriptions*. Dans la boîte de dialogue qui s'affiche, cliquez sur *Export as OPML…*. Dans la fenêtre qui s'ouvre, choisissez l'emplacement du fichier, vous pouvez également le renommer (l'extension doit rester .opml). Puis cliquez sur *Export OPML*.

## Rendu

Déposez les trois fichiers (pdf, bib et opml) sur le serveur de l'IUT.