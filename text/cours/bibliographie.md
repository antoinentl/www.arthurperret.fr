---
title: Bibliographie
author: Arthur Perret (Université Bordeaux Montaigne)
abstract: "Cette page liste quelques éléments utiles pour clarifier les attentes des enseignants concernant les citations et la bibliographie à l'université. Je présente aussi deux outils qui permettent d'automatiser en partie le travail bibliographique, au format texte : Zotero et Zettlr."
wip: true
updated: 30/03/2022
---

# Principes

La bibliographie obéit à deux grands principes :

Respect du droit d'auteur
: Vous devez citer les auteurs dont vous utilisez le travail.

Réfutabilité
: Votre travail doit s'appuyer sur des arguments vérifiables et potentiellement réfutables. Pour cela, il faut notamment identifier sans aucune ambiguïté les sources utilisées, c'est-à-dire fournir suffisamment d'éléments d'identification au lecteur pour qu'il puisse les rechercher et les localiser facilement.

La bibliographie s'oppose au plagiat. Plagier, c'est s’approprier frauduleusement le texte, les idées ou les faits d’un auteur et oublier délibérément ou par négligence de le citer.

# Définitions

Source
: Document servant d’appui pour la rédaction d’une problématique ou l’interprétation des résultats d’une recherche.

Référence
: Ensemble de métadonnées décrivant une source, qui permettent de l'identifier, et qui sont incluses dans le corps d’un texte.

Bibliographie
: Liste de références présentées sous forme longue, c'est-à-dire en incluant le maximum d'informations pertinentes sur la source. Généralement incluse en fin de document.

Note
: Référence présentée sous forme longue, en marge du texte (souvent en bas de page), reliée à une affirmation ou une citation via un appel de note (qui peut être un chiffre, une lettre ou un symbole). Ne s'accompagne généralement pas d'une bibliographie (qui serait redondante).

Appel bibliographique
: Référence présentée sous forme courte, c'est-à-dire en incluant uniquement le minimum nécessaire pour identifier la source. Toujours incluse immédiatement après une affirmation ou une citation, directement dans le corps du texte, et toujours accompagnée d'une bibliographie.

# Styles bibliographiques

Pour pouvoir être utilisée efficacement, une bibliographie doit être classé dans un certain ordre. Plusieurs types de classement sont possibles : ordre alphabétique (des auteurs, des titres), ordre chronologique (des dates de publication, de citation dans le texte), type de document, etc.

Par ailleurs, quelque soit le type de classement, il existe différentes manières de rédiger une référence. Ces différences portent sur la nature des informations incluses ou omises, leur ordre, la typographie (ponctuation, casse, abréviations), etc.

Un style bibliographique est une série de règles qui codifient la manière de citer, classer et rédiger les références. Il mélange souvent plusieurs types de classement. Par exemple, une bibliographie en style auteur-date est classée par ordre alphabétique des auteurs et, pour chaque auteur, par ordre chronologique des dates de publications.

Il existe des milliers de styles bibliographiques (10219 à ce jour dans [le répertoire Zotero](https://www.zotero.org/styles)). Mais :

- beaucoup sont des variations d'autres styles, avec des modifications parfois mineures : un changement d'ordre entre deux informations dans une référence, une ponctuation légèrement différente, etc ;
- on peut les regrouper en 5 grands formats, dont les principaux (les plus utilisés) sont le format [auteur-date](https://www.zotero.org/styles?format=author-date), le format [note](https://www.zotero.org/styles?format=note) et le format [numérique](https://www.zotero.org/styles?format=numeric) ;
- certains styles ont été érigés en normes, c'est-à-dire qu'ils sont préconisés par des organismes nationaux et internationaux, ce qui encourage à les utiliser.

Une règle essentielle à suivre quel que soit le style ou la norme utilisée, c'est de ne jamais en changer au cours d'un même travail rédactionnel.

En sciences de l'information et de la communication, les principaux formats utilisés sont auteur-date et note. Je recommande souvent d'utiliser [la norme ISO-690 au format auteur-date](https://www.zotero.org/styles?q=id%3Aiso690-author-date-fr-no-abstract&format=author-date).

# Comment citer ?

## Citation simple

Citer une partie du texte signifie retranscrire un extrait tel quel : respect de la ponctuation, des majuscules, des fautes ainsi que la mise en forme (notamment l'italique, généralement utilisée pour les langues étrangères, les titres d'œuvre, l'emphase).

La citation doit être entre guillemets.

La citation doit toujours être référencée. Au format auteur-date par exemple, on indique l'auteur, l'année et la localisation. Cette dernière correspond à l'emplacement de la citation dans le document : le plus souvent, on indique le numéro de page ; lorsque ce n'est pas applicable, on se rabat sur le numéro de section, de paragraphe…

Quand il y a deux auteurs, on les inscrit tous les deux. Ex : (Epron, Vitali-Rosati 2018).

Au-delà de deux auteurs, on ne reprend que le premier, suivi de l'abréviation « et al. »

## Citation en langue étrangère

On peut citer un passage en langue étrangère si on sait que les lecteurs maîtrisent la langue de l’extrait. On met alors le passage cité en italique (et toujours entre guillemets, toujours référencé).

Lorsque la langue originale ne peut pas être comprise par les lecteurs, deux options :

- soit on trouve une traduction déjà publiée de l’extrait et on indique dans la référence le nom du traducteur ainsi que les dates de publication et de traduction ;
- soit on doit traduire soi-même l’extrait, auquel cas on fait précéder la référence de la mention `[Notre traduction]`.

## Citation secondaire

Il arrive qu'on lise un document qui contienne lui-même une citation. En règle générale, il vaut mieux remonter à la source primaire plutôt que citer une source secondaire. Lorsqu'on ne peut pas accéder à la source primaire, on mentionne alors la source originale suivi de « cité dans » ou « cité par » et la source secondaire.

# Quelles métadonnées inclure ?

Les métadonnées à inclure pour une référence dépendent du type de document.

Une distinction générale importante à faire :

- les documents qui contenus dans un autre document (ex : un chapitre de livre, un article de revue, un billet de blog) ;
- les documents qui ne sont pas contenus dans un autre document (ex : un livre, une thèse, un rapport).

Pour tous les documents de manière générale, il convient d'inclure :

- titre
- auteur(s)
- date
- langue
- identifiant (si applicable, en fonction du document : ISBN, DOI…)

Pour les livres, on ajoutera la maison d'édition.

Pour les documents contenus dans un autre document, il faut veiller à ajouter des métadonnées concernant le document contenant. Les deux principales sont :

- les pages où apparaissent le document contenu (pour les articles de périodiques)
- le nom du document contenant (varie selon le type de document)

Il est très utile d'inclure un lien cliquable pour rendre la référence immédiatement accessible. Cela peut être l'URL d'une page, ou bien un identifiant couplé à un résolveur (ex : `https://doi.org/` suivi du DOI) qui le transforme en lien cliquable. **Mais attention**, ce n'est qu'une métadonnée parmi d'autres, elle ne remplace pas les autres, bien au contraire : sans le reste des métadonnées, impossible d'évaluer à quel type de document va nous renvoyer tel ou tel lien.

# Exemple pratique

Dans cet exemple, je montre différentes manières d'intégrer une référence dans un texte, en respectant  la norme ISO-690 au format auteur-date.

***

Si on se réfère à la source sans citer d'extrait, on ajoute la référence juste après le fait mentionné, sous la forme d'un appel :

```
On peut décrire l'édition comme la réunion de trois fonctions : une qui consiste à sélectionner et produire, une qui consiste à légitimer, et une qui consiste à diffuser (Epron, Vitali-Rosati 2018).
```

On peut rédiger différemment, en utilisant les noms d'auteurs et en ne mettant que la date entre parenthèses :

```
Epron et Vitali-Rosati (2018) décrivent l'édition comme la réunion de trois fonctions : une qui consiste à sélectionner et produire, une qui consiste à légitimer, et une qui consiste à diffuser.
```

Si c'est la première fois qu'on mentionne les auteurs, il peut être plus convenable d'inclure leurs prénoms :

```
Benoît Epron et Marcello Vitali-Rosati (2018) décrivent l'édition…
```

***

Si on se réfère à la source en citant un extrait relativement court (une phrase, ou moins de 2 lignes), on met l'extrait entre guillemets et on ajoute la référence après, en précisant la localisation de l'extrait :

```
On peut décrire l'édition comme la réunion de trois fonctions : « une fonction de choix et de production, une fonction de légitimation et une fonction de diffusion » (Epron, Vitali-Rosati 2018, p. 6).
```

***

Si on cite un extrait long (plus d'une phrase, ou plus de 2 lignes), on l'isole sous la forme d'un paragraphe, toujours entre guillemets et suivi de la référence avec la localisation :

```
Benoît Epron et Marcello Vitali-Rosati définissent l'édition de la manière suivante :

« L'édition peut être comprise comme un processus de médiation qui permet à un contenu d'exister et d'être accessible. On peut distinguer trois étapes de ce processus qui correspondent à trois fonctions différentes de l'édition : une fonction de choix et de production, une fonction de légitimation et une fonction de diffusion » (Epron, Vitali-Rosati 2018, p. 5-6).
```

Traditionnellement, la mise en forme de ces paragraphes est légèrement différente, pour indiquer leur spécificité. Généralement, cela se manifeste par un espace un peu plus important à gauche du paragraphe (appelé indifféremment renfoncement, retrait, indentation…).

Il existe un élément HTML dédié pour les paragraphes correspondant à une citation : `blockquote` (littéralement « citation en bloc »). La syntaxe Markdown équivalente est le chevron fermant `>` :

```
Benoît Epron et Marcello Vitali-Rosati définissent l'édition de la manière suivante :

> « L'édition peut être comprise comme un processus de médiation qui permet à un contenu d'exister et d'être accessible. On peut distinguer… » (Epron, Vitali-Rosati 2018, p. 5-6).
```

***

Quelle que soit la forme de la citation, on donne la référence sous forme longue dans une section dédiée (« Bibliographie », « Références », « Sources »…) en fin de document.

```
EPRON, Benoît et VITALI-ROSATI, Marcello, 2018. L’édition
    à l’ère numérique. Paris : La Découverte. Collection
    Repères. ISBN 978-2-7071-9935-5. 
```

La mise en forme de la bibliographie compte. Certains aspects sont pris en charge par le style bibliographique, par exemple le fait de mettre les titres d'œuvres en italique. D'autres aspects doivent parfois être faits manuellement dans le document final

Dan l'exemple ci-dessus, j'ai appliqué une indentation inversée (*hanging indent*), c'est-à-dire que la première ligne de la référence n'est pas indentée, tandis que les suivantes le sont. C'est l'inverse d'un alinéa. Ceci aide à parcourir la bibliographie visuellement de manière rapide.

# Outils

Certains outils permettent d'automatiser une partie du travail de collecte et d'organisation des sources, ainsi que l'intégration des citations et des bibliographies lors de la rédaction.

## Gestion bibliographique

Les principales fonctionnalités qu'on attend d'un bon logiciel bibliographique sont :

- collecter des références constituées de métadonnées (auteur, titre, date…) que l'on peut modifier, et auxquelles on peut attacher des pièces jointes (texte intégral de la référence, notes…) ;
- classer ces références dans des dossiers ou avec des mots-clés ;
- générer des bibliographies automatiquement.

Un exemple de logiciel bibliographique : [Zotero](https://www.zotero.org/).

Zotero inclut deux mécanismes pour collecter automatiquement les métadonnées d'une référence :

- un connecteur pour navigateur, qui permet de collecter les métadonnées d'une référence consultée sur le Web (très pratique pour les sources nativement numériques, comme les billets de blog, ou certains articles de revues scientifiques) ;
- un bouton *Ajouter un document par son identifiant*, qui permet de collecter les métadonnées d'une référence identifiée par un ISBN (livres), un ISSN (périodique), un DOI (article scientifique), etc.

**Attention :** les métadonnées collectées automatiquement par les logiciels bibliographiques sont rarement propres ou complètes, ce qui n'est pas leur faute mais plutôt celle des sources. Il faut donc toujours repasser derrière si on veut générer des bibliographies homogènes et sans erreurs.

## Rédaction

Les principales fonctionnalités qu'on attend d'un bon logiciel d'écriture (dans le contexte du mémoire) sont :

- structurer de longs documents et faciliter la navigation ;
- insérer des citations et générer des bibliographies ;
- exporter les fichiers dans différents formats.

Un exemple d'éditeur de texte Markdown avec des fonctionnalités académiques : [Zettlr](https://www.zettlr.com/)

Si vous avez un logiciel bibliographique comme Zotero, il peut générer un fichier de données dans lequel chaque référence sera exportée avec un identifiant `id`, qu'on appelle plus communément « clé de citation ». Zettlr peut lire ce fichier. Il propose alors une fonctionnalité très pratique : l'insertion des références via la touche `@`. Sélectionner une référence insère la clé de citation correspondante. En exportant le fichier Markdown, ces clés sont transformées en références et une bibliographie est générée en fin de document. Il est bien entendu possible de choisir un style bibliographique.

Si on reprend l'exemple de citation plus haut, voici à quoi ressemblerait le fichier de données (notez l'identifiant `id`) :

```
[
  {"id":"epron2018","author":[{"family":"Epron","given":"Benoît"},{"family":"Vitali-Rosati","given":"Marcello"}],"citation-key":"epron2018","collection-title":"Collection Repères","event-place":"Paris","ISBN":"978-2-7071-9935-5","issued":{"date-parts":[[2018]]},"language":"fr","publisher":"La Découverte","publisher-place":"Paris","title":"L'édition à l'ère numérique","type":"book"}
]
```

Si on renseigne ce fichier dans les réglages de Zettlr, on peut ensuite insérer des références via la touche `@`.

Ci-dessous, je donne à nouveau les différents exemples de citation, en mettant cette fois-ci d'abord la syntaxe en Markdown et ensuite le rendu une fois exporté.

***

```
On peut décrire l'édition comme la réunion de trois fonctions : une qui consiste à sélectionner et produire, une qui consiste à légitimer, et une qui consiste à diffuser [@epron2018].
```

devient :

```
On peut décrire l'édition comme la réunion de trois fonctions : une qui consiste à sélectionner et produire, une qui consiste à légitimer, et une qui consiste à diffuser (Epron, Vitali-Rosati 2018).
```

***

```
@epron2018 décrivent l'édition comme la réunion de trois fonctions : une qui consiste à sélectionner et produire, une qui consiste à légitimer, et une qui consiste à diffuser.
```

devient :

```
Epron et Vitali-Rosati (2018) décrivent l'édition comme la réunion de trois fonctions : une qui consiste à sélectionner et produire, une qui consiste à légitimer, et une qui consiste à diffuser.
```

***

```
Benoît Epron et Marcello Vitali-Rosati définissent l'édition de la manière suivante :

« L'édition peut être comprise comme un processus de médiation qui permet à un contenu d'exister et d'être accessible. On peut distinguer trois étapes de ce processus qui correspondent à trois fonctions différentes de l'édition : une fonction de choix et de production, une fonction de légitimation et une fonction de diffusion » [@epron2018, 5-6].
```

devient :

```
Benoît Epron et Marcello Vitali-Rosati définissent l'édition de la manière suivante :

« L'édition peut être comprise comme un processus de médiation qui permet à un contenu d'exister et d'être accessible. On peut distinguer trois étapes de ce processus qui correspondent à trois fonctions différentes de l'édition : une fonction de choix et de production, une fonction de légitimation et une fonction de diffusion » (Epron, Vitali-Rosati 2018, p. 5-6).
```

## Installation conseillée

### Zotero

Première étape : [téléchargez Zotero](https://www.zotero.org/download/).

::: note
Je recommande Zotero plutôt qu'un autre car c'est un logiciel libre, maintenu par une institution de recherche, et très utilisé dans le monde universitaire. On a donc une assez bonne garantie que le logiciel reste maintenu, qu'on reste propriétaire de ses données sans être espionné par une société privée, et qu'on peut trouver de l'aide facilement en prenant contact avec des bibliothécaires et documentalistes qui travaillent à la fac.
:::

Besoin d'aide ? [Tutoriel Zotero en français](https://formadoct.doctorat-bretagneloire.fr/zotero)

<!-- https://mate-shs.cnrs.fr/actions/tutomate/tuto45-zotero-pulliat/ -->

Deuxième étape : [téléchargez le plugin Better BibTeX pour Zotero](https://github.com/retorquere/zotero-better-bibtex/releases/download/v6.2.14/zotero-better-bibtex-6.2.14.xpi). Pour l'installer :

- Dans Zotero, cliquez sur *Outils* › *Extensions*.
- Cliquez sur l'engrenage dans le coin supérieur droit et choisissez *Install add-on from file*.
- Choisissez le fichier `.xpi` que vous venez de télécharger et cliquez sur *Installer*.
- Redémarrez Zotero.

Troisième étape : paramétrer les clés de citation.

- Dans les réglages de Zotero, cliquez sur l'onglet *Better BibTeX*.
- Sous *Clés de citation*, dans le champ *Format de clé*, saisissez `[auth:lower:nopunctordash][year]` (format recommandé).

Quatrième étape : une fois que vous avez des références, il faut exporter votre bibliothèque **(à faire une seule fois)**.

- Faites clic droit sur *Ma bibliothèque* et sélectionner *Exporter la bibliothèque*.
- Sélectionnez le format *Better CSL JSON*, cochez *Garder à jour* et cliquez sur OK.
- Sélectionnez un emplacement pour le fichier, renommez-le éventuellement (ex : `references.json`) et cliquez sur Enregistrer.

::: note
Cochez bien *Garder à jour* : vous n'aurez plus besoin de vous préoccuper du plugin, ni de faire d'export, car à chaque modification de la bibliothèque, l'export sera mis à jour automatiquement par Zotero.
:::

Besoin d'aide ? [Documentation du plugin Better BibTeX en anglais](https://retorque.re/zotero-better-bibtex/)

### Zettlr

[Téléchargez Zettlr](https://www.zettlr.com/download).

::: note
Je recommande Zettlr plutôt qu'un autre car il est spécialisé dans l'écriture académique, contrairement à des logiciels généralistes (VS Code, Obsidian…) qui disposent de plugins académiques mais ne sont pas conçus pour ça (ce qui se ressent négativement à l'installation et à l'usage).
:::

Ici, une seule étape : paramétrer les citations.

- Dans les réglages de Zettlr, cliquez sur l'onglet *Citations*.
- Dans le champ prévu à cet effet, indiquez l'emplacement du fichier de références bibliographiques que vous avez exporté via Zotero.
- Dans le champ en-dessous, indiquez l'emplacement du fichier de style bibliographique de votre choix. Je vous recommande de [télécharger le fichier correspondant à la norme ISO-690 au format auteur-date](https://www.zotero.org/styles/iso690-author-date-fr-no-abstract).

À ce stade, vous êtes prêts pour collecter des références dans Zotero et les citer en Markdown dans Zettlr.

Besoin d'aide ? [Documentation de Zettlr en français](https://docs.zettlr.com/fr/)

***

Pour passer de Zettlr à un autre environnement, il faut exporter vos fichiers Markdown créés dans Zettlr.

- Cliquez sur *Fichier* › *Exporter*.
- Choisissez un format et cliquez sur *Exporter*.

Lors de l'export, les clés de citation seront traitées. On obtient alors un fichier avec des citations mises en forme et une bibliographie créée automatiquement.

# Sources

Les sections « Principes » et « Comment citer » reprennent des éléments partagés dans un support de formation intitulé « La Bibliographie : règles et gestion des références avec le logiciel Zotero », créé par I. Perez et J. W. Schleich (centre de documentation P. Bartoli, INRA, Montpellier SupAgro).
