---
title: Introduction à la publication web
author: Arthur Perret (Université Bordeaux Montaigne)
abstract: "Ce cours constitue une introduction rapide à la publication web pour les débutants. Il présente les bases de HTML, de CSS et de l’architecture d’un site web, ainsi que des solutions pour la mise en ligne."
wip: true
updated: 20/06/2022
---

# Pré-requis

## État d’esprit

Ce cours s’adresse aux personnes qui s’intéressent à la publication sur le web, en particulier mes collègues et étudiants en sciences humaines.

J’ai pensé ce cours comme une intervention en amont des formations qui portent sur des outils de publication spécifiques (par exemple un CMS comme WordPress, ou un générateur de site statique comme Hugo). Le but ici est de comprendre ce que ces outils manipulent. Mon objectif est de faciliter l’appropriation de ce qu’est le Web, en réduisant sa complexité à quelques notions et techniques simples, qui permettent de réellement comprendre les choses. En ce sens, ce cours constitue moins une formation qu’une acculturation au Web.

Cette proposition requiert en retour un état d’esprit réceptif : comme dans beaucoup d’apprentissages, il faut accepter de suspendre un temps la question de l’utilité pour explorer les possibilités.

## Connaissances

Ce cours requiert de connaître ce qu’est le format texte, puisque c’est au format texte que s’expriment les technologies du Web. Si vous n’êtes pas sûr de maîtriser complètement cette notion, je vous invite à consulter [ma page dédiée au format texte](format-texte.html).

## Outils

Trois outils sont nécessaires dans ce cours :

1. Un ordinateur. Si les téléphones et les tablettes ont beaucoup gagné en capacité ces dernières années, ce sont des environnements qui restent beaucoup plus contraints et moins confortables que les ordinateurs pour manipuler les technologies du Web.
2. Un navigateur. Bien qu’elles soient moins grandes que par le passé, il existe toujours des différences dans la façon dont les navigateurs utilisent les technologies du Web. Il peut donc être utile de tester ses créations dans deux logiciels différents. Les navigateurs modernes les plus utilisés sont Firefox, Chrome, Opera, Safari et Edge.
3. Un éditeur de texte. N'utilisez pas de traitement de texte type Microsoft Word, LibreOffice Writer ou Google Docs : ces logiciels de bureautique sont inadaptés aux langages comme HTML et CSS, qui nécessitent de voir et de contrôler l'insertion de tous les caractères, en particulier les espaces, la ponctuation et les guillemets. Pour débuter, privilégiez un logiciel complet (avec un bon manuel d’utilisation) plutôt qu’un éditeur à *plugins* qui nécessite de composer ses fonctionnalités à la carte. Quelques recommandations : Notepad++, BBEdit, Sublime Text, Visual Studio Code, Emacs.

# Définitions

L’expression « technologies du Web » désigne souvent le trio HTML, CSS et JavaScript :

*HyperText Markup Language* (HTML)
: Le langage de balisage conçu pour représenter les pages web. Ce langage permet d’écrire de l’hypertexte, de structurer sémantiquement la page, de mettre en forme le contenu, de créer des formulaires de saisie, d’inclure des ressources multimédias et de créer des documents interopérables et accessibles.

*Cascading Style Sheets* (CSS)
: Le langage informatique qui décrit la présentation des documents HTML, autrement dit leur mise en forme. En français, CSS se traduit par « feuilles de style en cascade ».

JavaScript
: Le langage de programmation de scripts principalement employé dans les pages web interactives. À ce titre, JavaScript est une partie essentielle des applications web, c'est-à-dire des sites proposant des interactions complexes (authentification, transaction, personnalisation…).

Pour comprendre comment le Web fonctionne, il faut aussi connaître ce qu’est le protocole HTTP et quel rôle joue un navigateur :

*Hypertext Transfer Protocol* (HTTP)
: Le protocole de communication client-serveur\* développé pour le *World Wide Web*. Littéralement « protocole de transfert hypertexte ». HTTP est un standard Internet, donc défini par des RFC produits au sein de l'IETF. Le but du protocole HTTP est de permettre un transfert de fichiers (essentiellement au format HTML), localisés grâce à une chaîne de caractères appelée URL, entre un navigateur (le client) et un serveur web. HTTPS est une variante sécurisée de HTTP, de plus en plus utilisée.

::: note
\* L'expression « client-serveur » désigne un mode de transaction (souvent à travers un réseau) entre plusieurs programmes ou processus : l'un, qualifié de client, envoie des requêtes ; l'autre, qualifié de serveur, attend les requêtes des clients et y répond. « Client » désigne l'ordinateur sur lequel est exécuté le logiciel client, et « serveur » l'ordinateur sur lequel est exécuté le logiciel serveur. Les machines serveurs sont généralement dotées de capacités supérieures à celles des ordinateurs personnels afin de pouvoir répondre de manière efficace à un grand nombre de clients. Les machines clients sont souvent des ordinateurs personnels ou des terminaux mobiles (téléphone, tablette).
:::

Navigateur
: Un logiciel conçu pour consulter et afficher le *World Wide Web*. C'est un client HTTP, c'est-à-dire un logiciel conçu pour se connecter à un serveur HTTP. Il possède un moteur de rendu des standards du Web, c'est-à-dire un composant logiciel qui transforme un document HTML, ainsi que toutes les autres ressources associées à la page, en une représentation visuelle interactive pour l'utilisateur. Il peut également inclure un moteur JavaScript, c'est-à-dire un composant logiciel qui interprète et exécute du code en langage JavaScript.

<!-- La modularité des navigateurs, c'est-à-dire leur organisation en différents composants, permet d'activer ou désactiver certaines fonctionnalités de manière sélective : par exemple, on peut désactiver le rendu JavaScript (pour des questions de sécurité), et même le rendu CSS.

Les navigateurs permettent d'inspecter le code source de la page. Ceci affiche le code HTML avant son rendu par le navigateur. Il est également possible d'utiliser des outils de développement pour examiner le code source de manière plus précise et plus interactive, et même modifier l'affichage de la page. Ces outils constituent un excellent moyen de faire la rétro-ingénierie d'une page, que ce soit dans une logique d'apprentissage ou de recherche et développement.

Lorsque le navigateur n'arrive pas à fournir la page demandée, il affiche un code de réponse. Celui-ci est constitué de trois chiffres. Le premier chiffre indique la catégorie d'erreur : 30x : redirection ; 40x : erreur client ; 50x : erreur serveur. Les chiffres suivants indiquent la nature exacte de l'erreur. Exemple : erreur 404, le serveur n'a rien trouvé à l'adresse spécifiée par le client. Autre exemple : erreur 503, le serveur ne peut pas répondre car le trafic est trop dense. -->

Enfin, différentes technologies sur lesquelles repose le Web sont définies par des standards, c'est-à-dire des spécifications techniques normalisées. Deux instances s'occupent de définir et de faire évoluer ces standards, il est juste de les mentionner :

*Internet Engineering Task Force* (IETF)
: Élabore les standards composant les couches basses de l’infrastructure et du réseau Internet. Ce travail repose sur la rédaction de *Request for comments* (RFC), des documents de spécification technique. Le protocole HTTP est défini par des RFC. L'IETF est gérée par une organisation à but non lucratif, l'Internet Society.

*World Wide Web Consortium* (W3C)
: Élabore les standards composant les couches hautes du Web. Le W3C gère notamment la spécification des normes techniques pour HTML, CSS et JavaScript. Le W3C a une gouvernance ouverte et prend ses décisions par consensus.

# HTML

HTML est un langage dit de balisage, c’est-à-dire qu’il utilise des balises, des petites expressions codifiées qui donnent une indication sur la nature et la structure d’un contenu. Ces balises sont autant d’instructions interprétées et exécutées par la machine, en l’occurrence le navigateur. HTML comprend donc un vocabulaire, qui est basé sur une langue naturelle (l’anglais), et des règles de syntaxe, qui reposent sur l’utilisation de quelques caractères spéciaux.

## Éléments

Les éléments constituent la base de HTML. Ils servent à identifier et structurer le contenu. Ils remplissent différents rôles (liste non exhaustive) :

- organisation du texte : titres, paragraphes, listes, tableaux, liens, emphase, texte préformaté, blocs de citation ;
- inclusion de médias : sons, images, vidéos, cadres interactifs ;
- structuration sémantique du document : en-tête, article, section, aparté, navigation, pied de page.

Un élément HTML est formé d’un contenu encadré par une balise ouvrante `<nom>` et une balise fermante `</nom>`. Le `nom` de l’élément est insensible à la casse. Par convention, il est souvent en minuscules. Le nom de l’élément indique la nature de son contenu.

Exemple (avec l’élément `p` pour paragraphe) :

```html
<p>HTML c’est très facile.</p>
```

Un élément peut contenir un ou plusieurs autres éléments. Un élément B ouvert à l’intérieur d’un élément A doit être refermé avant que A se referme. C’est le principe de l’imbrication.

Exemple (avec un élément `strong` ouvert à l’intérieur d’un élément `p`, et donc refermé avant que `p` ne soit refermé) :

```html
<p>HTML c’est <strong>très</strong> facile.</p>
``` 

Les éléments se répartissent en deux catégories :

- les éléments de niveau bloc (*block*), définis par le fait qu’ils sont précédés et suivis d’un retour à la ligne (ils forment ainsi un bloc, d’où leur nom) ;
- les élément en ligne (*inline*), définis par le fait qu’ils sont toujours contenus dans des éléments de niveau bloc.

Un élément peut être vide, c’est-à-dire ne pas avoir de contenu : `<nom></nom>`. On peut l’écrire avec une notation spéciale : `<nom>` ou `<nom />` (plus explicite).

::: note
« Élément vide » est un faux ami : ces éléments servent généralement bien à insérer quelque chose mais qui ne se situe pas dans le document, et ils utilisent pour cela des attributs au lieu d’un contenu entre deux balises.
:::

## Attributs

Un attribut est une information associée à un élément HTML. C’est une forme de métadonnée, qui peut jouer différents rôles (technique, sémantique…). Un attribut est formé d’un nom et d’une valeur.

Exemple (avec l’attribut `lang` dont la valeur est un code linguistique dans la norme ISO 639) :

```html
<p lang="fr">HTML c’est très facile.</p>
```

Les guillemets qui entourent la valeur peuvent être omis si elle ne contient pas d’espaces.

## Anatomie d’un document HTML

Voici un exemple de document HTML minimaliste :

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Un document</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <p>Ceci est un document.</p>
  </body>
</html>
```

Légende :

- `<!DOCTYPE html>` : déclaration du type de document ;
- `html` : élément racine qui contient tout le code du document ;
- `head` : en-tête (métadonnées) ;
- `body` : corps (contenu).

Dans les métadonnées :

- `title` : titre du document ;
- `meta` : une métadonnée, ici le codage (`charset`) qui est l’UTF-8 ;
- `link` : un lien vers un autre fichier, dont la nature (`rel`) est une feuille de styles (`stylesheet`), et dont le chemin d’accès (`href`) est `styles.css`.

L’espace vide formé par les caractères d’espacement (espaces, tabulations, retours à la ligne…) n’est pas interprété par le navigateur. Il peut être utilisé pour améliorer le confort de lecture et d’écriture ; ceci inclut l’indentation, c’est-à-dire le fait d’ajouter de l’espace vide en début de ligne pour représenter visuellement la hiérarchie des éléments.

## Caractères spéciaux

Les caractères présentés ci-dessous sont réservés pour la syntaxe de HTML. Pour les inclure tels quels dans un document, il faut les écrire sous forme d’entité.

Caractère | Entité
----------|-----------
`<`       | `&lt;`
`>`       | `&gt;`
`"`       | `&quot;`
`'`       | `&apos;`
`&`       | `amp;`

Exemple :

```html
<p>L'élément &lt;p&gt; définit un paragraphe.</p>
```

Il existe de nombreuses autres entités correspondant à des caractères non reconnus par les codages anciens (ASCII, Latin-1…). Si le document est codé en UTF-8, il n’est pas nécessaire de les utiliser car ce codage permet d’exprimer tous les caractères nécessaires.

## Commentaires

Les commentaires `<!-- Ceci est un commentaire -->` sont ignorés par le navigateur et sont donc invisibles dans le rendu du document par le navigateur. Ceci permet d’annoter le code.

## Permissivité

HTML est un langage assez permissif. Cela se traduit d’abord par ses règles de syntaxe : il n’existe pas de bonne ou mauvaise façon d’écrire du HTML dans l’absolu ; tout dépend du contexte d’usage et des besoins de l’auteur.

Prenons un exemple. Les guillemets autour de la valeur d’un attribut sont omissibles dans plusieurs cas, mais en réalité ils sont presque toujours utilisés de manière systématique. On pourrait s’arrêter à ce constat mais il y a deux choses à apprendre ici.

La première, c’est que cette pratique majoritaire n’est pas due au hasard. C’est une habitude héritée de XHTML, une version de HTML définie via son cousin plus strict XML : comme dans tous les langages basés sur XML, les guillemets autour de la valeur de l’attribut sont obligatoires. Aujourd’hui, XHTML n’est pas très répandu ; mais il a profondément influencé la manière dont HTML est écrit.

Et la seconde chose intéressante à connaître, c’est qu’ïl y a de vrais arguments pour ou contre les guillemets. Certaines personnes observent qu’une syntaxe sans ambiguïté, c’est-à-dire ici qui inclut systématiquement les guillemets, rend le travail d’interprétation du navigateur plus simple et donc plus rapide (les documents chargent plus vite). D’autres argumentent en faveur d’une écriture simplifiée de HTML, qui omet les guillemets ou encore certaines balises fermantes. Cela complique le travail d’interprétation par les machines mais allège l’écriture, donc facilite la vie des auteurs. Et il y a encore d’autres arguments et contre-arguments dans ce débat.

Ce qu’il faut retenir, c’est qu’il peut être utile de s’intéresser au pourquoi des pratiques, afin d’apprendre réellement quelque chose et de ne pas répéter simplement ce qu’on a vu ailleurs.

L’autre aspect de la permissivité de HTML, c’est que les erreurs de syntaxe n’empêchent pas forcément l’affichage de la page. Contrairement à XML, qui ne tolère aucune erreur, HTML peut être interprété par un navigateur même si le code n’est pas parfaitement conforme à la spécification.

# CSS

CSS est le langage de mise en forme du Web. Alors que HTML combine le contenu et les instructions qui portent sur ce contenu (c’est le principe du balisage), CSS ne contient que des instructions. Comme pour un langage de programmation classique, on peut faire un parallèle assez juste entre un fichier CSS et une recette de cuisine.

CSS a une syntaxe différente de HTML, avec un nombre à peu près similaire de caractères spéciaux (peu nombreux). En revanche, son vocabulaire, toujours basé sur l’anglais, est beaucoup plus étendu, car il exprime tout le vocabulaire de la typographie, de la mise en forme, du design graphique, etc.

## Syntaxe

L’unité de base d’une feuille de styles CSS est la règle. Une règle repose sur un sélecteur, contient une ou plusieurs déclarations qui définissent chacune la valeur d’une propriété.

Exemple :

```css
h1 {
  color: red;
  font-size: 18px;
}
```

Cette règle établit que les éléments HTML `h1` (titres de niveau 1) doivent être de couleur rouge et avoir une taille de police de 18 pixels.

- `h1` est le sélecteur ;
- `color: red;` et `font-size: 18px;` sont des déclarations ;
- `color` et `font-size` sont des propriétés ;
- `red` et `18px` sont des valeurs de ces propriétés.

Une feuille de styles est généralement constituée d’une succession de règles.

Exemple :

```css
h1 {
  color: red;
  font-size: 18px;
}

p {
  color: black;
}
```

Comme en HTML, l’espace vide formé par les caractères d’espacement n’est pas interprété par le navigateur. Il est utilisé pour améliorer le confort de lecture et d’écriture. Ici non plus, il n’existe pas de bonne ou de mauvaise manière d’écrire, seulement des préférences personnelles et des considérations sur la taille des fichiers.

<!-- Continuer :

- sélecteurs qui ne sont pas des éléments : classes https://developer.mozilla.org/fr/docs/Learn/CSS/First_steps/Getting_started#ajouter_une_classe, identifiants, pseudo-classes, pseudo-éléments
- combinateurs https://developer.mozilla.org/fr/docs/Learn/CSS/First_steps/Getting_started#associer_sélecteurs_et_combinateurs

-->



## Gestion des règles contradictoires

CSS gère de manière très fine l’application des règles contradictoires. Ceci permet de définir des styles de manière globale et de créer des exceptions précises. Ceci repose sur plusieurs principes, qu’il faut connaître pour comprendre pourquoi on obtient tel ou tel résultat :

Héritage
: La plupart des valeurs de propriété se transmettent des éléments parents aux éléments enfants. Exceptions notables : largeur, marges, remplissage, bordures.

Cascade
: La dernière règle applicable dans l’ordre d’écriture s’applique en priorité.

Spécificité
: La règle applicable ayant le plus haut score de spécificité s’applique.

::: note
Le score de spécificité est calculé en additionnant la spécificité de tous les sélecteurs d’une règle. Du plus spécifique au moins spécifique, ce sont :

1. les styles en ligne (directement dans le HTML)
2. les sélecteurs d’identifiant
3. les sélecteurs de classe, d’attribut et de pseudo-classe
4. les sélecteurs d’élément et de pseudo-élément
:::

En plus de ces trois principes fondamentaux, deux autres mécanismes interviennent dans la gestion des règles contradictoires :

- À l’intérieur d’une règle, la valeur spéciale `!important` peut être appliquée à une déclaration. Celle-ci prévaut alors quel que soit le score de spécificité des autres règles applicables.
- Les navigateurs permettent aux utilisateurs d’appliquer des feuilles de styles. En cas de contradiction entre les règles contenues dans les feuilles de style du document et celles côté utilisateur, ces dernières s’appliquent en priorité.

Pour aller plus loin, consultez la page dédiée à la [gestion des règles contradictoires](https://developer.mozilla.org/fr/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance) sur le Mozilla Developer Network.

## Approche centrée HTML ou CSS ?

Il y a plusieurs approches possibles dans l’écriture de HTML et CSS. Il y a notamment une dichotomie utile à connaître en matière de mise en forme, [proposée par Adam Wathan](https://adamwathan.me/css-utility-classes-and-separation-of-concerns/), entre une approche plutôt centrée sur HTML et une autre plutôt centrée sur CSS.

Dans cette dichotomie, l’approche HTML-centrée consiste à mettre en forme le contenu en intervenant plutôt dans le code HTML via l’ajout de classes CSS. Les feuilles de style sont souvent réutilisables telles quelles, sans modification. Cette approche convient aux personnes qui préfèrent manipuler du HTML que du CSS, et qui aiment travailler avec un *design system*.

Exemple : la bibliothèque [Bootstrap](https://getbootstrap.com/), un système de classes qu’on peut copier tel quel pour ensuite modifier le HTML afin d’y appliquer les classes.

À l’inverse, l’approche CSS-centrée consiste à utiliser les sélecteurs CSS pour cibler et mettre en forme le contenu. Le HTML peut être re-stylé sans qu’il soit nécessaire de le modifier. Cette approche convient évidemment aux personnes qui ont une appétence pour les sélecteurs, ainsi qu’à celles qui préfèrent travailler avec du HTML générique, contenant le moins de classes possible.

Exemple : le site [CSS Zen Garden](http://www.csszengarden.com/).

::: note
À titre personnel, je pratique plutôt la seconde approche. Je suis sensible à l’idée d’un code HTML le plus sémantique et générique possible mais c’est surtout né d’une contrainte : presque tous les fichiers HTML sur lesquels je travaille sont générés par [Pandoc](pandoc.html) à partir de fichiers [Markdown](markdown.html), donc intervenir directement dans le HTML est généralement pénible ou impossible, et par conséquent j’ai dû apprendre à utiliser les sélecteurs plutôt qu’ajouter des classes dans le HTML. Or les sélecteurs en CSS, c’est un peu comme des [expressions régulières](expressions-regulieres.html) mais pour la mise en page : il faut trouver le bon motif pour « matcher » (faire correspondre) quelque chose. J’aime beaucoup les expressions régulières, j’en ai même fait un cours. Alors l’approche CSS-centrée m’a rapidement convaincu elle aussi.
:::

# JavaScript

::: note
Je ne maîtrise pas JavaScript, je ne l’aborderai donc pas de manière détaillée. Pour en savoir plus, n’hésitez pas à consulter [les pages dédiées à JavaScript](https://developer.mozilla.org/fr/docs/Learn/JavaScript) sur le Mozilla Developer Network.
:::

# Architecture d’un site

Un site est plus que la somme des pages qui le constituent. L’architecture d’un site inclut également (liste non exhaustive) :

- navigation (menus, listes) ;
- métadonnées ;
- flux (ex : RSS, Atom).

# Dépannage et rétro-ingénierie

La conception web est souvent itérative : on essaye quelque chose, on atteint en partie ou pas l’objectif, on réessaye, etc. Durant ce processus, on se heurte fréquemment à des résultats non attendus. Connaître les langages et leurs règles d’écriture permet de réduire cela mais jamais complètement. Dans ce contexte, certains outils fournissent une aide à la conception.

Pour savoir si des erreurs se sont glissées dans votre code HTML, utilisez un service tel que [le validateur du W3C](https://validator.w3.org). Le tutoriel HTML du Mozilla Developer Network inclut une section sur [l’interprétation des messages d'erreur](https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML/Debugging_HTML#interprétation_des_messages_derreur).

Pour faire la rétro-ingénierie d’une page Web, c’est-à-dire comprendre comment obtenir le même rendu (par exemple une mise en page que vous voulez imiter), utilisez la fonctionnalité du navigateur qui permet d’afficher le code source. Tous les sites obéissent au protocole HTTP : à partir du moment où la page s’affiche dans votre navigateur, c’est que le fichier HTML correspondant à été téléchargé sur votre machine, et donc vous pouvez en consulter le code. Il n’y a aucune exception.

Pour interagir avec le code de manière plus confortable directement dans le navigateur, utilisez les outils de développement (*developer tools*), notamment l’inspecteur web. En dépit de leur appellation, ces outils ne sont pas réservés aux développeurs professionnels mais s’adressent à toute personne ayant besoin d’interagir de manière avancée avec le code.

# Publication

La première chose à considérer avant de publier sur le Web, c’est la temporalité dans laquelle va s’inscrire la publication. Pour tout projet appelé à durer, je recommande d’acheter un nom de domaine. L’hébergement est beaucoup moins important que l’identification : peu importe sur quel serveur se trouvent les contenus, du moment que le nom de domaine et les URL ne changent pas. Le socle de l’identité en ligne, ce sont les URL, et la seule garantie de contrôler durablement les URL, c’est de posséder le domaine qui va avec.

> « *You are in control of your online identity if you own the URLs.* »
> <https://indieweb.org/web_hosting>

On peut utiliser un service d’hébergement avec un nom de domaine qu’on possède par ailleurs, ou bien louer simultanément nom de domaine et hébergement. Voici quelques services que je recommande par expérience personnelle ou suite à des retours positifs :

- Neocities (gratuit)
- Gandi, OVH, Ionos, O2Switch (payants)

Les données au format texte sont faciles à versionner. Le versionnement permet de disposer d’un historique qui agit notamment comme une sauvegarde en cas de problème. Certains services proposent simultanément le versionnement (souvent via Git) et la mise en ligne des fichiers (hébergement gratuit, avec possibilité d’utiliser un nom de domaine qu’on possède par ailleurs). Voici quelques recommandations :

- [GitHub Pages](https://pages.github.com/)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Framagit](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html)

# Liens utiles

Le portail **Mozilla Developer Network** constitue une référence incontournable selon moi. On y trouve des introductions rédigées, des tutoriels interactifs et des références exhaustives de tous les aspects des technologies du Web. À consulter en particulier :

- [Introduction à HTML](https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML)
- [Référence HTML](https://developer.mozilla.org/fr/docs/Web/HTML/Reference)
- [Introduction à CSS](https://developer.mozilla.org/fr/docs/Learn/CSS/First_steps/What_is_CSS)
- [Référence CSS](https://developer.mozilla.org/fr/docs/Web/CSS/Reference)

