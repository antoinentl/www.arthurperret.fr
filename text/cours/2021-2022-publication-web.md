---
title: Introduction à la publication web
author: Arthur Perret (Université Bordeaux Montaigne)
date: BUT Métiers du livre 2021-2022
toc-depth: 2
abstract: "Ce cours constitue une introduction pratique à la publication web. Aucune connaissance préalable n'est requise, les notions informatiques de base nécessaires pour comprendre le cours sont expliquées au début."
wip: true
updated: 11/04/2022
---

# Présentation rapide des technologies du Web

Les technologies sur lesquelles repose le Web sont définies par des standards, c'est-à-dire des spécifications techniques normalisées. Deux instances s'occupent de définir et de faire évoluer ces standards.

*Internet Engineering Task Force* (IETF)
: Organisation qui élabore les standards composant les couches basses de l’infrastructure et du réseau Internet. Ce travail repose sur la rédaction de *Request for comments* (RFC), des documents de spécification technique. Le protocole HTTP est défini par des RFC. L'IETF est gérée par une organisation à but non lucratif, l'Internet Society.

*World Wide Web Consortium* (W3C)
: Organisation créée par l’inventeur du Web, Tim Berners-Lee, pour élaborer les standards composant les couches hautes du Web. Le W3C gère notamment la spécification des normes techniques pour HTML, CSS et JavaScript, trois langages désignés collectivement sous l'expression « technologies du Web ». Le W3C a une gouvernance ouverte et prend ses décisions par consensus.

## Protocole HTTP

*Hypertext Transfer Protocol* (HTTP, littéralement « protocole de transfert hypertexte ») est un protocole de communication client-serveur développé pour le *World Wide Web*. HTTPS est la variante sécurisée de HTTP ; elle est de plus en plus utilisée. HTTP est un standard Internet, donc défini par des RFC produits au sein de l'IETF. Le but du protocole HTTP est de permettre un transfert de fichiers (essentiellement au format HTML), localisés grâce à une chaîne de caractères appelée URL, entre un navigateur (le client) et un serveur web.

L'expression « client-serveur » désigne un mode de transaction (souvent à travers un réseau) entre plusieurs programmes ou processus : l'un, qualifié de client, envoie des requêtes ; l'autre, qualifié de serveur, attend les requêtes des clients et y répond. « Client » désigne l'ordinateur sur lequel est exécuté le logiciel client, et « serveur » l'ordinateur sur lequel est exécuté le logiciel serveur. Les machines serveurs sont généralement dotées de capacités supérieures à celles des ordinateurs personnels afin de pouvoir répondre de manière efficace à un grand nombre de clients. Les machines clients sont souvent des ordinateurs personnels ou des terminaux mobiles (téléphone, tablette).

Lorsque le navigateur n'arrive pas à fournir la page demandée, il affiche un code de réponse. Celui-ci est constitué de trois chiffres. Le premier chiffre indique la catégorie d'erreur :

- 30x : redirection ;
- 40x : erreur client ;
- 50x : erreur serveur.

Les chiffres suivants indiquent la nature exacte de l'erreur. Exemple : erreur 404, le serveur n'a rien trouvé à l'adresse spécifiée par le client ; erreur 503, le serveur ne peut pas répondre car le trafic est trop dense.

## HTML

*HyperText Markup Language* (HTML) est le langage de balisage conçu pour représenter les pages web. Ce langage permet d’écrire de l’hypertexte, de structurer sémantiquement la page, de mettre en forme le contenu, de créer des formulaires de saisie, d’inclure des ressources multimédias et de créer des documents interopérables et accessibles.

## CSS

*Cascading Style Sheets* (CSS) est un langage informatique qui décrit la présentation des documents HTML. En français, CSS se traduit par « feuilles de style en cascade ».

## JavaScript

JavaScript est un langage de programmation de scripts principalement employé dans les pages web interactives. À ce titre, JavaScript est une partie essentielle des applications web, c'est-à-dire des sites proposant des interactions complexes (authentification, transaction, personnalisation…).

## Navigateur

Un navigateur web est un logiciel conçu pour consulter et afficher le *World Wide Web*. C'est un client HTTP, c'est-à-dire un logiciel conçu pour se connecter à un serveur HTTP. Il possède un moteur de rendu des standards du Web, c'est-à-dire un composant logiciel qui transforme un document HTML, ainsi que toutes les autres ressources associées à la page, en une représentation visuelle interactive pour l'utilisateur. Il peut également inclure un moteur JavaScript, c'est-à-dire un composant logiciel qui interprète et exécute du code en langage JavaScript.

La modularité des navigateurs, c'est-à-dire leur organisation en différents composants, permet d'activer ou désactiver certaines fonctionnalités de manière sélective : par exemple, on peut désactiver le rendu JavaScript (pour des questions de sécurité), et même le rendu CSS.

Les navigateurs permettent d'inspecter le code source de la page. Ceci affiche le code HTML avant son rendu par le navigateur. Il est également possible d'utiliser des outils de développement pour examiner le code source de manière plus précise et plus interactive, et même modifier l'affichage de la page. Ces outils constituent un excellent moyen de faire la rétro-ingénierie d'une page, que ce soit dans une logique d'apprentissage ou de recherche et développement.

# Typologies de la publication web

Il existe différentes manières de catégoriser les formes de publication sur le Web, selon qu'on examine des objets ou des processus, et selon qu'on se place dans une perspective informatique (les possibilités offertes par le médium) ou dans la perspective de l'activité de publication au sens large.

| | objets | processus |
|-|-|-|
| informatique | fichiers | programmes, protocoles |
| publication | formes éditoriales | activités éditoriales |

## Objets (perspective informatique)

### Fichiers HTML

Le standard HTML définit des éléments. Chaque élément permet de décrire un type de contenu.

Exemples :

élément HTML | type de contenu
---|---
`p` | paragraphe
`a` | lien hypertexte
`img` | image

Si on se place dans une perspective technique, les formes publiables sur le Web sont donc d'abord constituées des objets représentés par chaque élément HTML.

Le standard HTML ne range pas explicitement les différents éléments dans des catégories, mais rien n'empêche de le faire pour mieux s'y retrouver. Par exemple, la documentation proposée par Mozilla, [MDN Web Docs](https://developer.mozilla.org/fr/), inclut [une page de référence qui liste tous les éléments HTML](https://developer.mozilla.org/fr/docs/Web/HTML/Element) en les regroupant dans les catégories suivantes :

- racine principale
- métadonnées du document
- racine de sectionnement
- sectionnement du contenu
- contenu textuel
- sémantique du texte en ligne
- images et médias
- contenu embarqué
- SVG et MathML
- scripts
- gestion de l'édition
- contenu tabulaire
- formulaires
- éléments interactifs
- web components
- éléments obsolètes ou dépréciés

Certaines de ces catégories sont liées au fonctionnement technique de HTML. Par exemple, « racine principale » a trait à la structure arborescente d'un document HTML.

Mais plusieurs catégories nous renseignent directement sur les objets qu'on peut publier sur le Web : métadonnées, texte, images, tableaux, formulaires.

Et certaines catégories comme « contenu embarqué » suggèrent que le Web permet d'inclure des choses qui ne sont pas forcément des éléments HTML.

### Autres fichiers

Les objets publiables sur le Web ne sont pas limités à des pages web rédigées en HTML. Le protocole HTTP permet également aux clients et aux serveurs de s'échanger des fichiers de tous types.

Presque tous les téléchargements de fichiers à partir de sites Web se font aujourd'hui via HTTP.

::: résumé
Le Web repose sur le standard HTML. Celui-ci définit explicitement un certain nombre d'objets publiables (texte, images, éléments interactifs). À chaque type d'objet correspond un élément HTML. Mais il est également possible d'inclure des contenus exprimés dans d'autres formats, grâce à la transclusion, et de distribuer des fichiers via le téléchargement direct. En pratique, il est donc possible de publier énormément de choses via le Web.
:::

***

## Processus informatiques

Les objets (pages et fichiers) publiés sur le Web font l'objet de nombreux échanges organisés par des programmes et des protocoles. Ces processus incluent la génération des pages web, le transfert de fichiers vers les serveurs ou encore la syndication de contenus.

### Génération de pages web

Les pages distribuées au moment d'une requête HTTP ne sont pas toutes fabriquées de la même manière. On distingue trois modes de génération des pages web :

Dynamique
: On parle de site web dynamique lorsque les pages sont générées au moment de la réception des requêtes. Dans cette configuration, le créateur du site ne fabrique pas lui-même des fichiers HTML mais interagit avec une base de données dans laquelle sont stockés les contenus (textes, médias…). Lorsque le serveur reçoit une requête, il génère la page demandée et la renvoie au navigateur, qui la télécharge puis l'affiche. Ceci nécessite d'installer et de maintenir sur le serveur un environnement de génération (comme php).
: L'intérêt d'un site dynamique est de pouvoir adapter le contenu à l'internaute. C'est le fonctionnement de la plupart des applications web, mais également de plateformes de publication comme WordPress.

Statique
: On parle de site web statique lorsque les pages sont déjà prêtes indépendamment des requêtes. Lorsque le serveur reçoit une requête, il renvoie directement la page demandée au navigateur, sans avoir à la générer à la volée.
: L'intérêt d'un site statique par rapport au site dynamique est une plus grande simplicité de mise en œuvre et un plus faible coût en ressources.

Hybride
: Il est possible d'inclure des éléments dynamiques dans une page statique. Ceci repose généralement sur l'utilisation d'une API (*application programming interface*), c'est-à-dire un ensemble de définitions et de protocoles qui facilitent l'interaction entre logiciels.
: Par exemple, vous pouvez afficher sur votre blog un encadré rempli automatiquement avec vos dernières publications sur un réseau social comme Twitter. Pour ce faire, votre site et Twitter communiquent via l'API de Twitter, qui sert à faire transiter les données. La mise en œuvre passe souvent par un script en JavaScript intégré à la page.

### Transfert de fichiers

Le protocole HTTP est utilisé pour la plupart des téléchargements de fichiers. Mais il existe d'autres protocoles pour transférer des fichiers à destination du Web. Les plus courants empruntent Internet, la couche basse du réseau, pour alimenter en contenus les serveurs accessibles via le Web, la couche haute.

FTP (*File Transfer Protocol*) est l'un de ces protocoles. Il est destiné à transférer des fichiers entre clients et serveurs via Internet. Son utilisation la plus commune est de transférer les fichiers d'un site web au serveur sur lequel il est hébergé. Il est utilisable facilement via des logiciels libres et gratuits, comme par exemple [FileZilla](https://filezilla-project.org).

### Syndication de contenus

La syndication est un processus qui consiste à diffuser ses contenus via des acteurs tiers. C'est un terme utilisé traditionnellement pour la télévision par exemple : plusieurs chaînes peuvent diffuser un même contenu, par exemple une série, qui est alors dite « syndiquée ».

Sur le Web, la syndication désigne le fait de diffuser les contenus d'un site sur d'autres sites. Cela peut être fait à la main, par exemple en postant un lien vers un contenu sur un réseau social. Mais cela se fait souvent en créant un flux de données.

Un flux web se présente généralement sous la forme d'un fichier, rédigé dans un format de syndication comme RSS ou Atom. Le contenu de ce fichier est constitué d'entrées successives qui représentent chacune une publication. Chaque entrée peut être décrite par des métadonnées (titre, auteur, date de publication). Elle peut contenir un extrait de la publication, voire sa totalité.

Un flux publié sur le Web a une URL qui permet de le consulter, souvent par le biais d'une application spécialisée (lecteur de flux). La syndication permet ainsi de souscrire manuellement à des sources de contenus, sans nécessairement passer par une plateforme intermédiaire.

## Formes et pratiques (perspective scientifique)

En tant que discipline scientifique, l'informatique étudie les aspects fondamentaux du traitement automatique de l'information, et cherche des moyens d'innover (exemple : intelligence artificielle).

Les sciences de l'information et de la communication sont spécialistes des mêmes objets que l'informatique mais les étudient différemment. Elles vont s'intéresser aux catégories suivantes :

- signes (sémiologie) ;
- supports (médiologie) ;
- documents (documentation, documentologie) ;
- médias (communication, communicologie).

Vous pouvez trouver de nombreux travaux de recherche qui étudient ces formes et les pratiques qui sont associées. Le but est généralement d'en comprendre les implications à l'échelle individuelle et sociale (exemple : analyser les phénomènes d'aliénation et d'émancipation liés au numérique).

# Outils

Comme pour les types de contenus, on peut décrire la méthodologie de la création de sites web de différentes manières. Ici, nous décrivons deux grandes approches (fabrication manuelle et génération automatique) à travers les principaux types d'outils disponibles pour les mettre en œuvre : les éditeurs et les systèmes de gestion de contenus (*content management system*, CMS).

## Outils de fabrication manuelle

Fabriquer manuellement une ou plusieurs pages web consiste à créer chaque page soi-même, en réalisant chaque élément un par un dans chaque page. Dans cette approche, utiliser des modèles ainsi que le copier-coller permet de gagner du temps, mais toute opération de création ou de modification passe par la manipulation des éléments un par un.

On peut faire une analogie entre la fabrication web manuelle et la mise en forme manuelle (sans utiliser de styles) dans un traitement de texte.

### Éditeur WYSIWYG

Un éditeur WYSIWYG permet de fabriquer une page Web en élaborant simultanément la structure (architecture), les contenus (texte, médias) et la forme (styles) dans une interface qui simule le rendu d'un navigateur web. C'est le même principe que dans un traitement de texte mais appliqué à des pages web plutôt qu'à des feuilles A4.

Les avantages d'un éditeur WYSIWYG sont la facilité d'utilisation et l'observation immédiate du rendu graphique. Ses désavantages sont le manque de maîtrise sur la qualité du document HTML produit et les risques d'incompatibilité avec des navigateurs non prévus par l'éditeur.

L'utilisation de ces outils n'est pas recommandée aux personnes qui souhaitent s'investir durablement dans une activité de conception web, puisqu'ils ne permettent pas de comprendre et maîtriser les technologies sous-jacentes (HTML, CSS, JavaScript).

### Éditeur de texte

Un éditeur de texte permet d'afficher et d'éditer des fichiers au format texte. Tous les langages du Web s'expriment au format texte, il est donc possible de fabriquer une page web en créant directement des fichiers HTML, CSS et JavaScript dans un éditeur de texte.

Les interfaces des éditeurs de texte varient selon le public visé. Certains éditeurs s'adressent en priorité aux développeurs, en imitant les environnements dont ils sont déjà familiers (ex : ligne de commande, configuration via fichiers texte). D'autres suivent plutôt les règles d'interface du système d'exploitation (menus, boutons, fenêtres), ce qui rend leur prise en main plus facile aux non-développeurs.

Un éditeur de texte peut être spécialisé dans le traitement de certains fichiers, en incluant des fonctionnalités pensées pour faciliter spécifiquement leur manipulation. Il existe ainsi quelques éditeurs spécialisés dans la fabrication web.

### Éditeur hybride

Un éditeur hybride WYSIWYG format texte permet d'afficher le contenu dans l'un ou l'autre des deux modes. La cohabitation des deux reste cependant compliquée. En pratique, ces outils sont peu populaires.

### Service en ligne

De nombreux services en ligne proposent des interfaces de création de site web, couplées à de l'hébergement. Pour la plupart de ces services, l'outil de création proposé est un constructeur visuel, avec interface WYSIWYG, glisser-déposer, etc. 

## Outils de génération automatique

L'inverse de la fabrication manuelle est la génération automatique. Cela consiste à définir séparément la structure des pages (sous forme de gabarits ou modèles), les contenus et les styles ; un logiciel se charge ensuite de créer les pages d'après ces éléments.

### CMS avec base de données

Un CMS avec base de données est un logiciel capable de générer un site à partir de contenus stockés dans une base de données, et qui fournit une interface pour gérer ces contenus. La plupart des CMS avec base de données génèrent des sites de manière dynamique.

### CMS sans base de données

Un CMS sans base de données (*flat-file CMS*) est un logiciel qui permet de créer des contenus stockés directement dans leur format de destination (HTML), par l'intermédiaire d'une interface de gestion mais sans passer par une base de données. Un CMS sans base de données génère exclusivement des sites statiques.

### Générateur de site statique

Un générateur de site statique est un logiciel qui permet de créer des contenus stockés directement dans leur format de destination (HTML), par l'intermédiaire de fichiers de configuration au format texte, sans base de données et généralement sans interface de gestion.

### CMS sans générateur

Un CMS sans générateur (*headless CMS*) est un logiciel qui permet de gérer des contenus destinés à être utilisés par un générateur de site statique.

L'existence des CMS sans générateur apporte une grande flexibilité dans le choix d'outils de création de sites statiques. Associés aux générateurs, ils permettent de reproduire le fonctionnement d'un CMS sans base de données, tout en choisissant précisément quels outils combiner ensemble.

# Exercices

Voici quelques exercices pour prendre en main les bases de HTML et CSS.

**N'utilisez pas de traitement de texte type Microsoft Word, LibreOffice Writer ou Google Docs.** Ces logiciels sont inadaptés aux langages comme HTML, qui nécessitent que vous contrôliez l'insertion de tous les caractères, en particulier les espaces, la ponctuation et les guillemets.

**Utilisez un éditeur de texte**, comme Bloc-notes, Notepad++, Sublime Text, Atom, BBEdit, GEdit, etc.

## Exercice 1

Créez un fichier HTML. Sa première ligne doit contenir le doctype : `<!DOCTYPE html>`

Il doit également contenir les éléments HTML suivants :

- `html`
- `head`
- `title`
- `meta`
- `body`
- `h1`
- `p`

Pour connaître le rôle de chaque élément, consultez cette page de référence :

<https://developer.mozilla.org/fr/docs/Web/HTML/Element>

Le contenu des éléments est libre : mettez ce que vous voulez ! En dehors des éléments non répétables (`html`, `head`, `title`, `body`), vous pouvez répéter un élément autant de fois que vous voulez. Vous pouvez également mettre d'autres éléments si vous voulez.

Notez la distinction entre ce qui se trouve dans le `head` (contenu invisible, indications destinées au navigateur, métadonnées) et le `body` (contenu visible sur la page).

<details>
<summary>Correction (cliquer pour afficher)</summary>
```html
<!DOCTYPE html>
<html>
<head>
<title>Titre de la page</title>
<meta name="description" content="Une page web.">
</head>
<body>
<h1>Un titre</h1>
<p>Du texte.</p>
<p>Encore du texte.</p>
</body>
</html>
```
</details>

## Exercice 2

**N'oubliez pas :** n'utilisez pas de traitement de texte type Microsoft Word, LibreOffice Writer ou Google Docs, utilisez un éditeur de texte comme Bloc-notes, Notepad++, Sublime Text, Atom, BBEdit, etc.

::: note
Pour Notepad++, voici trois réglages utiles :

1. Si le logiciel n'est pas en français, cliquez sur *Settings* › *Preferences* › *General* › *Localization* et sélectionnez le français.
2. Cliquez sur *Réglages* › *Préférences* › *Autocomplétion* › *Insertion automatique* › *Balise HTML/XML fermante* pour que Notepad++ insère automatiquement la balise fermante après avoir saisi une balise ouvrante.
3. Cliquez sur *Affichage* › *Retour à la ligne automatique* pour activer le repli des lignes qui dépassent la largeur de la fenêtre.
:::

Créez un dossier `exercice` avec deux sous-dossiers : `images` et `styles`. Dans `exercice`, créez un fichier `index.html`.

Ajoutez au moins un exemplaire de chaque chose suivante au fichier HTML :

- Un lien hypertexte vers la page de votre choix.
- Une image hébergée sur le Web (le chemin de l'image doit être une URL).
- Une image téléchargée et placée au même endroit que le fichier `index.html`.
- Une image téléchargée et placée dans le sous-dossier `images`.
- Une feuille de styles `styles.css` placée dans le sous-dossier `styles`.

<details>
<summary>Correction (cliquer pour afficher)</summary>
```html
<!DOCTYPE html>
<html>
<head>
<title>Titre de la page</title>
<meta name="description" content="Une page web.">
<link rel="stylesheet" href="styles/styles.css">
</head>
<body>
<h1>Un titre</h1>
<p>Un <a href="https://developer.mozilla.org/fr/">lien</a>.</p>
<img src="http://exemple.com/image.jpg">
<img src="image.jpg">
<img src="images/image.jpg">
</body>
</html>
```
</details>

## Exercice 3

Faites une copie du dossier précédent. Vous devriez avoir un fichier CSS dans le dossier `styles`. Ouvrez ce fichier et modifiez-le comme indiqué ci-dessous, en vous aidant de la [page d'introduction à CSS du MDN](https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/CSS_basics).

- Modifiez la mise en forme des paragraphes. Commencez par modifier une propriété, comme la couleur. Puis, modifiez plusieurs propriétés, selon votre choix (expérimentez !).
- Appliquez une même déclaration CSS à plusieurs éléments HTML d'un coup.
- Ajoutez une classe à un élément et appliquez une déclaration CSS à cette classe. Par exemple, si votre page commence par un `h1`, ajoutez-lui l'attribut `class` avec une valeur comme `titre` et appliquez des déclarations à la classe `titre` dans votre feuille de styles.
- Modifiez l'élément `body` : donnez lui une largeur maximale (`max-width`) en pixels (`px`) et centrez-le grâce aux marges (`margin`).
- Modifiez la police pour au moins un élément.

<details>
<summary>Correction (cliquer pour afficher)</summary>
```css
p {
  color: #111111;
  text-align: center;
}
h1, p {
  font-family: Georgia, serif;
}
.titre {
  font-size: 2em;
  line-height: 1.1;
}
body {
  max-width: 900px;
  margin: 0 auto;
}
```
</details>

## Exercice 4

**Cet exercice est la synthèse des trois précédents. C'est un entraînement (non noté) pour l'exercice final (qui lui sera noté).**

Créez un dossier et nommez-le d'après votre nom et prénom, comme ceci : `NomPrénom` (pas d'espaces).

Téléchargez au moins deux images et placez-les dans `NomPrénom`.

Dans `NomPrénom`, créez un fichier `index.html`. Il doit contenir les éléments suivants :

- `html`
- `head`
- `title`
- `link`
- `meta` (2 minimum)
- `body`
- `h1` (2 minimum)
- `h2` (2 minimum)
- `p`
- `a`
- `img` (2 minimum)
- `section` (2 minimum)

Dans `NomPrénom` toujours, créez un fichier `styles.css`. Il doit modifier ainsi la mise en forme du fichier HTML :

- Donnez une largeur maximale (`max-width`) de `700px` à l'élément `body` et centrez-le grâce aux marges.
- Changez la police de l'élément `body`.
- Définissez l'interligne (`line-height`) à `1.5` pour l'élément `body`.
- Définissez une interligne plus réduite pour les titres (`h1`, `h2`).
- Modifiez la couleur des liens (`a`) lorsqu'ils sont survolés (pseudo-classe `:hover`).
- Donnez une largeur maximale aux images pour qu'elles ne soient pas plus larges que `body`.
- Donnez une bordure (`border`) aux sections (`section`).

**Bonus** (facultatif) :

- Changez la couleur des paragraphes qui suivent immédiatement un titre de niveau 1 (utilisez le [combinateur](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Selectors#les_combinateurs) `+`).
- Ajoutez un fleuron `❦` après chaque élément `section` (utilisez le [pseudo-élément](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Selectors#les_pseudo-éléments) `::after`).
- Définissez une couleur de fond pour l'élément `html`, appliquée uniquement lorsque la fenêtre fait au maximum 700px de large (utilisez une [requête média](https://developer.mozilla.org/fr/docs/Web/CSS/@media) `@media` avec la condition `max-width`). Modifiez les dimensions de la fenêtre pour vérifiez que cela fonctionne.

::: note
Ces trois consignes bonus ne sont pas obligatoires. Mais si vous avez réussi tout le reste, essayez de les faire. Les liens donnés pour chaque consigne bonus mènent vers des pages explicatives avec des exemples. Tester ces manipulations un peu plus avancées vous donnera une idée du potentiel que recèle CSS.
:::

Faites un clic droit sur le dossier `NomPrénom` et compressez-le au format ZIP. Envoyez le fichier `NomPrénom.zip` à [arthur.perret@u-bordeaux-montaigne.fr](mailto:arthur.perret@u-bordeaux-montaigne.fr).

**Le rendu n'est pas noté mais est obligatoire : une absence de rendu entraîne un malus sur la note finale.**

