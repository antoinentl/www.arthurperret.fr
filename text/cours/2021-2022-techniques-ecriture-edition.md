---
title: Techniques d’écriture et d’édition
author: Arthur Perret (Université Bordeaux Montaigne)
date: DUT Infonum 2021-2022
abstract: "Ce cours correspond au module Pratiques professionnelles spécialisées du Programme pédagogique national en Information-communication. L’objectif du module est d’« approfondir les techniques professionnelles pour permettre à l’étudiant de développer ses compétences dans le domaine de spécialité auquel il se destine ». Ici, j'ai choisi d'orienter le cours sur l'automatisation des tâches d'écriture et d'édition. Ces tâches sont omniprésentes dans les spécialités du domaine info-com ; leurs bases sont enseignées dans plusieurs modules de la filière, de la bureautique au Web en passant par le traitement de données textuelles. Il est donc plutôt opportun de consacrer un cours à consolider ces acquis, y ajouter de nouvelles techniques, et combiner tout cela dans une situation complexe. C'est l'objet des différentes parties de ce cours."
css: 2021-2022-techniques-ecriture-edition.css
updated: 10/04/2022
---

# Infos pratiques

## Calendrier

Le cours a lieu le mardi matin. Il se déroule en 10 séances pour un total de 28 heures.

## Modalités d'évaluation

Le cours fait l'objet d'une évaluation finale sous la forme d'un TP noté en présentiel.

Le cours est rythmé par des TP non notés mais dont le rendu est obligatoire. Chaque TP non rendu sans justification entraîne un malus sur la note de l'évaluation finale.

# Bureautique avancée

- Quelques astuces d'interface. Exemple : sélection multiple non continue.
- Structurer le document avec les styles de paragraphe.
- Numérotation automatique des titres.
- Table des matières automatique.
- Styles de page et enchaînements de styles.
- Insertion de champs de texte remplis automatiquement.
- Quelques notions de typographie et orthotypographie.

## Bureautique avancée — TP1

### Objectifs

Dans ce TP, nous allons passer en revue les premières possibilités d'automatisation offertes par les styles dans un logiciel de type traitement texte (LibreOffice Writer). À partir d'un texte non stylé, il va s'agir d'appliquer et de mettre à jour des styles, puis d'utiliser quelques fonctionnalités : numérotation des titres, génération d'une table des matières.

### Préparation

Ce TP nécessite LibreOffice Writer. Si vous travaillez sur une machine personnelle et que vous n'avez pas déjà installé LibreOffice, [cliquez ici](https://fr.libreoffice.org/download/telecharger-libreoffice/), choisissez votre système d'exploitation et cliquez sur « Télécharger ». Au premier lancement de LibreOffice, si l'interface du logiciel est en anglais, [cliquez ici](https://download.documentfoundation.org/libreoffice/stable/7.1.4/mac/x86_64/LibreOffice_7.1.4_MacOS_x86-64_langpack_fr.dmg) pour télécharger l'interface traduite en français, puis installez-la. **Avertissement pour les utilisateurs de macOS :** lancez bien une première fois LibreOffice avant d'installer l'interface traduite.

Le texte que nous allons utiliser est un article du Site du Zéro, un site de tutoriels qui est l'ancêtre du site de cours en ligne OpenClassrooms. [Cliquez ici](orthotypographie.txt){download="orthotypographie.txt"} pour récupérer le fichier `orthotypographie.txt`. Ouvrez-le avec un éditeur de texte comme Bloc-notes, Notepad++ (Windows) ou TextEdit, BBEdit (macOS) et copiez le contenu du fichier. Puis, lancez LibreOffice, créez un nouveau document et collez-y le contenu du fichier.

### Appliquer et modifier des styles

Ouvrez le panneau des styles : vous pouvez soit choisir Styles › Gérer les styles, soit utiliser le raccourci clavier indiqué pour cette action, soit cliquer sur l'icône des styles dans la barre latérale droite. Vérifiez que le mode d'affichage réglable en bas du panneau est bien sur « Hiérarchie ».

Appliquez le style « Titre principal » au titre du texte, le style « Titre 1 » aux titres de sections et le style « Titre 2 » aux titres de sous-sections.

::: note
**Pour savoir quelles lignes correspondent à des titres**, consultez [le texte original en ligne](http://sdz.tdct.org/sdz/l-orthotypographie-bien-ecrire-pour-bien-etre-lu.html).

**Pour sélectionner plusieurs lignes séparées**, maintenez la touche Cmd (Mac) ou Ctrl (Windows) enfoncée en même temps que vous sélectionnez les lignes.

**Rappel du fonctionnement de la sélection :**

- 1 clic : place le curseur à l'emplacement du clic
- double clic : sélectionne le mot
- triple clic : sélectionne la ligne
- quadruple clic : sélectionne le paragraphe
:::

Faites clic droit › Modifier sur le style « Titre » (il s'agit du style sous lequel sont regroupés tous les autres styles dont le nom commence par « Titre… »). Dans l'onglet Police, sélectionnez « Source Sans Pro », « Gras » puis cliquez sur OK.

> **Question :** pourquoi les paragraphes stylés avec « Titre principal », « Titre 1 » et « Titre 2 » se retrouvent modifiés alors que vous avez modifié seulement le style « Titre » ? *(Indice : faites clic droit › Modifier sur l'un de ces styles et consultez l'onglet Gestionnaire.)*

Faites clic droit › Modifier sur le style « Style de paragraphe par défaut ». Dans l'onglet Retraits et espacement › Espacement, réglez Sous le paragraphe sur « 0,20 ». Dans l'onglet Police, sélectionnez « Source Serif Pro ». Cliquez sur OK.

### Générer une table des matières automatique

Cliquez sur Outils › Numérotation des chapitres › Numérotation.

Sélectionnez le niveau 1. Réglez le champ Nombre sur « 1, 2, 3… ». Vérifiez que Style de paragraphe est réglé sur « Titre 1 ».

Sélectionnez le niveau 2. Réglez le champ Nombre sur « 1, 2, 3… » et le champ Afficher les sous-niveaux sur « 2 ». Vérifiez que « Style de paragraphe » est réglé sur « Titre 2 ».

Cliquez sur OK.

Effacez la table des matières présente au début du fichier, y compris le titre « Table des matières ». Il doit rester une simple ligne vide entre le titre principal et le premier titre de section.

Cliquez sur Insertion › Table des matières et index › Table des matières, index ou bibliographie.

Dans Type › Créer un index ou table des matières, réglez « Évaluer jusqu'au niveau » sur « 1 ».

Cliquez sur OK.

### Rendu

Cliquez sur Fichier › Enregistrer et nommez votre fichier **en respectant la forme suivante** : « 2022-01-25 Nom Prénom Bureautique Avancée TP1.odt ».

Cliquez sur Fichier › Exporter vers › Exporter au format PDF… puis dans Général › Structure, cochez « Exporter le plan et autre éléments de structure ». Cliquez sur Exporter. Vérifiez que le PDF ainsi créé affiche un sommaire cliquable sur la première page, mais également un sommaire déroulant et cliquable complet via l'interface du lecteur de PDF.

Déposez les deux fichiers (odt et pdf) sur le serveur de l'IUT, dans le répertoire dédié au TP.

## Bureautique avancée — TP2

### Objectifs

Ce TP aborde des fonctionnalités avancées de mise en page via les styles dans un traitement de texte : les enchaînements entre styles, le remplissage des en-têtes et pieds de page, ainsi que l'empagement.

### Préparation

Faites une copie de la correction du TP1 et renommez-la « 2022-01-25 Nom Prénom Bureautique avancée TP2.odt ». Ouvrez le document avec LibreOffice.

### Enchaînements

Modifiez les styles de paragraphe suivants d'après les indications :

Style de paragraphe par défaut
: Enchaînements › Options, cochez les cases qui permettent d'activer le traitement des lignes veuves et orphelines, réglez-les sur 2.

Titre 1
: Enchaînements › Sauts › Insérer, type « Page », position « Avant », avec le style de page « Première page ».

Titre de table des matières
: Enchaînements › Sauts › Insérer, type « Page », position « Avant », avec le style de page « Première page ».

Modifiez les styles de page suivants d'après les indications :

Première page
: Gestionnaire › Style › Style de suite : « Page gauche ».
: Page › Paramètres de mise en page › Mise en page : « Droite uniquement ».

Page gauche
: Gestionnaire › Style › Style de suite : « Page droite ».
: Page › Paramètres de mise en page › Mise en page : « Gauche uniquement ».

Page droite
: Gestionnaire › Style › Style de suite : « Page gauche ».
: Page › Paramètres de mise en page › Mise en page : « Droite uniquement ».

### En-tête et pied de page

Définissez le titre du document : Fichier › Propriétés › Description › Titre, valeur « Tutoriel : L'orthotypographie : bien écrire pour bien être lu ».

Remplissez les en-têtes :

1. Cliquez dans la zone d'en-tête d'une page de gauche (ni une première page, ni une page vide, ni une page de droite). Sur le bouton bleu *En-tête*, cliquez sur le symbole + pour créer l'en-tête. Cliquez sur Insertion › Champ › Titre.
2. Cliquez dans la zone d'en-tête d'une page de droite (ni une première page, ni une page vide, ni une page de gauche). Sur le bouton bleu *En-tête*, cliquez sur le symbole + pour créer l'en-tête. Cliquez sur Insertion › Champ › Autres champs… › Document, type « Chapitre », format « Nom de chapitre ».
3. Modifiez le style de paragraphe « En-tête » : centré, en italique, 11 pt.
4. Modifiez les styles de page « Page droite » et « Page gauche » : réglez En-tête › Hauteur sur 1 cm.

Remplissez les pieds de page :

1. Cliquez dans la zone de pied de page d'une page de gauche. Sur le bouton bleu *Pied de page*, cliquez sur le symbole + pour créer le pied de page. Cliquez sur Insertion › Numéro de page. Faites de même dans une page de droite.
2. Modifiez le style de paragraphe « Pied de page » pour qu'il soit centré, 11 pt.
3. Modifiez les styles de page « Page droite » et « Page gauche » : réglez Pied de page › Hauteur sur 1 cm.

Vérifiez que les pages ayant le style « Première page » n'ont ni en-tête ni pied de page.

### Empagement

Modifiez les marges des styles de page suivants (dans les réglages de chaque style, Page › Marges) :

Style de page | Gauche | Droite | Haut | Bas
--------------|---|---|---|---
Première page | 2,8 | 4,2 | 3,5 | 4,9
Page droite | 2,8 | 4,2 | 2,5 | 3,9
Page gauche | 4,2 | 2,8 | 2,5 | 3,9

> *Explication : les valeurs de base de l'empagement sont celles utilisées pour le style « Première page ». Mais les en-têtes et pieds de page faussent les calculs, car ils ne sont pas compris dans les marges. La solution consiste alors à définir manuellement la hauteur des en-têtes et pieds de page, et retrancher cette hauteur aux marges hautes et basses sur les styles de page où ils sont présents.*

### Export

Cliquez sur Fichier › Exporter vers › Exporter au format PDF pour ouvrir la fenêtre Options PDF. Dans cette fenêtre, cochez les options suivantes :

- Général › Archive PDF/A
- Général › Accessibilité Universelle
- Structure › Exporter le plan et autres éléments de structure
- Structure › Exporter les pages vides insérées automatiquement

Cliquez sur Exporter.

> *Un avertissement concernant l'accessibilité des liens peut apparaître. Pour le moment (LibreOffice 7) il n'y a pas de solution pour traiter cet avertissement, donc il faut l'ignorer et cliquer sur OK.*

> *LibreOffice Writer mémorise ces réglages. Une fois que vous les avez définis pour le document, vous pouvez cliquer sur Fichier › Exporter › Exporter directement au format PDF pour passer directement au choix du nom et de l'emplacement.*

### Rendu

Déposez les fichiers odt et pdf sur le serveur de l'IUT.

## Bureautique avancée — TP3

### Objectifs

Ce TP aborde rapidement les fonctionnalités de recherche et remplacement dans un traitement de texte

### Préparation

Faites une copie de la correction du TP1 et renommez-la « 2022-02-01 Nom Prénom Bureautique avancée TP3.odt ». Ouvrez le document avec LibreOffice.

### Exercice

Dans un traitement de texte, la fonction Rechercher lit le document à partir de la position du curseur, puis en arrivant à la fin du document, elle recommence au début du document, jusqu'à rencontrer à nouveau le curseur.

Cliquez au début du titre pour placer le curseur au tout début du document.

Cliquez sur *Édition* › *Rechercher et remplacer*.

Vérifiez que seules les options suivantes sont cochées : *Respecter la casse*, *Sensible aux diacritiques*. Ceci force le moteur de recherche à prendre en compte les majuscules et les caractères accentués dans votre requête.

**Effectuez les recherches et remplacements suivants :**

Remplacez « bonjour » par « Bonjour ».

Remplacez « Mac OS X » par « macOS ».

Remplacez l'apostrophe droite « `'` » par l'apostrophe courbe « ’ ».

Remplacez le tiret cadratin « — » par le tiret demi-cadratin « – », **sauf dans la phrase** « il peut être cadratin — large comme un M, demi-cadratin — large comme un N » (page 5), où le premier tiret doit bien rester un tiret cadratin, seul le deuxième tiret doit être modifié.

::: note
Il existe trois longueurs de tiret : le plus court, qu'on appelle tiret du six en référence à sa position sur les claviers de PC Windows « - », le tiret demi-cadratin « – » qui a la longueur d'un N, et le tiret cadratin « — » qui a longueur d'un M.

Sur Windows, pour insérer les tirets cadratin et demi-cadratin, qui ne sont pas accessibles directement au clavier, deux solutions :

Soit passer par un raccourci clavier, avec le pavé numérique : saisissez `Alt` + `0151` pour le tiret cadratin, `Alt` + `0150` pour le tiret demi-cadratin.

Soit passer par l'application *Table des caractères* dans Windows pour les insérer manuellement. Dans la Table des caractères, cochez *Affichage avancé*. Dans le champ *Rechercher*, saisissez le nom ou une partie du nom du caractère que vous cherchez. Les caractères correspondants s'affichent dans la grille au-dessus. Vous pouvez naviguer entre les caractères avec les flèches du clavier. Quand vous avez localisé le bon caractère, cliquez sur *Sélectionner* puis *Copier*. Le caractère se trouve maintenant dans votre presse-papiers. Vous pouvez le coller là où vous en avez besoin en faisant `Ctrl` + `C`.
:::

Remplacez l'espace qui précède un deux-points « : » par une espace insécable.

::: note
Sur Windows, pour insérer une espace insécable, qui n'est pas accessible directement au clavier, deux solutions :

Soit passer par un raccourci clavier, avec le pavé numérique : saisissez `Alt` + `255` ou bien `Alt` + `0160`.

Soit passer par l'application *Table des caractères* dans Windows pour les insérer manuellement.
:::

::: note
En typographie, le caractère qui permet d'insérer un intervalle vide s'appelle **une** espace (nom féminin). Voir [Espace (typographie) sur Wikipédia](https://fr.wikipedia.org/wiki/Espace_(typographie)).
:::

Remplacez également l'espace qui suit un guillemet ouvrant `«` par une espace insécable.

Remplacez également l'espace qui précède un guillemet fermant `»` par une espace insécable.

Remplacez les espaces précédant un point-virgule « ; », un point d'interrogation « ? » et un point d'exclamation « ! » par une espace insécable fine.

::: note
Sur Windows, pour insérer une espace insécable fine, avec le pavé numérique : saisissez `Alt` + `+202F`. **Attention :** vous avez bien lu, il y a un `+` à saisir après `Alt` et avant `202F`. Utilisez la touche `+` du pavé numérique.
:::

Pour finir : cherchez toutes les lignes qui ne finissent pas par un caractère de ponctuation. Comment faire ?

### Rendu

Déposez le fichier odt sur le serveur de l'IUT.

# Expressions régulières

Trouver une information précise dans un fichier informatique, comme le mot « bonjour », c'est plutôt simple : on ouvre ce fichier dans un programme doté d'une fonctionnalité de recherche, on saisit le mot « bonjour », et on obtient les occurrences de cette expression dans le fichier, s'il y en a.

Mais comment trouver le mot « bonjour » uniquement à un endroit précis, par exemple au début du texte ? Ce n'est pas possible avec une recherche classique.

Autre problème : comment trouver toutes les occurrences particulières d'un motif (par exemple, tous les numéros de téléphone dans un tableur, ou tous les URL dans un fichier HTML) ? Encore une fois, la recherche classique ne le permet pas.

Une solution possible, c'est d'utiliser des expressions régulières.

## Définition et fonctionnement

Une expression régulière est une chaîne de caractères type, un motif (*pattern*), qui décrit un ensemble de chaînes de caractères possibles. C'est un modèle interprété par un moteur, lequel va essayer de trouver des correspondances du modèle dans le texte recherché.

Les moteurs à expressions régulières fonctionnent de manière très simple. Ils lisent les fichiers caractère par caractère et évaluent chaque caractère :

- si le caractère lu ne correspond pas au premier caractère de l'expression régulière cherchée, alors le moteur l'ignore et avance d'un caractère dans le contenu ;
- en revanche, si le caractère lu correspond au premier caractère de l'expression régulière cherchée, alors le moteur le relève et teste le caractère suivant ; si une suite de caractères correspond à l'intégralité de l'expression régulière cherchée, alors le moteur relève une occurrence.

**Un exemple :** combien d'occurrences de la chaîne de caractères `chat` pouvez-vous trouver dans la liste suivante ?

```
chat
chien
chatouilles
chalutier
achat
```

<details>
<summary>Solution <em>(cliquer pour révéler)</em></summary>

<pre><code><span class="code-highlight">chat</span>
chien
<span class="code-highlight">chat</span>ouilles
chalutier
a<span class="code-highlight">chat</span>
</code></pre>

</details>

Les expressions régulières relèvent du formalisme. Elles ne comprennent pas le langage naturel, et elles ne fonctionnent pas sur la base de mots ou de phrases. À la place, elles sont constituées de caractères, dont certains jouent un rôle spécial. L'ensemble des caractères spéciaux et leurs règles d'écriture constituent la syntaxe des expressions régulières. Cette syntaxe permet de formaliser des motifs, indépendamment de leur signification ou absence de signification.

## Le problème de l'implémentation

Il existe plusieurs moteurs et bibliothèques pour faire fonctionner des expressions régulières. En fonction du système d'exploitation, du logiciel dans lequel on les utilise, la syntaxe peut changer. Le mieux est d'utiliser régulièrement le même outil, et d'en choisir un bien fait (c'est-à-dire qui adhère à un standard ouvert, et qui propose des fonctionnalités pratiques).

Exemple 1 : dans les outils basés sur .NET (Microsoft), `\b` (le marqueur de début ou fin de mot) ne fonctionne pas avec les caractères accentués (pas de support Unicode).

Exemple 2 : selon l'environnement logiciel, la capture des groupements peut s'écrire `\1` ou `$1`.

Autre variation possible : le comportement par défaut des expressions régulières dépend du moteur. La plupart du temps, elle seront « gourmandes » (le moteur essaye de trouver la correspondance la plus longue) et sensibles à la casse (le moteur distingue les lettres capitales de celles en bas de casse). Mais tout ceci est réglable, soit dans l'interface graphique du logiciel utilisé, soit via des caractères spéciaux.

Il faut donc se renseigner sur le comportement des expressions régulières dans un environnement donné,  tester ce comportement, et rester vigilant.

## Syntaxe

### La plus petite expression

`a` : le caractère `a`.

`.` : un unique caractère quelconque.

Exemple : `pl.t` correspond à « plat » ou « plot » mais pas « plaît » ou « planet ».

### Concaténation et union

ø : lorsque deux expressions ne sont séparées par aucun caractère, elles forment une nouvelle expression par concaténation.

Exemple : `ab` correspond à « ab ».

`|` : l'une ou l'autre des expressions séparées par la barre droite. (On parle d'union car les résultats sont compris dans l'union des deux ensembles définis de part et d'autre de la barre droite.)

Exemple : `a|b` correspond à « a » ou « b ».

### Classes

`[]` : un caractère parmi ceux entre crochets.

Exemple : `[a7,]` correspond à « a », « 7 » ou « , ».

`[a-z]` : un caractère parmi ceux définis dans l'intervalle entre crochets. Seuls certains intervalles sont prédéfinis, comme les chiffres et les lettres.

Exemple : `[0-9]` correspond à un chiffre entre 0 et 9 ; c'est l'équivalent de `[0123456789]`.

`[^]` : un caractère n'appartenant pas à l'ensemble défini entre crochets.

Exemple : `[^aeiouy]` correspond à « b » mais pas « a ».

::: note
La classe « inversée » `[^]` est un outil extraordinairement efficace dès qu'on est en présence de paires de caractères servant à délimiter quelque chose : des guillemets `""`, des tabulations `\t`, des balises `<a></a>`… En effet, quel que soit le contenu entre les deux délimiteurs, il ne peut inclure de délimiteur, par définition ! Il n'est alors pas nécessaire de chercher comment formaliser le contenu, il suffit de le définir comme étant « tout sauf le délimiteur ». On peut ainsi écrire `"[^"]+"` pour trouver toutes les phrases entre guillemets, par exemple.
:::

### Classes prédéfinies

`\w` = `[a-zA-Z_0-9]` : un caractère alphanumérique.

`\W` = `[^a-zA-Z_0-9]` : un caractère non alphanumérique.

`\d` = `[0-9]` : un chiffre décimal.

`\D` = `[^0-9]` : un caractère n'étant pas un chiffre décimal.

`\s` = `[ \t\n\x0B\f\r]` : un caractère d'espacement (notamment espace ` `, tabulation `\t`, retour chariot `\n`).

### Quantificateurs

Les quantificateurs sont des caractères spéciaux qui s'appliquent à ce qui les précède immédiatement pour préciser une quantité.

`?` : ce qui précède est présent zéro ou une fois.

Exemple : `toto?` correspond à « tot » ou « toto » mais pas « totoo ».

`*` : ce qui précède est présent zéro ou plusieurs fois.

Exemple : `toto*` correspond à « tot », « toto », « totoo », « totooo »…

`+` : ce qui précède est présent une ou plusieurs fois.

Exemple : `toto+` correspond à « toto », « totoo », « totooo » mais pas « tot ».

`{n}`: ce qui précède est présent exactement n fois.

Exemple : `a{3}` correspond à « aaa » mais pas « aa ».

`{n,m}`: ce qui précède est présent entre n et m fois.

Exemple : `a{2,4}` correspond à « aa » ou « aaa » mais pas « a ».

`{n,}`: ce qui précède est présent au moins n fois.

Exemple : `a{3,}` correspond à « aaa », « aaaa »…

### Groupement

`()` : groupement d'une expression.

Les groupements ont deux utilités.

1. Isoler des parties d'une expression pour restreindre ou étendre le champ d'application d'un caractère spécial.

Exemple 1 : `(a|b)c` correspond à « ac » ou « bc ». Les ensembles de part et d'autre de la barre droite `|` sont restreints à la parenthèse (ici le « choix » est entre `a` et `b`, pas entre `a` et `bc`).

Exemple 2 : `(01)+` correspond à « 01 », « 0101 », « 010101 », etc. (le `+` s'applique au motif entre parenthèses).

2. Capturer des correspondances. Les groupements capturés peuvent être utilisés dans une expression régulière de remplacement.

`\n` (ou `$n` suivant le logiciel) : le énième groupement capturé.

`\0` (ou `$0` suivant le logiciel) : la correspondance entière.

Exemple :

```
Bègles 33130
Bordeaux 33800
```

Si on cherche `33(\d{3})` :

- dans `Bègles 33130`, `\1` correspond à « 130 », `0` correspond à « 33130 » ;
- dans `Bordeaux 33800`, `\1` correspond à « 800 », `0` correspond à « 33800 ».

### Caractères spéciaux

`^` : début de ligne.

Exemple : `^a` correspond à « a » en début de ligne mais pas « ba ».

`$` : fin de ligne.

Exemple : `a$` correspond à « a » en fin de ligne mais pas « ab ».

`\b` : position située entre un caractère alphanumérique (`\w`, par exemple une lettre ou un chiffre) et un caractère non alphanumérique.

Exemple : `\ba` correspond à « a » en début de mot mais pas en milieu ou en fin de mot (le premier « a » de « amant » mais pas le deuxième).

::: note
On définit souvent `\b` comme la position de début ou fin de mot. Mais les expressions régulières ne comprennent pas le concept de « mot ». `\b` correspond en fait à la frontière entre un caractère appartenant à la classe prédéfinie `\w` et un autre caractère n'y appartenant pas. Ceci inclut les positions de début ou fin de mot, mais ne s'y limite pas. Ainsi, `\b\d` correspond à un chiffre en « début de mot » : cela pourrait être par exemple le premier chiffre d'un numéro de téléphone.
:::

### Échappement

Un caractère normalement utilisé dans la syntaxe peut être précédé d'une barre oblique inverse `\` pour être interprété littéralement.

Exemple : `a\+b` correspond à « a+b » et pas à « aaaaab ».

Les caractères entre crochets (classe) sont toujours interprétés littéralement, il n'y a donc pas besoin de les échapper.

Exemple : `[a+]` correspond à « a » ou « + ».

### *Lookarounds*

Les *lookarounds* testent la présence ou l'absence d'un motif juste avant ou juste après le motif cherché.

`(?=motif)` : assertion positive avant (*positive lookahead*), vraie si le motif est vérifié.

Exemple : `a(?=5)` correspond à `a5b` mais pas à `ab5`.

`(?!motif)` : assertion négative avant (*negative lookahead*), vraie si le motif échoue.

Exemple : `mascar(?!p)` correspond à `mascara` mais pas à `mascarpone`.

`(?<=motif)`: assertion positive arrière (*positive lookbehind*), vraie si le motif est vérifié.

Exemple : `(?<=re)mède` correspond à `remède` mais pas à `intermède`.

`(?<!motif)` : assertion négative arrière (*negative lookbehind*), vraie si le motif échoue.

Exemple : `(?<!re)mise` correspond à `démise` mais pas à `remise`.

::: note
Les *lookarounds* sont très pratiques pour rendre une expression régulière plus discriminante en posant des contraintes sur le contexte autour du motif recherché, sans pour autant inclure ce contexte dans les résultats de recherche. En effet, lorsqu'un moteur à expressions régulières teste un *lookaround*, il n'avance pas : il reste en place, et n'avance au caractère suivant que lorsque la condition définie par le *lookaround* a été testée. Ceci facilite beaucoup les choses lorsqu'on enchaîne recherche et remplacement.

Exemple : si on cherche les occurrences de `toto` uniquement entre deux guillemets, il peut être intéressant d'écrire `(?<")toto(?=")` plutôt que `"toto"`.
:::

## Exercices

### Exercice 1 : Cyrano

Téléchargez [le fichier Cyrano ci-joint](cyrano.txt){download="cyrano.txt"}. Ouvrez-le avec un éditeur de texte (pas un traitement de texte). Utilisez la fonction Recherche en activant les expressions régulières, la sensibilité à la casse et le bouclage. Pour chaque énoncé ci-dessous, trouvez l'expression régulière qui permet de répondre à la question. Je vous conseille de noter chaque solution dans un fichier pour ne pas les perdre en attendant la correction. Inscrivez une expression régulière par ligne et rien d'autre, en commençant par la première ligne. Si vous ne trouvez pas la solution à un énoncé, laissez une ligne vide et revenez-y plus tard.

#. Trouvez toutes les occurrences de la lettre `n`.
#. Trouvez toutes les occurrences des lettres `n`, `e` et `z` (une seule expression régulière).
#. Trouvez toutes les occurrences de la chaîne de caractère `nez`.
#. Trouvez toutes les occurrences du **mot** `nez`.
#. Trouvez toutes les occurrences du mot `marquis` (minuscules, majuscules confondues).
#. Trouvez toutes les lignes qui commencent par la lettre L en majuscule `L` ou en minuscule `l`.
#. Trouvez toutes les lignes qui commencent par une majuscule.
#. Trouvez toutes les lignes qui finissent par un point d’exclamation `!`
#. Trouvez toutes les lignes qui finissent par une ellipse, c'est-à-dire trois points `...`
#. Trouvez toutes les lignes qui finissent par un signe de ponctuation fort (point `.` point d’interrogation `?` point d’exclamation `!`).
#. Trouvez toutes les lignes qui ne finissent pas par un des signes de ponctuation suivants : virgule `,` point-virgule `;` deux–points `:`
#. Trouvez toutes les lignes qui finissent par une voyelle (majuscule ou minuscule).
#. Trouvez toutes les lignes qui commencent par une consonne (majuscule ou minuscule).
#. Trouvez tous les nombres (série de un ou plusieurs chiffres).
#. Trouvez toutes les dates sous forme d’année (ex : `1655`).
#. Trouvez tous les mots qui contiennent trois voyelles qui se suivent (ex : `monsieur`).
#. Existent-ils dans ce fichier des mots qui contiennent exactement 5 voyelles qui se suivent ?
#. Trouvez toutes les occurrences du verbe `faire` au présent de l’indicatif (au singulier et au pluriel).
#. Trouvez toutes les occurrences du verbe `aimer` à l’infinitif, et conjugué au présent et à l’imparfait singulier de l’indicatif (ex : `aimer`, `aime`, `aimais`).
#. Donnez le nombre de lignes où toutes les lettres sont en majuscules.
#. Trouvez la voyelle la plus fréquente dans ce fichier après la voyelle `e` (qui est la plus fréquente en français).
#. Trouvez tous les mots qui commencent par `a` et se terminent par `é`.
#. Trouvez toutes les lignes qui commencent et qui finissent par une voyelle.
#. Trouvez tous les mots composés de 8, 9 ou 10 caractères.
#. Trouvez tous les mots composés exactement de 10 caractères.
#. Trouvez le mot le plus long dans ce fichier.
#. Trouvez le(s) mot(s) le(s) plus long(s) composé(s) uniquement à partir de lettres qui se trouvent sur les touches de la première rangée de votre clavier.
#. Trouvez tous les mots composés (ex : `porte-avion`).
#. Trouvez toutes les lignes comportant des nombres en chiffres romains.
#. Comptez le nombre de répliques de Cyrano et de Roxane.
#. Vérifiez si la fameuse réplique `C’est un roc !... c’est un pic !... c’est un cap ! Que dis-je, c’est un cap ? C’est une péninsule !` figure dans ce fichier.
#. Trouvez toutes les lignes vides.
#. Trouvez tous les mots contenant `cap` sauf le mot `capitaine`.

<details>
<summary>Correction (cliquer pour afficher)</summary>
#. `n`
#. `n|e|z` ou bien `[nez]`
#. `nez`
#. `\bnez\b`
#. `marquis|Marquis|MARQUIS`
#. `^[Ll]`
#. `^[A-Z]`
#. `!$`
#. `\.\.\.$` ou bien `\.{3}$`
#. `[.?!]$`
#. `[^,;:]$`
#. `[aeiouyAEIOUY]$`
#. `^[zrtpqsdfghjklmwxcvbnZRTPQSDFGHJKLMWXCVBN]`
#. `[0-9]+  ou bien  [0123456789]+`
#. `[0-9]{4}`
#. `[aeiouyAEIOUY]{3}`
#. `[aeiouyAEIOUY]{5}`
#. `\b[fF](ais|ait|aisons|ont|aites)\b`
#. `\b(aimer|aime|aimes|aimais|aimait)\b`
#. `^[A-Z ]+$`
#. `a`
#. `\ba\w+é\b`
#. `^[aeiouyAEIOUY].+[aeiouyAEIOUY]$`
#. `\b\w{8,10}\b`
#. `\b\w{10}\b`
#. `\b\w{20,}\b`
#. `\b[azertyuiop]{9}\b`
#. `\b\w+(-\w+)+`
#. `\b[XVI]+\b`
#. `^(CYRANO|ROXANE)`
#. `C'est un roc !`
#. `^$`
#. `cap(?!itaine)`
</details>

### Exercice 2 : listing

Téléchargez [le fichier Listing ci-joint](listing.txt){download="listing.txt"}. Ouvrez-le avec un éditeur de texte (pas un traitement de texte). Il s'agit d'un tableau de données au format TSV (*tab-separated values*) : la première ligne contient les entêtes des colonnes, chaque ligne contient des valeurs séparées par des tabulations (`\t`).

Utilisez la fonction Recherche en activant les expressions régulières, la sensibilité à la casse et le bouclage. Pour chaque énoncé ci-dessous, trouvez l'expression régulière qui permet de répondre à la question. Je vous conseille de noter chaque solution dans un fichier pour ne pas les perdre en attendant la correction. Inscrivez une expression régulière par ligne et rien d'autre, en commençant par la première ligne. Si vous ne trouvez pas la solution à un énoncé, laissez une ligne vide et revenez-y plus tard.

1. Retrouvez dans ce fichier tous les codes postaux.
2. Retrouvez tous les codes postaux de la région lyonnaise présents dans le fichier ainsi que les villes qui correspondent à ces codes.
3. Retrouvez toutes les chaînes qui représentent le n° et le nom de la rue (ex : 16 RUE VICTOR HUGO ou 5 AVENUE DES CHAMPS ELYSEES).
4. Combien de numéros de téléphone dans ce fichier représentent des numéros de téléphone portable ?
5. Retrouvez toutes les personnes dont le numéro de téléphone n’est pas connu (ex : GALLINO BERNARD 17 AVENUE J.JAURES 38 240 MEYLAN 15/6/1951 00:00:00).
6. Retrouvez tous les noms de famille qui commencent par une consonne et se terminent par une voyelle.
7. Retrouvez toutes les dates de naissance dans les années soixante-dix et quatre-vingt (ex : 15/4/1970 ou 16/11/1985).
8. Retrouvez toutes les dates de naissance entre 1970 et 1980 (année 1980 incluse).

Pour la suite, vous allez devoir utiliser les fonctions Rechercher **et** Remplacer de votre éditeur de texte.

Trouvez l'expression régulière qui permet de répondre à chaque énoncé. **Attention :** toutes les questions qui suivent impliquent de tester des modifications du fichier sur lequel vous travaillez. N'oubliez pas que vous pouvez annuler la ou les dernière(s) modification(s) apportée(s) dans le fichier en utilisant Ctrl (ou Cmd sur Mac) + Z. Vous pouvez également télécharger à nouveau le fichier depuis cette page si vous avez fait des modifications irréversibles.

9. Supprimez dans ce fichier toutes les chaînes de caractères représentant l’heure présentes à la fin de chaque ligne.
10. Supprimez les chaînes de caractères représentant l’heure qui se trouvent entre la date de naissance et la date d’inscription. Après cette modification la date de naissance doit être séparée de la date d’inscription par une tabulation `\t`.
11. Modifiez tous les numéros d’adhérents en ajoutant devant chaque numéro un `10`.
12. Modifiez les chaînes de caractères représentant le code postal en supprimant l’espace qui sépare le code du département de celui de la commune.
13. Homogénéisez toutes les dates présentes dans ce fichier en convertissant celles qui ne suivent pas le format `jj/mm/aaaa` dans ce format. Exemple : `12/1/1960` en `12/01/1960`.
14. Modifiez tous les numéros de téléphone de la manière suivante : `06 76 45 55 12` en `06.76.45.55.12`.
15. Inversez l’ordre des noms et des prénoms dans ce fichier de la manière suivante : `95008 JANDRU BERNARD` en `95008 BERNARD JANDRU`.

<details>
<summary>Correction (cliquer pour afficher)</summary>
#. `\d{2} \d{3}`
#. `69 \d{3}\t[-\w]+`
#. `\d+ [ .A-Z]+`
#. `0[67]( \d{2}){4}`
#. `\d{2} \d{3}\t[^\t]+\t\d+/`
#. `(?<=\d{5}\t)[BCDFGHJKLMNPQRSTVWXZ][A-Z ]+[AEIOUY](?=\t)`
#. `\d{1,2}/\d{1,2}/19[78]\d`
#. `\d{1,2}/\d{1,2}/(197\d|1980)`
#. Rechercher : `\d{2}:\d{2}:\d{2}$`  
Remplacer : (laisser le champ vide)
#. Rechercher : `\d{2}:\d{2}:\d{2}\t`  
Remplacer : `\t`
#. Rechercher : `^`  
Remplacer : `10`
#. Rechercher : `(\d{2}) (\d{3})`  
Remplacer : `$1$2`
#. Rechercher : `(?<!\d)\d/`  
Remplacer : `0$0`
#. Rechercher : `(\d{2}) (\d{2}) (\d{2}) (\d{2}) (\d{2})`  
Remplacer : `$1.$2.$3.$4.$5`
#. Rechercher : `^([^\t]+)\t([^\t]+)\t([^\t]+)`  
Remplacer : `$1\t$3\t$2`
</details>

### Exercice 3 : multicanal

Téléchargez [le fichier multicanal ci-joint](multicanal.txt){download="multicanal.txt"}. Ouvrez-le avec un éditeur de texte (pas un traitement de texte). Il s'agit d'un texte rédigé en Markdown.

Utilisez la fonction Recherche en activant les expressions régulières, la sensibilité à la casse et le bouclage. Pour chaque énoncé ci-dessous, trouvez l'expression régulière qui permet de répondre à la question **et modifiez le texte**. Je vous conseille de noter chaque solution dans un fichier pour ne pas les perdre en attendant la correction.

#. Remplacez tous les URL (`"Titre de la page" (https://www.exemple.fr/)`) par un lien Markdown (`[Titre de la page](https://www.exemple.fr/)`).
#. Certaines lignes ne contiennent qu'un nom de fichier image (ex : `image.jpg`) et sa légende ; transformez-les pour appliquer la syntaxe Markdown (ex : `![légende](image.jpg)`).
#. Transformez tous les paragraphes composés uniquement d'une citation (qui commencent et finissent par un guillemet `"`) en blocs de citation Markdown.
#. Remplacez les apostrophes droites (`'`) par des apostrophes courbes (`’`).
#. Remplacez les guillemets droits (`"`) par des guillemets français ouvrants (`«`) et fermants (`»`) selon l'emplacement.
#. Transformez tous les titres de niveau 3 en titres de niveau 4.
#. Transformez tous les titres de niveau 2 en titres de niveau 3.
#. Transformez tous les titres de niveau 1 en titres de niveau 2.
#. Transformez tous les tirets utilisés pour faire une rupture thématique (`---`) en astérisques (`***`).
#. Remplacez tous les doubles tirets (`--`) par un tiret demi-cadratin (`–`).
#. Transformez toutes les astérisques (`*`) utilisées pour créer un item de liste en tirets simples (`-`).
#. Transformez toutes les astérisques (`*`) utilisées pour l'italique en tirets du bas (`_`).
#. Remplacez les astérisques utilisées pour le gras par l'élément HTML `mark`, sauf quand elles sont utilisées autour du mot `**gras**`.
#. Remplacez les espaces normales par des espaces insécables là où les règles typographiques françaises l'exigent (à vous de trouver ces règles, et comment saisir une espace insécable).
#. Faites pareil pour les espaces insécables fines avec une autre expression régulière.

<details>
<summary>Correction (cliquer pour afficher)</summary>
#. Rechercher : `« ([^»]+) » \((http[^)]+)\)`  
Remplacer : `[\1](\2)`
#. Rechercher : `^« ([^»]+) » \(([^)]+)\)`  
Remplacer : `![\1](\2)`
#. Rechercher : `^"[^"]+"$`  
Remplacer : `> \0`
#. Rechercher : `'`  
Remplacer : `’`
#. Rechercher : `"\b` ou `\b"`  
Remplacer : `« ` ou ` »`
#. Rechercher : `^### `  
Remplacer : `#### `
#. Rechercher : `^## `  
Remplacer : `### `
#. Rechercher : `^# `  
Remplacer : `## `
#. Rechercher : `^---$`  
Remplacer : `***`
#. Rechercher : `(?<!-)--(?!-)`  
Remplacer : `–`
#. Rechercher : `^\* `  
Remplacer : `- `
#. Rechercher : `(?<!\*)\*([^*]+)\*(?!\*)`  
Remplacer : `_\1_`
#. Rechercher : `\*\*(?!gras)([^*]+)\*\*`  
Remplacer : `<mark>\1</mark>`
#. Rechercher : `(«) | ([»:])`  
Remplacer : `\1 \2`
#. Rechercher : ` ([;?!])`  
Remplacer : ` \1`
</details>

### Exercice 4 : HTML

Téléchargez [le fichier multicanal version HTML ci-joint](multicanal.html){download="multicanal.html"}. Ouvrez-le avec un éditeur de texte (pas un traitement de texte).

Utilisez la fonction Recherche en activant les expressions régulières, la sensibilité à la casse et le bouclage. Pour chaque énoncé ci-dessous, trouvez l'expression régulière qui permet de répondre à la question **et modifiez le texte**. Je vous conseille de noter chaque solution dans un fichier pour ne pas les perdre en attendant la correction.

#. Remplacez les espaces normales par des espaces insécables entre les guillemets `« »`, ainsi que devant les deux-points `:`, les points-virgules `;`, les points d'interrogation `?` et les points d'exclamation `!`.
#. Transformez toutes les listes non ordonnées `ul` en listes ordonnées `ol`.
#. Ajoutez un élément `span` avec la classe `lettrine` autour de la première lettre des paragraphes `p` qui suivent immédiatement un titre de niveau 2 `h2`.
#. Modifiez l'attribut `href` des éléments `a` pour ajouter `https://www.quaternum.net` devant les URL qui commencent par `/`.
#. Modifiez l'attribut `src` des éléments `img` pour ajouter `https://www.quaternum.net/images/` devant l'URL de chaque fichier image.

<details>
<summary>Correction (cliquer pour afficher)</summary>
#. Rechercher : `(«) | ([»:;?!])`  
Remplacer : `$1 $2`
#. Rechercher : `(</?)ul>`  
Remplacer : `\1ol>`
#. Rechercher : `(?<=</h2>\n)<p>(.)`  
Remplacer : `<p><span class="lettrine">\1</span>`
#. Rechercher : `href="/`  
Remplacer : `href="https://www.quaternum.net/`
#. Rechercher : `src="`  
Remplacer : `src="https://www.quaternum.net/images/`
</details>

