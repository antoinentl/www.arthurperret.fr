---
title: "Introduction à la publication web : évaluation"
author: Arthur Perret (Université Bordeaux Montaigne)
date: BUT Métiers du livre 2021-2022
abstract: "Évaluation du cours Introduction à la publication web."
---

# Consignes

Téléchargez [le fichier odt ci-joint](2021-2022-publication-web-evaluation.odt){download="2021-2022-publication-web-evaluation.odt"} et ouvrez-le avec LibreOffice. C’est un document qui présente des informations sur le Web. **Le but de l’exercice est de créer une page web qui présente les mêmes informations, avec la même structure.**

Créez un dossier et nommez-le d'après votre nom et prénom, comme ceci : `NomPrénom` (pas d'espaces).

Téléchargez les trois images ci-dessous et placez-les dans `NomPrénom`.

::: gallery
![](../img/2021-2022-publication-web-evaluation-html.png)![](../img/2021-2022-publication-web-evaluation-css.png)![](../img/2021-2022-publication-web-evaluation-javascript.png)
:::

En utilisant Notepad++ (ou Bloc-notes si Notepad++ est inaccessible), créez un fichier `index.html` et un fichier `styles.css` dans `NomPrénom`.

Remplissez le fichier `index.html` en copiant-collant le contenu du document que vous avez ouvert dans LibreOffice.

::: note
Seul le contenu textuel peut être collé dans du fichier ODT vers le fichier HTML. Les images et la mise en forme disparaissent.
:::

Ajoutez des éléments HTML dans le fichier `index.html` de manière à structurer correctement les éléments que vous avez copiés. Le fichier doit contenir les éléments suivants :

- `html`
- `head`
- `title` (« Quelques éléments à propos du Web »)
- `meta` (avec un attribut `author` ayant pour valeur votre prénom et votre nom)
- `link` (vers le fichier `styles.css`)
- `body`
- `h1`
- `h2`
- `p`
- `a`
- `em`
- `img` (avec un attribut `alt` contenant une brève description de l’image)
- `ul`
- `li`

::: note
Pour savoir quel élément HTML appliquer à quel endroit du texte, examinez bien le document que vous avez ouvert dans LibreOffice. Regardez les styles utilisés, notamment pour les niveaux de titre.
:::

Ajoutez des déclarations dans le fichier `styles.css` afin de modifier la mise en forme du fichier HTML de la manière suivante :

- Donnez une largeur maximale de 700 pixels à l'élément `body`.
- Centrez l’élément `body` grâce aux marges.
- Appliquez à l’élément `body` la police de votre choix (autre que Times et Times New Roman), avec comme choix alternatif la famille générique des polices à empattements.
- Réglez la taille de la police sur 1.25 em pour l’élément `body`.
- Réglez l’interligne sur 1.5 pour l'élément `body`.
- Réglez l’interligne sur 1.2 pour les titres.
- Appliquez aux titres la police de votre choix (autre que Times et Times New Roman), avec comme choix alternatif la famille générique des polices sans empattements.
- Appliquez la couleur `#aa0000` aux liens lorsqu’ils ont été visités.
- Donnez une hauteur maximale aux images pour qu’elles ne dépassent pas 100 pixels de haut.

<!-- 
- Donnez une largeur maximale (`max-width`) de `700px` à l'élément `body` et centrez-le grâce aux marges.
- Changez la police de l'élément `body`.
- Définissez l'interligne (`line-height`) à `1.5` pour l'élément `body`.
- Définissez une interligne plus réduite pour les titres (`h1`, `h2`).
- Modifiez la couleur des liens (`a`) lorsqu'ils sont survolés (pseudo-classe `:hover`).
- Donnez une largeur maximale aux images pour qu'elles ne soient pas plus larges que `body`.
- Donnez une bordure (`border`) aux sections (`section`).
 -->

**Bonus** (facultatif) :

- Remplacez les puces devant les items de liste par des tirets.
- Positionnez les trois images sur la droite de leur paragraphe respectif en les faisant flotter (`float`) et donnez-leur une petite marge de 15 pixels à gauche.
- En utilisant une requête média, faites en sorte que lorsque la fenêtre fait au maximum 700px de large, les images ne flottent plus et n’aient plus de marge supplémentaire à gauche.


<!-- 
- Changez la couleur des paragraphes qui suivent immédiatement un titre de niveau 1 (utilisez le [combinateur](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Selectors#les_combinateurs) `+`).
- Ajoutez un fleuron `❦` après chaque élément `section` (utilisez le [pseudo-élément](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Selectors#les_pseudo-éléments) `::after`).
- Définissez une couleur de fond pour l'élément `html`, appliquée uniquement lorsque la fenêtre fait au maximum 700px de large (utilisez une [requête média](https://developer.mozilla.org/fr/docs/Web/CSS/@media) `@media` avec la condition `max-width`). Modifiez les dimensions de la fenêtre pour vérifiez que cela fonctionne.
 -->

::: note
Les consignes bonus ne sont pas obligatoires mais elles rapportent des points supplémentaires.
:::

# Rendu

Faites un clic droit sur le dossier `NomPrénom` et compressez-le au format ZIP.

Déposez le fichier `NomPrénom.zip` dans le dossier prévu à cet effet sur le serveur de l’IUT :

`W:/enseignement-mlp/commun-BUT/PERRET/Publication web/EVALUATION CLASSE 2`