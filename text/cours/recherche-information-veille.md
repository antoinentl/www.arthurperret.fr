---
title: Recherche d’information et veille
author: Arthur Perret (Université Bordeaux Montaigne)
abstract: "Cette page porte sur les notions de recherche d'information et de veille informationnelle, ainsi que sur les méthodes pour mettre en place une stratégie de recherche et de collecte d'information en adéquation avec des besoins particuliers. Il s’agit d’une initiation aux questions suivantes : mettre en place un questionnement préalable à une recherche d'information ; prendre en main les outils de recherche les plus courants ; repérer les sources pertinentes en fonction des objectifs informationnels, en définir la qualité et analyser les résultats obtenus ; restituer ces résultats de manière éclairée dans un format adapté."
wip: true
updated: 01/07/2022
---

# Généralités

## Définitions

Recherche d’information
: Ensemble d’opérations qui permettent de rassembler des données ou des informations structurées, pertinentes et sûres, et de les livrer à une échéance convenue.

Veille
: Processus itératif, manuel ou automatisé, qui permet d’échelonner la recherche d’information dans le temps ou de la poursuivre au-delà de la livraison d’un dossier de recherches en vue de son actualisation permanente.

## Méthodologie

La recherche d'information et la veille font appel à des **savoir-faire** communs :

- questionner besoins et attentes ;
- connaître ou identifier les sources de référence du domaine de recherche grâce à des outils de recherche et de veille ;
- traiter les informations (valider, sélectionner, résumer, synthétiser, analyser) ;
- livrer des résultats de qualité et réutilisables.

Elles sollicitent également une **éthique professionnelle** commune :

- pluralité des sources ;
- citation explicite des auteurs et des contextes d’énonciation ;
- respect de l’intégrité du contenu ;
- respect de la propriété intellectuelle en vigueur et de la licéité des informations.

Elles obéissent enfin à une **méthodologie** commune :

- analyser le besoin d'information ;
- définir une stratégie de recherche ;
- mener la recherche ;
- livrer les résultats.

Des différences existent toutefois entre recherche d'information et veille en ce qui concerne les outils utilisés et la forme des livrables.

# Besoin d’information

Pour envisager une stratégie de recherche, il faut analyser le besoin d'information, c'est-à-dire ce qui pousse un individu ou une organisation à s'engager dans une activité de recherche d'information. Cela peut être la sensation d'une lacune dans ses connaissances, ou bien d'une incertitude ; le ressort de cette sensation peut être d'ordre conceptuel (on souhaite comprendre) ou décisionnel (on doit décider).

Analyser un besoin d'information revient à identifier clairement :

- le commanditaire ;
- l’objectif poursuivi ;
- les pré-requis de connaissance du destinataire ;
- le type de documentation recherchée : donnée, information, analyse, rapport, étude… ;
- le niveau d’expertise ;
- la langue.

Le besoin d'information se précise et se concrétise dans l'interaction itérative avec un système d'information. Dit autrement, c’est une analyse qui doit être commencée avant d’entamer le travail de recherche proprement dit, mais qui s’affine au fur et à mesure qu’on effectue ce travail.

# Stratégie de recherche

## Sources

On appelle source l'origine d'une information.

Une stratégie de recherche commence souvent par l'identification des sources de référence dans le domaine concerné. Il s'agit de déterminer si la question a déjà été clarifiée et contextualisée. Ces sources de référence renvoient elles-mêmes vers d'autres sources, qu'il s'agit de consulter à leur tour, et ainsi de suite.

### Types de sources

Source-auteur, source-émetteur
: Personne physique ou personne morale.

Source-document
: Support d'information.

Ressource
: Production documentaire qui regroupe diverses sources-documents. Ex : site, base de données. Les bases de données documentaires (BDD) et autres catalogues sont des aides à la recherche, des documents et des sources déjà sélectionnées et validées par des professionnels.

### Degré des sources

Source primaire
: Correspond à la première publication relatant, traitant ou produisant une information originale et jusque-là inédite.

Source secondaire
: Utilise des sources primaires pour construire une synthèse ou une analyse. Elle ne comporte pas d’information inédite.

Source tertiaire
: Présente des informations issues de sources secondaires. Les encyclopédies comme Wikipédia sont considérées comme des sources tertiaires.

### Types d'émetteurs, types d'information

Chaque type de source-émetteur est associé à un type d'information.

Sources officielles
: Les institutions publiques sont une source de l'information légale. En France : [DILA](https://www.dila.premier-ministre.gouv.fr) (Légifrance, Service public, Vie publique), Assemblée nationale, ministères, collectivités locales…

Sources académiques
: Les chercheurs sont une source d'information scientifique. Ressources : plateformes d'édition (ex : OpenEdition Journals, Cairn, Persée), catalogues de bibliothèques universitaires (ex : Babord+), moteurs de recherche spécialisés (ex : Worldcat, Sudoc, Isidore, BASE, Google Scholar, etc.), archives (ex : HAL, TEL).

Presse, audiovisuel
: Les titres de presse et les médias audiovisuels sont une source d'information journalistique. Attention toutefois à ne pas confondre les faits (censés être rapportés le plus fidèlement possible) des analyses (qui peuvent être débattues) et des chroniques ou éditoriaux (qui expriment une opinion plus ou moins éclairée).

Acteurs
: Les entreprises et associations sont une source d'information diverse. Cette dernière peut être généraliste ou spécialisée, blanche (publiée) ou grise (non publiée), objective ou partisane.

### Validation des sources et des informations

Exactitude
: Il faut savoir repérer des sources fiables : qui est l’auteur ? Quelle est son expertise, sa notoriété auprès de ses pairs ? Quels sont ses buts et ceux de son canal de diffusion ? A-t-il des partis pris (politiques, économiques) ou parle-t-il en toute objectivité ? Il faut également savoir vérifier l’information : est-elle déjà “contrôlée” ? Faut-il la croiser ? Est-elle à jour (un fait “établi” peut être rectifié par de nouvelles découvertes) ? La citation est-elle fidèle à l’original ? 

Traçabilité 
: Non sourcée, une information n’est pas utilisable.

Licéité
: L’information doit être obtenue dans le respect des contraintes légales : respect du secret (médical, industriel), respect de la propriété intellectuelle. Exemple : France Archives propose [un service en ligne](https://francearchives.fr/@docs/) permettant de connaîtr la licéité de la communication des différents documents administratifs.

## Outils de recherche

Pour trouver des sources et *in fine* des informations pertinentes, différents outils peuvent être utilisés :

- outils généralistes (dictionnaires ; encyclopédies ; moteurs de recherche grand public comme [Google](https://www.google.fr/advanced_search) ou [Qwant](https://www.qwant.com/)) ;
- outils spécialisés (BDD, catalogues, moteurs de recherche, portails divers) ;
- outils de veille (suivi, collecte) ;
- utilitaires (logiciels, programmes, applications).

### Outils généralistes

Les dictionnaires et encyclopédies sont les premières sources à consulter lors de la définition d'un sujet de recherche. Les dictionnaires en ligne permettent parfois de réaliser des recherches rapidement et facilement via l'URL. Exemple :

`http://www.cnrtl.fr/definition/mot` (remplacer « mot » par le mot cherché)

Ensuite, les moteurs de recherche connus du grand public indexent les ressources disponibles sur le Web. Ils offrent souvent deux modes de recherche :

Recherche simple
: Mode de recherche qui n'utilise qu'un seul champ de saisie. En fonction du moteur, il est possible d'utiliser des symboles et des mots spéciaux pour affiner la recherche. Voir par exemple [cette page de l'aide Google](https://support.google.com/websearch/answer/2466433?hl=fr&ref_topic=3081620). Parmi les outils les plus répandus on trouve l'expression exacte `""`, l'exclusion `-` ou encore les opérateurs booléens comme `OR` et `AND`.

Recherche avancée
: Mode de recherche qui utilise plusieurs champs, définis en fonction des métadonnées connues par l'outil. Exemple : [recherche avancée Google Scholar](https://scholar.google.com/#d=gs_asd).

Dans chacun de ces modes, il est souvent possible d’utiliser des techniques de recherche dite experte : opérateurs logiques (booléens), recherche de toutes les formes d'un radical (via l'utilisation du *wildcard* `*`), filtres restreignant les résultats à un type de contenu (ex : `filetype:pdf` sur Google), etc.

Enfin, des utilitaires comme [Liquid](http://www.liquid.info/liquid.html) (macOS) permettent de centraliser plusieurs outils de recherche dans une barre de recherche unique pour gagner du temps.

### Outils spécialisés

On parle d'outil spécialisé pour désigner des outils de recherche d'information dédiés à un domaine particulier. Ils ont les mêmes caractéristiques que les outils généralistes. Les outils spécialisés sont souvent répertoriés sur des portails (annuaires de liens).

Prenons l'exemple de la recherche d'information scientifique, abordée notamment dans le tutoriel [CERISE](http://urfist.chartes.psl.eu/cerise/). Parcourir la littérature scientifique sur un sujet donné consiste à faire un état de l'art. La première règle de l'état de l'art, c'est d'utiliser celui des autres, on va donc se tourner vers des ressources précises :

- articles de type état de l'art (*literature review*) ;
- livres introductifs (ex : collection *Que sais-je ?*) ;
- manuels et autres supports de cours ;
- thèses et mémoires.

Pour cela, il existe des moteurs de recherche spécialisés :

- [Google Scholar](https://scholar.google.fr/)
- [BASE](https://www.base-search.net/)
- [Isidore](https://isidore.science/) (SHS, *open access*)
- [TEL](https://tel.archives-ouvertes.fr/) (thèses de doctorat et mémoires de HDR)
- [Dumas](https://dumas.ccsd.cnrs.fr/) (mémoires de master)
- [Portail du SCD](http://scd.u-bordeaux-montaigne.fr/contenus/tout)

D'autres ressources spécialisées (archives, BDD, catalogues) permettent également de trouver des informations pertinentes :

- [Monoskop](https://monoskop.org/Monoskop)
- [ADIT](http://www.adit.fr/fr/adit/presentation)
- [arXiv.org e-Print archive](http://arxiv.org/)
- [Directory of Open Access Journals](http://doaj.org/)
- [Europe PubMed Central](http://europepmc.org/)
- [Scholar](https://scholar.google.fr/)
- [MemSIC Mémoires de master](http://memsic.ccsd.cnrs.fr/)
- [OpenDOAR Directory of OA Repositories](http://opendoar.org/)
- [Persée : Portail de revues](http://www.persee.fr/web/guest/home)
- [Revues.org : open electronic publishing](http://www.revues.org/)
- [Roar Registry of OA Repositories](http://roar.eprints.org/)
- [Scimago Journal &amp; Country Rank](http://www.scimagojr.com/)
- [TermSciences - Terminologie Scientifique](http://www.termsciences.fr/)
- [Thèses.fr](http://www.theses.fr/)
- [Web of Science](http://apps.webofknowledge.com/)
- [Bibnum Education](http://www.bibnum.education.fr/)

De nombreux outils de recherche spécialisés sont payants, comme par exemple les BDD de presse Europresse et Factiva. [Le portail des BDD disponibles à l'université Bordeaux Montaigne](https://scd.u-bordeaux-montaigne.fr/contenus/tout) permet d'accéder à certaines ressources via votre login universitaire.

Enfin, des logiciels comme [Publish or Perish](https://harzing.com/resources/publish-or-perish) (macOS, Windows, Linux) permettent d'automatiser les requêtes sur les moteurs de recherche académiques et d'extraire les résultats.

### Outils de veille

La veille utilise les mêmes outils que la recherche d'information, plus quelques outils qui lui sont spécifiques. On peut les regrouper en deux catégories :

- outils de suivi permettant d'être notifié de l'apparition de nouvelles sources et publications d'après une recherche enregistrée (alertes via moteurs de recherche) ou un abonnement à un flux (flux RSS et Atom) ;
- outils de collecte permettant de réaliser une curation (sélection de contenu) publique ou non (ex : enregistrement de textes via Pocket, signets sociaux via Twitter et Diigo).

La veille étant une activité itérative, elle nécessite des outils de gestion, qui prenne souvent la forme d'un tableau de bord. Des tableaux de bord spécifiques existent pour certains réseaux sociaux (ex : [Tweetdeck](https://tweetdeck.twitter.com) pour Twitter). Des tableaux de bord de veille génériques permettent d'agréger différentes sources et d'obtenir des informations sur la veille, comme des statistiques (ex : [Netvibes](https://www.netvibes.com/fr), [Hootsuite](https://www.hootsuite.com/fr/)). Des outils de gestion plus génériques encore permettent de gérer les tâches associées à la veille, comme n'importe quel projet (ex : [Trello](https://trello.com/)).

# Livrables

La recherche d’information et la veille débouchent sur des productions documentaires. Il est important d’en définir les caractéristiques à l’avance, c’est même indispensable lorsqu’on travaille pour un commanditaire.

Types de contenu
: Panorama de presse, synthèse documentaire, résumés informatifs de documents, bibliographie accompagnée ou pas de résumés indicatifs, sitographie, bulletin d’information, infographie, visualisation de données, sources réutilisables dans le cadre d'une veille, etc.

Sélection du contenu
: L’information doit être pertinente, c'est-à-dire qu'elle doit répondre au besoin initial de l’utilisateur : conforme au sujet, utilisable, issue de sources appropriées (langue, niveau d’expertise, nature). Il faut aussi maîtriser le volume de l'information : éviter une sélection redondante, veiller à la complémentarité et au traitement des divers aspects ou angles de vue.

Modalités de transmission
: Via un service tiers (plateforme sociale, partage de fichiers), sous forme de documents (traitement de texte, PDF, tableur, fichier OPML…).

Caractéristiques documentaires des livrables
: Page de garde, sommaire automatique, pagination, règles de nommage, etc.

## Bibliographie

Une bibliographie est une liste de sources mise en forme suivant une certaine convention. La bibliographie permet de répondre aux impératifs éthiques et méthodologiques de la recherche d'information : c'est un répertoire méthodique des sources, et la garantie intellectuelle des différents points de vue consultés. Pour être exploitable, elle doit donner un maximum d'informations permettant d'accéder aux sources, et être normée de manière homogène.

La méthodologie de la bibliographie obéit aux principes suivants :

Sélection
: On ne peut pas tout lire. La bibliographie est donc un travail sélectif, qui passe par une pratique de repérage et d'organisation des sources.

Organisation
: On ne peut pas se souvenir de tout ce qu'on a lu. Il faut donc organiser les sources sélectionnées, en les décrivant via des métadonnées et en les classant. Les métadonnées garantissent l'exploitabilité des sources. Le classement permet notamment de visualiser les lacunes d'une recherche d'information en cours.

Normes
: On doit présenter ce qu'on a lu de manière normée et homogène. Pour citer des références et constituer une bibliographie dans un document, il convient de respecter les normes pertinentes dans le cadre d'une recherche d'information donnée. Il existe des normaes internationales, suivies par les bibliothèques en particulier, telle ISO 690 ou encore ISBD (International Standard Bibliographic Description). En sciences, les règles varient d’une discipline à une autre, et souvent également au sein d’une même discipline.

L'utilisation d'un gestionnaire de données bibliographiques tel que [Zotero](https://www.zotero.org) est fortement conseillée. Entre autres ressources, vous pouvez consulter [le tutoriel de la Boîte à outils](https://boiteaoutils.info/2015/03/formations-bibliographie/) pour obtenir des conseils sur comment vous former à ces outils.

Les fonctionnalités d'un logiciel de gestion bibliographique sont souvent les suivantes :

- collecter (récupération automatique de références) ;
- organiser (stockage de références et enrichissement des métadonnées, annotation, gestion, import, export, recherche, etc.) ;
- conserver les documents (fichiers PDF, images, audio, vidéo, captures de pages Web, etc.) ;
- citer (générer une bibliographie et faire des citations) ;
- synchroniser et partager (travail collaboratif, stockage en ligne).

Consultez ma page [Bibliographie](bibliographie.html) pour plus d’informations.

## Document de synthèse

Un document de synthèse restitue les informations repérées dans le corpus signalé en bibliographie pour leur donner sens en lien avec les attentes du commanditaire. On extrait des informations pertinentes et complémentaires des divers documents sélectionnés, et on signale la source de ces informations par un renvoi vers la bibliographie ou par une note en bas de page.

::: note
Exemple d'appels bibliographiques au format auteur-date :

> Les éoliennes contribuent à la production électrique renouvelable. Ainsi une statistique officielle établit que le développement de cette source d’énergie, aujourd’hui 4,5 % de la production totale d’électricité, permet à la France de se rapprocher des objectifs de l’accord de Paris (Ministère de l’environnement, 2019). Cependant, une étude montre que l’énergie éolienne rencontre une opposition marquée de la part des populations (Smith et al., 2018). Par ailleurs, d’autres aspects préoccupants sont signalés : dégradation des paysages, bruit et intermittence de la production (Durepaire, 2018).

Exemple d'appel bibliographique au format note :

> Les éoliennes contribuent à la production électrique renouvelable. Ainsi une statistique du Ministère de l’environnement établit que le développement de cette source d’énergie, aujourd’hui 4,5 % de la production totale d’électricité, permet à la France de se rapprocher des objectifs de l’accord de Paris. Cependant, une étude montre que l’énergie éolienne rencontre une opposition marquée de la part des populations^1^. Par ailleurs, on liste d’autres aspects préoccupants : dégradation des paysages, bruit et intermittence de la production.
>
> ^1^ MISSELWITZ, Benjamin, 2018. L’énergie éolienne : étude des impacts environnementaux,  Science et énergie  [en ligne]. 18 septembre 2015. pp. 8‑20. [Consulté le 12 octobre 2018]. DOI 10.3390/nu7095380. Disponible à l’adresse : http://www.mdpi.com/2072-6643/7/9/5380/
:::

Un bon document de synthèse doit être navigable de manière efficace. Au minimum, il faut numéroter les pages. Pour les documents de plus de 3 pages, il est indispensable de produire un document avec sommaire ou table des matières. Pour les longs documents (plusieurs dizaines de pages), il est utile d'inclure des en-têtes et pieds de page facilitant le repérage dans le document (exemple : titre courant).

## Sources réutilisables

Les livrables qui mentionnent des sources devraient toujours être accompagné de fichiers de données réutilisables.

Les applications de lecture de flux RSS/Atom permettent d'exporter les flux enregistrés sous la forme d'un fichier unique dans un format de données standard (privilégier OPML).

::: note
[OPML Generator](https://opml-gen.ovh) est une application en ligne permettant de générer un fichier OPML sans installer d’outil ni créer de compte. Collez simplement dans la zone de saisie une liste des URL des flux RSS que vous souhaitez compiler (une URL par ligne).

Privilégiez les lecteurs de flux RSS qui permettent l’import et l’export au format OPML, comme par exemple [NetNewsWire](https://netnewswire.com) et [Feedly](https://feedly.com).
:::

Les gestionnaires de références bibliographiques permettent également d'exporter des collections sous la forme de fichiers dans des formats standards (privilégier BibTeX et CSL JSON).
