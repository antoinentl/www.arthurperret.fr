---
title: Arthur Perret - Doctorant en sciences de l'information et de la communication
keywords: [documentation, données, métadonnées, information scientifique, visualisation, écriture, édition, publication, épistémologie]
---

# Bio

::: {data-lang-fr=""}
Je suis doctorant en sciences de l’information et de la communication à l'[Université Bordeaux Montaigne](https://www.u-bordeaux-montaigne.fr/fr/index.html). J'enseigne à l’[IUT Bordeaux Montaigne](https://www.iut.u-bordeaux-montaigne.fr/) comme ATER et je fais partie de l’équipe E3D du laboratoire [MICA](https://mica.u-bordeaux-montaigne.fr/) (Médiations, Informations, Communication, Arts). Je travaille sur les données, documents et dispositifs impliqués dans les processus d'écriture, de documentation et d’édition. [Ma thèse](https://theses.fr/s190804) porte sur l’héritage épistémologique de Paul Otlet ; elle est co-dirigée par [Olivier Le Deuff](http://www.guidedesegares.info/a-propos/) et [Bertrand Müller](https://cnrs.academia.edu/BertrandMüller), et a été financée initialement dans le cadre du programme ANR [HyperOtlet](https://hyperotlet.hypotheses.org/). J’ai également co-organisé les journées d'étude [Reticulum](http://reticulum.info) en SIC et design, ainsi que les rencontres [Inachevé d'imprimer](https://inacheve-dimprimer.net) sur la publication numérique. Depuis 2021, je conçois le logiciel [Cosma](https://cosma.graphlab.fr) avec [Guillaume Brioudes](https://myllaume.fr) et l'équipe du [GraphLab](https://www.graphlab.fr).
:::

::: {data-lang-en=""}
I am a PhD candidate in information and communication science at [Université Bordeaux Montaigne](https://www.u-bordeaux-montaigne.fr/fr/index.html). I teach at [IUT Bordeaux Montaigne](https://www.iut.u-bordeaux-montaigne.fr/) and I am a member of team E3D within the [MICA](https://mica.u-bordeaux-montaigne.fr/) lab. I study the data, documents and devices involved in writing, documentation and publishing. My dissertation deals with the epistemological legacy of Paul Otlet, under the supervision of [Olivier Le Deuff](http://www.guidedesegares.info/a-propos/) and [Bertrand Müller](https://cnrs.academia.edu/BertrandMüller), and was initially funded by France's National Research Agency through the [HyperOtlet](https://hyperotlet.hypotheses.org/) program. I also co-organized the [Reticulum](http://reticulum.info) info-com-design conference, and the [Inachevé d'imprimer](https://inacheve-dimprimer.net) talks on digital publishing. Since 2021, I am principal investigator for [Cosma](https://cosma.graphlab.fr), working with [Guillaume Brioudes](https://myllaume.fr) and the team at [GraphLab](https://www.graphlab.fr).
:::

::: {data-lang-fr=""}
**Liens :** [CV](cv.html) • [HAL](https://cv.archives-ouvertes.fr/arthur-perret) • [ORCID](https://orcid.org/0000-0002-4130-6669) • [Twitter](https://twitter.com/arthurperret) • [S'abonner via RSS](https://www.arthurperret.fr/feed.xml)
:::

::: {data-lang-en=""}
**Links:** [Resume](cv.html) • [HAL](https://cv.archives-ouvertes.fr/arthur-perret) • [ORCID](https://orcid.org/0000-0002-4130-6669) • [Twitter](https://twitter.com/arthurperret) • [Subscribe via RSS](https://www.arthurperret.fr/feed.xml)
:::

::: {data-lang-fr=""}
**Mots-clés :** documentation, organisation des connaissances, données, métadonnées, information scientifique, visualisation, écriture, édition, publication, épistémologie.
:::

::: {data-lang-en=""}
**Keywords:** documentation, knowledge organization, data, metadata, scientific information, visualization, writing, text editing, publishing, epistemology.
:::

::: {data-lang-en=""}
### A note for English-reading visitors

*Thank you for taking an interest in my work. For now, only some of my publications are in English: check the articles section, and the 2022 blog posts.*
:::

# Portrait

<figure>
<img src="img/portrait.jpg" onclick="this.src = ((this.src === 'https://www.arthurperret.fr/img/portrait.jpg') ? 'https://www.arthurperret.fr/img/scriptor.jpg' : 'https://www.arthurperret.fr/img/portrait.jpg');">
</figure>

# Articles

::: {data-lang-fr=""}
Textes évalués par les pairs, dont certains en version auteur pré- ou post-publication
:::

::: {data-lang-en=""}
Peer-reviewed articles, author's version shared pre or post-publication
:::
## Articles de revue {data-lang-fr="Articles de revue" data-lang-en="Journal articles"}

- [Former à la notion de réseau par la conception et l’interprétation : l’atelier Reticulum](https://journals.openedition.org/revuehn/2930) (2022) Avec Florian Harmand
- [Hyperdocumentation: origin and evolution of a concept](articles/2019-09-26-hyperdocumentation.html) (2019) Avec Olivier Le Deuff ⟡ Outstanding Paper [Emerald Literati Awards 2020](https://www.emeraldgrouppublishing.com/journal/jd/literati-awards/journal-documentation-literati-award-winners-2020)

## Articles de congrès {data-lang-fr="Articles de congrès" data-lang-en="Conference papers"}

- [Idéation et hypertexte](articles/2022-06-16-ideation-hypertexte.html) (2022, pré-publication)
- [Fonction documentaire de preuve et données numériques](articles/2020-09-09-fonction-documentaire-preuve-donnees-numeriques.html) (2021)
- [Rhizome Blues: Introducing Document Teratology](articles/2020-12-03-rhizome-blues.html) (2020) Avec Olivier Le Deuff & Clément Borel
- [La logique hyperdocumentaire dans l'organisation de l'information et sa représentation](articles/2019-11-08-logique-hyperdocumentaire-structuration-visualisation.html) (2019)
- [Surfer dans l’Otletosphère. Des outils pour visualiser et interroger le réseau de Paul Otlet](https://archivesic.ccsd.cnrs.fr/sic_02480515) (2019) Avec Olivier Le Deuff, Jean David & Clément Borel
- [Paul Otlet sur le Web. Une étude cartographique](https://archivesic.ccsd.cnrs.fr/sic_02480493v1) (2019) Avec Olivier Le Deuff, Jean David & Clément Borel
- [Documentarité et données, instrumentation d'un concept](articles/2019-10-09-documentarite-et-donnees.html) (2019) Avec Olivier Le Deuff
- [Writing documentarity](articles/2019-06-13-writing-documentarity.html) (2019)
- [Paul Otlet and the ultimate prospect of documentation](articles/2019-06-13-paul-otlet-and-the-ultimate-prospect-of-documentation.html) (2019) Avec Olivier Le Deuff
- [The Architext of Biblion: Digital Echoes of Paul Otlet](articles/2018-10-04-the-architext-of-biblion.html) (2018)
- [Matière à pensées. Outils d’édition et médiation de la créativité](articles/2018-06-14-matiere-a-pensees.html) (2018)

## Autres articles {data-lang-fr="Autres articles" data-lang-en="Others"}

- [All papers are data papers: from open principles to digital methods](articles/2020-07-22-all-papers-are-data-papers.html) (2020) Avec Olivier Le Deuff
- [Pour un réinvestissement critique du concept de dispositif](articles/2019-08-13-reinvestissement-critique-concept-dispositif.html) (2019)
- [Technologies intellectuelles](articles/2018-05-24-technologies-intellectuelles.html) (2018)

# Blog

::: {data-lang-fr=""}
Carnet de recherche, partage de réflexions et d'expérimentations en cours
:::

::: {data-lang-en=""}
Open research notes, ongoing reflections and experiments
:::

## 2022

- [History and information: notes from a workshop](blog/2022-06-29-history-information.html)
- [Single-source publishing with Pandoc and Make](blog/2022-06-22-single-source-publishing-pandoc-make.html)
- [Publication multiformats avec Pandoc et Make](blog/2022-06-22-publication-multiformats-pandoc-make.html)
- [Liens wiki et identifiants](blog/2022-06-15-liens-wiki-et-identifiants.html)
- [The reason you don’t ‘get’ Zettelkasten](blog/2022-05-20-the-reason-you-dont-get-zettelkasten.html)
- [Et toi, qu’est-ce que tu fiches ?](blog/2022-05-20-et-toi-qu-est-ce-que-tu-fiches.html)
- [Pourquoi tenir un blog scientifique](blog/2022-04-11-pourquoi-tenir-un-blog-scientifique.html)
- [Papier, crayon, smartphone](blog/2022-03-30-papier-crayon-smartphone.html)
- [Les risques du butinage](blog/2022-03-09-les-risques-du-butinage.html)
- [Analyze, synthesize, visualize: about cards, links & graphs](blog/2022-02-17-analyze-synthesize-visualize.html)
- [Analyser, synthétiser, visualiser : le triptyque fiche, lien, graphe](blog/2022-02-17-analyser-synthetiser-visualiser.html)
- [What is the point of a graph view?](blog/2022-02-13-what-is-the-point-of-a-graph-view.html)
- [À quoi sert une vue graphe ?](blog/2022-02-13-a-quoi-sert-une-vue-graphe.html)
- [Cosma, from record to graph](blog/2022-01-30-cosma-from-record-to-graph.html)
- [Prise de notes : à la main ou à l’ordinateur ?](blog/2022-01-21-prise-de-notes-a-la-main-ou-ordinateur.html)

## 2021

- [L’impensé des formats : pourquoi écrit-on des articles ?](blog/2021-12-28-impense-des-formats-pourquoi-ecrit-on-des-articles.html)
- [Sémantique et mise en forme (2) : consolider les fondations](blog/2021-12-01-semantique-et-mise-en-forme-2.html)
- [Zettlr 2.0](blog/2021-10-20-zettlr-2.html)
- [L’écriture académique au format texte](cours/2021-09-21-ecriture-academique-format-texte.html)
- [Un exemple de glossaire visualisé avec Cosma](blog/2021-09-14-un-exemple-de-glossaire-visualise-avec-cosma.html)
- [Cosma, de la fiche au graphe](blog/2021-09-04-cosma-de-la-fiche-au-graphe.html)
- [Du notebook au bloc-code](blog/2021-06-11-du-notebook-au-bloc-code.html)
- [Comment critiquer les technologies de l'information et de la communication ?](blog/2021-03-26-comment-critiquer-les-tic.html)
- [Présentiel et distanciel : critique d'une fausse dichotomie](blog/2021-03-15-presentiel-distanciel-critique-fausse-dichotomie.html)
- [L'impensé des formats : réflexion autour du PDF](blog/2021-03-07-impense-des-formats-reflexion-autour-du-pdf.html)
- [Une nouvelle étape dans mon travail de veille](blog/2021-03-03-une-nouvelle-etape-dans-mon-travail-de-veille.html)
- [Avancer](blog/2021-03-03-avancer.html)

## Années précédentes {data-lang-fr="Années précédentes" data-lang-en="Previous years"}

<details>

<summary data-lang-fr="Voir les billets" data-lang-en="Show the posts">Voir les billets</summary>

### 2020

- [De la fiche au graphe documentaire](blog/2020-11-23-fiche-graphe-documentaire.html)
- [Histoire typographique de la légèreté](blog/2020-10-25-histoire-typographique-legerete.html)
- [Enseignement et automatisation avec Pandoc](blog/2020-10-19-enseignement-automatisation-pandoc.html)
- [Dr Pandoc & Mr Make](blog/2020-09-14-dr-pandoc-and-mr-make.html)
- [Un carré documentologique](blog/2020-08-05-carre-documentologique.html)
- [Visualisation d’une documentation personnelle réticulaire](blog/2020-06-25-visualisation-documentation-personnelle-reticulaire.html)
- [Écrire et éditer](blog/2020-05-22-ecrire-et-editer.html)
- [Nativement numérique](blog/2020-04-21-nativement-numerique.html)
- [L’âge de raison (technologique)](blog/2020-04-08-mes-outils.html)
- [La bibliographie annotée](blog/2020-01-18-bibliographie-annotee.html)
- [Ancres et liens](blog/2020-01-02-ancres-et-liens.html)

### 2019

- [Citations marginales](blog/2019-12-17-citations-marginales.html)
- [Pour un autre carnet de recherche numérique](blog/2019-11-18-pour-un-autre-carnet-de-recherche-numerique.html)
- [Le diaporama au temps du balisage (2)](blog/2019-10-07-le-diaporama-au-temps-du-balisage-2.html)
- [Mot compte triple](blog/2019-09-12-mot-compte-triple.html)
- [Recommandation : Zettlr](blog/2019-09-06-recommandation-zettlr.html)
- [Au doigt et à l’œil](blog/2019-07-18-au-doigt-et-a-l-oeil.html)
- [Wordless 2 : négocier la réécriture](blog/2019-06-05-wordless-2-negocier-la-reecriture.html)
- [Le CV post-digital](blog/2019-05-07-le-cv-post-digital.html)
- [Wordless](blog/2019-04-12-wordless.html)
- [Et à la fin, écrire…](blog/2019-03-14-et-a-la-fin-ecrire.html)
- [Le diaporama au temps du balisage](blog/2019-03-07-le-diaporama-au-temps-du-balisage.html)
- [L’âge de raison (typographique)](blog/2019-02-11-l-age-de-raison-typographique.html)

### 2018

- [Dr Jekyll & Mr TeX](blog/2018-12-17-dr-jekyll-et-mr-tex.html)
- [Sémantique et mise en forme, ouvrir la boîte de Pandoc ?](blog/2018-12-04-semantique-et-mise-en-forme.html)
- [Une micro-chaîne éditoriale multicanal pour l’écriture scientifique](blog/2018-10-18-une-micro-chaine-editoriale.html)

</details>

# Veille {data-lang-fr="Veille" data-lang-en="Shared links"}

::: {data-lang-fr=""}
Signalement de ressources et de liens utiles ([consulter les sources](liens.html))
:::

::: {data-lang-en=""}
Useful resources and links from other places ([list of sources](links.html))
:::

- [Des lieux communs aux jardins numériques](veille/2022-06-08-lieux-communs-jardins-numeriques.html)
- [Collecter, reformuler, cartographier](veille/2022-05-18-collecter-reformuler-cartographier.html)
- [Sans contexte, un rétrolien ne sert à rien](veille/2022-05-08-contexte-retrolien.html)
- [To markdown or not to markdown](veille/2022-05-08-to-markdown-or-not-to-markdown.html)
- [Colophon Cards](veille/2022-05-08-colophon-cards.html)
- [Pandoc et les citations au format docx](veille/2022-04-07-pandoc-citations-docx.html)
- [Des PDF via Pandoc avec d’autres moteurs que LaTeX](veille/2022-04-06-pdf-pandoc-sans-latex.html)
- [Écriture académique au format texte : trois liens](veille/2022-04-01-ecriture-academique-format-texte-trois-liens.html)
- [HTML est un langage de programmation](veille/2022-03-21-html-est-un-langage-de-programmation.html)
- [La barre est basse](veille/2022-03-11-la-barre-est-basse.html)
- [Contre les everything buckets](veille/2022-03-10-contre-les-everything-buckets.html)
- [Six arguments en faveur du format texte](veille/2022-03-09-six-arguments-format-texte.html)
- [Il n’y a pas plus moderne qu’un site qui fonctionne](veille/2022-02-11-pas-plus-moderne-qu-un-site-qui-fonctionne.html)
- [Le vrai paradoxe de LaTeX](veille/2022-02-11-le-vrai-paradoxe-de-latex.html)
- [De l’importance de “vendre” le format texte](veille/2022-02-09-de-l-importance-de-vendre-le-format-texte.html)
- [Débugue tes humanités](veille/2022-01-14-debugue-tes-humanites.html)
- [Markdown et vous](veille/2022-01-05-markdown-et-vous.html)
- [Telescope](veille/2022-01-04-telescope.html)
- [Les perspectives vertigineuses de l’open access](veille/2021-12-05-perspectives-vertigineuses-open-access.html)
- [Les données ont-elles évincé ou éclipsé les documents ?](veille/2021-11-30-donnees-document-chabin.html)
- [Sources pour une veille sur la culture numérique](veille/2021-11-17-sources-veille-culture-numerique.html)
- [Word et LaTeX : dépasser le débat stérile entre amateurisme et élitisme](veille/2021-10-26-word-et-latex-depasser-le-debat-sterile-amateurisme-elitisme.html)
- [Un moteur de découverte de flux RSS](veille/2021-10-25-un-moteur-de-decouverte-de-flux-rss.html)
- [Enfants du numérique, perdus en bureautique](veille/2021-09-24-enfants-du-numerique-perdus-en-bureautique.html)
- [Le non-livre, ou l'indifférence vis-à-vis de la chose mal faite](veille/2021-08-28-le-non-livre.html)

<details>
<summary data-lang-fr="Voir les entrées précédentes" data-lang-en="Previous links">Voir les entrées précédentes</summary>

- [Édition multi-modale à partir d’une source unique](veille/2021-08-26-edition-multi-modale-source-unique.html)
- [Le goût de la chose bien faite](veille/2021-08-19-le-gout-de-la-chose-bien-faite.html)
- [Quarto](veille/2021-08-17-quarto.html)
- [Maintenir un site statique avec un Makefile](veille/2021-08-07-maintenir-un-site-statique-avec-un-makefile.html)
- [Org-mode et Pandoc : une ressource et un regret](veille/2021-07-27-org-mode-et-pandoc-une-ressource-un-regret.html)
- [Tree-query : prémices d’une évolution ?](veille/2021-07-24-tree-query.html)
- [Treeverse : le fil, l'arbre et le réseau](veille/2021-07-23-treeverse.html)
- [Sioyek : quand les lecteurs PDF innovent plus que les navigateurs internet](veille/2021-07-21-sioyek.html)
- [Ordinateurs et créativité](veille/2021-07-04-ordinateurs-et-creativite.html)
- [Des espaces d'échange pour les tools for thought](veille/2021-06-30-rapport-operas-futur-ecriture-scientifique.html)
- [Rapport OPERAS sur le futur de l'écriture scientifique](veille/2021-06-30-espaces-echange-tools-for-thought.html)
- [Études du livre au XXIe siècle](veille/2021-06-27-etudes-livre-21e-siecle.html)
- [Écriture exécutable au HN Lab](veille/2021-06-27-ecriture-executable-au-hn-lab.html)
- [Rapport du SNE 2020-2021](veille/2021-06-25-rapport-sne-2020.html)
- [Rétrospective sur le livre numérique](veille/2021-06-17-retrospective-livre-numerique.html)
- [D'Aristote à Arial : introduction à la classification typographique](veille/2021-06-17-prise-notes-pre-requis-solutions.html)
- [Prise de notes : pré-requis et solutions](veille/2021-06-17-essor-tablettes-encre-electronique.html)
- [L’essor des tablettes à encre électronique](veille/2021-06-17-d-aristote-a-arial.html)
- [La difficile progression des “tools for thought”](veille/2021-06-09-difficile-progression-tools-for-thought.html)
- [Données et programmation : la décennie qui vient](veille/2021-06-04-donnees-et-programmation-la-decennie-qui-vient.html)
- [Constellations créatrices](veille/2021-05-28-constellations-creatrices.html)
- [La première guerre des notebooks](veille/2021-05-27-la-premiere-guerre-des-notebooks.html)
- [Un manuel de visualisation de données](veille/2021-05-26-un-manuel-de-visualisation.html)
- [Une synthèse sur la bibliographie annotée](veille/2021-05-24-synthese-bibliographie-annotee.html)
- [Arena](veille/2021-05-23-arena.html)
- [Tout sur l’email](veille/2021-05-12-tout-sur-email.html)
- [Cartographier la littérature scientifique](veille/2021-04-29-cartographier-la-litterature-scientifique.html)
- [Au revoir Vox-ATypI](veille/2021-04-28-au-revoir-vox-atypi.html)
- [Future of Text](veille/2021-04-25-future-of-text.html)
- [Chérie, j'ai rétréci le Web](veille/2021-04-18-cherie-jai-retreci-le-web.html)
- [Défendre XML](veille/2021-04-16-typogenese.html)
- [Typogénèse](veille/2021-04-16-defendre-xml.html)
- [Déplacement des graphes](veille/2021-04-14-deplacement-des-graphes.html)
- [Deux tirets](veille/2021-04-09-deux-tirets.html)
- [Digital scholarship workflows](veille/2021-03-30-digital-scholarship-workflows.html)
- [Le libre, une idée obsolète ?](veille/2021-03-25-le-libre-une-idee-obsolete.html)
- [The Meme Hustler](veille/2021-03-23-the-meme-hustler.html)
- [Imdone](veille/2021-03-22-imdone.html)
- [Un hackathon Paged.js à EnsadLab](veille/2021-03-21-un-hackathon-pagedjs-a-ensadlab.html)
- [Le récit plutôt que le réseau](veille/2021-03-15-le-recit-plutot-que-le-reseau.html)
- [What's in a name?](veille/2021-03-11-whats-in-a-name.html)
- [Mutualiser les coûts du paratexte scientifique](veille/2021-03-08-mutualiser-les-couts-du-paratexte-scientifique.html)
- [Plain Text as a Force Multiplier](veille/2021-03-05-plain-text-as-a-force-multiplier.html)
- [Zotero passe un cap](veille/2021-03-04-zotero-passe-un-cap.html)
- [OpenDataSphère](veille/2021-03-04-opendatasphere.html)
- [Dans les recoins de la double page](veille/2021-03-02-dans-les-recoins-de-la-double-page.html)
- [Text wrangling, éditer en txt](veille/2021-02-26-text-wrangling-editer-en-txt.html)
- [Le futur de l'écriture académique en SHS](veille/2021-02-26-le-futur-de-lecriture-academique-en-shs.html)
- [Publications as data?](veille/2021-02-25-publications-as-data.html)
- [Gemini et la typographie](veille/2021-02-10-gemini-et-la-typographie.html)
- [Reticulum 3](veille/2021-02-08-reticulum-3.html)
- [Faire savoir les savoirs](veille/2021-02-08-faire-savoir-les-savoirs.html)
- [Uniformisation typographique des logos](veille/2021-01-27-uniformisation-typographique-des-logos.html)
- [Making-of du site d'Anthony Masure](veille/2021-01-20-making-of-du-site-anthony-masure.html)
- [De WhatsApp à Signal](veille/2021-01-19-de-whatsapp-a-signal.html)
- [Autopandoc, mais sur GitLab](veille/2021-01-08-autopandoc-mais-sur-gitlab.html)
- [Temporalité et texte brut](veille/2021-01-07-temporalite-et-texte-brut.html)
- [Récit d'une migration de Jekyll à Pandoc](veille/2021-01-07-recit-dune-migration-de-jekyll-a-pandoc.html)
- [Les tableaux au format texte](veille/2021-01-07-les-tableaux-au-format-texte.html)
- [Le Web est-il devenu trop compliqué ?](veille/2021-01-07-le-web-est-il-devenu-trop-complique.html)
- [Élaborer un plan au format texte](veille/2021-01-07-elaborer-un-plan-au-format-texte.html)
- [Des cours sur les données](veille/2021-01-04-information-a-historical-companion.html)
- [Bibliographies réutilisables](veille/2021-01-04-des-cours-sur-les-donnees.html)
- [Information: A Historical Companion](veille/2021-01-04-bibliographies-reutilisables.html)
- [PanWriter](veille/2020-12-17-panwriter.html)
- [Actes de DOCAM 2020](veille/2020-12-04-actes-de-docam-2020.html)
- [Pourquoi fabriquer son propre générateur de site statique ?](veille/2020-12-03-pourquoi-fabriquer-son-propre-generateur-de-site-statique.html)
- [Git is simply too hard](veille/2020-11-19-git-is-simply-too-hard.html)
- [Rédaction durable avec Pandoc et Markdown](veille/2020-11-06-redaction-durable-avec-pandoc-et-markdown.html)

</details>

# Cours

Ressources pédagogiques permanentes, mises à jour ponctuellement

- [Bibliographie](cours/bibliographie.html)
- [Écriture académique](cours/ecriture-academique-format-texte.html)
- [Expressions régulières](cours/expressions-regulieres.html)
- [Format texte](cours/format-texte.html)
- [Markdown](cours/markdown.html)
- [Pandoc](cours/pandoc.html)
- [Publication web](cours/publication-web.html)
- [Recherche d’information et veille](cours/recherche-information-veille.html)
- [Sérialisation de données](cours/serialisation.html)

# Colophon

::: {data-lang-fr=""}
Sauf mention contraire, les contenus publiés sur ce site sont réutilisables suivant les termes de la licence Creative Commons Attribution 4.0 International (CC BY), cf. [résumé](https://creativecommons.org/licenses/by/4.0/deed.fr) et [texte intégral](https://creativecommons.org/licenses/by/4.0/legalcode.fr).

Ce site réutilise les ressources suivantes (licence mentionnée entre parenthèses) :

- [ET Book](https://edwardtufte.github.io/et-book/) : 2015 Dmitry Krasny, Bonnie Scranton, Edward Tufte (MIT).
- [Inconsolata](https://levien.com/type/myfonts/inconsolata.html) : 2006 The Inconsolata Project Authors (OFL).
- [Pandoc](https://pandoc.org/) : 2006-2020 John MacFarlane (GPL)
- [Pandoc-sidenote](https://github.com/infologie/pandoc-sidenote) : 2016 Jacob Zimmerman (MIT)
- *Femme avec tablettes de cire et stylet (dite "Sappho")* : Musée archéologique national de Naples (CC BY-SA)
:::

::: {data-lang-en=""}
Except otherwise mentioned, the contents of this website are licensed under Creative Commons Attribution 4.0 International (CC BY) ([summary](https://creativecommons.org/licenses/by/4.0/deed.en), [full text](https://creativecommons.org/licenses/by/4.0/legalcode.en)).

The following works are reused according to their respective licenses :

- [ET Book](https://edwardtufte.github.io/et-book/) : 2015 Dmitry Krasny, Bonnie Scranton, Edward Tufte (MIT).
- [Inconsolata](https://levien.com/type/myfonts/inconsolata.html) : 2006 The Inconsolata Project Authors (OFL).
- [Pandoc](https://pandoc.org/) : 2006-2020 John MacFarlane (GPL)
- [Pandoc-sidenote](https://github.com/infologie/pandoc-sidenote) : 2016 Jacob Zimmerman (MIT)
- Fresco showing a woman so-called Sappho holding writing implements, from Pompeii, Naples National Archaeological Museum (CC BY-SA)
:::